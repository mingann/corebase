#!/bin/sh
SCSS_MASTER="./Firm/src/scss/_style_master.scss"
newer=`find $1 -newer "$SCSS_MASTER"`
if [ "$newer" == "$1" ]
then
    echo "$1 is newer than "$SCSS_MASTER
else
	touch $1
    echo "$1 is older than "$SCSS_MASTER
fi 

