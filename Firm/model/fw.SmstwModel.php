<?php
/**
 * every8d http 簡訊發送
 * source: http://global.every8d.com.tw/api/
 *
 */
include_once(_DIR_FRAMWEORK.'include/lib_http.php');

class SmstwModel Extends Model_Base{

	protected $smsHost;
	protected $sendSMSUrl;
	protected $getCreditUrl;
	protected $batchID='';
	protected $credit=0.0;
	protected $processMsg='';
	protected $userID;
	protected $password;
	
	function __construct(){
		$conf=CZ::config_get('sms.every8d');
		if(!empty($conf)){
			$this->smsHost=$conf['server'];	
			$this->userID=$conf['user'];
			$this->password=$conf['password'];
		}
	}

	function host_reset($sms_host=NULL){
		if(is_null($sms_host) && empty($this->smsHost)){
			$this->smsHost = 'api.every8d.com';
		}
		else{
			$this->smsHost=$sms_host;
		}
		$this->sendSMSUrl = 'http://'.$this->smsHost.'/API21/HTTP/sendSMS.ashx';
		$this->getCreditUrl = 'http://'.$this->smsHost.'/API21/HTTP/getCredit.ashx';
	}

	/// <summary>
	/// 取得帳號餘額
	/// </summary>
	/// <param name="userID">帳號</param>
	/// <param name="password">密碼</param>
	/// <returns>true:取得成功；false:取得失敗</returns>
	function credit_get(){
		$success = FALSE;
//		$postDataString = "UID=" . $this->userID . "&PWD=" . $this->password;
		$postData=array(
			'UID'=>$this->userID,
			'PWD'=>$this->password,
		);
//		$resultString = $this->httpPost($this->getCreditUrl, $postDataString);
		$result = http_post_form($this->getCreditUrl, $this->getCreditUrl, $postData);
		if(empty($result['STATUS']['http_code']) || $result['STATUS']['http_code']!='200'){
			return FALSE;
		} 
		else {
			$resultString = $result['FILE'];
			if(substr($resultString,0,1) == '-'){
				$this->processMsg = $resultString;
			}
			else{
				$success = true;
				$this->credit = $resultString;
			}
		}
		return $success;
	}
	
	/// <summary>
	/// 傳送簡訊
	/// </summary>
	/// <param name="subject">簡訊主旨，主旨不會隨著簡訊內容發送出去。用以註記本次發送之用途。可傳入空字串。</param>
	/// <param name="content">簡訊發送內容</param>
	/// <param name="mobile">接收人之手機號碼。格式為: +886912345678或09123456789。多筆接收人時，請以半形逗點隔開( , )，如0912345678,0922333444。</param>
	/// <param name="sendTime">簡訊預定發送時間。-立即發送：請傳入空字串。-預約發送：請傳入預計發送時間，若傳送時間小於系統接單時間，將不予傳送。格式為YYYYMMDDhhmnss；例如:預約2009/01/31 15:30:00發送，則傳入20090131153000。若傳遞時間已逾現在之時間，將立即發送。</param>
	/// <returns>true:傳送成功；false:傳送失敗</returns>
	function sms_send($subject, $content, $mobile, $sendTime){
		$success = FALSE;

//		$postDataString = "UID=" . $this->userID;
//		$postDataString .= "&PWD=" . $this->password;
//		$postDataString .= "&SB=" . $subject;
//		$postDataString .= "&MSG=" . $content;
//		$postDataString .= "&DEST=" . $mobile;
//		$postDataString .= "&ST=" . $sendTime;
		$postData = array(
			'UID'=>$this->userID,
			'PWD'=>$this->password,
			'SB'=>$subject,
			'MSG'=>$content,
			'DEST'=>$mobile,
			'ST'=>$sendTime,
		);
		$result = http_post_form($this->sendSMSUrl, $this->sendSMSUrl, $postData);

		if(empty($result['STATUS']['http_code']) || $result['STATUS']['http_code']!='200'){
			return FALSE;
		} 
		else{
			$resultString = $result['FILE'];
			if(substr($resultString,0,1) == '-'){
				$this->processMsg = $resultString;
			} 
			else {
				$success = true;
				$strArray = split(',', $resultString);
				$this->credit = $strArray[0];
				$this->batchID = $strArray[4];
			}
		}
		return $success;
	}
	
	/**
	改用 lib_http
		
	function httpPost($url, $postData){
       	$result = "";
		$length = strlen($postData);
		$fp = fsockopen($this->smsHost, 80, $errno, $errstr);
		$header = "POST " . $url . " HTTP/1.0\r\n";
		$header .= "Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n"; 
		$header .= "Content-Length: " . $length . "\r\n\r\n";
		$header .= $postData . "\r\n";
		
		fputs($fp, $header, strlen($header));
		while (!feof($fp)) {
			$res .= fgets($fp, 1024);
		}
		fclose($fp);
		$strArray = split("\r\n\r\n", $res);
		$result = $strArray[1];
        return $result;
	}
	 */
}

