<?php
date_default_timezone_set('Asia/Taipei');
if(strcmp(substr($_SERVER['REMOTE_ADDR'],0,10),'60.199.208.')){
	header('HTTP/1.1 404 Not Found.');
	die('<h1>404 Not Found.</h1>');
}
$html=isset($_POST['html'])?$_POST['html']:'';
if(strlen($html)){
	$fname='/srv/web/tmp/z_'.microtime(TRUE);
	file_put_contents($fname.'.html', $html);
	if(file_exists($fname.'.html')){
		$cmd='/opt/wkhtmltopdf/bin/wkhtmltopdf '.$fname.'.html '.$fname.'.pdf';
		exec($cmd);
		$c=file_get_contents($fname.'.pdf');
		@unlink($fname.'.html');
		@unlink($fname.'.pdf');
		echo $c;	
	}
}

