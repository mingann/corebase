<?php
class VhostModel extends Model_Base {

	public function __construct(){
		$this->dbid='common';
		$this->table_name='vhosts';
	}

	public function config_getter($site){
		$query=array(
			'select'=>'*',
			'from'=>$this->table_name,
			'where'=>array(
				'vhostFqdn=:site',
				array(
					':site'=>$site
				)
			),
		);
		$vhost=DB::row($this->dbid,$query);
		if(!$vhost){
			$query=array(
				'select'=>'*',
				'from'=>'vhost_subs',
				'where'=>array(
					'vhostFqdnAlias=:site',
					array(
						':site'=>$site
					)
				),
			);
			$vhost=DB::row($this->dbid,$query);
			if(!$vhost){
				return NULL;
			}
			$query=array(
				'select'=>'*',
				'from'=>$this->table_name,
				'where'=>array(
					'vhostFqdn=:site',
					array(
						':site'=>$vhost['vhostFqdn'],
					)
				),
			);
			$vhost=DB::row($this->dbid,$query);
		}
		return $vhost;
	}

	public function sys_config(){
		return include(_DIR_DOCS.'etc/sys.php');
	}
}

