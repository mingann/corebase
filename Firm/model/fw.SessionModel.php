<?php
class SessionModel extends Model_Base {


	public function __construct(){
		$this->dbid='session';
		$this->table_name='sess';
		$this->field_pk='sessId';
		$this->field_pky_charset='uid4';
		$this->fields=array(
			'sessId'=>array('charset'=>'uid4','max'=>32,'req'=>1),
			'projectId'=>array('charset'=>'string','max'=>16,'req'=>0),
			'sessHash'=>array('charset'=>'string','max'=>32,'req'=>0),
			'userUid'=>array('charset'=>'string','max'=>32,'req'=>0),
			'sessData'=>array('charset'=>'string','max'=>65535,'req'=>0),
			'sessExtend'=>array('charset'=>'boolean','req'=>0),
			'sessUA'=>array('charset'=>'string','max'=>255,'req'=>0),
			'sessMobile'=>array('charset'=>'boolean','req'=>0),
			'sessLanguage'=>array('charset'=>'safe','max'=>6,'req'=>1),
			'sessRemote'=>array('charset'=>'safe','max'=>24,'req'=>1),
			'sessTimeUpdate'=>array('charset'=>'timestamp','req'=>0),
			'sessTimeVisit'=>array('charset'=>'timestamp','req'=>0),
			'sessTimeLogin'=>array('charset'=>'timestamp','req'=>0),
		);

	}

	public function data_get($id){
		$query=array(
			'select'=>$this->field_str(),
			'from'=>$this->table_name,
			'where'=>array(
				'sessId=:id',
				array(':id'=>$id),
			),
		);

		$sess=DB::row('session',$query);
		if($sess){
			$this->bind_pk=$id;
		}
		return $sess;
	}

	public function data_update($data){
		DB::update('session',$this->table_name,$data,array(
			'WHERE sessId=:id',
			array(
				':id'=>$this->bind_pk
			)
		));
	}

	/**
	 * lists 
	 * 取得session列表
	 * 
	 * @param mixed $latest_min 最近幾分鐘內有更新
	 * @param mixed $login_only 是否只取得已登入帳號 FALSE:否, TRUE:是
	 * @access public
	 * @return void
	 */
	public function lists($latest_min,$login_only){
		$query=array(
			'select'=>'*',
			'from'=>$this->table_name,
			'where'=>array(
				'sessTimeUpdate>=:time',
				array(
					':time'=>date('Y-m-d H:i:s',_SYS_TIMESTAMP-$latest_min*60),
				)
			),
		);
		if($login_only){
			$query['where'][0].=' AND userUid<>""';
		}
		return DB::data($this->dbid,$query);

	}

}

