<?php
class CoreModel extends Model_Base {
	private $sys_type='file';

	public function sys_type_in_db($dbid, $table, $field, $where){
		$this->sys_type='db';
	}
	public function sys_timestamp(){
		return DB::utc_timestamp_get('common');
	}

	public function sys_config(){
		if(!defined('_SYS_CONFIG') || !strcmp(_SYS_CONFIG, 'file')){
			return include(_DIR_DOCS.'etc/sys.php');
		}
		else{
			return sys_config_db($_SERVER['HTTP_HOST']);
		}
	}
}

