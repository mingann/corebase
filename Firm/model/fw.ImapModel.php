<?php
class ImapModel extends Model_Base {
	private $user='';
	private $passwd='';
	private $host='';

	public $inbox=array();
	public $unseen=array();

	private $to=''; // ex: {imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX
	private $mailbox=NULL;

	public $messages=NULL;

	public function __construct(){
		$this->dbid='common';
		$this->table_name='';
	}
	
	/**
	 * 
	 *
	 * @param
	 *      host: fqdn 或 gmail
	 * @return
	 * {imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX
	 */
	public function conn_to($conf){
		if(!strcasecmp($conf['host'],'gmail')){
			$conf['host']='imap.gmail.com';
			$conf['port']='993';
			$conf['path']='/imap/ssl/novalidate-cert';
			$conf['inbox']='INBOX';
			$conf['options']=NULL;
			$conf['uri']= '{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX';
		}
		else{
			if(!isset($conf['host'])){ $this->mailbox=NULL; return FALSE; }
			if(!isset($conf['port'])){ $conf['port']='993'; }
			if(!isset($conf['path'])){ $conf['path']='/imap/ssl'; }
			if(!isset($conf['inbox'])){ $conf['inbox']='INBOX'; }
		}
		$this->host=$conf['host'];
		$this->user=$conf['user'];
		$this->passwd=$conf['passwd'];
		if(!empty($conf['uri'])){
			$this->to=$conf['uri'];
		}
		else{
			$conf['uri']='{'.$conf['host'].':'.$conf['port'].$conf['path'].'}'.$conf['inbox'];
			$this->to=$conf['uri'];
		}
		if(empty($conf['options'])){
			$this->mailbox=imap_open($this->to, $conf['user'], $conf['passwd']);
		}
		else{
			$this->mailbox=imap_open($this->to, $conf['user'], $conf['passwd'], $conf['options']);
		}
		if(empty($this->mailbox)){
			$this->mailbox=FALSE;
			return FALSE;
		}
		return TRUE;
	}

	public function close(){
		imap_close($this->mailbox);
	}

	public function inbox_unseen(){
		// Gets status information about the given mailbox
		$status = imap_status($this->mailbox, '{' . $this->host . '}INBOX', SA_ALL);
		if ($status === false) {
			echo 'Error : ' . imap_last_error();
		}
		else {
			$this->inbox['all']=$status->messages;
			$this->inbox['unseen']=$status->unseen;

			// No unseen messages
			if ($status->unseen == 0) {
				// echo 'No unseen messages';
			} else {
				// Gets UIDs for unseen messages
				$this->messages = imap_search($this->mailbox, 'UNSEEN');

				if ($this->messages === false) {
					// echo 'Error : ' . imap_last_error();
				} else {
					// For each all unseen messages
					foreach($this->messages as $uid ) {
						// Get header of the message
						$message = imap_headerinfo($this->mailbox, $uid);

						if ($message) {
							// From message
							$from = $message->from[0];
							$from_address = $from->mailbox.'@'.$from->host;

							$to = $message->to[0];
							$to_address = $to->mailbox.'@'.$to->host;
							// Subject
							$subject = isset($message->subject) ? imap_utf8($message->subject) : 'No subject';

							// Re-format date
							$date = date('U', strtotime($message->date));

							$size = isset($message->Size) ? $message->Size : 0;
							// Add to data array
							$this->unseen[] = array(
								'uid'=>$uid,
								'from'=>$from_address,
								'to'=>$to_address,
								'date'=>$date,
								'size'=>$size,
								'subject'=>$subject
							);
						}
					}
				}
			}
		}
	}

	public function mailbody($message_uid){
		

	}
}

