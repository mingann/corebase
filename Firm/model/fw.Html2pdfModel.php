<?php

class Html2pdfModel extends Model_Base {
	private $bin=NULL;
	private $nobin='online'; // online:使用線上服務, self:自建的線上服務

	public function __construct(){
		$bin_list=array(
			'/opt/wkhtmltopdf/bin/wkhtmltopdf',
			'/usr/local/bin/wkhtmltopdf',
		);
		if(is_null($this->bin)){
			for($i=0,$n=count($bin_list);$i<$n;$i++){
				if(file_exists($bin_list[$i])){
					$this->bin=$bin_list[$i];
					break;
				}
			}
		}
	}

	public function nobin_set($srv){
		$this->nobin=$srv;
	}

	public function file_do($src, $dst){
		$dst_dir=dirname($dst);
		$src_sub=strtolower(strrchr($src, '.'));
		if(!in_array($src_sub,array('.html','.htm'))){
			return 4; // 來源檔案不是 html/htm
		}
		if(!file_exists($src)){
			return 2; // 沒有來源檔案
		}
		if((!is_dir($dst_dir) && !is_writable($dst_dir)) && !is_writable($dst)){
			return 3; // 無法寫入目標
		}
		if(is_null($this->bin)){
			if(!$this->nobin){
				return 1; // 沒有指令
			}
			else{
				return $this->{'file_conv_'.$this->nobin}(file_get_contents($src));
			}
		}
		else{
			@exec($this->bin.' '.$src.' '.$dst);
			return 0;
		}
	}

	public function mem_do($html){
		if(!is_null($this->bin)){
			$fpath=tempnam('/tmp','htm');
			file_put_contents($fpath.'.html', $html);
			$c=$this->file_do($fpath.'.html', $fpath.'.pdf');
			if($c==0){
				@unlink($fpath.'.html');
				$c=file_get_contents($fpath.'.pdf');
				@unlink($fpath.'.pdf');
			}
		}
		else{
			$c=$this->conv_self($html);
		}
		return $c;
	}

	private function file_conv_online($src, $dst){

	}

	private function conv_self($html){
		$srv_url='http://demo-2.hello-world.asia/srv/html2pdf.php';
		$data=array(
			'html'=>$html,
		);
		$c=CZ::model('http')->post_form($srv_url, $srv_url, $data);
		if($c['STATUS']['http_code']==200){
			return $c['FILE'];
		}
	}
}
