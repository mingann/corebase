<?php
include_once(_DIR_FRAMEWORK.'include/lib_http.php');

class FbModel extends Model_Base {
	private $version='v2.8';
	private $app_id='';
	private $app_secret='';

	private $params=array();

	public function init($ver){
		$this->app_id=CZ::config_get('sns.facebook.app_id');
		$this->app_secret=CZ::config_get('sns.facebook.app_secret');
		$versions_allowed=array(
			'v2.8'=>TRUE,
		);
		if(empty($versions_allowed[$ver])){
			$this->version='';
			return FALSE;
		}
		else{
			$this->version=strval($ver);
		}
		return TRUE;
	}

	/**
	 * param_setter 
	 * 設定參數，依據 facebook 改版所需要的參數也不完全相同，所以全部存到 $this->params
	 * 
	 * @param mixed $name 
	 * @param mixed $val 
	 * @access public
	 * @return void
	 */
	public function param_setter($name,$val){
		$this->params[$name]=$val;
	}

	public function code_validate($code){
		if(!$this->version){
			return FALSE;
		}
		return $this->{'code_validate_'.str_replace('.','_',$this->version)}($code);
	}

	public function me($access_token,$access_fields){
		if(!$this->version){
			return FALSE;
		}
		return $this->{'me_'.str_replace('.','_',$this->version)}($access_token,$access_fields);
	}

	private function redirect_uri_check(){
		if(!isset($this->params['redirect_uri'])){
			$this->params['redirect_uri']=(empty($_SERVER['HTTPS'])?'http://':'https://').$_SERVER['HTTP_HOST'].'/index/login_facebook_callback/';
		}
	}

	/**
	 * me 
	 * 
	 * @param mixed $access_token 
	 * @param string $fields 要取得資料的欄位，fb要有設定對應的權限
	 * @access public
	 * @return void
	 */
	public function me_v2_8($access_token,$access_fields){
		$graph='https://graph.facebook.com/me?fields='.$access_fields.'&access_token='.$access_token;
		$this->redirect_uri_check();
		$resp=http_get($graph,$this->params['redirect_uri']);
		if($resp['STATUS']['http_code']==200){
			return json_decode($resp['FILE'],TRUE);
		}
		return json_decode($resp['FILE'],TRUE);
	}

	public function code_validate_v2_8($code){
		$graph='https://graph.facebook.com/v2.8/oauth/access_token?client_id='.$this->app_id.'&redirect_uri='.urlencode($this->params['redirect_uri']).'&client_secret='.$this->app_secret.'&code='.$code;
		$this->redirect_uri_check();
		$resp=http_get($graph,$this->params['redirect_uri']);
		if($resp['STATUS']['http_code']==200){
			return json_decode($resp['FILE'],TRUE);
		}
		return json_decode($resp['FILE'],TRUE);
	}


}

