<?php
/**
mail sender 採用 CZ::config_get('mailsender');
 type 	類別 smtp,  gmail, sendinblue
 smtp
 	server
	[port]	預設 25
	[secure]預設 ssl
	[auth]	預設 TRUE
	account
	password
	from_email
	from_name
 gmail
 	account
	password
	from_email
	from_name
 sendinblue
 	[version]	預設 2.0
 	key
	from_email
 	from_name
*/
class SendmailModel extends Model_Base {

	private $confed=FALSE;

	private $conf;

	private $mail;

	private $product_server=0;
	/**
	 * conf_setter 
	 * 設定寄信參數
	 * 
	 * @param $conf auto 表示載入 sys.php 的設定
	 * @access public
	 * @return void
	 */
	public function conf_setter($conf=NULL){
		if(defined('_SERVER_PRODUCT') && _SERVER_PRODUCT){
			$this->product_server=1;
		}
		if(is_null($conf) || !strcmp($conf,'auto')){
			$conf=CZ::config_get('mailsender');
		}
		if(!isset($conf['type'])){
			$conf['type']='smtp';
		}
		switch($conf['type']){
			case 'sendinblue':
				break;
			case 'gmail':
				$conf['server']='smtp.gmail.com';
				$conf['port']=465;
				$conf['secure']='ssl';
				$conf['auth']=TRUE;
			case 'smtp':
			default:
		}
		$this->conf=$conf;
		$this->confed=TRUE;
	}


	/**
	 * send 
	 * 
	 * @param mixed $to 
	 *	array(
 	 *		email
	 *		[name]
	 *		[cc]=>array(
	 *			email,
	 *			[name]
	 *		),
	 *		[bcc]=>array(
	 *			email,
	 *			[name]
	 *		),
	 *		[replaces]=>array(....) 字串取代
	 *	)
	 * @param mixed $subject 
	 * @param mixed $content 
	 * @access public
	 * @return void
	 */
	public function send($to, $subject, $content){
		if(!$this->confed){
			$this->conf_setter();
		}
		if(!isset($to[0]) && isset($to['name'])){
			$to=array($to);
		}
		$conf=$this->conf;
		if(!$this->product_server){
			_e('isnot product server');
			return FALSE;
		}
		switch($conf['type']){
			case 'sendinblue':
				include_once(_DIR_FRAMEWORK.'include/Sendinblue/Mailin.php');
				$this->mail = new Mailin('https://api.sendinblue.com/v2.0',$conf['key']);
				mb_internal_encoding('UTF-8');
				return $this->exec_sendinblue($to,$subject,$content);
//				$this->exec_smtp($to,$subject,$content);
				break;
			case 'smtp':
			case 'gmail':
				include_once(_DIR_FRAMEWORK.'include/phpmailer/class.phpmailer.php');
				mb_internal_encoding('UTF-8');

				$this->mail           = new PHPMailer();
				$this->mail->IsSMTP();                         	// set mailer to use SMTP
				$this->mail->Host     = $conf['server'];	     	// specify main and backup server
				$this->mail->Port     = $conf['port'];
				if($conf['auth']){
					$this->mail->SMTPAuth = TRUE;
				}
				if(!empty($conf['secure'])){
					$this->mail->SMTPSecure = $conf['secure']; 					// Gmail的SMTP主機需要使用SSL連線
				}
				$this->mail->Username = $conf['account'];        		// SMTP username
				$this->mail->Password = $conf['password']; 		// SMTP password

				$this->mail->From     = $conf['from_email'];		// 寄件者
				$this->mail->FromName = $conf['from_name'];		// 寄件者
				$this->mail->AddReplyTo($conf['from_email'], $conf['from_name']);
				$this->mail->IsHTML(TRUE);
				return $this->exec_smtp($to,$subject,$content);
				break;
		}


	}

	public function exec_smtp($to,$subject,$content){
		$result=array();
		for($i=0,$n=count($to);$i<$n;$i++){
			if(!$this->product_server){
				if(strcasecmp(strstr($to[$i]['email'],'@'),'@mail.mingann.info')){
					continue;
				}
			}
			$mailer=$this->mail;
			if(isset($to[$i]['replaces'])){
				foreach($to[$i]['replaces'] as $id => $v){
					$subject=str_replace('[#'.$id.']',$v,$subject);
					$content=str_replace('[#'.$id.']',$v,$content);
				}
			}
			if(!empty($to[$i]['cc'])){
				for($j=0,$m=count($to[$i]['cc']);$j<$m;$j++){
					$mailer->AddCC($to[$i]['cc'][$j]['email'],$to[$i]['cc'][$j]['name']);
				}
			}
			if(!empty($to[$i]['bcc'])){
				for($j=0,$m=count($to[$i]['bcc']);$j<$m;$j++){
					$mailer->AddBCC($to[$i]['bcc'][$j]['email'],$to[$i]['bcc'][$j]['name']);
				}
			}
			if(is_array($to[$i]['email'])){
				for($j=0,$m=count($to[$i]['email']);$j<$m;$j++){
					$mailer->AddAddress($to[$i]['email'][$j],$to[$i]['name'][$j]);
				}
			}
			else{
				$mailer->AddAddress($to[$i]['email'],$to[$i]['name']);
			}
			$mailer->Subject=$subject;
			$mailer->Body=$content;
			if (!$mailer->Send()) {
				$result[$i]=$mailer->ErrorInfo;
			}
			else{
				$result[$i]=TRUE;
			}
		}
		return $result;
	}

	public function exec_sendinblue($to,$subject,$content){
		$result=array();
		$conf=$this->conf;
		$result=array();
		for($i=0,$n=count($to);$i<$n;$i++){
			if(!$this->product_server){
				if(strcasecmp(strstr($to[$i]['email'],'@'),'@mail.mingann.info')){
					continue;
				}
				else{
				}
			}
			else{
			}
			if(isset($to[$i]['replaces'])){
				foreach($to[$i]['replaces'] as $id => $v){
					$subject=str_replace('[#'.$id.']',$v,$subject);
					$content=str_replace('[#'.$id.']',$v,$content);
				}
			}

			$data = array(
				'from' => array($conf['from_email'],$conf['from_name']),
				'subject' => $subject,
				'html' => $content,
			);
			if(!empty($to[$i]['cc'])){
				$data['cc']=array();
				for($j=0,$m=count($to[$i]['cc']);$j<$m;$j++){
					$data['cc'][$to[$i]['cc'][$j]['email']]=$to[$i]['cc'][$j]['name'];
				}
			}
			if(!empty($to[$i]['bcc'])){
				$data['bcc']=array();
				for($j=0,$m=count($to[$i]['bcc']);$j<$m;$j++){
					$data['bcc'][$to[$i]['bcc'][$j]['email']]=$to[$i]['bcc'][$j]['name'];
				}
			}
			if(is_array($to[$i]['email'])){
				$data['to']=array();
				for($j=0,$m=count($to[$i]['email']);$j<$m;$j++){
					$data['to'][$to[$i]['email'][$j]['email']]=$to[$i]['email'][$j]['name'];
				}
			}
			else{
				$data['to'][$to[$i]['email']]=$to[$i]['name'];
			}

			$sent=$this->mail->send_email($data);
			if(!strcasecmp($sent['code'],'success')){
				$result[$i]=TRUE;
			}
			else{
				$result[$i]=FALSE;
			}
		}
		return $result;
	}

	public function in_quque(){
		if(xdate('i')){

		}	

	}

}

