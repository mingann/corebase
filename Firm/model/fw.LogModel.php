<?php
class LogModel extends Model_Base {


	public function __construct(){
		$this->dbid='session';
		$this->table_name='sess';
		$this->field_pk='sessId';
		$this->field_pky_charset='uid4';
		$this->fields=array(
			'sessId'=>array('charset'=>'uid4','max'=>32,'req'=>1),
			'sessHash'=>array('charset'=>'string','max'=>32,'req'=>0),
			'userUid'=>array('charset'=>'string','max'=>32,'req'=>0),
			'sessData'=>array('charset'=>'string','max'=>65535,'req'=>0),
			'sessExtend'=>array('charset'=>'boolean','req'=>0),
			'sessUA'=>array('charset'=>'string','max'=>255,'req'=>0),
			'sessMobile'=>array('charset'=>'boolean','req'=>0),
			'sessLanguage'=>array('charset'=>'safe','max'=>6,'req'=>1),
			'sessRemote'=>array('charset'=>'safe','max'=>24,'req'=>1),
			'sessTimeUpdate'=>array('charset'=>'timestamp','req'=>0),
			'sessTimeVisit'=>array('charset'=>'timestamp','req'=>0),
			'sessTimeLogin'=>array('charset'=>'timestamp','req'=>0),
		);

	}

	public function data_get($id){
		$query=array(
			'select'=>$this->field_str(),
			'from'=>$this->table_name,
			'where'=>array(
				'sessId=:id',
				array(':id'=>$id),
			),
		);
		$sess=DB::row('session',$query);
		if($sess){
			$this->bind_pk=$id;
		}
		return $sess;
	}

	public function update($data){
		DB::update('session',$this->table_name,$data,array(
			'WHERE sessId=:id',
			array(
				':id'=>$this->bind_pk
			)
		));
	}
}

