<?php

/**
 * server: 資料庫相對於 AP 的 IP 位址
 * account: 授權登入的帳號
 * password: 授權登入的密碼
 * port: 埠口
 * dbname: 資料庫名稱
 * [instance]: 部分的 database server 需要提供
 * mode: 單一資料庫，無多重資料庫，多重資料庫的 dbname 後面要加序號
 * driver: database driver, 預設為 mysql
 */

return array(
	'common'=>array(
		'server'=>'127.0.0.1',
		'account'=>'',
		'password'=>'',	
		'port'=>3306,
		'dbname'=>'',
		'mode'=>'only',// 單一資料庫，無多重資料庫，多重資料庫的 dbname 後面要加序號
		'driver'=>'mysql',
	),
	'admin'=>array(
		'server'=>'127.0.0.1',
		'account'=>'',
		'password'=>'',
		'port'=>3306,
		'dbname'=>'',
		'mode'=>'any',
		'driver'=>'mysql',
	),





);

