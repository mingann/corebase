<?php
/**
 * pre-define:
 *	_DIR_DOCS:	project directory path
 *	_DIR_APP:	application directory path, has two sub-directories: controller and template 
 */

date_default_timezone_set('UTC');
mb_internal_encoding('UTF-8');

/*
include(_DIR_FRAMEWORK.'include/function.php');
include(_DIR_FRAMEWORK.'include/model.base.php');
include(_DIR_FRAMEWORK.'include/controller.base.php');
include(_DIR_FRAMEWORK.'include/pdo.class.php');
DB::conf_load(_DIR_DOCS.'etc/db.php');
include(_DIR_FRAMEWORK.'include/cz.php');
include(_DIR_FRAMEWORK.'include/me.php');


*/

include(_DIR_FRAMEWORK.'include/function.php');
include(_DIR_FRAMEWORK.'include/model.base.php');
include(_DIR_FRAMEWORK.'include/controller.base.php');
include(_DIR_FRAMEWORK.'include/pdo.class.php');
DB::conf_load(_DIR_DOCS.'etc/db.php');
include(_DIR_FRAMEWORK.'include/cz.php');
//include(_DIR_FRAMEWORK.'include/me.php');
//ME::init();
//CZ::request_set();
define('_TIMEZONE',CZ::timezone_get());
include(_DIR_DOCS.'locale/zh-tw/languages.php');