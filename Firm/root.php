<?php
/**
 * pre-define:
 *	_DIR_DOCS:	project directory path
 *	_DIR_APP:	application directory path, has two sub-directories: controller and template 
 */

if(defined('_DEBUG') && _DEBUG){
	error_reporting(-1);
	ini_set('display_errors',1);
}
else{
	error_reporting(0);
	ini_set('display_errors',0);
}
if(!defined('_NO_SESSION')){
	define('_NO_SESSION',FALSE);
}
if(defined('_HTTPS_ONLY') && _HTTPS_ONLY && empty($GLOBALS['skip_https_check'])){
	if(empty($_SERVER['HTTPS'])){
		if(empty($_SERVER['REQUEST_URI'])){
			header('refresh:0;url=https://'.$_SERVER['HTTP_HOST'].'/');
		}
		else{
			header('refresh:0;url=https://'.$_SERVER['HTTP_HOST'].'/'.$_SERVER['REQUEST_URI']);
		}
		die('');
	}
}

date_default_timezone_set('UTC');
mb_internal_encoding('UTF-8');

include(_DIR_FRAMEWORK.'include/function.php');
include(_DIR_FRAMEWORK.'include/model.base.php');
include(_DIR_FRAMEWORK.'include/controller.base.php');
include(_DIR_FRAMEWORK.'include/pdo.class.php');
DB::conf_load(_DIR_DOCS.'etc/db.php');
include(_DIR_FRAMEWORK.'include/cz.php');

if(!_NO_SESSION){
	include(_DIR_FRAMEWORK.'include/me.php');
	ME::init();
}
CZ::request_set();
define('_TIMEZONE',CZ::timezone_get());
include(_DIR_DOCS.'locale/'.CZ::$lang.'/languages.php');

header('Server: IIS7.0');
header('X-Powered-By: ASP.Net 6.5');
header('Vary: Accept-Encoding');

if(defined('_XFrameOptions')){
	if(_XFrameOptions=='deny'){
		header('X-Frame-Options: DENY');
	}
	else{
		header('X-Frame-Options: SAMEORIGIN');
	}
}
if(defined('_FrameAncestors')){
	header('Content-Security-Policy: frame-ancestors \''._FrameAncestors.'\'');
}
header('X-XSS-Protection: "1;mode=block"');
header('X-Content-Type-Options:nosniff');

//header('Set-Cookie: hidden=value; httpOnly');
if(!_NO_SESSION){
	if(ME::is_login()){
		header('Cache-Control: private');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
	}
}
CZ::controller_do(CZ::controller(),CZ::command());
if(!_NO_SESSION){
	ME::close();
}



