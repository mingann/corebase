<?php
/**
 * 20180302 新增靜態變數 _SESSION_LIFETIME (單位:秒)
 *
 *
 *
 */
if(!defined('_PROJECT_ID')){
	define('_PROJECT_ID','N/A');
}

class ME {

	static private $id='';
	static private $prefix='';

	static private $datum=array();

	/**
	 * 是否為使用狀態中
	 *  
	 */
	static private $open=0;

	/**
	 * 這個 process 中是否有 session 資料被異動過
	 *  
	 */
	static private $modify=0;

	/**
	 * 從資料庫取出時的上次修改時間 timestamp
	 *  
	 */
	static private $time_update=0;

	/**
	 * self::$create_id
	 * 0: SSID exists
	 * 1: SSID nonexists, create that
	 * 2: SSID exists, create one and replace SSID
	 */
	static private $create_id=0;

	static private $cookie_name='SSID';

	static public function init($ssid=NULL){
		if(defined('_PREFIX_SESSION')){
			self::$prefix=_PREFIX_SESSION;
		}
		$is_browser=TRUE;
		self::$open=1;
		self::$datum=self::clear();
		if(!is_null($ssid)){
			self::$id=$ssid;
			$is_browser=FALSE;
		}
		else if(!isset($_COOKIE) && isset($_POST[self::$cookie_name])){
			self::$id=$_POST[self::$cookie_name];	
		}
		else if(isset($_COOKIE[self::$cookie_name])){
			self::$id=$_COOKIE[self::$cookie_name];
		}
		else{
			self::$create_id=1;
			self::$id='';
		}
		if(strlen(self::$id)>10){
			$sess=CZ::model('session')->data_get(self::$id);
			if(!$sess){
				self::$id='';
			}
			else{
				self::setter($sess);
			}
		}
		if(!self::$id && $is_browser){
			self::add_new();
		}
	}

	static public function halt(){
		self::close();
		die('');
	}

	static public function id(){
		return self::$id;
	}

	static public function user_uid(){
		return self::$datum['u']['uid'];
	}

    static public function email_valid(){
        return self::$datum['u']['email'] ;
    }

    static public function first_login(){
        return self::$datum['u']['firstlogin'] ;
    }

    static public function fb_login(){        
        return self::$datum['u']['fblogin'] ;
    }

	static public function user_name(){
		return self::$datum['u']['name'];
	}

	static public function setter($data){
		self::$id=$data['sessId'];
		if($data['sessExtend']){
			$sess_extend=CZ::model('session')->extend_get('sessId',self::$id);
		}
		if(strlen($data['sessData'])<2){
			self::$datum=self::clear();
		}
		else{
			self::$datum=json_decode($data['sessData'],TRUE);
		}
	}

	static private function clear(){
		return array(
			'u'=>array(
				'uid'=>'',
				'name'=>'',
				'admin'=>0,
				'perm'=>0,
                'email'=>0,
                'fblogin'=>0,
                'firstlogin'=>0,
			),
			'd'=>array(
			),
		);
	}


	static public function add_new(){
		$i=0;
		while(1){
			$i++;
			self::$id=uid4(self::$prefix);
			$sess=CZ::model('session')->find_one('sessId',self::$id);
			if(!$sess || $i>100){
				break;
			}
		}
		CZ::model('session')->add(
			array(
				'sessId'=>self::$id,
				'projectId'=>_PROJECT_ID,
				'sessHash'=>uid4(),
				'userUid'=>'',
				'sessData'=>'',
				'sessExtend'=>0,
				'sessUA'=>'',
				'sessMobile'=>0,
				'sessLanguage'=>CZ::config_get('site.lang.default'),
				'sessRemote'=>remote_addr(),
				'sessTimeUpdate'=>_SYS_DATETIME,
				'sessTimeVisit'=>_SYS_DATETIME,
				'sessTimeLogin'=>_SYS_DATETIME,
			)
		);
		setcookie(self::$cookie_name,self::$id,_SYS_TIMESTAMP+86400*32,'/',_DOMAIN, FALSE, TRUE);
	}

	/**
	 * logout 
	 * 
	 * @param mixed $data_items 要清除的 data 陣列 index
	 * @static
	 * @access public
	 * @return void
	 */
	static public function logout($data_items=NULL){
		self::$datum['u']=array(
			'uid'=>'',
			'name'=>'',
			'admin'=>0,
			'perm'=>0,
			'role'=>array(),
			'email'=>0,
			'fblogin'=>0,
			'firstlogin'=>0
		);
		if(!is_null($data_items)){
			if(is_array($data_items)){
				for($i=0,$n=count($data_items);$i<$n;$i++){
					self::data_unset($data_items[$i]);
				}
			}
			else if(is_string($data_items)){
				self::data_unset($data_items);
			}

		}
		self::$modify=1;
	}

	static public function become($pk,$name,$perm=0,$admin=0,$email=0,$fblogin=0,$firstlogin=0){
		self::$datum['u']['uid']=$pk;
		self::$datum['u']['name']=$name;
		self::$datum['u']['perm']=$perm;
		self::$datum['u']['role']=array();
		self::$datum['u']['admin']=$admin?1:0;
        self::$datum['u']['email']=$email?1:0;
        self::$datum['u']['fblogin']=$fblogin?1:0;
        self::$datum['u']['firstlogin']=$firstlogin?1:0;
		self::$modify=1;
	}

	static public function refresh(){

	}

	static public function close(){
		if(self::$modify){
			self::$modify=0;
			self::$open=0;
			self::save();
		}
	}

	static public function data_set($vn,$val){
		$vn2=$vn;
		$vn=explode('.',$vn);
		$num_vn=count($vn);
		if($num_vn==1){
			self::$datum['d'][$vn[0]]=$val;
			self::$modify=1;
		}
		else if($num_vn==2){
			self::$datum['d'][$vn[0]][$vn[1]]=$val;
			self::$modify=1;
		}
		else if($num_vn==3){
			self::$datum['d'][$vn[0]][$vn[1]][$vn[2]]=$val;
			self::$modify=1;
		}
		else if($num_vn==4){
			self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]]=$val;
			self::$modify=1;
		}
		else if($num_vn==5){
			self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]]=$val;
			self::$modify=1;
		}
		else if($num_vn==6){
			self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]][$vn[5]]=$val;
			self::$modify=1;
		}
		else{
			self::$datum['d'][$vn2]=$val;
			self::$modify=1;
		}
	}

	static public function data_get($vn){
		$vn2=$vn;
		$vn=explode('.',$vn);
		$num_vn=count($vn);
		if($num_vn==1){
			return isset(self::$datum['d'][$vn[0]])?self::$datum['d'][$vn[0]]:NULL;
		}
		else if($num_vn==2){
			return isset(self::$datum['d'][$vn[0]][$vn[1]])?self::$datum['d'][$vn[0]][$vn[1]]:NULL;
		}
		else if($num_vn==3){
			return isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]])?self::$datum['d'][$vn[0]][$vn[1]][$vn[2]]:NULL;
		}
		else if($num_vn==4){
			return isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]])?self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]]:NULL;
		}
		else if($num_vn==5){
			return isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]])?self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]]:NULL;
		}
		else if($num_vn==6){
			return isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]][$vn[5]])?self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]][$vn[5]]:NULL;
		}
		else{
			return isset(self::$datum['d'][$vn2])?self::$datum['d'][$vn2]:NULL;
		}
	}

	static public function data_unset($vn){
		$vn2=$vn;
		$vn=explode('.',$vn);
		$num_vn=count($vn);
		if($num_vn==1){
			if(isset(self::$datum['d'][$vn[0]])){
				unset(self::$datum['d'][$vn[0]]);
				self::$modify=1;
			}
		}
		else if($num_vn==2){
			if(isset(self::$datum['d'][$vn[0]][$vn[1]])){
				unset(self::$datum['d'][$vn[0]][$vn[1]]);
				self::$modify=1;
			}
		}
		else if($num_vn==3){
			if(isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]])){
				unset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]]);
				self::$modify=1;
			}
		}
		else if($num_vn==4){
			if(isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]])){
				unset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]]);
				self::$modify=1;
			}
		}
		else if($num_vn==5){
			if(isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]])){
				unset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]]);
				self::$modify=1;
			}
		}
		else if($num_vn==6){
			if(isset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]][$vn[5]])){
				unset(self::$datum['d'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]][$vn[5]]);
				self::$modify=1;
			}
		}
		else{
			if(isset(self::$datum['d'][$vn2])){
				unsset(self::$datum['d'][$vn2]);
				self::$modify=1;
			}
		}
	}

	/**
	 * user_set 
	 * ME::user_set('profile.image.path','profile/aisdhoaidasd.png');
	 * 
	 * @param mixed $vn 
	 * @param mixed $val 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function user_set($vn,$val){
		$vn=explode('.',$vn);
		$num_vn=count($vn);
		if($num_vn==1){
			self::$datum['u'][$vn[0]]=$val;
		}
		else if($num_vn==2){
			self::$datum['u'][$vn[0]][$vn[1]]=$val;
		}
		else if($num_vn==3){
			self::$datum['u'][$vn[0]][$vn[1]][$vn[2]]=$val;
		}
		else if($num_vn==4){
			self::$datum['u'][$vn[0]][$vn[1]][$vn[2]][$vn[3]]=$val;
		}
		else if($num_vn==5){
			self::$datum['u'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]]=$val;
		}
		self::$modify=1;
	}

	/**
	 * user_get 
	 * ME::user_get('profile.image.path');
	 *	=> 'profile/aisdhoaidasd.png'
	 * ME::user_get('profile');
	 *	=> array('image'=>array('path'=>'.....'));
	 * 
	 * @param mixed $vn 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function user_get($vn){
		$vn=explode('.',$vn);
		$num_vn=count($vn);
		if($num_vn==1){
			return self::$datum['u'][$vn[0]];
		}
		else if($num_vn==2){
			return self::$datum['u'][$vn[0]][$vn[1]];
		}
		else if($num_vn==3){
			return self::$datum['u'][$vn[0]][$vn[1]][$vn[2]];
		}
		else if($num_vn==4){
			return self::$datum['u'][$vn[0]][$vn[1]][$vn[2]][$vn[3]];
		}
		else if($num_vn==5){
			return self::$datum['u'][$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]];
		}
	}

	static public function save(){
		if(self::$modify || (_SYS_TIMESTAMP > self::$time_update+86400)){
			$data=array(
				'userUid'=>self::$datum['u']['uid'],
				'sessData'=>json_encode(self::$datum),
				'sessTimeUpdate'=>_SYS_DATETIME,
				'sessRemote'=>remote_addr(),
			);
			self::$time_update=_SYS_DATETIME;
			CZ::model('session')->data_update($data);
			self::$modify=0;
		}
	}

	static public function is_admin(){
		return empty(self::$datum['u']['admin'])?FALSE:TRUE;
	}

	static public function is_login(){
		return empty(self::$datum['u']['uid'])?FALSE:TRUE;
	}

	static public function is_me($user_uid){
		return strcmp($user_id,self::$datum['u']['uid'])?FALSE:TRUE;
	}

	static public function is_perm($perm){
		return (self::$datum['u']['perm']==$perm)?TRUE:FALSE;
	}

	//=======================================================
	// 更新版的登入身份 2018-02 ver. 2
	// 用 self::$datum['d']['multi_users'][$user_order] 儲存多個登入身份，然後將目前使用的帳號儲存在 self::$datum['u'] 
	// 取代原本的 become(), is_admin(), is_login(), is_perm()
	// become() -> become_user()
	// is_admin() -> has_role() 檢查身份
	// is_login() -> logined()
	// is_perm() -> has_perm() 檢查是否有指定的權限
	//
	//
	//=======================================================


	/**
	 * 
	 *
	 * role 和 perm 的關係要傳入前處理好，這邊只儲存不做運算
	 * 
	 * @param
	 * @param array $user_role array('角色1', '角色2', ....)
	 *        string $user_role '角色1'
	 * @return
	 */
	static public function become_user($user_uid, $user_id, $user_display_name, $user_role, $user_perm='', $extensions=NULL){
		if(is_array($user_perm)){
			$user_perm=perm_str($user_perm);
		}
		self::$datum['u']['uid']=$user_uid;
		self::$datum['u']['id']=$user_id;
		self::$datum['u']['name']=$user_display_name;
		self::$datum['u']['perm']=$user_perm;
		if(is_array($extensions)){
			foreach($extensions as $key => $val){
				if(!in_array($key,array('uid','id','name','role','perm'))){
					self::$datum['u'][$key]=$val;
				}
			}
		}
		if(!is_null($user_role)){
			if(!isset(self::$datum['u']['role'])){
				self::$datum['u']['role']=array();
			}
			if(is_array($user_role)){
				for($i=0,$n=count($user_role);$i<$n;$i++){
					self::$datum['u']['role'][$user_role[$i]]=1;
				}
			}
			else{
				self::$datum['u']['role']=array($user_role=>1);
			}
		}
		$existed=-1;
		$num_users=0;
		if(!isset(self::$datum['d']['multi_users'])){
			self::$datum['d']['multi_users']=array();
			$num_users=0;
		}
		else{
			for($i=0,$num_users=count(self::$datum['d']['multi_users']);$i<$num_users;$i++){
				if(!strcmp(self::$datum['d']['multi_users'][$i]['uid'], $user_uid)){
					$existed=$i;
					break;
				}
			}
		}
		if($existed>=0){
			self::$datum['d']['multi_users'][$existed]=self::$datum['u'];
		}
		else{
			self::$datum['d']['multi_users'][$num_users]=self::$datum['u'];
		}
		self::$modify=1;
		return TRUE;
	}

	/**
	 * change_user 
	 * 變更為其他已登入使用者
	 * 
	 * @param int $user_order 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function change_user($user_order=0){
		if(!isset(self::$datum['d']['multi_users'][$user_order])){
			return FALSE;
		}
		else if(strcmp(self::$datum['d']['multi_users'][$user_order]['uid'], self::$datum['u']['uid'])){
			self::$datum['u']=self::$datum['d']['multi_users'][$user_order];
			self::$modify=1;
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * logout_user_only 
	 * 
	 * @param user_uid/uint/NULL $user_order NULL表示只登出當前使用的使用者，指定 userUid 或 user_order 表示登出特定的已登入身份
	 * @static
	 * @access public
	 * @return void
	 */
	static public function logout_user_only($user_order=NULL){
		if(is_null($user_order)){
			self::logout();
		}
		else if(is_string($user_order) && !strcmp('current',$user_order) && !empty(self::$datum['u']['uid'])){
			$uid=self::$datum['u']['uid'];
			$removed_user_order=-1;
			$new_multi_user=array();
			self::$modify=1;
			for($i=0,$n=count($datum['d']['multi_users']);$i<$n;$i++){
				if(!strcmp($uid,self::$datum['d']['multi_users'][$i]['uid'])){
					$removed_user_order=$i;
					unset(self::$datum['d']['multi_users'][$i]);
				}
				else{
					$new_multi_user[]=self::$datum['d']['multi_users'][$i];
				}
			}
			if($removed_user_order>=0){
				self::$datum['d']['mutli_users']=$new_multi_user;
			}
			if(count(self::$datum['d']['multi_users'])){
				self::$datum['u']=self::$datum['d']['multi_users'][0];	
			}
			else{
				self::logout();
			}
		}
		else if(is_int($user_order) && $user_order>=0 && isset(self::$datum['d']['multi_users'][$user_order])){
			$uid=self::$datum['d']['multi_users'][$user_order]['uid'];
			$n=count(self::$datum['d']['multi_users']);
			if($user_order==($n-1)){
				self::$modify=1;
				unset(self::$datum['d']['multi_users'][$user_order]);
				$n=count(self::$datum['d']['multi_users']);
			}
			if($n>0){
				self::$modify=1;
				self::$datum['u']=self::$datum['d']['multi_users'][0];
			}
			else{
				self::logout();
			}
		}
		else if(is_string($user_order) && strlen($user_order)){
			$removed_user_order=-1;
			$new_multi_user=array();
			for($i=0,$n=count(self::$datum['d']['multi_users']);$i<$n;$i++){
				if(!strcmp(self::$datum['d']['multi_users'][$i]['uid'], $user_order)){
					self::$modify=1;
					$removed_user_order=$i;
					unset(self::$datum['d']['multi_users'][$i]);
				}
				else{
					$new_multi_user[]=self::$datum['d']['multi_users'][$i];
				}
			}
			if($removed_user_order>=0){
				self::$datum['d']['multi_users']=$new_multi_user;
			}
			if(!strcmp($user_order,self::$datum['u']['uid'])){
				if(count(self::$datum['d']['multi_users'])){
					self::$modify=1;
					self::$datum['u']=self::$datum['d']['multi_users'][0];
				}
				else{
					self::logout();
				}
			}
		}

	}

	/**
	 * has_perm 
	 * 
	 * @param mixed $perm 
	 * @param mixed $all_match 當 $perm 為 array 或 string 時，是否須(TRUE)完全符合所需權限，或是(FALSE)只要部分符合
	 * @static
	 * @access public
	 * @return void
	 */
	static public function has_perm($perm, $all_match=TRUE){
		if(is_int($perm) && $perm<32 && $perm>=0){
			return perm_exist(self::$datum['u']['perm'],$perm);	
		}
		else if($all_match && is_array($perm)){
			for($i=0,$n=count($perm);$i<$n;$i++){
				if(!perm_exist(self::$datum['u']['perm'],$perm[$i])){
					return FALSE;
				}
			}
			return TRUE;
		}
		else if(!$all_match && is_array($perm)){
			for($i=0,$n=count($perm);$i<$n;$i++){
				if(perm_exist(self::$datum['u']['perm'],$perm[$i])){
					return TRUE;
				}
			}
			return FALSE;
		}
		else if($all_match && is_string($perm)){
			if(perm_cmp(self::$datum['u']['perm'],$perm,1)){
				return TRUE;
			}
			return FALSE;
		}
		else if(!$all_match && is_string($perm)){
			if(perm_cmp(self::$datum['u']['perm'],$perm,0)){
				return TRUE;
			}
			return FALSE;
		}
		return FALSE;
	}

	static public function logined(){
		return empty(self::$datum['u']['uid'])?FALSE:TRUE;
	}

	static public function has_role($role_id){
		return empty(self::$datum['u']['role'][$role_id])?FALSE:TRUE;
	}

	static public function all_users(){
		return self::$datum['d']['multi_users'];
	}

}

