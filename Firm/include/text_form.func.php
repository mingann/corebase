<?php
/**
 * 將使用者輸入的格式化文字轉為表單
 * 用兩個大括號括起來表示表單元件
 *
 * 使用方式：
 * {{欄位名稱 資料類別 資料屬性}}
 *  欄位名稱可以使用空白、TAB、單雙引號、逗號、句點以外的任意字串
 *  資料類別
 *    文字類：
 *     string: 字串，未設定時預設為使用者自行輸入的字串
 *     text: 長文字，上下會換行
 *   日期時間類：
 *     date: 日期
 *     year-month: 年月
 *     month-day: 月日
 *	     
 */
function text_to_form($text){
	if(preg_match_all('/\{\{([^\s\}]+)(\s+[a-zA-Z0-9\-]+)?(\s+[^\}]+)?\}\}/u',$text,$m,PREG_SET_ORDER)){
		for($i=0,$n=count($m);$i<$n;$i++){
			$text=str_replace($m[$i][0],option_to_elem(
				$m[$i][1],
				isset($m[$i][2])?$m[$i][2]:'text',
				NULL,
				isset($m[$i][3])?$m[$i][3]:NULL
			),$text);
		}
		echo $text;
	}
}

function text_to_form_edit($text, $profile){
	if(preg_match_all('/\{\{([^\s\}]+)(\s+[a-zA-Z0-9\-]+)?(\s+[^\}]+)?\}\}/u',$text,$m,PREG_SET_ORDER)){
		for($i=0,$n=count($m);$i<$n;$i++){
			$text=str_replace($m[$i][0],option_to_elem(
				$m[$i][1],
				isset($m[$i][2])?$m[$i][2]:'text',
				isset($profile[$m[$i][0]])?$profile[$m[$i][0]]:NULL,
				isset($m[$i][3])?$m[$i][3]:NULL
			),$text);
		}
	}

}

/**
 * 
 *
 * @param string $text
 * @param array $profile 
 * @return
 */
function text_with_profiile($text, $profile){
	foreach($profile as $field => $val){
		while(1){
			$n=strpos($text, '{{'.$field);
			if($n!==FALSE){
				$ed=strpos($text, '}}',$n);
				if(is_array($val)){
					$text=str_replace(substr($text,$n,$ed-$n+2),'<span class="user-options"><span class="option">'.join('</span><span class="option">',$val).'</span></span>',$text);
				}
				else{
					$text=str_replace(substr($text,$n,$ed-$n+2),$val,$text);
				}
			}
			else{
				break;
			}
		}
	}
	return $text;
}

function option_to_elem($name, $type, $default, $conf=NULL){
	$elem='';
	switch($type){
		case 'date':
			$elem='<input type="text" data-type="date" class="date" />';
			break;
		case 'date-hour':


			break;
		case 'textarea':
			$elem='<div class="row-fluid"><textarea></textarea></div>';
			break;
		case 'text':
		default:
			$elem='<input type="text" data-type="string" class="" />';
	}
	return $elem;
}


