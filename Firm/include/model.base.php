<?php
/**
 * $datasource=CZ::model('datasource_table');
 *
 *
 */

class Model_Base {

	protected $fields=array();
	protected $field_select='';

	/**
	 * field_auto_increment 
	 * auto_increment 的欄位名稱，若無則設為 FALSE
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $field_auto_increment=FALSE;

	/**
	 * field_pk 
	 * public key 的欄位名稱，若有 auto_increment 時使用 auto_increment 的欄位
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $field_pk=FALSE;

	/**
	 * field_pk_charset 
	 * public key 的欄位型別
	 *   若為 auto_increment 則為 bigint
	 *   若為 uid4 則為 char(32)/varchar(32)
	 *   若為 id 則為 char
	 * @var string
	 * @access protected
	 */
	protected $field_pk_charset='uid4';

	/**
	 * bind_id 
	 * 綁定的 pk 值
	 * 
	 * @var string
	 * @access protected
	 */
	protected $bind_pk='';

	/**
	 * dbid 
	 * 所要使用的 dbid(根據 etc/db.php 的設定)
	 * 
	 * @var string
	 * @access protected
	 */

	protected $dbid='common';

	/**
	 * table_name 
	 * 所要操作的資料表
	 * 
	 * @var string
	 * @access protected
	 */
	protected $table_name='';

	/**
	 * relation_tables 
	 * 相關資料表
	 * 
	 * @var array
	 * @access protected
	 */
	protected $relation_tables=array();

	/**
	 * field_datum 
	 * 欄位資料
	 * 
	 * @var array
	 * @access protected
	 */
	protected $field_datum=array();

	/**
	 * field_datum_index 
	 * 
	 * @var float
	 * @access protected
	 */
	protected $field_datum_index=0;


	/**
	 * cache_data
	 * 暫存資料，用於setval()和update()
	 *
	 */
	protected $cache_data=array();
	protected $cache_where=NULL;

	/**
	 * field_order 
	 * order by
	 *
	 * @var string
	 * @access public
	 */
	public $field_order='';


	protected $cache_leftjoin=array();
	protected $cache_order=array();
	protected $cache_group=array();

	protected $tree_type=FALSE;	// 樹狀結構模式，mptt 需要設定以下幾個欄位名稱，reftree 需要設定 REFTREE 相關
	protected $mptt_lnode='';	// MPTT 左節點欄位名稱
	protected $mptt_rnode='';	// MPTT 右節點欄位名稱
	protected $mptt_ref='';		// MPTT 上層主鍵欄位名稱
	protected $mptt_level='';	// MPTT 紀錄層數欄位名稱
	protected $mptt_type='';	// MPTT 分類欄位名稱，可以無，表示全表使用一個 MPTT 結構o

	protected $reftree_level='';
	protected $reftree_ref='';
	protected $reftree_type='';
	protected $reftree_fields=array();
	protected $reftree_num_fields=0;


	public function __construct(){
		$this->cache_data=array();
		$this->cache_where=NULL;
	}


	public function field_set($field_select){
		$this->field_select=$field_select;
		return $this;
	}

	protected function field_str(){
		if($this->field_select){
			return $this->field_select;
		}
		return '*';
	}

	/**
	 * finds 
	 * 搜尋指定欄位符合 $val
	 * 
	 * @param mixed $field_name 
	 * @param mixed $val 
	 * @param string $fields 
	 * @access public
	 * @return void
	 */
	public function finds($field_name, $val, $fields='*'){
		$query=array(
			'select'=>$fields,
			'from'=>$this->table_name,
			'where'=>array(
				$field_name.'=:val',
				array(
					':val'=>$val,
				),
			),
		);
		return DB::data($this->dbid,$query);
	}

	/**
	 * find_one 
	 * 和 one() 的不同是在於要指定欄位
	 * 
	 * @param mixed $field_name 
	 * @param mixed $val 
	 * @access public
	 * @return void
	 */
	public function find_one($field_name,$val,$select_fields=''){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				$field_name.'=:val',
				array(
					':val'=>$val,
				),
			),
		);
		if(strlen($select_fields)){
			$query['select']=$select_fields;
		}
		return DB::row($this->dbid,$query);
	}

	/**
	 * one 
	 * 取得一筆資料，只需傳入 field_pk 的值
	 * 
	 * @param mixed $pk 
	 * @access public
	 * @return void
	 */
	public function one($pk, $select_fields=''){
		if(!$this->field_auto_increment && $this->field_pk){
			$query=array(
				'select'=>'',
				'from'=>$this->table_name,
				'where'=>array(
					$this->field_pk.'=:val',
					array(
						':val'=>$pk,
					),
				),
			);
			if(strlen($select_fields)){
				$query['select']=$select_fields;
			}
			$row=DB::row($this->dbid,$query);
			return $row;
		}
		else if($this->field_auto_increment && is_numeric($pk)){
			$query=array(
				'select'=>'',
				'from'=>$this->table_name,
				'where'=>array(
					$this->field_auto_increment.'=:val',
					array(
						':val'=>$pk,
					),
				),
			);
			if(strlen($select_fields)){
				$query['select']=$select_fields;
			}
			$row=DB::row($this->dbid,$query);
			return $row;
		}
	}

	/**
	 * save 
	 * 
	 * @param mixed $data 
	 * @param mixed $pk 
	 * @param string $exists $pk 是否為已存在，當 $this->field_auto_increment=FALSE 時才有用，因為 $this->field_pk 可以指定新增
	 * 		TRUE: 表示傳入的 $pk 是已存在的
	 *		FALSE:  表示傳入的 $pk 是未存在的，要指定新增
	 * @access public
	 * @return void
	 */
	public function save($data,$pk=NULL,$exists=TRUE){
		$exe='';
		if(!$this->field_auto_increment && $this->field_pk){
			if(!empty($data[$this->field_pk])){
				$res=DB::add($this->dbid,$this->table_name,$data);
				$exe='add';
			}
			else if(!empty($pk)){
				if($exists){
					$res=DB::update($this->dbid,$this->table_name,$data,array('WHERE '.$this->field_pk.'=:val',array(':val'=>$pk)));
					$exe='update';
				}
				else{
					if(!isset($data[$this->field_pk])){
						$data[$this->field_pk]=$pk;
					}
					$res=DB::add($this->dbid,$this->table_name,$data);
					$exe='add';
				}
			}
		}
		else if($this->field_auto_increment && $this->field_pk){
			if(!is_numeric($pk)){
				$res=DB::add($this->dbid,$this->table_name,$data);
				$exe='add';
			}
			else{
				$res=DB::update($this->dbid,$this->table_name,$data,array('WHERE '.$this->field_pk.'=:val',array(':val'=>$pk)));
				$exe='update';
			}
		}
	}

	/**
	 * filter_set
	 * 設定依條件過濾的 query
	 *
	 * $settings
	 * @param
	 * @return
	 */
	public function filter_set($query, $settings){
		$where=array();
		for($i=0,$n=count($settings);$i<$n;$i++){
			if(isset($settings[$i][0])){
				$where[]=$settings[1];
				if(is_array($setting[$i][3])){
					$query['bind_array'][$settings[$i][2]]=$settings[$i][3];
				}
				else{
					$query['where'][1][$settings[$i][2]]=$settings[$i][3];
				}
			}
		}
		if(count($where)){
			$query['where'][0].=join(' AND ',$where);
		}
		return $query;
	}

	/**
	 * find_records
	 * 和 records() 的不同是在於要指定欄位
	 * 和 find_one() 類似，只是傳入的 $val 為陣列，可以查詢多筆 $val 
	 * 
	 * @param mixed $field_name 
	 * @param mixed $val 
	 * @access public
	 * @return void
	 */
	public function find_records($field_name,$val,$select_fields=''){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				$field_name.' IN (:vals)',
			),
			'bind_array'=>array(
				':vals'=>$val,
			),
		);
		if(strlen($select_fields)){
			$query['select']=$select_fields;
		}
		return DB::data($this->dbid,$query);
	}

	/**
	 * records
	 * 和 one() 類似，只是傳入的 $pk 為陣列，可以查詢多筆 $pk 
	 * 沒有 records_check() 
	 * 
	 * @param mixed $pk 
	 * @access public
	 * @return void
	 */
	public function records($pk, $select_fields=''){
		if(!$this->field_auto_increment && $this->field_pk){
			$query=array(
				'select'=>'',
				'from'=>$this->table_name,
				'where'=>array(
					$this->field_pk.' IN (:vals)',
				),
				'bind_array'=>array(
					':vals'=>$pk,
				),
			);
			if(strlen($select_fields)){
				$query['select']=$select_fields;
			}
			$row=DB::data($this->dbid,$query);
			return $row;
		}
		else if($this->field_auto_increment){
			$query=array(
				'select'=>'',
				'from'=>$this->table_name,
				'where'=>array(
					$this->field_auto_increment.' IN (:vals)',
				),
				'bind_array'=>array(
					':vals'=>$pk,
				),
			);
			if(strlen($select_fields)){
				$query['select']=$select_fields;
			}
			$row=DB::data($this->dbid,$query);
			return $row;
		}
	}
	/**
	 * cache_clear 
	 * 清除cache
	 * 
	 * @access public
	 * @return void
	 */
	public function cache_clear(){
		$this->cache_data=array();
		$this->cache_where=NULL;
		return $this;
	}

	/**
	 * setvals 
	 * 一次設定多個欄位值，格式為 array('欄位'=>值,......);
	 * 
	 * @param mixed $data 
	 * @access public
	 * @return void
	 */
	public function setvals($data){
		$this->cache_data=$data;
		return $this;
	}

	/**
	 * setval 
	 * 一次設定一個欄位值
	 * 
	 * @param mixed $field 
	 * @param mixed $val 
	 * @access public
	 * @return void
	 */
	public function setval($field, $val){
		$this->cache_data[$field]=$val;
		return $this;
	}

	/**
	 * update 
	 * 執行 UPDATE，和 setvals/steval 搭配 where 使用
	 * CZ::model('model name')->cache_clear()->setvals($data)->where($condition,$binds)->update();
	 * 
	 * @access public
	 * @return void
	 */
	public function update(){
		return DB::update($this->dbid,$this->table_name,$this->cache_data,$this->cache_where);	
	}

	/**
	 * where 
	 * 
	 * @param mixed $condition WHERE條件，例如 field1=:f1 AND field2=:f2
	 * @param mixed $binds $condition中，綁定:欄位的值，例如array(':f1'=>$field1,':f2'=>$field2)
	 * @access public
	 * @return void
	 */
	public function where($condition, $binds=NULL){
		if(is_null($binds)){
			$this->cache_where=$condition;
		}
		else{
			$this->cache_where=array($condition,$binds);
		}
		return $this;
	}

	public function setfields($fields){
		$this->cache_fields=$fields;
	}

	public function setfrom($from){
		$this->cache_from=$from;
	}

	public function setorder($order){
		$this->cache_order=$order;
	}

	public function setgroup($group){
		$this->cache_group=$group;
	}

	public function leftjoin($table, $on, $binds=NULL){
		$this->cache_lefjoin.=' LEFT JOIN '.$table.' ON '.$on;
		if(!is_null($binds)){
			$this->cache_binds=array_merge($this->cache_binds, $binds);
		}
		return $this;
	}

	/**
	 * query_build 
	 * 
	 * @param mixed $select_array 
	 *   select
   	 *   from
	 * @access public
	 * @return void
	 */
	public function query_build($query){
		if(!isset($query['where']) && !empty($this->cache_where)){
			$query['where']=$this->cache_where;

		}


		return $this;
	}

	public function one_check(&$id,$field=NULL,$select_fields=''){
		if(is_null($field)){
			$data=$this->one($id,$select_fields);
		}
		else{
			$data=$this->find_one($field,$id,$select_fields);
		}
		if(!$data){
			$id='';
			return $this->empty_array();
		}
		else{
			return $data;
		}
	}

	/**
	 * select
	 * 執行 SELECT，和 fields 搭配 where 使用
	 * CZ::model('model name')->cache_clear()->setvals($data)->where($condition,$binds)->update();
	 * 
	 * @access public
	 * @return void
	 */

	public function select(){


	}

	/**
	 * record_next 
	 * 當在 model 或 controller 設定完一筆資料的值後，執行 record_next() 新增另一筆空資料
	 * 搭配 record_assign 和 record_exec_add/record_exec_update 使用
	 * 
	 * @access public
	 * @return void
	 */
	public function record_next(){
		$this->field_datum[$this->field_datum_index]=array();
		foreach($this->fields as $field => $v){
			$this->field_datum[$this->field_datum_index][$field]=$v['default'];
		}
		$this->field_datum_index++;
	}

	/**
	 * record_assign 
	 * 搭配 record_next 和 record_exec_add/record_exec_update 使用
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function record_assign($field, $value){
		if(isset($this->fields[$field])){
			$this->field_datum[$this->field_datum_index][$field]=$value;
		}
	}

	public function record_exec_add(){
		if($this->field_datum_index==1){
			DB::add($this->dbid,$this->table_name,$this->field_datum[0]);
		}
		else{
			$this->field_datum['default']='';
			DB::add($this->dbid,$this->table_name,$this->field_datum);
		}
	}

	public function record_exec_update(){


	}

	public function record_clear(){
		$this->field_datum=array();
		$this->field_datum_index=0;
	}

	public function add($data){
		DB::add($this->dbid,$this->table_name,$data,$this->field_auto_increment?TRUE:FALSE);
	}

	public function adds($datum){
		DB::adds($this->dbid,$this->table_name,$data,$this->field_auto_increment?TRUE:FALSE);
	}

	/**
	 * reftree_field_set_default 
	 * 只有 Ref & Level，沒有 MPTT 時，設定預設的欄位名稱格式
	 * 
	 * @param mixed $prefix 
	 * @param mixed $type 
	 * @access public
	 * @return void
	 */
	public function reftree_field_set_default($prefix, $type=FALSE){
		$this->reftree_level=$prefix.'Level';
		$this->reftree_ref=$this->field_pk.'Ref';
		if($type){
			$this->reftree_type=$prefix.'Type';
		}
		else{
			$this->reftree_type='';
		}
		$this->reftree_num_fields=count($this->fields);
		$this->reftree_fields=array_keys($this->fields);
	}

	public function reftree_root_getter($type=NULL){
		if($this->reftree_type && is_null($type)){
			return NULL;
		}
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
		);
		if($this->reftree_type){
			$query['where']=array(
				$this->reftree_type.'=:type',
				array(
					':type'=>$type,
				),
			);
		}
		return DB::row($this->dbid,$query);
	}

	public function reftree_root_create($node){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
		);
		if($this->reftree_type){
			$query['where']=array(
				$this->reftree_type.'=:type',
				array(
					':type'=>$node[$this->reftree_type],
				),
			);
		}
		$exists=DB::row($this->dbid,$query);
		if($exists){
			return FALSE;
		}
		$node[$this->reftree_level]=0;
		$node[$this->reftree_ref]='';
		DB::add($this->dbid,$this->table_name,$node);
		return TRUE;
	}

	/**
	 * reftree_parents 
	 * 找出指定 node 的所有父節點，由根節點開始排序，level=0 表示根節點
	 * 
	 * @param mixed $node 
	 * @access public
	 * @return void
	 */
	public function reftree_parents($node){
		$level=$node[$this->reftree_level];	
		$query=array(
			'select'=>'',
			'from'=>$this->table_name .' AS tbl'.$level,
		);

		for($j=0;$j<$this->reftree_num_fields;$j++){
			$query['select'].=',tbl'.$level.'.'.$this->reftree_fields[$j].' AS '.$this->reftree_fields[$j].$level;
		}
		$query['select']=substr($query['select'],1);
		for($i=($level-1);$i>=0;$i--){
			for($j=0;$j<$this->reftree_num_fields;$j++){
				$query['select'].=',tbl'.$i.'.'.$this->reftree_fields[$j].' AS '.$this->reftree_fields[$j].$i;
			}
			$query['from'].=' LEFT JOIN '.$this->table_name.' AS tbl'.$i.' ON tbl'.$i.'.'.$this->field_pk.'=tbl'.($i+1).'.'.$this->reftree_ref;
		}
		$res=DB::row($this->dbid,$query);
		if(!$res){
			return array();
		}
		$res=array();
		for($i=0;$i<=$level;$i++){
			$r=array();
			for($j=0;$j<$this->reftree_num_fields;$j++){
				$r[$this->reftree_fields[$j]]=isset($res[$this->reftree_fields[$j].$i])?$res[$this->reftree_fields[$j].$i]:NULL;
			}
			$res[$i]=$r;
		}
		return array('d'=>$res,'args'=>array('num'=>count($res)));;
	}

	public function reftree_child($node_info, $field_order=NULL, $field_order_scend=NULL){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				$this->reftree_ref.'=:ref AND '.$this->reftree_level.'=:level',
				array(
					':ref'=>$node_info[$this->reftree_ref],
					':level'=>$node_info[$this->reftree_level]+1,
				)
			),
		);
		if($field_order){
			$field_order_scend=(!strcasecmp($field_order_scend,'desc'))?'DESC':'ASC';
			$query['order']=$field_order.' '.$field_order_scend;
		}
		if($this->reftree_type){
			$query['where'][0].=' AND '.$this->reftree_type.'=:type';
			$query['where'][1][':type']=$node_info[$this->reftree_type];
		}
		return DB::data($this->dbid,$query);
	}

	/**
	 * reftree_insert_into 
	 * 將節點移到另一個節點下
	 * 
	 * @param mixed $node 
	 * @param mixed $new_node 
	 * @access public
	 * @return void
	 */
	public function reftree_insert_into($node, $new_node){
		$new_node_parents=$this->reftree_parents($new_node);

		for($i=0;$i<$new_node_parents['args']['num'];$i++){
			// 若 new_node 在 node 的子孫層，則無法移動
			if(!strcasecmp($new_node_parents['d'][$i][$this->field_pk], $node[$this->field_pk])){
				return FALSE;
			}
		}

		$new_node[$this->reftree_ref]=$node[$this->field_pk];
		$new_node[$this->reftree_level]=$node[$this->reftree_level]+1;
		if($this->reftree_type){
			$new_node[$this->reftree_type]=$node[$this->reftree_type];
		}
		DB::add($this->dbid,$this->table_name,$new_node);
	}


	/**
	 * mptt_field_set_default 
	 * 設定預設的欄位名稱格式
	 * 
	 * @param string $prefix 前綴名稱
	 * @param boolean $type 是否有分類欄位 
	 * @access public
	 * @return void
	 */
	public function mptt_field_set_default($prefix, $type=FALSE){
		$this->mptt_lnode=$prefix.'LNode';
		$this->mptt_rnode=$prefix.'RNode';
		$this->mptt_level=$prefix.'Level';
		$this->mptt_ref=$this->field_pk.'Ref';
		if($type){
			$this->mptt_type=$prefix.'Type';
		}
		else{
			$this->mptt_type='';
		}
	}

	/**
	 * mptt_root_getter 
	 * 取得根節點
	 * 
	 * @param mixed $type 
	 * @access public
	 * @return void
	 */
	public function mptt_root_getter($type=NULL){
		if($this->mptt_type && is_null($type)){
			return NULL;
		}
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'order'=>$this->mptt_lnode,
		);
		if($this->mptt_type){
			$query['where']=array(
				$this->mptt_type.'=:type',
				array(
					':type'=>$type,
				),
			);
		}
		return DB::row($this->dbid,$query);
	}

	public function mptt_root_create($node){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
		);
		if($this->mptt_type){
			$query['where']=array(
				$this->mptt_type.'=:type',
				array(
					':type'=>$node[$this->mptt_type],
				),
			);
		}
		$exists=DB::row($this->dbid,$query);
		if($exists){
			return FALSE;
		}
		$node[$this->mptt_lnode]=1;
		$node[$this->mptt_rnode]=2;
		$node[$this->mptt_level]=0;
		if(isset($node[$this->mptt_ref])){
			unset($node[$this->mptt_ref]);
		}
		DB::add($this->dbid,$this->table_name,$node);
		return TRUE;
	}

	/**
	 * mptt_child 
	 * 列出子節點，不含孫節點
	 * 
	 * @param mixed $node_info 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_level 
	 * @param mixed $field_order 排序的欄位名稱
	 * @param option $field_order_scend 排序方向，[ASC]/DESC
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_child($node_info, $field_order=NULL, $field_order_scend=NULL){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				$this->mptt_lnode.'>:lnode AND '.$this->mptt_rnode.'<:rnode AND '.$this->mptt_level.'=:level',
				array(
					':lnode'=>$node_info[$this->mptt_lnode],
					':rnode'=>$node_info[$this->mptt_rnode],
					':level'=>$node_info[$this->mptt_level]+1,
				)
			),
		);
		if($field_order){
			$field_order_scend=(!strcasecmp($field_order_scend,'desc'))?'DESC':'ASC';
			$query['order']=$field_order.' '.$field_order_scend;
		}
		if($this->mptt_type){
			$query['where'][0].=' AND '.$this->mptt_type.'=:type';
			$query['where'][1][':type']=$node_info[$this->mptt_type];
		}
		return DB::data($this->dbid,$query);
	}

	/**
	 * mptt_parents 
	 * 列出指定節點的父祖節點(不含自己)
	 * 
	 * @param mixed $node_info 
	 * @param mixed $this->mptt_lnode 左節點欄位名稱
	 * @param mixed $this->mptt_rnode 右節點欄位名稱
	 * @access public
	 * @return void
	 */
	public function mptt_parents($node_info){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				$this->mptt_lnode.'<:lnode AND '.$this->mptt_rnode.'>:rnode',
				array(
					':lnode'=>$node_info[$this->mptt_lnode],
					':rnode'=>$node_info[$this->mptt_rnode],
				)
			),
			'order'=>$this->mptt_lnode,
		);
		if($this->mptt_type){
			$query['where'][0].=($this->mptt_type?' '.$this->mptt_type.'=:type':'');
			$query['where'][1][':type']=$node_info[$this->mptt_type];
		}
		return DB::data($this->dbid,$query);
	}

	/**
	 * mptt_children 
	 * 列出指定節點的子孫節點(不含自己)
	 * 
	 * @param mixed $node_info 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @access public
	 * @return void
	 */
	public function mptt_children($node_info){
		$query=array(
			'select'=>'',
			'from'=>$this->table_name,
			'where'=>array(
				$this->mptt_lnode.'>:lnode AND '.$this->mptt_rnode.'<:rnode',
				array(
					':lnode'=>$node_info[$this->mptt_lnode],
					':rnode'=>$node_info[$this->mptt_rnode],
				)
			),
			'order'=>$this->mptt_lnode,
		);
		if($this->mptt_type){
			$query['where'][0].=($this->mptt_type?' '.$this->mptt_type.'=:type':'');
			$query['where'][1][':type']=$node_info[$this->mptt_type];
		}
		return DB::data($this->dbid,$query);
	}

	/**
	 * mptt_node_gap_into 
	 * 插在下層的第一個，挪出 node_size 的空間
	 * 
	 * @param mixed $node 
	 * @param mixed $node_size 要插入多少寬度的 gap
	 * @access public
	 * @return void
	 */
	public function mptt_node_gap_into($node, $node_size){
		// UPDATE [table] t1 LEFT JOIN [table] t2 ON t1.rnode > t2.lnode SET t1.rnode=t1.rnode+`node_size` WHERE t2.uid=`node[uid]` (AND t1.type=`type` AND t2.type=`type`)
		// UPDATE [table] t1 LEFT JOIN [table] t2 ON t1.lnode > t2.lnode SET t1.rnode=t1.lnode+`node_size` WHERE t2.uid=`node[uid]` (AND t1.type=`type` AND t2.type=`type`)
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'+'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'>:lnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$data=array(
			$this->mptt_lnode=>array($this->mptt_lnode.'+'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_lnode.'>:lnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);
	}

	/**
	 * mptt_node_gap_last
	 * 插在下層的最後一個，挪出 node_size 的空間
	 * 
	 * @param mixed $node 
	 * @param mixed $node_size 要插入多少寬度的 gap
	 * @access public
	 * @return void
	 */
	public function mptt_node_gap_last($node, $node_size){
		// UPDATE [table] t1 LEFT JOIN [table] t2 ON t1.rnode >= t2.rnode SET t1.rnode=t1.rnode+`node_size` WHERE t2.uid=`node[uid]` (AND t1.type=`type` AND t2.type=`type`)
		// UPDATE [table] t1 LEFT JOIN [table] t2 ON t1.lnode > t2.rnode SET t1.rnode=t1.lnode+`node_size` WHERE t2.uid=`node[uid]` (AND t1.type=`type` AND t2.type=`type`)
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'+'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'>=:rnode',
			array(
				':rnode'=>$node[$this->mptt_rnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$data=array(
			$this->mptt_lnode=>array($this->mptt_lnode.'+'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_lnode.'>:rnode',
			array(
				':rnode'=>$node[$this->mptt_rnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);
	}

	/**
	 * mptt_node_gap_last
	 * 插在同一層的下一個，挪出 node_size 的空間
	 */
	public function mptt_node_gap_next($node, $node_size){
		// UPDATE [table] t1 LEFT JOIN [table] t2 ON t1.rnode > t2.rnode SET t1.rnode=t1.rnode+`node_size` WHERE t2.uid=`node[uid]` (AND t1.type=`type` AND t2.type=`type`)
		// UPDATE [table] t1 LEFT JOIN [table] t2 ON t1.lnode > t2.rnode SET t1.rnode=t1.lnode+`node_size` WHERE t2.uid=`node[uid]` (AND t1.type=`type` AND t2.type=`type`)
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'+'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'>:rnode',
			array(
				':rnode'=>$node[$this->mptt_rnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$data=array(
			$this->mptt_lnode=>array($this->mptt_lnode.'+'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_lnode.'>:rnode',
			array(
				':rnode'=>$node[$this->mptt_rnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);
	}

	/**
	 * mptt_insert_into
	 * 插入在指定 node 的裡面第一個(下一層)
	 * 
	 * @param mixed $node 
	 * @param mixed $new_node 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_insert_into($node, $new_node){
		$this->mptt_node_gap_into($node, 2);

		$new_node[$this->mptt_lnode]=$node[$this->mptt_lnode]+1;
		$new_node[$this->mptt_rnode]=$node[$this->mptt_lnode]+2;
		$new_node[$this->mptt_ref]=$node[$this->field_pk];
		$new_node[$this->mptt_level]=$node[$this->mptt_level]+1;
		if($this->mptt_type){
			$new_node[$this->mptt_type]=$node[$this->mptt_type];
		}
		DB::add($this->dbid,$this->table_name,$new_node);
	}

	/**
	 * mptt_append_into
	 * 插入在指定 node 的裡面最後一個(下一層)
	 * 
	 * @param mixed $node 
	 * @param mixed $new_node 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_append_into($node, $new_node){
		$this->mptt_node_gap_last($node, 2);

		$new_node[$this->mptt_lnode]=$node[$this->mptt_rnode];
		$new_node[$this->mptt_rnode]=$node[$this->mptt_rnode]+1;
		$new_node[$this->mptt_ref]=$node[$this->field_pk];
		$new_node[$this->mptt_level]=$node[$this->mptt_level]+1;
		if($this->mptt_type){
			$new_node[$this->mptt_type]=$node[$this->mptt_type];
		}
		DB::add($this->dbid,$this->table_name,$new_node);
	}

	/**
	 * mptt_insert_next
	 * 插入在指定 node 的下一個(同一層)
	 * 
	 * @param mixed $node 
	 * @param mixed $new_node 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_insert_next($node, $new_node){
		$this->mptt_node_gap_next($node, 2);
		$new_node[$this->mptt_lnode]=$node[$this->mptt_rnode]+1;
		$new_node[$this->mptt_rnode]=$node[$this->mptt_rnode]+2;
		$new_node[$this->mptt_ref]=$node[$this->mptt_ref];
		$new_node[$this->mptt_level]=$node[$this->mptt_level];
		if($this->mptt_type){
			$new_node[$this->mptt_type]=$node[$this->mptt_type];
		}
		DB::add($this->dbid,$this->table_name,$new_node);
	}

	/**
	 * mptt_node_gap_close 
	 * 關閉節點的左右節點距離，節點被刪除或移走
	 * 
	 * @param mixed $node 節點還存在原位時的資訊
	 * @access public
	 * @return void
	 */
	public function mptt_node_gap_close($node){
		$node_size=$node[$this->mptt_rnode]-$node[$this->mptt_lnode]+1;
		// 將原來被移走節點的 lnode, rnode 關閉
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'-'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'>:lnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$data=array(
			$this->mptt_lnode=>array($this->mptt_lnode.'-'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_lnode.'>:lnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);
	}

	/**
	 * mptt_node_gap_close_only
	 * 關閉節點的左右節點距離，原節點保留
	 * 
	 * @param mixed $node 節點還存在原位時的資訊
	 * @access public
	 * @return void
	 */
	public function mptt_node_gap_close_only($node){
		$node_size=$node[$this->mptt_rnode]-$node[$this->mptt_lnode]-1;
		// 將原來被移走節點的 lnode, rnode 關閉
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'-'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'>:lnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$data=array(
			$this->mptt_lnode=>array($this->mptt_lnode.'-'.$node_size),
		);
		$where=array(
			'WHERE '.$this->mptt_lnode.'>:lnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);
	}

	/**
	 * mptt_delete_node_with_children
	 * 刪除指定的節點，包含全部的子節點
	 * 
	 * @param mixed $node 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_delete_node_with_children($node){
		$node_size=$node[$this->mptt_rnode]-$node[$this->mptt_lnode]+1;
		if($node_size<2) return FALSE;

		$where=array(
			'WHERE '.$this->mptt_lnode.'>=:lnode AND '.$this->mptt_rnode.'<=:rnode',
			array(
				':lnode'=>$node[$this->mptt_lnode],
				':rnode'=>$node[$this->mptt_rnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$node[$this->mptt_type];
		}
		DB::del($this->dbid,$this->table_name,$where);

		$this->mptt_node_gap_close($node);
	}


	/**
	 * mptt_move_node_with_children 
	 * 將節點移到另一個節點下方(下一層)，連同全部子節點
	 * 
	 * @param mixed $node 
	 * @param mixed $move_to_node 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_level 紀錄層數的欄位名稱
	 * @param mixed $this->mptt_ref	 紀錄上層 node_id 的欄位名稱
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_move_into_with_children($node, $to_node){
		$node_size=$node[$this->mptt_rnode]-$node[$this->mptt_lnode]+1;
		if($node_size<2) return FALSE;

		$this->mptt_node_gap_into($to_node, $node_size);

		// 將移動的節點及下方子孫節點改變 rnode, lnode, level
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'+('.($to_node[$this->mptt_lnode]-$node[$this->mptt_lnode]+1).')'),
			$this->mptt_lnode=>array($this->mptt_lnode.'+('.($to_node[$this->mptt_lnode]-$node[$this->mptt_lnode]+1).')'),
			$this->mptt_level=>array($this->mptt_level.'+('.($to_node[$this->mptt_level]-$node[$this->mptt_level]+1).')'),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'<=:rnode AND '.$this->mptt_lnode.'>=:lnode',
			array(
				':rnode'=>$node[$this->mptt_rnode],
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$to_node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		// 變更移動節點的 ref
		$data=array(
			$this->mptt_ref=>$to_node[$this->field_pk],
		);
		$where=array(
			'WHERE '.$this->field_pk.'=:id',
			array(
				':id'=>$node[$this->field_pk],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$to_node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$this->mptt_node_gap_close($node);
	}
	/**
	 * mptt_move_node_with_children 
	 * 將節點移到另一個節點後方(同一層)，連同全部子節點
	 * 
	 * @param mixed $node 
	 * @param mixed $move_to_node 
	 * @param mixed $this->mptt_lnode 
	 * @param mixed $this->mptt_rnode 
	 * @param mixed $this->mptt_level 紀錄層數的欄位名稱
	 * @param mixed $this->mptt_ref	 紀錄上層 node_id 的欄位名稱
	 * @param mixed $this->mptt_type 
	 * @access public
	 * @return void
	 */
	public function mptt_move_next_with_children($node, $to_node){
		$node_size=$node[$this->mptt_rnode]-$node[$this->mptt_lnode]+1;
		if($node_size<2) return FALSE;

		// 將目標節點的 lnode & rnode 撐開
		$this->mptt_node_gap_next($to_node, $node_size);


		// 將移動的節點及下方子孫節點改變 rnode, lnode, level
		$diff=$to_node[$this->mptt_rnode]+1-$node[$this->mptt_lnode];
		$data=array(
			$this->mptt_rnode=>array($this->mptt_rnode.'+('.$diff.')'),
			$this->mptt_lnode=>array($this->mptt_lnode.'+('.$diff.')'),
			$this->mptt_level=>array($this->mptt_level.'+('.($to_node[$this->mptt_level]-$node[$this->mptt_level]).')'),
		);
		$where=array(
			'WHERE '.$this->mptt_rnode.'<=:rnode AND '.$this->mptt_lnode.'>=:lnode',
			array(
				':rnode'=>$node[$this->mptt_rnode],
				':lnode'=>$node[$this->mptt_lnode],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$to_node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		// 變更移動節點的 ref
		$data=array(
			$this->mptt_ref=>$to_node[$this->mptt_ref],
		);
		$where=array(
			'WHERE '.$this->field_pk.'=:id',
			array(
				':id'=>$node[$this->field_pk],
			),
		);
		if($this->mptt_type){
			$where[0].=' AND '.$this->mptt_type.'=:type';
			$where[1][':type']=$to_node[$this->mptt_type];
		}
		DB::update($this->dbid,$this->table_name,$data,$where);

		$this->mptt_node_gap_close_only($to_node);
	}


	/**
	 * fields_validate 
	 * 驗證 fields 和 relation_tables 的欄位是否正確
	 * 
	 * @access public
	 * @return void
	 */
	public function fields_validate(){
		echo '<h3>Validate for '.$this->table_name.'</div>';
		$this->table_fileds_validate($this->dbid,$this->table_name,$this->fields);
		if(!empty($this->relation_tables)){
			foreach($this->relation_tables as $table_name => $fields){
				echo '<h3>Validate for '.$table_name.'</div>';
				$this->table_fileds_validate($fields['dbid'],$table_name,$fields);
			}
		}
		echo '<div>Validate end.</div>';
	}
	private function table_fields_validate($dbid,$table,$fields){
		$meta=DB::table_meta($dbid,$table);
		$num_fields=count($fields);
		$counter=0;
		for($i=0,$n=count($meta);$i<$n;$i++){
			if(isset($fields[$meta[$i]['field']])){
				$counter++;
				switch($meta[$i]['charset']){
					case 'tinyint':
					case 'smallint':
					case 'mediumint':
					case 'int':
					case 'bigint':
					case 'char':
					case 'varchar':
						if($fields[$meta[$i]['field']]['max']>$meta[$i]['option']){
							echo '<div>Class field "'.$meta[$i]['field'].'" too length.</div>';
						}
						break;
					case 'decimal':

						break;
					case 'enum':
					case 'set':

						break;
					default:

						break;
				}
			}
			else{
				echo '<div>Field "'.$meta[$i]['field'].'" not exists in class-&gt;fields</div>';
			}
		}

	}


}


