<?php
/**
 * 介面語系
 * POST > GET > COOKIE > Browser Accept-Encoding > site default > config
 *
 *
 */
class CZ {

	/**
	 * 記錄已載入的 model 
	 *
	 */
	static private $model_loads=array();

	/**
	 * 已經 include 過的 controller
	 *  
	 */
	static private $controller_loads=array();

	/**
	 * 網站的各個參數設定
	 */
	static private $config=array();

	static private $req_controller='';
	static private $req_command='';

	static public $lang='zh-tw';
	static public $lang_default='zh-tw';

	static private $grids=array();

	static public $page_head=1;
	static public $page=1;

	//static private $link_files=array();

	/**
	 * 依照檔案類型紀錄最後變更紀錄
	 *  
	 */
	static private $link_files_update=array();
	static private $link_files=array();

	static private $obj_response=array();

	static private $obj_filesystem=array();
	
	static public function init(){
		if(defined('_MULTI_SERVER') && _MULTI_SERVER){
			$vhost_config=self::model('vhost')->config_getter($_SERVER['HTTP_HOST']);
			if(empty($vhost_config)){
				self::$config=self::model('core')->sys_config();
			}
			else{
				self::$config=json_decode($vhost_config['vhostConfig'],TRUE);
			}
		}
		else{
			self::$config=self::model('core')->sys_config();
		}
		self::lang_detect();

		$file_timeup=_DIR_DOCS.'etc/css_js_timeup.php';
		if(_DEBUG){
			if(@!!filemtime($file_timeup)){ // file exists
				self::$link_files_update=include($file_timeup);
				if(!isset(self::$link_files_update['js'])){
					self::css_js_timeup();
				}
			}
			else{
				self::css_js_timeup();
			}
		}
		else{
			if(@!!filemtime($file_timeup)){ // file exists
				self::$link_files_update=include($file_timeup);
			}
			else{
				//self::$link_files_update['js']=(floor(date('U')/100000)*100000)-1451577600;
				self::$link_files_update['js']=date('U')-1451577600;
				self::$link_files_update['css']=self::$link_files_update['js'];
			}
		}
	}

	static private function css_js_timeup(){
		$t_corebase_scss=0;
		$t_proj_scss=0;
		$t_proj_js=0;
		$base_timestamp=1451577600; // 2016-01-01 00:00:00
		if(!is_dir(_DIR_DOCS.'src/scss/')){
			return FALSE;
		}
		$d=opendir(_DIR_FRAMEWORK.'src/scss/');
		while(($f=readdir($d))!=FALSE){
			if(strcmp('.scss',strrchr($f,'.'))){
				continue;
			}
			$t=filemtime(_DIR_FRAMEWORK.'src/scss/'.$f);
			if($t_corebase_scss<1 || $t_corebase_scss<$t){
				$t_corebase_scss=$t;
			}
		}
		closedir($d);
		$t_proj_scss=filemtime(_DIR_DOCS.'src/scss/style.scss');
		$t_proj_js=filemtime(_DIR_DOCS.'public/js/onload.js');
		if($t_proj_scss < $t_corebase_scss){
			if(_DEBUG){ 
				@exec('touch '._DIR_DOCS.'src/scss/style.scss');
				$t_proj_scss=filemtime(_DIR_DOCS.'src/scss/style.scss');
			}
		}
		$t_proj_scss-=$base_timestamp;
		$t_proj_js-=$base_timestamp;
		if(!isset(self::$link_files_update['css']) || !isset(self::$link_files_update['js']) || $t_proj_scss > self::$link_files_update['css'] || $t_proj_js > self::$link_files_update['js']){
			if(_DEBUG && is_writable(_DIR_DOCS.'etc/css_js_timeup.php')){
				file_put_contents(_DIR_DOCS.'etc/css_js_timeup.php','<'.'?php'."\n".'return array(\'js\'=>'.$t_proj_js.',\'css\'=>'.$t_proj_scss.');');
			}
			self::$link_files_update=array(
				'js'=>$t_proj_js,
				'css'=>$t_proj_scss
			);
		}
	}

	static public function lang_detect(){
		$lang_assign=FALSE;
		$langs_allowed=self::config_get('site.lang.allowed');
		if(is_null($langs_allowed)){
			$lang_default=self::config_get('site.lang.default');
			if(is_null($lang_default)){
				$langs_allowed=array('zh-tw');
			}
			else{
				$langs_allowed=array($lang_default);
			}
		}
		if(isset($_POST['lang'])){
			if($langs_allowed=='*' || in_array($_POST['lang'],$langs_allowed)){
				self::$lang=$_POST['lang'];
				setcookie('lang',self::$lang,date('U')+31622400,'/',_DOMAIN);
				$lang_assign=TRUE;
			}
		}
		if(!$lang_assign && isset($_GET['lang'])){
			if($langs_allowed=='*' || in_array($_GET['lang'],$langs_allowed)){
				self::$lang=$_GET['lang'];
				setcookie('lang',self::$lang,date('U')+31622400,'/',_DOMAIN);
				$lang_assign=TRUE;
			}
		}
		if(!$lang_assign && isset($_COOKIE['lang'])){
			if($langs_allowed=='*' || in_array($_COOKIE['lang'],$langs_allowed)){
				self::$lang=$_COOKIE['lang'];
				$lang_assign=TRUE;
			}
		}
		if(!$lang_assign && !isset($_COOKIE['lang'])){
			if(!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
				$http_lang=explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
				for($i=0,$n=count($http_lang);$i<$n && !$lang_assign;$i++){
					if(in_array(strlen($http_lang[$i]),array(2,5))){
						$http_lang[$i]=strtolower($http_lang[$i]);
						$lang=strstr($http_lang[$i],';');
						if(!$lang) $lang=$http_lang[$i];
						switch($lang){
							case 'zh':
								$lang='zh-tw';
								break;
							case 'en':
								$lang='en-us';
								break;
						}
						if($langs_allowed=='*' || in_array($lang,$langs_allowed)){
							self::$lang=$lang;
							setcookie('lang',self::$lang,date('U')+31622400,'/',_DOMAIN);
							$lang_assign=TRUE;
						}
					}
				}
			}
		}
	}

	static public function lang_setter($lang){
		$allowed_langs=self::config_get('site.lang.allowed');
		if($allowed_langs=='*' || in_array($lang,$allowed_langs)){
			setcookie('lang',$lang,_SYS_TIMESTAMP+86400*32,'/',_DOMAIN);
		}
	}

	/**
	 * request_set 
	 * 需在 ME 載入後使用
	 * 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function request_set(){
		$e=isset($_GET['e'])?$_GET['e']:'';
		if(!$e || $e=='/'){
			if(!_NO_SESSION){
				if(ME::is_admin()){
					self::$req_controller=self::config_get('site.home-admin.controller');
					self::$req_command=self::config_get('site.home-admin.command');
				}
				else if(ME::is_login()){
					self::$req_controller=self::config_get('site.home-user.controller');
					self::$req_command=self::config_get('site.home-user.command');
				}
				else{
					self::$req_controller=self::config_get('site.home.controller');
					self::$req_command=self::config_get('site.home.command');
				}
			
			}
			else{
				self::$req_controller=self::config_get('site.home.controller');
				self::$req_command=self::config_get('site.home.command');
			}
		}
		else{
			$e=explode('/',$e);
			$num_e=count($e);
			if($num_e==1){
				self::$req_controller=$e[0];
				self::$req_command='index';
			}
			else if($num_e==2){
				self::$req_controller=$e[0];
				if(!strlen($e[1])){
					self::$req_command='index';
				}
				else{
					self::$req_command=$e[1];
				}
			}
			else if($num_e==3){
				self::$req_controller=$e[0];
				self::$req_command=$e[1];
				$subname=strrchr($e[2],'.');
				if(strlen($subname)){
					$subname=strtolower($subname);
					if(in_array($subname,array('.phtml','.html','.php','.aspx','.jsp','.asp','.js','.pl'))){
						$e[2]=substr($e[2],0,strlen($subname)*-1);
					}
				}
				$_GET['p']=$e[2];
			}
			else if($num_e>=4){
				self::$req_controller=$e[0];
				self::$req_command=$e[1];
				$subname=strrchr($e[2],'.');
				if(strlen($subname)){
					$subname=strtolower($subname);
					if(in_array($subname,array('.phtml','.html','.php','.aspx','.jsp','.asp','.js','.pl'))){
						$e[2]=substr($e[2],0,strlen($subname)*-1);
					}
				}
				$_GET['p']=$e[2];
				$subname=strrchr($e[3],'.');
				if(strlen($subname)){
					$subname=strtolower($subname);
					if(in_array($subname,array('.phtml','.html','.php','.aspx','.jsp','.asp','.js','.pl'))){
						$e[3]=substr($e[3],0,strlen($subname)*-1);
					}
				}
				$_GET['q']=$e[3];
			}
		}
		if(isset($GLOBALS['controller_alias'][self::$req_controller])){
			self::$req_controller=$GLOBALS['controller_alias'][self::$req_controller];
		}
	}

	/**
	 * config_get 
	 * 
	 * @param mixed $var_name 例如 a.b 表示取得 CZ::$config['a']['b'] 的值，若不存在則回傳 NULL
	 * @static
	 * @access public
	 * @return void
	 */
	static public function config_get($vn=NULL){
		if(is_null($vn)){
			return self::$config;
		}
		$vn=explode('.',$vn);
		$num_vn=count($vn);
		if($num_vn<2){
			return isset(self::$config[$vn[0]])?self::$config[$vn[0]]:NULL;
		}
		if($num_vn<3){
			return isset(self::$config[$vn[0]][$vn[1]])?self::$config[$vn[0]][$vn[1]]:NULL;
		}
		if($num_vn<4){
			return isset(self::$config[$vn[0]][$vn[1]][$vn[2]])?self::$config[$vn[0]][$vn[1]][$vn[2]]:NULL;
		}
		if($num_vn<5){
			return isset(self::$config[$vn[0]][$vn[1]][$vn[2]][$vn[3]])?self::$config[$vn[0]][$vn[1]][$vn[2]][$vn[3]]:NULL;
		}
		else if($num_vn<6){
			return isset(self::$config[$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]])?self::$config[$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]]:NULL;
		}
		else if($num_vn>=6){
			$x=isset(self::$config[$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]])?self::$config[$vn[0]][$vn[1]][$vn[2]][$vn[3]][$vn[4]]:NULL;
			if(is_null($x)) return NULL;
			$vn_num-=1;
			for($i=5;$i<=$vn_num;$i++){
				if(!isset($x[$vn[$i]])){
					 return NULL;
				}
				else{
					$x=$x[$vn[$i]];
					if($i==$vn_num){
						return $x;
					}
				}
			}
			return NULL;
		}
	}

	/**
	 * timezone 
	 * 取得要給使用者的時區
	 * 
	 * @static
	 * @access public
	 * @return 時區分鐘數
	 */
	static public function timezone_get(){
		return 480;
	}

	/**
	 * model 
	 * 載入 model
	 * 
	 * @param mixed $tbl 
	 * @param string $name alias name
	 * @static
	 * @access public
	 * @return void
	 */
	static public function model($tbl,$name=NULL){
		if(is_null($name)) $name=$tbl;
		if(isset(self::$model_loads[$name])){
			return self::$model_loads[$name];
		}
		switch($tbl){
			case 'core':
			case 'fb':
			case 'html2pdf':
			case 'http':
			case 'imap':
			case 'log':
			case 'session':
			case 'vhost':
			case 'websocket':
				$tbl=ucfirst($tbl).'Model';
				require(_DIR_FRAMEWORK.'model/fw.'.$tbl.'.php');
				self::$model_loads[$name]=new $tbl();
				break;
			case 'sendmail':
				$tbl='SendmailModel';
				require(_DIR_FRAMEWORK.'model/fw.SendmailModel.php');
				self::$model_loads[$name]=new $tbl();
				break;
			default:
				$tbl=ucfirst($tbl).'Model';
				if(@!filemtime(_DIR_DOCS.'www/model/'.$tbl.'.php')){
					die('Cannot find model '.$tbl);
				}
				require(_DIR_DOCS.'www/model/'.$tbl.'.php');
				if(!class_exists($tbl)){
					die('無法載入資料來源 '.$tbl);
				}
				self::$model_loads[$name]=new $tbl();
		}
		return self::$model_loads[$name];
	}

	/**
	 * control 
	 * 載入controller
	 * 
	 * @param mixed $name 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function control($name,$alias_name=NULL){
		if(is_null($alias_name)) $alias_name=$name;
		if(isset(self::$controller_loads[$alias_name])){
			return self::$controller_loads[$alias_name];
		}
		switch($name){
			case '':
				$controller_name=strtolower($name).'.controller.php';
				$controller_class_name=ucfirst($name).'_Controller';
				require(_DIR_FRAMEWORK.'controller/fw.'.$controller_name.'.php');
				self::$model_loads[$alias_name]=new $controller_class_name();
				break;
			case 'mailer':
				break;
			default:
				$controller_fname=strtolower($name).'.controller.php';
				$controller_class_name=ucfirst($name).'_Controller';
				if(@!filemtime(_DIR_DOCS.'www/controller/'.$controller_fname)){
					die('Cannot load controller '.$name);
				}
				require(_DIR_DOCS.'www/controller/'.$controller_fname);
				if(!class_exists($controller_class_name)){
					die('無法載入控制項 '.$controller_class_name);
				}
				self::$controller_loads[$alias_name]=new $controller_class_name();
		}
		return self::$controller_loads[$alias_name];
	}

	/**
	 * link_mtime 
	 * 回應 css 或 js 檔案的最後更新時間戳記，用來處理瀏覽器 cache expire 問題
	 * 
	 * @param mixed $type 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function link_mtime($fname){
		if(($ext=strrchr($fname,'.'))){
			if($ext=='.js'){
				if(isset(self::$link_files_update['js'])){
					return self::$link_files_update['js'].'_'.$fname;
				}
			}
			else if($ext=='.css'){
				if(isset(self::$link_files_update['css'])){
					return self::$link_files_update['css'].'_'.$fname;
				}
			}
		}
		if(isset(self::$link_files_update[$fname])){
			return self::$link_files_update[$fname];
		}
		return '';
	}


	/**
	 * link_add 
	 * 加入 js 或 css 等引用連結
	 * 
	 * @param mixed $fpath 路徑，不含副檔名
	 * @param mixed $type 檔案類型 js, css
	 * @param mixed $attach 引用連結額外參數 screen:media="screen only"
	 * @static
	 * @access public
	 * @return void
	 */
	static public function link_add($fpath,$type,$attach=''){
		if(!in_array($type,array('js','css')))return FALSE;
		if(!strcasecmp(substr($fpath,0,4),'http') || !strcmp(substr($fpath,0,2),'//')){
			// 外部連結的，不檢查最後更新時間
			// array([string]路徑,[option]類別,[string]引用連結的額外參數,[boolean]是否為外部連結,[boolean]是否已輸出過)
			// $att
			self::$link_files[]=array('fpath'=>$fpath,'type'=>$type,'attach'=>$attach,'outlink'=>1,'outputed'=>0);
		}
		else if(!strcmp(substr($fpath,0,1),'/')){
			self::$link_files[]=array('fpath'=>$fpath,'type'=>$type,'attach'=>$attach,'outlink'=>0,'outputed'=>0);
			$t=filemtime(_DIR_DOCS.'public'.$fpath.'.'.$type) - mktime(0,0,0,1,1,2015);
			if(!isset(self::$link_files_update[$type])){
				self::$link_files_update[$type]=$t;
			}
			else if(self::$link_files_update[$type]<$t){
				self::$link_files_update[$type]=$t;
			}
		}
	}

	/**
	 * link_output 
	 * 指定要輸出的引用連結
	 * 
	 * @param mixed $assigned 輸出類型,js or css or both
	 * @static
	 * @access public
	 * @return void
	 */
	static public function link_output($assigned='all'){
		//$config=include(_DIR_DOCS.'etc/js_css_update.php');
		if(in_array($assigned,array('all','css'))){
			for($i=0,$n=count(self::$link_files);$i<$n;$i++){
				if(self::$link_files[$i]['type']!='css'){
					continue;
				}
				else if(self::$link_files[$i]['outlink']){
					echo '<link rel="stylesheet" href="'.self::$link_files[$i]['fpath'].'.css" type="text/css" charset="utf-8"'.(self::$link_files[$i]['attach']?' '.self::$link_files[$i]['attach']:'').' />';
					self::$link_files[$i]['outputed']=1;
				}
				else{
					echo '<link rel="stylesheet" href="'.self::$link_files[$i]['fpath'].'.css" type="text/css" charset="utf-8"'.(self::$link_files[$i]['attach']?' '.self::$link_files[$i]['attach']:'').' />';
					self::$link_files[$i]['outputed']=1;
				}
			}
		}
		if(in_array($assigned,array('all','js'))){
			for($i=0,$n=count(self::$link_files);$i<$n;$i++){
				if(self::$link_files[$i]['type']!='js'){
					continue;
				}
				else{
					echo '<script src="'.self::$link_files[$i]['fpath'].'.js" type="text/javascript"></script>';
					self::$link_files[$i]['outputed']=1;
				}
			}
		}
	}

	/**
	 * image_url 
	 * 根據 imageUid 回傳圖檔路徑
	 * 
	 * @param uid4 $image_uid 
	 * @param string $type 
	 * @param string $subname 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function image_url($image_uid,$type='s',$subname='.png'){
		return '/images/'.substr($image_uid,0,2).'/'.substr($image_uid,-3).'/'.$image_uid.'_'.$type.$subname;
	}

	/**
	 * ajax_json_output 
	 * 輸出 json for ajax
	 * 
	 * @param array $data 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function ajax_json_output($data){
		echo 'while(1){/* ';
		if(is_array($data)){
			echo json_encode($data);
		}
		else{
			echo $data;
		}
		echo ' */};';
		die('');
	}

	static public function error_die($string_label){
		if(!isset($GLOBALS['string']['Ly_unable_load_controller'])){


		}
	}

	/**
	 * page 
	 * 載入訊息頁面，完整的頁面
	 * 
	 * @param mixed $page_id 
	 * @param mixed $args 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function page($page_id,$param=NULL){
		switch($page_id){
			case '301':
				header('HTTP/1.1 301 Moved Permanently', TRUE, 301);
				if(isset($param['url'])){
					header('Location:'.$param['url']);
					die('');
				}
				else if(is_string($param)){
					header('Location:'.$param);
					die('');
				}
				break;
			case '302':
				header('HTTP/1.1 302 Moved Permanently', TRUE, 302);
				if(isset($param['url'])){
					header('Location:'.$param['url']);
					die('');
				}
				else if(is_string($param)){
					header('Location:'.$param);
					die('');
				}
				break;
			case '403':
				header('HTTP/1.1 403 Forbidden', TRUE, 403);
				break;
			case '404':
				header('HTTP/1.0 404 Not Found', TRUE, 404);
				break;
		}
		include(_DIR_DOCS.'www/layout/'.self::$lang.'/page/'.$page_id.'.phtml');
		ME::halt();
	}

	/**
	 * grid_setter 
	 * 設定 grid 是否為可用，通常是 controller 處理
	 * 
	 * @param mixed $grid_name 
	 * @param mixed $able 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function grid_setter($grid_name,$able){
		self::$grids[$grid_name]=$able;
	}

	/**
	 * grid 
	 * layout 排版，在 layout 裡定義各個區塊要使用的 controller 及區塊名稱，然後由 controller->grid($args) 處理
	 * controller 裡要定義可用的 grid，Command 可以停用 grid
	 * 使用方式
	 *	<div class="co12-12 hidden-xs"><?php CZ::grid('index','user_info',array('user_uid'=>$user_uid)); ?></div>
	 * 
	 * @param mixed $controller_name 
	 * @param mixed $grid_name 
	 * @param mixed $opt 
	 * @param mixed $param 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function grid($controller_name, $grid_name, $args=NULL){
		if(!empty(self::$grids[$grid_name])){
			if(!isset(self::$controller_loads[$controller_name])){
				include(_DIR_DOCS.'www/controller/'.$controller_name.'.controller.php');
				$controller=ucfirst($controller_name).'_Controller';
				self::$controller_loads[$controller_name]=new $controller;
			}
			self::$controller_loads[$controller_name]->grider($grid_name,$args);
		}
	}

	static public function grid_exists($grid_name){
		if(!empty(self::$grids[$grid_name])){
			return FALSE;
		}
		return TRUE;
	}

	static public function widget($controller_name,$command,$name=NULL){
		if(is_null($name)){
			$name=$controller_name;
		}
		if(!isset(self::$controller_loads[$name])){

		}	
	}

	static public function controller(){
		return self::$req_controller;
	}

	static public function command_setter($command_name=NULL){
		if($command_name===NULL){
			self::$req_command='';
		}
		else{
			self::$req_command=$command_name;
		}
	}

	static public function command(){
		return self::$req_command;
	}

	static public function controller_do($controller_name,$command_name){
		if(!isset(self::$controller_loads[$controller_name])){
			if(in_array($controller_name,array('setup','cron'))){
				if(@!filemtime(_DIR_FRAMEWORK.'controller/'.$controller_name.'.controller.php')){
					refreshURL('/?a=error_path&rf='.$controller_name);
					//die('Cannot access controller '.$controller_name);
				}
				include(_DIR_FRAMEWORK.'controller/'.$controller_name.'.controller.php');
				$controller=ucfirst($controller_name).'_Controller';
				if(!class_exists($controller)){
					die('Unable to load controller class '.$controller.' of CoreBase.');
				}
				self::$controller_loads[$controller_name]=new $controller;
			}
			else{
				if(@!filemtime(_DIR_DOCS.'www/controller/'.$controller_name.'.controller.php')){
					refreshURL('/?a=error_path&rf='.$controller_name);
					//die('Cannot access controller '.$controller_name);
				}
				include(_DIR_DOCS.'www/controller/'.$controller_name.'.controller.php');
				$controller=ucfirst($controller_name).'_Controller';
				if(!class_exists($controller)){
					die('Unable to load controller class '.$controller);
				}
				self::$controller_loads[$controller_name]=new $controller;
			}
		}
		if(isset(self::$controller_loads[$controller_name]->request_customize) && !empty(self::$controller_loads[$controller_name]->request_customize)){
			if(strlen(self::$controller_loads[$controller_name]->request_customize) && method_exists(self::$controller_loads[$controller_name], 'Request_'.self::$controller_loads[$controller_name]->request_customize)){
				$run_continue=self::$controller_loads[$controller_name]->{'Request_'.self::$controller_loads[$controller_name]->request_customize}()	;
			}
			else{
				self::$controller_loads[$controller_name]->Requst_customize();
			}
		}
		else{
			if(!empty(self::$controller_loads[$controller_name]->command_alias)){
				if(isset(self::$controller_loads[$controller_name]->command_alias[$command_name])){
					$command_name=self::$controller_loads[$controller_name]->command_alias[$command_name];
				}
			}
			$command='Command_'.ucfirst($command_name);
			if(!method_exists(self::$controller_loads[$controller_name],$command)){
				refreshURL('/?a=error_query&rf='.$command.'%20of%20'.$controller);
				//die('無法載入功能: '.$command.' of '.$controller);
			}
			self::$controller_loads[$controller_name]->$command();
		}
	}


	/**
	 * timezone_shift 
	 * 調整為 CZ 的時區時間
	 * 
	 * @param mixed $datetime local的時間
	 * @param int $from_timezone local的時區(分鐘數)
	 * @static
	 * @access public
	 * @return void
	 */
	static public function timezone_shift($datetime, $from_timezone=NULL){
		if(!is_numeric($datetime)){
			$datetime=strtotime($datetime);
		}
		if(is_null($from_timezone)){
		}
		else{
			$datetime -= ($from_timezone*60);
		}
		return date('Y-m-d H:i:s',$datetime);
	}


	/**
	 * response_data
	 * ajax 回應陣列
	 */
	static public function response($id='default'){
		if(!isset(self::$obj_response[$id])){
			self::$obj_response[$id]=new RESPONSE_ELEM();
		}
		return self::$obj_response[$id];
	}


	/**
	 * filesystem 
	 * 檔案儲存處理
	 * 
	 * @param string $id 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function filesystem($id='default'){
		

	}
} 

class RESPONSE_ELEM {
	private $error=0;
	private $error_mesg='';
	private $content;
	private $reload=0;
	private $redirect='';

	private $content_type='html';

	public function __construct(){
		$this->elem=array(
			'error'=>0,
			'mesg'=>'',
			'content'=>'',
			'reload'=>0,
			'message'=>'',
			'redirect'=>''
		);
	}

	public function error($errno,$mesg='',$replaces=NULL){
		$this->elem['error']=$errno;
		if(!is_null($replaces) && is_array($replaces)){
			$this->elem['mesg']=_tl($mesg, $replaces, FALSE);
		}
		else{
			$this->elem['mesg']=$mesg;
		}
		return $this;
	}

	public function content($content){
		$this->elem['content']=$content;
		return $this;
	}

	public function redirect($redirect){
		$this->elem['reload']=0;
		$this->elem['redirect']=$redirect;
		return $this;
	}

	public function reload($reload_second){
		$this->elem['reload']=$reload_second;
		return $this;
	}

	public function alert_then_redirect($delay_second, $alert_message, $redirect=NULL){
		$this->elem['reload']=$delay_second;
		$this->elem['message']=$alert_message;
		if(!is_null($redirect)){
			$this->elem['redirect']=$redirect;
		}
		return $this;
	}

	/**
	 * 輸出
	 *
	 */
	public function output($add_while=TRUE){
		if(!$add_while){
			echo json_encode($this->elem);
		}
		else{
			echo 'while(1);/*'.json_encode($this->elem,JSON_UNESCAPED_UNICODE).'*/';
		}
		if(!_NO_SESSION){
			ME::halt();
		}
		else{
			die('');
		}
	}

}

class FILESYSTEM_ELEM {

	public function  __construct(){


	}


}

CZ::init();
define('_SYS_TIMESTAMP',CZ::model('core')->sys_timestamp());// 調整為 UTC 時區的 UNIX Timestamp
define('_SYS_DATETIME',xdate('datetime',_SYS_TIMESTAMP));
define('_SYS_DATE',xdate('date',_SYS_TIMESTAMP));

