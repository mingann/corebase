<?php

	namespace WSS;

	include('autoload.php');

	class WebSocketServer extends DataProcess implements IWebSocketServer, WbConfig{
		
		static $CONF_SRC_KEY='g#lP7Ls51';
		
		/**
		 *  Construct
		 *
		 *  @access Public
		 *  @param
		 *  @return
		 *
		 */
		public function __construct($data='') {
			
			// Server Config
			$this->server = self::SERVER;  // where is the websocket server
			$this->portocol = self::PORTOCOL; // for security
			$this->port = self::PORT;
			$this->pemfile = self::PEMFILE;
			$this->error =0;
			
			
			if($data['server']){
				$this->server = $data['server'];  // where is the websocket server
				$this->portocol = $data['portocol'];  // where is the websocket server
			}
			
			
			// Define
			
			
			$this->UID=[];
			$this->SSID=[];
			$this->clients = [];
			$this->clientsData = array();
			$this->socketData = array();
			$this->handshakes = [];
			$this->headersUpgrade = [];
			$this->currentConn = null;
			
			// Start Server
			$this->createServer();
		}		
		
		/**
		 *  Create WSS Server 
		 *
		 *  @access Private
		 *  @param null
		 *  @return null
		 *
		 */
		private function createServer () {
		
			$errno = null;
			$errorMessage = '';
			$context = stream_context_create();

			// local_cert must be in PEM format
			stream_context_set_option($context, 'ssl', 'local_cert', $this->pemfile);
			stream_context_set_option($context, 'ssl', 'allow_self_signed', true);
			stream_context_set_option($context, 'ssl', 'verify_peer', false);	
		
			$server = stream_socket_server(
					$this->portocol . $this->server . ":" . $this->port,
					$errno, 
					$errorMessage,
					STREAM_SERVER_BIND|STREAM_SERVER_LISTEN,
					$context
			);
			
			if ($server === false) {
				die('Could not bind to socket: ' . $errno . ' - ' . $errorMessage . PHP_EOL);
			} else {	
				echo "Server Started : " . date('Y-m-d H:i:s') . "\n";
				echo "Master socket  : " . $context . "\n";
				echo "Listening on   : " . $this->portocol . $this->server . ":" . $this->port . "\n\n";
				$this->eventListener($server);
			}
		}
		
		/**
		 *  Loop for listen event 
		 *
		 *  @access Private
		 *  @param String
		 *  @return null
		 *
		 */
		private function eventListener($server) {

			while(true) {
				//prepare readable sockets
				$readSocks = $this->clients;
				$readSocks[] = $server;

				//start reading and use a large timeout
				if (!stream_select($readSocks, $write, $except, self::STREAM_SELECT_TIMEOUT,5000)) {
					die('something went wrong while selecting');
				}
				
				
				//new client
				
				if (in_array($server, $readSocks)) {
					$newClient = stream_socket_accept($server); // must be 0 to non-block          
					
					if($newClient === false)
					{
						
						print_r($server);
						print_r($readSocks);
						print_r("\n===========die  die die error read header:([".$this->error."])\n");
						$this->error++;
						if($this->error >= 50){
							break;
						}
						
						//continue;
					}
					else{
						// print remote client information, ip and port number
						$socketName = stream_socket_get_name($newClient, true);
						echo "New Client = " . $socketName . "\n";
						
						// important to read from headers here coz later client will change and there will be only msgs on pipe
						echo "********check point read beforce*******\n";
						echo "\n********Error:([".$this->error."])*******\n";
						print_r($newClient);
						stream_set_timeout($newClient, 3);
						$headers = @fread($newClient, self::MAX_BYTES_READ);
						echo "********check point read after*******\n";

						if ($newClient) {
							echo "********check point*******\n";
							$hKey = $this->handShake($newClient, $headers);
							if(!empty($hKey)) {
								$this->clients[] = $newClient;
								$head_ssid=$this->headerSSID($newClient, $headers);
								
								echo "ssid = " . $head_ssid . "\n";
							}
							echo "Handshake key = " . $hKey . "\n";
						}
					}
					//delete the server socket from the read sockets
					unset($readSocks[array_search($server, $readSocks)]);
				}
				
				
				
				/*
				if (in_array($server, $readSocks)) {
					$newClient = stream_socket_accept($server, 0); // must be 0 to non-block          
					if ($newClient) {
						// print remote client information, ip and port number
						$socketName = stream_socket_get_name($newClient, true);
						echo "New Client = " . $socketName . "\n";
						
						// important to read from headers here coz later client will change and there will be only msgs on pipe
						$headers = @fread($newClient, self::HEADER_BYTES_READ);
						
						if ($newClient) {
							echo "********check point*******\n";
							$hKey = $this->handShake($newClient, $headers);
							if(!empty($hKey)) {
								$this->clients[] = $newClient;
								$head_ssid=$this->headerSSID($newClient, $headers);
								
								echo "ssid = " . $head_ssid . "\n";
							}
							echo "Handshake key = " . $hKey . "\n";
						}
					}
					//delete the server socket from the read sockets
					unset($readSocks[array_search($server, $readSocks)]);
				}
				*/
				
				
				
				
				
				
				 //message from existing client
				foreach ($readSocks as $kSock => $sock) {
					$data = $this->decode(fread($sock, self::MAX_BYTES_READ));
					if ($data) {
						$dataType = $data['type'];
						$dataPayload = $data['payload'];
						$this->currentConn = $sock;
						
						if (empty($data) || $dataType === self::EVENT_TYPE_CLOSE) { // close event triggered from client - browser tab or close socket event
							// trigger CLOSE event
							try {
								echo "Close: " . $this->currentConn . "\n";
								$this->close($this->currentConn);
							} catch (Exception $e) {
								echo "Exception: " . $e . "\n";
							}
							$clientIndex = array_search($sock, $this->clients);
							unset($this->clients[$clientIndex]);
							unset($this->clientsData[$clientIndex]);
							unset($readSocks[$kSock]); // to avoid event leaks
							
							$this->removeData($clientIndex);
							continue;
						}
						
						if ($dataType === self::EVENT_TYPE_TEXT) {
							// trigger MESSAGE event
							try {
								echo "Trigger MESSAGE event \n";
								$this->onMessage($this->currentConn, $dataPayload);
							} catch (Exception $e) {
								echo "Exception: " . $e . "\n";
							}
						}
					}
				}
			}
		}
		
		private function headerSSID($client, $headers) {
			

			$conn_id=array_search($client, $this->clients);
			preg_match("/Cookie:\s(.*)\n/", $headers, $match);
			if (!empty($match[1])) {
				$cookie=explode(";",$match[1]);
				if($cookie){
					foreach($cookie as $cookie_str){
						list($tkey,$tval)=explode("=",$cookie_str);
						if(trim($tkey)=="SSID"){
							$ssid=trim($tval);
						}
					}
				}
				if(empty($this->SSID[$conn_id])){
					$this->SSID[$conn_id]=$ssid;
				}
				return $ssid;
			}
		}
		/**
		 *  Get current connection 
		 *
		 *  @access Private
		 *  @param String
		 *  @return String
		 *
		 */
		private function handShake($client, $headers) {
			$match = [];
			$key = empty($this->handshakes[intval($client)]) ? 0 : $this->handshakes[intval($client)];
			preg_match(self::SEC_WEBSOCKET_KEY_PTRN, $headers, $match);
			if (empty($match[1])) {
				return false;
			}

			$key = $match[1];
			$this->handshakes[intval($client)] = $key;

			// sending header according to WebSocket Protocol
			$secWebSocketAccept = base64_encode(sha1(trim($key) . self::HEADER_WEBSOCKET_ACCEPT_HASH, true));
			$this->setHeadersUpgrade($secWebSocketAccept);
			$upgradeHeaders = $this->getHeadersUpgrade();

			echo "upgradeHeaders = " . $upgradeHeaders ."\n";
			fwrite($client, $upgradeHeaders);
			return $key;
		}
		
		/**
		 *  Set header information 
		 *
		 *  @access Private
		 *  @param String
		 *  @return null
		 *
		 */
		private function setHeadersUpgrade($secWebSocketAccept) {
			$this->headersUpgrade = [
				self::HEADERS_UPGRADE_KEY => self::HEADERS_UPGRADE_VALUE,
				self::HEADERS_CONNECTION_KEY => self::HEADERS_CONNECTION_VALUE,
				self::HEADERS_SEC_WEBSOCKET_ACCEPT_KEY => ' '. $secWebSocketAccept // the space before key is really important
			];
		}
		
		/**
		 *  Get header information 
		 *
		 *  @access Private
		 *  @param String
		 *  @return String
		 *
		 */
		private function getHeadersUpgrade() {
			$handShakeHeaders = self::HEADER_HTTP1_1 . self::HEADERS_EOL;
			if (empty($this->headersUpgrade)) {
				die('Headers array is not set' . PHP_EOL);
			}
			foreach ($this->headersUpgrade as $key => $header) {
				$handShakeHeaders .= $key . ':' . $header . self::HEADERS_EOL;
				if ($key === self::HEADERS_SEC_WEBSOCKET_ACCEPT_KEY) { // add additional EOL fo Sec-WebSocket-Accept
					$handShakeHeaders .= self::HEADERS_EOL;
				}
			}
			return $handShakeHeaders;
		}
		
		/**
		 *  Close connection
		 *
		 *  @access Private
		 *  @param String
		 *  @return null
		 *
		 */
		private function close($sockConn) {
			if (is_resource($sockConn)) {
				fclose($sockConn);
			}
		}
		
		/**
		 *  Handle data
		 *
		 *  @access Private
		 *  @param String, String
		 *  @return null
		 *
		 */
		private function onMessage ($sock, $dataPayload) {
			
			$data=json_decode($dataPayload,true);
			//$this->send($sock, json_encode(array(0)));
			
			
			echo "\n===========================\n";
			echo "From (Client) Sock:\n";
			print_r($sock);
			echo "\n===========================\n";
			print_r($data);
			echo "\n===========================\n";

			$action="Action_".$data['action'];
			unset($data['action']);

			$conn_id=array_search($sock, $this->clients);

			
			if(method_exists($this,$action)){

				$this->$action($conn_id,$data);
			}else{
				echo "\n==== Caution : Action handler '{$action}' not found! =====\n";
			}
			

		}
		
		/**
		 *  Send data
		 *
		 *  @access Private
		 *  @param String, String
		 *  @return boolean
		 *
		 */	
		private function send($client, $data) {
			
			if(!empty($data)){
				$len = fwrite($client, $this->encode($data));
				echo "Len = $len\n";
			}
			
			
			if ($len !== 0) {
				return true;
			} else {
				return false;
			}
		}
		
		private function sendData($conn_id,$action, $data) {
			$data['action']=$action;
			$client=$this->clients[$conn_id];
			echo "\n===========================\n";
			echo "To (Client) Sock:";
			print_r($client);
			echo "\n===========================\n";
			print_r($data);
			echo "\n===========================\n";
		
			$this->send($client,json_encode($data));
			
		}
		
		private function removeData($conn_id) {
			$allconn=$this->socketData;
			
			
			$uid=$this->UID[$conn_id];
			$ssid=$this->SSID[$conn_id];
			$hid=$this->hash_key($uid.'_#_'.$ssid);
			
			echo "\nDDDDDDDDDDDDDDDDDDDDD\n";
										
			print_r($this->socketData);
			print_r($this->UID);
			print_r($this->SSID);
			echo "\nDelete conn_id:{$conn_id}=====".$this->socketData[$uid]['group'][$hid][$conn_id]."\n";
			unset($this->socketData[$uid]['group'][$hid][$conn_id]);
			unset($this->UID[$conn_id]);
			unset($this->SSID[$conn_id]);
			
			
			if(count($this->socketData[$uid]['group'][$hid])<=0){
				unset($this->socketData[$uid]['group'][$hid]);
			}
			if(count($this->socketData[$uid]['group'])<=0){
				unset($this->socketData[$uid]);
			}
			
			echo "\n================\n";
			print_r($this->socketData);
			echo "\n================\n";
			print_r($this->UID);
			print_r($this->SSID);
			
			
			echo "\nDDDDDDDDDDDDDDDDDDDDD\n";
			
			
			
			
			/*
			if($allconn){
				foreach($allconn as $uid => $user){
					if($user['group']){
						foreach($user['group'] as $hid => $hid_data){
							if($hid_data){
								foreach($hid_data as $hid_conn_id ){
									if($hid_conn_id==$conn_id){
										
									}
								}
							}
						}	
					}
				}
			}*/
			
			
			
			//$allconn=$this->socketData[$uid]['group'][$hid];
		}	
		
		
		
		
	public function get_conn($uid,$hid){
		$allconn=$this->socketData[$uid]['group'][$hid];
		$is_work=0;
		if(!empty($allconn)){
			$is_work=1;
			$groupconn=$this->socketData[$uid]['group'];
			
		}
		return ($is_work==1) ? $groupconn : false;
	}
	
	///// key ////
	public function hash_key($str){
		$str1=hash("md5",$str.self::$CONF_SRC_KEY);
		return $str1;
	}
	
	
	static public function _vaildate($user_uid,$ssid){
		
		$lastDate=date("Y-m-d",strtotime("-3 days"));

		$query=array(
			"select" => "*",
			"from" => "sess",
			"where" => array("userUid=:user_uid AND sessId=:ssid AND sessTimeUpdate>=:lastDate"),
			"bind" => array(
				":user_uid" => $user_uid,
				":ssid" => $ssid,
				":lastDate" => $lastDate,
			),
		);
		
		$data=\DB::data('session',$query);
		if($data['args']['num']>0){
			return $data;
		}
		
		return false;
	}

	///// ACTIONS ////
	private function Action_register($conn_id,$data){
		
		$guid='';
		if(!empty($data)){
			
			$ssid=$this->SSID[$conn_id];
			$validate=self::_vaildate($data['uid'],$ssid);
			
			if(!empty($validate)){
				
				$this->UID[$conn_id]=$data['uid'];
				
				/*
				if(empty($data['rid'])){$data['rid']=$data['uid'];}
				
				$tmp=explode(';',$data['rid']);
				foreach($tmp as $tmp_val){
					$tmp1[$tmp_val]=$tmp_val;
				}
				ksort($tmp1);
				$ustr=count($tmp1)>1 ? implode(';',$tmp1) : $data['rid'];
				*/
				$uid=$data['uid'];
				$hid=$this->hash_key($uid.'_#_'.$ssid);


				
				$this->socketData[$uid]['group'][$hid][$conn_id]=$conn_id;
				$data['hid']=$hid;
				
				$this->sendData($conn_id,'registred',$data);
			}

		}
	}
	
	public function Action_notification_realtime_get($conn_id,$data){
			
			if(!empty($data['uid'])  && !empty($data['hid'])){
				$conn=$this->get_conn($data['uid'],$data['hid']);
				
				if(!empty($conn)){
					$defaultPhoto_m='/css/img/default_profile_pic_m.png';
					$defaultPhoto_s='/css/img/default_profile_pic_s.png';
					
					$user_uid=$data['uid'];
					$notifications=\CZ::model('notifications')->unread_getter($user_uid);

					$counter=$notifications['args']['cnt'];
					if($notifications['args']['num']>0){
						foreach($notifications['d'] as $inx => $notifications_data){
							$msg_info=json_decode($notifications_data['unotificationData'],true);
							$xuser=\CZ::model('users')->xuser($notifications_data['userUidFrom'],0,0,0,0); 
							if( !empty($xuser['basic']['userProfile']) ){
									$notifications['d'][$inx]['userFromImgUrl']=\CZ::model('images')->imagePath_getter($xuser['basic']['userProfile'],'_s') ;
							}
							else{
								$notifications['d'][$inx]['userFromImgUrl']=$defaultPhoto_s;
							}
							if($notifications_data['userUidFrom']=='countmin-system'){
								$notifications['d'][$inx]['userFromImgUrl']="/css/img/system_notification_icon.png";
							}
							
							if(!empty($msg_info)){
								foreach($msg_info as $key => $text){
									$msg_info[$key]=html($text,false);
								}
							}
							$msg_info['_output']=0;
							
							$notifications['d'][$inx]['msg']=_tl($notifications_data['noticeId'],$msg_info,'text');
						}
					}
					$unread_message_counter=\CZ::model('chatrooms')->unread_getter($user_uid);
					$unread_xuser=\CZ::model('users')->xuser($user_uid,1,0,0,0);
					
					
					
					$resp=array(
						'realtimes'=>$notifications,
						'counter'=>$counter,
						'user' =>$unread_xuser,
						'chat_unread'=>$unread_message_counter,
						'error'=>0,
					);

					foreach($conn as $conn_group => $conn_group_data){
						foreach($conn_group_data as $conn_id ){
							$this->sendData($conn_id,'notification_realtime_get',$resp);
						}
					}
				}
			}
	}
	
		
		
		
		
		
		
		
		
		
		
		
		
	}
?>
