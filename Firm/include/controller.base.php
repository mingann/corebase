<?php

class Controller {

	/**
	 * name 
	 * controller name
	 * 
	 * @var string
	 * @access protected
	 */
	protected $name='';

	/**
	 * layout 
	 * default layout, 存在 www/template/layout/
	 * 
	 * @var string
	 * @access protected
	 */
	protected $layout='index';

	protected $view='';

	protected $layout_grids=array();

	protected $body_class='';

	/*
	 * controller 是否有自定義的 querystring parser，若不為 0 則使用 Request_customize();
	 */
	public $request_customize=0;

	/*
	 * 從 querystring($_GET[e]) 取得的 command_name 別名對照到真正的 Command_name;
	 * 通常為了縮短網址用
	 */
	public $command_alias=array(); 

	protected function global_assign($var_index, $vars){
		CZ::$globals[$var_index]=$vars;
	}

	protected function layout_setter($layout){
		$this->layout=$layout;
	}

	protected function layout_getter(){
		return $this->layout;
	}

	protected function layout_path($controller,$command,$command_extension=''){
		if(!$command_extension){
			$command_extension='.phtml';
		}
		else{
			$command_extension='.'.$command_extension.'.phtml';
		}
		$this->view=_DIR_DOCS.'www/template/'.$controller.'/'.$command.$command_extension;
		if(!strcmp($this->layout,'empty')){
			return _DIR_DOCS.'www/layout/empty.phtml';
		}
		else{
			return _DIR_DOCS.'www/layout/'.CZ::$lang.'/'.$this->layout.'.phtml';
		}
	}

	protected function grid_path($controller,$grid){
		return _DIR_DOCS.'www/template/'.$controller.'/'.$grid.'.grid.phtml';
	}

	public function grider($grid_name,$args){
		if(isset($this->layout_grids[$grid_name])){
			$this->{'Grid_'.$this->layout_grids[$grid_name]['grid']}($args);
		}
	}

}

