<?php
function uuid4() {
	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
		// 32 bits for $query['where']=$this->cache_where;time_low"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),

		// 16 bits for "time_mid"
		mt_rand(0, 0xffff),

		// 16 bits for "time_hi_and_version",
		// four most significant bits holds version number 4
		mt_rand(0, 0x0fff) | 0x4000,

		// 16 bits, 8 bits for "clk_seq_hi_res",
		// 8 bits for "clk_seq_low",
		// two most significant bits holds zero and one for variant DCE1.1
		mt_rand(0, 0x3fff) | 0x8000,

		// 48 bits for "node"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	);
}
function uid4($prefix=NULL) {
	$uuid=base_convert(sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',
		// 32 bits for "time_low"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff),

		// 16 bits for "time_mid"
		mt_rand(0, 0xffff),

		// 16 bits for "time_hi_and_version",
		// four most significant bits holds version number 4
		mt_rand(0, 0x0fff) | 0x4000,

		// 16 bits, 8 bits for "clk_seq_hi_res",
		// 8 bits for "clk_seq_low",
		// two most significant bits holds zero and one for variant DCE1.1
		mt_rand(0, 0x3fff) | 0x8000,

		// 48 bits for "node"
		mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	),16,36);
	if(empty($prefix)){
	}
	else{
		if(strlen($prefix)>2){
			$prefix=substr($prefix,0,2);
		}
		$prefix.='-';
	}
	return $prefix.sprintf('%015s4-%s',substr($uuid,0,15),substr($uuid,15));
}

/**
 * attr 
 * for html dom attribute, ex: <input data-alert="<?php attr($str); ?>" />
 * 
 * @param mixed $str 
 * @param mixed $output 
 * @access public
 * @return void
 */
function attr($str,$output=TRUE){
	if(!$output){
		return strtr(htmlspecialchars($str),array("\r"=>'',"\n"=>'','\''=>'&#39;','('=>'&#40;'));
	}
	echo strtr(htmlspecialchars($str),array("\r"=>'',"\n"=>'','\''=>'&#39;','('=>'&#40;'));
}
/**
function toAttrJs($str,$output=TRUE){
	if(!$output){
		return str_replace('\'','\\\'',strtr(htmlspecialchars($str),array("\r\n"=>'\\r\\n',"\r"=>'\\r',"\n"=>'\\n','\\'=>'\\\\')));
	}
	echo str_replace('\'','\\\'',strtr(htmlspecialchars($str),array("\r\n"=>'\\r\\n',"\r"=>'\\r',"\n"=>'\\n','\\'=>'\\\\')));
}
 **/
function js($str,$output=TRUE){
	if(!$output){
		return str_replace('</','<"+"/',strtr(addslashes($str),array("\r\n"=>'\\r\\n',"\r"=>'\\r',"\n"=>'\\n')));
	}
	echo str_replace('</','<"+"/',strtr(addslashes($str),array("\r\n"=>'\\r\\n',"\r"=>'\\r',"\n"=>'\\n')));

}
/**
 * jsw 
 * for javascript document.write("<?php jsw($str); ?>");
 * 
 * @param mixed $str 
 * @param mixed $output 
 * @access public
 * @return void
 */
function jsw($str,$output=TRUE){
	if(!$output){
		return preg_replace('/[\r\n]+/','',nl2br(str_replace("\r",'',addslashes(htmlspecialchars($str)))));
	}
	echo preg_replace('/[\r\n]+/','',nl2br(str_replace("\r",'',addslashes(htmlspecialchars($str)))));
}
/**
 * html 
 * for output html view, ex: <div><?php html($str); ?></div>
 * 
 * @param mixed $str 
 * @param mixed $output 
 * @access public
 * @return void
 */
function html($str,$output=TRUE){
	if(!$output){
		return nl2br(htmlspecialchars($str,ENT_QUOTES));
	}
	echo nl2br(htmlspecialchars($str,ENT_QUOTES));
}

function num($num, $output=TRUE, $decimals=0, $dec_point='.',$thousands_sep=','){
	if(is_string($num)){
		$num=trim($num);
	}
	if(!$output){
		if(!is_numeric($num)) return '';
		return number_format($num,$decimals,$dec_point,$thousands_sep);
	}
	if(!is_numeric($num)){
	   	echo '';
	}
	else{
		echo number_format($num,$decimals,$dec_point,$thousands_sep);
	}
}
/**
 * 當傳入的 $num 為字串且不為數字格式時，使用 value/html/textarea 輸出
 */
function num_value($num, $output=TRUE, $decimals=0, $dec_point='.',$thousands_sep=','){
	if(is_string($num)){
		$num=trim($num);
	}
	if(!$output){
		if(!is_numeric($num)) return value($num,FALSE);
		return number_format($num,$decimals,$dec_point,$thousands_sep);
	}
	if(!is_numeric($num)){
	   	value($num);
	}
	else{
		echo number_format($num,$decimals,$dec_point,$thousands_sep);
	}
}
function num_html($num, $output=TRUE, $decimals=0, $dec_point='.',$thousands_sep=','){
	if(is_string($num)){
		$num=trim($num);
	}
	if(!$output){
		if(!is_numeric($num)) return html($num,FALSE);
		return number_format($num,$decimals,$dec_point,$thousands_sep);
	}
	if(!is_numeric($num)){
		html($num);
	}
	else{
		echo number_format($num,$decimals,$dec_point,$thousands_sep);
	}
}
function num_textarea($num, $output=TRUE, $decimals=0, $dec_point='.',$thousands_sep=','){
	if(is_string($num)){
		$num=trim($num);
	}
	if(!$output){
		if(!is_numeric($num)) return textarea($num,FALSE);
		return number_format($num,$decimals,$dec_point,$thousands_sep);
	}
	if(!is_numeric($num)){
	   	textarea($num);
	}
	else{
		echo number_format($num,$decimals,$dec_point,$thousands_sep);
	}
}
/**
 * anum
 * 若 $num 為整數，則不顯示小數點，若為浮點數，則依 $decimals 顯示小數點後位數
 */
function anum($num, $output, $decimals=0, $dec_point='.',$thousands_sep=','){
	if(is_string($num)){
		$num=trim($num);
	}
	if(!is_numeric($num)){
		$num='';
	}
	else{
		if(strpos($num, '.')===FALSE){
			$num=number_format($num, 0, $dec_point, $thousands_sep);
		}
		else{
			$num=number_format($num, $decimals, $dec_point, $thousands_sep);
		}
	}
	if(!$output){
		return $num;
	}
	echo $num;
}
function numeric($num, $output=TRUE){
	$num=intval($num,10);
	if($num>100000000000){
		$num=round($num/1000000000).'G';
	}
	else if($num>10000000000){
		$num=(round($num/100000000)/10).'G';
	}
	else if($num>10000000){
		$num=round($num/1000000).'M';
	}
	else if($num>1000000){
		$num=(round($num/1000000)/10).'M';
	}
	else if($num>10000){
		$num=round($num/1000).'K';
	}
	else{
		$num=number_format($num);
	}
	if($output){
		echo $num;
	}
	else {
		return $num;
	}

}
function textarea($str,$output=TRUE){
	if(!$output){
		return htmlspecialchars($str,ENT_QUOTES);
	}
	echo htmlspecialchars($str,ENT_QUOTES);
}
function value($str,$output=TRUE){
	if(!$output){
		return strtr(nl2br(htmlspecialchars($str,ENT_QUOTES)),array("\r"=>'',"\n"=>''));
	}
	echo strtr(nl2br(htmlspecialchars($str,ENT_QUOTES)),array("\r"=>'',"\n"=>''));
}
function v($str){
	return strtr(nl2br(htmlspecialchars($str,ENT_QUOTES)),array("\r"=>'',"\n"=>''));
}

/**
 * date_age 
 * 顯示日期與年齡
 * 
 * @param mixed $date Y-m-d
 * @param mixed $output 
 * @access public
 * @return void
 */
function date_age($date, $output=TRUE){
	
}

/**
 * editor 
 * for richtext editor's <textarea> content
 * 
 * @param mixed $str 
 * @param mixed $output 
 * @access public
 * @return void
 */
function editor($str,$output=TRUE){
	if(!$output){
		return htmlspecialchars($str,ENT_QUOTES);
	}
	echo htmlspecialchars($str,ENT_QUOTES);
}


/**
 * HTML的補強函式
 * $replaces為要替代的字詞
 */
function _html($str,$replaces=NULL,$output=TRUE){
	if(!is_null($replaces)){
		foreach($replaces as $id => $val){
			$str=str_replace('[#'.$id.']',$val,$str);
		}
	}
	if($output){
		echo nl2br(htmlspecialchars($str,ENT_QUOTES));
	}
	else{
		return nl2br(htmlspecialchars($str,ENT_QUOTES));
	}
}

function _breadcrumb($breadcrumb=NULL){
	if(isset($breadcrumb)){
		echo '<div class="breadcrumb">';
		echo '<ul class="nostyle">';
		echo '<li class="root"><span><a href="/">'._tl('Ly_home',FALSE).'</a></span></li>';
		for($i=0,$n=count($breadcrumb);$i<$n;$i++){
			if($i==($n-1)){
				$style='end';
			}
			else{
				$style='path';
			}
			if(isset($breadcrumb[$i]['link'])){
				echo '<li class="'.$style.'"><span><a href="'.$breadcrumb[$i]['link'].'">'.html($breadcrumb[$i]['text'],FALSE).'</a></span></li>';
			}
			else{
				echo '<li class="'.$style.'"><span>'.html($breadcrumb[$i]['text'],FALSE).'</span></li>';
			}
		}
		echo '</ul>';
		echo '</div>';
	}
}

/**
 * 給mysql schema where like使用的
 *
 * $prelike和$sublike可以是空白、%、_
 * 預設是%keyword%
 *
 * @param string $row_name 
 * @param string $keyword 
 * @param string $prelike 
 * @param string $sublike 
 * @param boolean $blurred keyword是否視為一個單一字串，或是以空白分隔為多個搜尋關鍵字
 * @access public
 * @return void
 */
function like($row_name,$keyword,$prelike='%', $sublike='%', $blurred=FALSE){
	if($prelike!=='' && $prelike!=='_') $prelike='%';
	if($sublike!=='%' && $sublike!=='_') $sublike='%';
	if(!$prelike && !$sublike){
		return $row_name.'="'.mysql_real_escape_string($keyword).'"';
	}
	else{
		if($blurred){
			$keyword=preg_replace('/ +/',' ',$keyword);
			if(!strcmp($keyword,' ')){
				return $row_name.'="'.$prelike.' '.$sublike.'"';
			}
			$keyword=explode(' ',strtr($keyword,array('%'=>'\%','_'=>'\_')));
			$where=array();
			for($i=0,$n=count($keyword);$i<$n;$i++){
				$where[]=$row_name.' LIKE "'.$prelike.mysql_real_escape_string(trim($keyword[$i])).$sublike.'"';
			}
			if($n==1){
				return $where[0];
			}
			else if($n>1){
				return '('.join(' OR ',$where).')';
			}
		}
		else{
			$keyword=strtr(addslashes($keyword),array('%'=>'\%','_'=>'\_'));
			return $row_name.' LIKE "'.$prelike.mysql_real_escape_string($keyword).$sublike.'"';
		}
	}
}

/**
 * form_tl 
 * 將內容套上表單元件
 * 
 * @param mixed $tl 
 * @param mixed $elems 
 * @access public
 * @return void
 */
function form_tl($word, $elems){
	if(!isset($GLOBALS['string'][$word])){
		echo $word;
		return '';
	}
	$word=$GLOBALS['string'][$word];
	foreach($elems as $id => $elem){
		switch($elem['type']){
			case 'textarea':

			case '':

			case 'select':

			case 'radio':


			case 'checkbox':


			default:
		}
	}	
}

/**
 * date_def 
 * 判斷使用者輸入的日期時間字串，若為空值，設定為資料庫預設值
 * 
 * @param mixed $date 
 * @access public
 * @return void
 */
function date_def($date){
	if(!$date){ return '1000-00-00'; }
	else return $date;
}

/**
 * datetime_output 
 * 顯示用的日期時間字串處理
 * 
 * @param mixed $dt 日期[時間]字串，格式為 Y-m-d[ H[:i[:s]]]
 * @param mixed $format 輸出的格式，參考 date()，不過僅限於年月日時分秒，不支援週、天數、時區等
 * @param mixed $default_label 當$dt字串為資料庫datetime預設值或空字串時，要顯示的語系詞彙(標籤)，Ly_*
 * @access public
 * @return void
 */
function datetime_output($dt,$format=NULL,$default_label=NULL){
	if($dt<'1001-01-01' || empty($dt) || strlen($dt)<10){
		$dt='';
	}
	if(!$dt && is_null($default_label)){
		return '';
	}
	else if(!$dt){
		return _tl($default_label,FALSE);
	}
	if(!is_null($format)){
		$dt=explode(' ',$dt);
		str_replace('/','-',$dt[0]);
		str_replace('.','-',$dt[0]);
		$dt[0]=explode('-',$dt[0]);
		if(count($dt[0])==3){
			$y=intval($dt[0][0],10);
			$m=intval($dt[0][1],10);
			$d=intval($dt[0][2],10);
		}
		$h=0;
		$i=0;
		$s=0;
		if(isset($dt[1])){
			$dt[1]=explode(':',$dt[1]);
			if(isset($dt[1][0])){
				$h=intval($dt[1][0],10);
			}
			if(isset($dt[1][1])){
				$i=intval($dt[1][1],10);
			}
			if(isset($dt[1][2])){
				$s=intval($dt[1][2],10);
			}
		}
		if(preg_match_all('/([a-zA-Z])/',$format,$matches)){
			for($i=0,$n=count($matches[1]);$i<$n;$i++){
				$v='';
				switch($matches[1][$i]){
					case 'Y':
						$v=$y;
						break;
					case 'm':
						$v=sprintf('%02d',$m);
						break;
					case 'd':
						$v=sprintf('%02d',$d);
						break;
					case 'H':
						$v=sprintf('%02d',$h);
						break;
					case 'i':
						$v=sprintf('%02d',$i);
						break;
					case 's':
						$v=sprintf('%02d',$s);
						break;
					case 'j':
						$v=$d;
						break;
					case 'n':
						$v=$m;
						break;
					case 'h':
						$v=sprintf('%02d',$h%12);
						break;
					case 'G':
						$v=$h;
						break;
					case 'g':
						$v=$h%12;
						break;
					case 'a':
						$v=$h>=12?'pm':'am';
						break;
					case 'A':
						$v=$h>=12?'PM':'AM';
						break;
					case 'M':
						$v=_tl('Ly_month_short_'.$v,FALSE);
						break;
					case 'y':
						$v=$y%100;
						break;
					case 'F':
						$v=_tl('Ly_month_full_'.$v,FALSE);
						break;
				}
				$format=str_replace($matches[1][$i],$v,$format);
			}
			return $format;
		}
	}
}

/**
 * option_status 
 * 顯示radio/select(單選)被選擇的選項
 * 
 * @param array $options index => array('class'=>'', 'text'=>'')
 * @param mixed $selected
 * @param boolean $output 是否輸出html 
 * @access public
 * @return void
 */
function option_status($options, $selected, $option_id='', $output=TRUE){
	if(isset($options[$selected])){
		if($output){
			echo '<div class="option-status'.($option_id?' '.$option_id:'').'">'.(isset($options[$selected]['class'])?'<span class="'.$options[$selected]['class'].'"></span>':'').'<span class="text">'.htmlspecialchars($options[$selected]['text']).'</span></div>';
		}
		else{
			return $selcted;
		}
	}
	else{
		if($output){
		}
		else{
			return NULL;
		}
	}
}

/**
 * option_array_status 
 * 複數選項
 * 
 * @param array $options 
 * @param array $selected 
 * @param string $option_id 
 * @param mixed $output 
 * @access public
 * @return void
 */
function option_array_status($options, $selected, $option_id='', $output=TRUE){
	if(isset($options[$selected])){
		if($output){
			echo '<div class="option-status'.($option_id?' '.$option_id:'').'">'.(isset($options[$selected]['class'])?'<span class="'.$options[$selected]['class'].'"></span>':'').'<span class="text">'.htmlspecialchars($options[$selected]['text']).'</span></div>';
		}
		else{
			return $selcted;
		}
	}
	else{
		if($output){
		}
		else{
			return NULL;
		}
	}

}

/**
 * option_pow_status
 *
 * @param array $options
 * @param uint $selected
 * @param string $option_id
 * @param boolean $output
 * @return
 */
function option_pow_status($options, $selected, $option_id='', $output=TRUE){


}

/**
 * option_string_stauts
 *
 * @param array $options
 * @param string $selected 被選擇的選項，使用$separator分隔
 * @param char $separator 分隔符號
 * @param string $option_id
 * @param boolean $output
 * @return
 */
function option_mixied_string_status($options, $selected, $separator=';', $option_id='', $output=TRUE){
	$selected=trim($selected, $separator." \t\n\r\0\x0B");
	$selected=explode($separator, $selected);
	$n=count($selected);
	if(!$n || $n==1 && !strlen($selected[0])){
		$selected='';
	}
	else{
		for($i=0;$i<$n;$i++){
			if(isset($options[$selected[$i]])){
				$selected[$i]='<div class="option-status'.($option_id?' '.$option_id:'').'">'.(isset($options[$selected[$i]]['class'])?'<span class="'.$options[$selected[$i]]['class'].'"></span>':'').'<span class="text">'.htmlspecialchars($options[$selected[$i]]['text']).'</span></div>';
			}
		}
		$selected=join('',$selected);
	}
	if($output){
		echo $selected;
	}
	else{
		return $selected;
	}
}

function option_string_status($options, $selected, $separator=';', $option_id='', $output=TRUE){
	$selected=trim($selected, $separator." \t\n\r\0\x0B");
	$selected=explode($separator, $selected);
	$n=count($selected);
	if(!$n || ($n==1 && !strlen($selected[0]))){
		$selected='';
	}
	else{
		for($i=0;$i<$n;$i++){
			if(isset($options[$selected[$i]])){
				$selected[$i]='<div class="option-status'.($option_id?' '.$option_id:'').' option-'.($selected[$i]).'"><span class="text">'.htmlspecialchars($options[$selected[$i]]).'</span></div>';
			}
		}
		$selected=join('',$selected);
	}
	if($output){
		echo $selected;
	}
	else{
		return $selected;
	}
}

function json_struct_html($data_id,$data,$default=array()){
	if(!is_array($data)) { return FALSE; }
	foreach($data as $row => $d){
		$label=isset($d['l'])?$d['l']:$row;
?>
		<div class="control-group">
			<div class="control-label"><?php HTML($label); ?></div>
			<div class="controls">
<?php
		if(isset($d['o'])){
			SELECT_ONE($d['o'],(isset($default[$row])?$default['row']:0));
		}		
		else if(!isset($d['t']) || empty($d['t']) || $d['t']=='string'){
			if(isset($default[$row])){
				HTML($default[$row]);
			}
			else{
				echo '&nbsp;';
			}
		}
		else if(isset($d['t'])){
			switch($d['t']){
				case 'boolean':
					SELECT_ONE(array(0=>_tl('Ly_no',FALSE),1=>_tl('Ly_yes',FALSE)),(isset($default[$row])?$default[$row]:0));
					break;
				case 'uint':case 'int':case 'float':
					NUM(isset($default[$row])?$default[$row]:0);
					if(isset($d['u'])){
						SELECT_ONE($d['u'],(isset($default[$row])?$default['row']:0));
					}
					break;
				case 'text':
					if(isset($default[$row])){
						HTML($default[$row]);
					}
					else{
						echo '&nbsp;';
					}
					break;
			}

		}
?>
			</div>
		</div>
<?php
	}
}
/**
 * json_struct 
 *
 * @param array $data 陣列，欄位參數
 * @param array $default 輸入值
 * @access public
 * @return void
 */
function json_struct($data_id,$data,$default=array()){
	if(!is_array($data)) { return FALSE; }
	foreach($data as $row => $d){
		$label=isset($d['l'])?$d['l']:$row;
?>
		<div class="control-group">
			<div class="control-label"><?php HTML($label); ?></div>
			<div class="controls">
<?php
		if(isset($d['o'])){
?>				<select name="<?php echo $data_id; ?>[<?php echo $row; ?>]" size="1"><?php
					OPTION($d['o'],(isset($default[$row])?$default[$row]:0));
?>				</select><?php
		}
		else if(!isset($d['t']) || empty($d['t']) || $d['t']=='string'){
?>				<input type="text" name="<?php echo $data_id; ?>[<?php echo $row; ?>]" class="span12" data-type="string" value="<?php echo isset($default[$row])?VALUE($default[$row],FALSE):''; ?>" />
<?php
		}
		else if(isset($d['t'])){
			switch($d['t']){
				case 'boolean':
					RADIO(array(0=>_tl('Ly_no',FALSE),1=>_tl('Ly_yes',FALSE)),(isset($default[$row])?$default[$row]:0),$data_id.'['.$row.']');
					break;
				case 'uint':case 'int':case 'float':
?>				<input type="text" name="<?php echo $data_id; ?>[<?php echo $row; ?>]" class="span4" data-type="<?php echo $d['t']; ?>" /><?php if(isset($d['u'])){
?><select name="<?php echo $data_id; ?>[<?php echo $row; ?>-unit]" size="1"><?php
						OPTION($d['u'],(isset($default[$row])?$default[$row]:0));
?></select><?php
					}
					break;
				case 'text':
?>				<textarea name="<?php echo $data_id; ?>[<?php echo $row; ?>]" class="span12"><?php echo isset($default[$row])?VALUE($default[$row],FALSE):''; ?></textarea>
<?php
					break;
			}
		}

?>
				
				<?php if(isset($d['h'])){ ?><div class="hint"><?php HTML($d['h']); ?></div><?php } ?>
			</div>
		</div>

<?php
	}
}

/**
 * 當要回傳JSON格式資料時，前後要使用json_return_open和json_return_close
 * 
 * @access public
 * @return void
 */
function json_return_open(){
	echo 'while(1);/'.'**';
}
function json_return_close(){
	echo '**'.'/';
}

/**
 * regexp_not_tag_end 
 * regexp 取得非結束標籤內容
 *
 * ex: 將 <i></i> 換成 <em></em>
 * 	$result = preg_replace('/<i>('.regexp_not_tag_end('<\/i>').')<\/i>/','<em>$1</em>', $string);
 * 
 * @param mixed $pattern 結束標籤
 * @access public
 * @return void
 */
function regexp_not_tag_end($pattern){
	$pat1 = substr($pattern,0,1); // <
	$pat2 = substr($pattern,1);   // \/tag>
	return "(?>[^$pat1]*)(?>$pat1(?!$pat2)[^$pat1]*)*";
}

/**
 * RADIO 
 * 
 * @param mixed $options 
 * @param int $default 
 * @param string $name 
 * @param string $str 
 * @param mixed $attribute 
 * @access public
 * @return void
 */
function radio($options,$default=0,$name='',$str=''){
	$assign=TRUE;
	if(!$str){
		$str='<label for="[#name]_[#key]"><input type="radio" name="[#name]" value="[#key]" id="[#name]_[#key]"[#checked] /><span class="radio"></span>[#text]</label>';
	}
	if($name){
		$str=str_replace('[#name]',$name,$str);
	}
	else{
		$str=str_replace(' name="[#name]"','',$str);
	}
	if(!isset($options[$default])) $assign=FALSE;
	$seek=0;
	$str2=$str;
	foreach($options as $key => $text){
		$str=$str2;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		if((!$assign && $seek==0) || ($key==$default)){
			$str=str_replace('[#checked]',' checked="checked"',$str);
		}
		else{
			$str=str_replace('[#checked]','',$str);
		}
		$seek++;
		echo $str;
	}
}

/**
 * OPTION 
 * 
 * @param mixed $options 
 * @param int $default 
 * @param string $str 
 * @param mixed $attribute 
 * @access public
 * @return void
 */
function option($options,$default=0,$str=''){
	if(!$str){
		$str='<option value="[#key]"[#selected]>[#text]</option>';
	}
	$str2=$str;
	foreach($options as $key => $text){
		$str=$str2;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		if($key==$default){
			$str=str_replace('[#selected]',' selected',$str);
		}
		else{
			$str=str_replace('[#selected]','',$str);
		}
		echo $str;
	}
}

/**
 * option_pow 
 * 多重選，用 pow 的方式(索引是0~30的整數)
 * 
 * @param mixed $options 
 * @param int $default 
 * @param string $str 
 * @access public
 * @return void
 */
function option_pow($options,$default=0,$str=''){
	if(!$str){
		$str='<option value="[#key]"[#selected]>[#text]</option>';
	}
	foreach($options as $key => $text){
		$str2=$str;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		$p=pow(2,$key);
		if(($p & $default) == $p){
			$str=str_replace('[#selected]',' selected',$str);
		}
		else{
			$str=str_replace('[#selected]','',$str);
		}
		echo $str;
	}
}

function option_array($options,$default=array(),$str=''){
	if(!$str){
		$str='<option value="[#key]"[#selected]>[#text]</option>';
	}
	foreach($options as $key => $text){
		$str2=$str;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		if(in_array($key,$default)){
			$str=str_replace('[#selected]',' selected',$str);
		}
		else{
			$str=str_replace('[#selected]','',$str);
		}
		echo $str;
	}
}

function select_one($options, $default=0, $str=''){
	if(!$str){
		$str='[#text]';
	}
	if(isset($options[$default])){
		$str=str_replace('[#text]',HTML($options[$default]),$str);
	}
	else{
		$str='&nbsp;'; // _tl(/**選項不存在**/'Ly_option_nonexists');
	}
	echo $str;
}

/**
 * CHECKBOX_select 
 * form post後取得checkbox結果
 * 
 * @param mixed $options_key
 * @param mixed $name
 * @access public
 * @return uint
 */
function checkbox_pow_result($options,$name){
	$res=form($name.'.','boolean','post');
	$sum=0;
	foreach($options as $k => $v){
		if(!empty($res[$k])){
			$sum|=pow(2,$k);
		}	
	}
	return $sum;
}

/**
 * CHECKBOX_array_result 
 * form post後取得checkbox結果
 * 
 * @param mixed $options_key 
 * @param mixed $name 
 * @access public
 * @return array
 */
function checkbox_array_result($options,$name){
	$res=form($name.'.','boolean','post');
	$sum=array();
	foreach($options as $k => $v){
		if(!empty($res[$k])){
			$sum[]=$k;
		}	
	}
	return $sum;
}

/**
 * CHECKBOX_pow 
 * 至多容納32個項目，適合選取項目是用正整數儲存的
 * 
 * @param mixed $options 
 * @param int $default 
 * @param string $str 
 * @access public
 * @return void
 */
function checkbox_pow($options,$default=0,$name,$str=''){
	if(!$str){
		$str='<label for="[#name]-[#key]"><input type="checkbox" name="[#name][[#key]]"[#checked] id="[#name]-[#key]" /><span class="checkbox"></span>[#text]</label>';
	}
	$str=str_replace('[#name]',$name,$str);
	$str2=$str;
	foreach($options as $key => $text){
		$str=$str2;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		if(pow(2,$key) & $default){
			$str=str_replace('[#checked]',' checked="checked"',$str);
		}
		else{
			$str=str_replace('[#checked]','',$str);
		}
		echo $str;
	}
}

function checkbox_pow_label($options,$default=0,$name,$str=''){
	if(!$str){
		$str='<label for="[#name]-[#key]"><input type="checkbox" name="[#name][[#key]]"[#checked] id="[#name]-[#key]" /><span class="checkbox"></span>[#text]</label>';
	}
	$str=str_replace('[#name]',$name,$str);
	$str2=$str;
	foreach($options as $key => $text){
		$str=$str2;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		if(pow(2,$key) & $default){
			$str=str_replace('[#checked]',' checked="checked"',$str);
		}
		else{
			$str=str_replace('[#checked]','',$str);
		}
		echo $str;
	}
}

function checkbox_pow_display_label($checked, $lang_string, $index, $output=TRUE){
	$pow=pow(2,$index);
	$str='<label class="checkbox check-'.($pow==($checked&$pow)?'on':'off').'"> '._tl($lang_string, FALSE).'</label>';
	if($output){
		echo $str;
	}
	else{
		return $str;
	}
}

/**
 * checkbox_pow_display 
 * 
 * @param array $options array(index=>顯示的文字，需要html處理過,...)
 * @param uint $checked 
 * @param uint $index 
 * @param boolean $output echo 或 return
 * @access public
 * @return void
 */
function checkbox_pow_display($options,$checked,$index,$output=TRUE){
	$pow=pow(2,$index);
	$str='';
	if(!isset($options[$index])){
	}
	else{
		$str='<label class="checkbox check-'.($pow==($checked&$pow)?'on':'off').'"> '.$options[$index].'</label>';
	}
	if($output){
		echo $str;
	}
	else{
		return $str;
	}
}

/**
 * CHECKBOX_array 
 * 適合選取項目是用陣列儲存的
 *
 * @param mixed $options 
 * @param array $default 
 * @param string $str 
 * @access public
 * @return void
 */
function checkbox_array($options,$default=array(),$name,$str=''){
	if(!$str){
		$str='<label for="[#name]-[#key]"><input type="checkbox" name="[#name][[#key]]"[#checked] id="[#name]-[#key]" /><span class="checkbox"></span>[#text]</label>';
	}
	$str=str_replace('[#name]',$name,$str);
	$str2=$str;
	foreach($options as $key => $text){
		$str=$str2;
		$str=str_replace('[#key]',$key,$str);
		$str=str_replace('[#text]',HTML($text,FALSE),$str);
		if(in_array($key,$default)){
			$str=str_replace('[#checked]',' checked="checked"',$str);
		}
		else{
			$str=str_replace('[#checked]','',$str);
		}
		echo $str;
	}
}

/**
 * checkbox_pow_display 
 * 
 * @param array $options array(index=>顯示的文字，需要html處理過,...)
 * @param uint $checked 
 * @param uint $index 
 * @param boolean $output echo 或 return
 * @access public
 * @return void
 */
function radio_display($options,$checked,$index,$output=TRUE){
	$str='';
	if(!isset($options[$index])){
	}
	else{
		$str='<label class="radio radio-'.(($checked==$index)?'on':'off').'"> '.$options[$index].'</label>';
	}
	if($output){
		echo $str;
	}
	else{
		return $str;
	}
}

function radio_display_label($checked, $lang_string, $index, $output=TRUE){
	$str='<label class="checkbox check-'.(($checked==$index)?'on':'off').'"> '._tl($lang_string, FALSE).'</label>';
	if($output){
		echo $str;
	}
	else{
		return $str;
	}
}


/**
 * OPT_RES
 * 取得RADIO,OPTION,CHECKBOX_pow等的資料
 */
function opt_res($options, $result){
	if(isset($options[$result])){
		return $result;
	}
	else{
		$keys=array_keys($options);
		return $keys[0];
	}
}

function label($label_string, $tips=''){
	echo '<span><span><span>'.$label_string.'</span></span></span>';
	if($tips){
		echo '<div class="label-tips"><span><span><span>'.$tips.'</span></span></span></div>';
	}
}

/**
 * password_nonblack 
 * 檢查密碼是否不在黑名單裡
 * 重複一樣、重複兩個符號、全大寫、全小寫、全數字、以及常用的passw0rd，都回傳FALSE
 * ps 暫時沒使用
 * 
 * @param mixed $password 
 * @param mixed $is_test 
 * @access public
 * @return void
 */
function password_nonblock($password,$is_test=FALSE){
	if(in_array($password,array('password','PASSWORD','Password','PASSW0RD','passw0rd','passwd','123456','12345678','01234567','1234567890','abcdefghi','qwertyuiop','asdfghjkl','zxcvbnm','qwertyasdfg','QWERTYASDFG'))){
		return FALSE;
	}
	if(preg_match('/^(.)\1+$/',$password) || preg_match('/^(..)\1+$/',$password) || preg_match('/^([A-Z])+$/',$password) || preg_match('/^[a-z]+$/',$password) || preg_match('/^[0-9]+$/',$password)){
		return FALSE;
	}
	return TRUE;
}

/**
 * path
 * 根據uid取得檔案路徑
 * 
 * @param mixed $uid  
 * @param string $convert 
 * @param string $extname 副檔名，預設png，空字串或empty時不設定副檔名
 * @access public
 * @return void
 */
function path($uid,$convert='_g',$extname='png'){
	$_prefix=substr($uid,2,1);
	if(empty($extname)){
		$extname='';
	}
	else{
		$extname='.'.$extname;;
	}
	if(!strcmp($_prefix,'-')){
		$dir1=substr($uid,3,3);
		$dir2=substr($uid,-3);
		return $dir1.'/'.$dir2.'/'.$uid.$convert.$extname;
	}
	else{
		$dir1=substr($uid,0,3);
		$dir2=substr($uid,-3);
		return $dir1.'/'.$dir2.'/'.$uid.$convert.$extname;
	}
	return '';
}

/**
 * similar 
 * 計算兩個數字陣列的相似程度
 * $a 和 $b 的索引名稱及數量要一致
 * 
 * @access public
 * @param array $a
 * @param array $b
 * @return float 介於 -1000~1000，數值越大表示相似度越高
 */
function similar($a, $b){
	$num_a=count($a);
	$num_b=count($b);
	if($num_a != $num_b) return 0;

	$inner=0;// 內積
	$len_a=0;// a 的長度
	$len_b=0;// b 的長度
	foreach($a as $id => $v){
		if(!isset($b[$id])){
			return 0;
		}
		$inner+=($v * $b[$id]);
		$len_a+=($v * $v);
		$len_b+=($b[$id] * $b[$id]);
	}
	if($len_a==0 || $len_b==0){
		return 0;
	}
	$abs = sqrt($len_a) * sqrt($len_b);
	if($abs == 0){
		return 0;
	}
	$cos = $inner / $abs;
	return ($cos * 1000);
}

function _date($date,$format=NULL,$output=TRUE){
	echo $date;
}

function _time($time,$format=NULL,$output=TURE){
	switch($format){
		case 'Hi':
			$time=substr($time,0,2).substr($time,3,2);
			break;
		case 'H:i':
			$time=substr($time,0,5);
			break;
		default:
	}
	echo $time;
}

/**
 * _tl 準備用於取代 __ 和 _S
 * @param string $word 詞彙檔裡的字彙，若不存在則不轉換直接輸出
 * @param mixed $replaces 
 *   若為array: 詞彙中要替換掉的字，將[#name]替換成$replaces['name']
 *   若為boolean: 是否要輸出
 *   若為string: 檢查是否為特定規格的字
 *   若有$replaces['_format']: 表示取代$output_format這項參數
 *   若有$replaces['_output']: 表示取代$output這項參數
 * @param mixed $output_format 檢查是否為特定規格的字
 *   若為boolean: 是否要輸出
 *   若為string: 檢查是否為特定規格的字
 * @param mixed $output 是否要輸出
 *   若為boolean: 是否要輸出
 *   若為string: 檢查是否為特定規格的字
 * output_format有：
 * - html:	預設的，使用htmlspecialchars
 * - text:	原始字串，不處理直接使用
 * - value:	給<input value />使用
 * - textarea:	給<textarea></textarea>使用
 * - label:	疊三層<span>的標籤
 */
function _tl($word){
	if(isset($GLOBALS['string'][$word])){
		$word=$GLOBALS['string'][$word];
	}
	else if(strcmp(substr($word,0,1),'L')){
		return '';
	}
	$num=func_num_args();
	$replaces=NULL;
	$output_format='html';
	$output=TRUE;
	if($num>1){
		for($i=0;$i<$num;$i++){
			$arg=func_get_arg($i);
			switch(gettype($arg)){
				case 'array':
					$replaces=$arg;
					break;
				case 'string':
					$output_format=strtolower($arg);
					break;
				case 'boolean':
					$output=$arg;
					break;
			}
		}
	}
	if(is_array($replaces)){
		if(isset($replaces['_format'])){ 
			$output_format=strtolower($replaces['_format']); 
			unset($replaces['_format']);
		}
		if(isset($replaces['_output'])){ 
			$output=$replaces['_output']; 
		}
		if(strcmp($output_format,'input')){
			foreach($replaces as $k=>$v){
				$word=str_replace('[#'.$k.']',$v,$word);
			}
		}
	}

	switch($output_format){
		case 'label':
			$word='<span><span><span>'.nl2br(htmlspecialchars($word)).'</span></span></span>';
			break;
		case 'attr':
		case 'value':
			$word=value($word,FALSE);
			break;
		case 'textarea':
			$word=textarea($word,FALSE);
			break;
		case 'text':
			break;
		case 'html':
		default:
			$word=html($word,FALSE);
			break;
	}
	if(!strcmp($output_format,'input')){
		foreach($replaces as $k=>$v){
			if(is_array($v)){
				$elem=input_builder($v);
			}
			else{
				$elem=html($v,FALSE);
			}
			$word=str_replace('[#'.$k.']',$elem,$word);
		}
	}

	if($output){
		echo $word;
	}
	else{
		return $word;
	}
}

function input_builder($options){
	$str='';
	if(!isset($options['type']) || !in_array($options['type'],array('select','radio','checkbox'))){
		$default_options=array(
			'value'=>'',
			'type'=>'text',
			'name'=>'',
			'id'=>'',
			'class'=>'',
			'placehoder'=>'',
			'data-type'=>'text',
			'data-format'=>'',
			'data-error-mesg'=>'',
			'readonly'=>'',
			'disabled'=>'',
		);
		$options=array_merge($default_options,$options);
		switch($options['data-type']){
			case 'uint':
			case 'integer':
			case 'int':
			case 'ufloat':
			case 'float':
			case 'number':
			case 'numeric':
			case 'pky':
				$str='<input type="text" class="'.$options['class'].'" data-type="'.$options['data-type'].'" value="'.value($options['value'],FALSE).'"'.($options['readonly']?' readonly="true"':'').' />';
				break;
			case 'uid':
			case 'safe':
			case 'ansii':
			case 'letter':
			case 'tel':
			case 'url':
			case 'email':
			case 'email/valid':
				$str='<input type="text" class="'.$options['class'].'" data-type="'.$options['data-type'].'" value="'.value($options['value'],FALSE).'"'.($options['readonly']?' readonly="true"':'').' />';
				break;
			case 'date':
				$str='<input type="text" class="'.$options['class'].'" data-type="'.$options['data-type'].'" value="'.value($options['value'],FALSE).'"'.($options['readonly']?' readonly="true"':'').' />';
				break;
			case 'date/valid':
			case 'year':
			case 'year-month':
			case 'time':
			case 'hour':
			case 'date-min':
			case 'hour-min':
			case 'date-min/valid':
			case 'time8601':
			case 'datetime/valid':
			case 'datetime':
			case 'text':
			default:
				$str='<input type="text" data-type="'.$options['datatype'].'" value="'.value($options['value'],FALSE).'"'.($options['readonly']?' readonly="true"':'').' />';
		}
	}
	else if(!strcmp($options['type'],'select')){
		$default_options=array(
			'name'=>'',
			'id'=>'',
			'class'=>'',
			'size'=>1,
			'please'=>'',
			'please-value'=>'',
			'option'=>array(),
			'value'=>'',
		);
		$options=array_merge($default_options,$options);
		$str='<select'.(empty($options['name'])?'':' name="'.$options['name'].'"').(empty($options['size'])?'':' size="'.$optinos['size'].'"').'>';
		if(strlen($options['please'])){
			if(strlen($options['please-value'])){
				$str.='<option value="'.$options['please-value'].'">'.$options['please'].'</option>';;
			}
			else{
				$str.='<option value="">'.$options['please'].'</option>';
			}
			$str.=select_option_list($options['option'],$options['value']);
		}
		$str.='</select>';
	}
	return $str;
}

function select_option_builder($option,$selected=''){
	$str='';
	foreach($option as $id => $text){
		if(is_array($text)){
			$str.='<optgroup label="'.value($id,FALSE).'">';
			$str.=select_option_list($text,$selected);
			$str.='</optgroup>';
		}
		else{
			$str.='<option value="'.value($id,FALSE).'"'.(!strcmp($selected, $id)?' selected="selected"':'').'>'.html($text,FALSE).'</option>';
		}
	}
	return $str;
}

/**
 * $string是已經escape過
 * $replaces的內容要escape
 */
function _t($str, $replaces=NULL){
	if(!is_null($replaces)){
		foreach($replaces as $id => $v){
			$str=str_replace($str, '[#'.$id.']', htmlspecialchars($v));
		}
	}
	return $str;
}

function post_redirect($action_url, $post, $sec=0){
?><form method="post" action="<?php echo $action_url; ?>" id="post-redirect-form">
<?php foreach($post as $name => $val){ ?>
	<input type="hidden" name="<?php echo $name; ?>" value="<?php VALUE($val); ?>" />
<?php } ?>
</form>
<script type="text/javascript"><!--
function post_redirect_go(){
	document.getElementById('post-redirect-form').submit();
}
post_redirect_go();
//--></script>
<?php
}


function rtxt($str,$output=TRUE){
	$str=htmlspecialchars($str,ENT_QUOTES);

	$listStart=strpos($str,'[list');
	$listClose=strpos($str,'[/list]');
	if($listClose!==false && $listStart!==false){
		$str=preg_replace('/\[list([^\]]*)\]\s*\[\*\](.+?)\[\/list\]/sei',"RTXTList('$2','$1')",$str);
		$str=preg_replace('/\[list\]/','',$str);
		$str=preg_replace('/\[\/list\]/','',$str);
	}
	if(strpos($str,'[/url]')!==false){
		$str=preg_replace('/\[url=([:\/a-z0-9#A-Z\-_\?&%\.]+)\](.+?)\[\/url\]/si','<a href="$1">$2</a>',$str);
	}
	if(strpos($str,'[/font]')!==false){
		$str=preg_replace('/\[font=([a-z0-9# A-Z\-]+)\](.+?)\[\/font\]/se',"RTXTFont('$1',1).'$2'.RTXTFont('$1',2)",$str);
	}
	$str=preg_replace('/\[#([a-zA-Z0-9]+)\]/s','<a name="$1"> </a>',$str);
	$str=preg_replace('/^!!!!([^\r\n]+)[\r\n]+/','<h4 class="topic">$1</h4>'."\n",$str);
	$str=preg_replace('/[\r\n]!!!!([^\r\n]+)[\r\n]+/',"\n".'<h4 class="topic">$1</h4>'."\n",$str);
	$str=preg_replace('/[\r\n]!!!!([^\r\n]+)$/',"\n".'<h4 class="topic">$1</h4>',$str);
	$str=preg_replace('/^!!!([^\r\n]+)[\r\n]+/','<h3 class="topic">$1</h3>'."\n",$str);
	$str=preg_replace('/[\r\n]!!!([^\r\n]+)[\r\n]+/',"\n".'<h3 class="topic">$1</h3>'."\n",$str);
	$str=preg_replace('/[\r\n]!!!([^\r\n]+)$/',"\n".'<h3 class="topic">$1</h3>',$str);
	if(!$output){
		return nl2br($str);
	}
	echo nl2br($str);
}
function rtxtlist($str,$model=false){
	$list='ul';
	if(!$model)$model='list';
	else $model=str_replace('=','',$model);
	if(!strcasecmp($model,'1') || !strcasecmp($model,'number')){
		$list='ol';
		$model='decimal';
	}
	$str='<'.$list.' class="'.$model.'"><li>'.preg_replace('/[\s\n\r]*\[\*\]/','</li><li>',$str).'</li></'.$list.'>';
	return $str;
}
function rtxtfont($str,$pos){
	$str=trim(strtolower($str));
	$str=explode(' ',$str);
	if($pos==2){
		if(in_array('b',$str)){
			return '</strong>';
		}
		else if(in_array('i',$str)){
			return '</em>';
		}
		return '</span>';
	}
	$color='';
	$bgcolor='';
	$em=false;
	$strong=false;
	$underline=false;
	for($i=0,$n=count($str);$i<$n;$i++){
		if(strlen($str[$i])<1)continue;
		if(in_array($str[$i],array('red','blue','glary','green','yellow'))){
			$color=$str[$i];
		}
		else if((strlen($str[$i])==4 || strlen($str[$i])==7) && substr($str[$i],0,1)=='#'){
			$color=$str[$i];
		}
		else if(in_array($str[$i],array('bg-red','bg-blue','bg-glary','bg-green','bg-yellow'))){
			$bgcolor=substr($str[$i],3);
		}
		else if((strlen($str[$i])==7 || strlen($str[$i])==7) && substr($str[$i],0,4)=='bg-#'){
			$bgcolor=substr($str[$i],3);
		}
		else if(!strcmp('b',$str[$i])){
			$strong=true;
		}
		else if(!strcmp('i',$str[$i])){
			$em=true;
		}
		else if(!strcmp('u',$str[$i])){
			$underline=true;
		}
	}
	$str='';
	if($strong){
		$str='<strong style="font-weight:bold;';
		if($color) $str.='color:'.$color.';';
		if($bgcolor) $str.='background-color:'.$bgcolor.';';
		if($em) $str.='font-family:italic;';
		if($underline) $str.='font-decoration:underline;';
		$str.='">';
	}
	else if($em){
		$str='<em style="font-family:italic;';
		if($color) $str.='color:'.$color.';';
		if($bgcolor) $str.='background-color:'.$bgcolor.';';
		if($underline) $str.='font-decoration:underline;';
		$str.='">';
	}
	else{
		$str='<span style="';
		if($color) $str.='color:'.$color.';';
		if($bgcolor) $str.='background-color:'.$bgcolor.';';
		if($underline) $str.='font-decoration:underline;';
		$str.='">';
	}
	return $str;
}

function _resp( $response_word, $replaces=NULL, $output_json=NULL){
	$GLOBALS['cz']->response_message[]=array('id'=>$response_word,'replaces'=>$replaces);
	if(is_array($output_json) && !empty($output_json)){
		$GLOBALS['cz']->exagged=TRUE;
		_response_json($output_json,TRUE);
	}
	else if($GLOBALS['cz']->exagged){
		/** 若已經是輸出字串至前端後 **/
		if(!$GLOBALS['cz']->page['phtml']){
			/** POST的輸出 **/
			_response_json();
		}
		else {
			_response();
		}
	}
}
function _response_json($addition=NULL,$output_die=TRUE,$is_xml=FALSE){
	if(!is_array($addition)){
		$addition=array('error'=>0,'message'=>_response(TRUE,TRUE));
	}
	else{
		$addition['message']=_response(TRUE,TRUE);
		if(!isset($addition['error']))$addition['error']=0;
	}
	if($output_die){
		if($is_xml){
			die(xml_encode($addition));
		}
		else{
			die(json_encode($addition));
		}
	}
	else{
		if($is_xml){
			echo xml_encode($addition);
		}
		else{
			echo json_encode($addition);
		}
	}
}
function _response($reset=TRUE,$json=FALSE){
	//if(!$GLOBALS['cz']->exagged) 
	$GLOBALS['cz']->exagged=TRUE;
	$output='';
	$resp_num=count($GLOBALS['cz']->response_message);
	if($resp_num<1){
		return FALSE;
	}
	if($resp_num>1){
		$where=array();
		for($i=0;$i<$resp_num;$i++){
			$where[$GLOBALS['cz']->response_message[$i]['id']]=1;
		}
		$where=array_keys($where);
		$n=count($where);
	}
	else{
		$where[0]=$GLOBALS['cz']->response_message[0]['id'];
		$n=1;
	}
	if($n==1 && $resp_num==1){
		$sql=array(
			'select'=>'',
			'from'=>'responses',
			'where'=>'responseId="'.$where[0].'"',
		);
		$resp=$GLOBALS['db']->row('vgroup',$sql);
		if(!$resp){
			if($json){
				$output=_S($GLOBALS['cz']->response_message[0]['id'], $GLOBALS['cz']->response_message[0]['replaces']);
			}
			else{
				echo '<p class="response-message">';
				__($GLOBALS['cz']->response_message[0]['id'], $GLOBALS['cz']->response_message[0]['replaces']);
				echo '</p>';
			}
		}
		else{
			if(!is_array($GLOBALS['cz']->response_message[0]['replaces'])){
				if($json){
					$output=$resp['responseContent'];
				}
				else{
					echo '<p class="response-message">'.$resp['responseContent'].'</p>';
				}
			}
			else{
				foreach($GLOBALS['cz']->response_message[0]['replaces'] as $index => $string){
					$resp['responseContent']=str_replace('[#'.$index.']',$string,$resp['responseContent']);
				}
				if($json){
					$output=$resp['responseContent'];
				}
				else{
					echo '<p class="response-message">'.$resp['responseContent'].'</p>';
				}
			}
		}
	}
	else{
		$query=array(
			'select'=>'',
			'from'=>'responses',
			'where'=>'responseId IN ("'.join('","',$where).'")',
			'index'=>'responseId',
			'value'=>'responseContent',
		);
		$resp=$GLOBALS['db']->select('vgroup',$query);
		$msg=array();
		for($i=0;$i<$resp_num;$i++){
			if(!isset($resp[$GLOBALS['cz']->response_message[$i]['id']])){
				$msg[]=_S($GLOBALS['cz']->response_message[$i]['id'], $GLOBALS['cz']->response_message[$i]['replaces']);
			}
			else{
				$tmp=$resp[$GLOBALS['cz']->response_message[$i]['id']];
				foreach($GLOBALS['cz']->response_message[$i]['replaces'] as $index => $string){
					$tmp=str_replace('[#'.$index.']',$string,$tmp);
				}
				$msg[]=$tmp;
			}
		}
		$msg=array_unique($msg);
		$msg=array_values($msg);
		if($json){
			$output=array();
			for($i=0,$num=count($msg);$i<$num;$i++){
				$output[]=$msg[$i];
			}
		}
		else{
			echo '<p class="response-message"><ol>';
			for($i=0,$num=count($msg);$i<$num;$i++){
				echo '<li>'.$msg[$i].'</li>';
			}
			echo '</ol></p>';
		}
	}
	if($reset){
		$GLOBALS['cz']->response_message=array();
	}
	if($json){
		return $output;
	}
}

/**
 * 在form中插入form_exe用以防範CSRF(跨網站的偽造要求)
 *
 * 根據Firm變更session id取得方式
 */
function form_exe($exe){
	$csrf_seed=rand(65536,1048575);
	$csrf_hash=sprintf('%27s%05s',substr(sha1($csrf_seed.ME::id()._USER_PASSWORD_HASHSEED),3,27),strtolower(dechex($csrf_seed)));
?>
	<input type="hidden" name="exe" value="<?php echo $exe; ?>" />
	<input type="hidden" name="csrf" value="<?php echo $csrf_hash; ?>" />
<?php
}

function form_csrf_check(){
	$csrf=form('csrf','safe','post');
	$csrf_seed=strtolower(substr($csrf,-5));
	$csrf_hash=sprintf('%27s%05s',substr(sha1(hexdec($csrf_seed).ME::id()._USER_PASSWORD_HASHSEED),3,27),$csrf_seed);
	return (!strcmp($csrf_hash, $csrf)?TRUE:FALSE);
}

/**
 * 多個多層array相加
 */
function array_add(){
	$n=func_num_args();
	$arrays=func_get_args();
	if($n<1)return array();
	else if($n==1)return func_get_arg(0);
	else {
		for($i=1;$i<$n;$i++){
			$arrays[0]=array_between_add($arrays[0],$arrays[$i]);
		}
	}
	return $arrays[0];
}
function array_between_add($a0,$a1){
	$keys0=array_keys($a0);
	$keys1=array_keys($a1);
	$keys=array_unique(array_merge($keys0,$keys1));
	for($i=0,$n=count($keys);$i<$n;$i++){
		if(!isset($a0[$keys[$i]]) && isset($a1[$keys[$i]])){
			$a0[$keys[$i]]=$a1[$keys[$i]];
		}
		else if(isset($a0[$keys[$i]]) && !isset($a1[$keys[$i]])){
		}
		else if(is_array($a0[$keys[$i]]) && is_array($a1[$keys[$i]])){
			$a0[$keys[$i]]=array_between_add($a0[$keys[$i]],$a1[$keys[$i]]);
		}
	}
	return $a0;
}
/**
 * my_array_merge
 * 多維陣列的合併。當$d和$a有相同索引時，$a會取代$d，否則使用$d的值
 * 
 * @param mixed $d 預設值
 * @param mixed $a 設定值
 * @access public
 * @return void
 */
function my_array_merge($d,$a){
	foreach($d as $k => $v){
		if(isset($a[$k])){
			$dx=is_array($v);
			$ax=is_array($a[$k]);
			if($dx && $ax){
				$d[$k]=my_array_merge($v, $a[$k]);
			}
			else if(!$dx && !$ax){
				$d[$k]=$a[$k];
			}
		}
	}
	return $d;
}


function die_message($fpath,$v){
	if(@!!filemtime(_DIR_CORE.'lib/'.$fpath.'.phtml')){
		require(_DIR_CORE.'lib/'.$fpath.'.phtml');
	}
	die(1);
}

function _status($options, $value){
	if(!isset($options[$value])) $value=0;
?><div class="ico-<?php echo $value; ?>" title="<?php VALUE($options[$value]); ?>"><span class="<?php echo $value; ?>"><span><span><?php html($options[$value]); ?></span></span></div>
<?php
}

function _st($value){
	echo '<div class="ico-'.$value.'" title="'._S('Status_'.$value).'"><span class="'.$value.'"><span><span>'._S('Status_'.$value).'</span></span></span></div>';
}

function opt_str2index($val,$opts){
/**
 * 用在頁面一開始取得資料時，將傳入值限縮在指定的選項內，並替換為字串
 */
	foreach($opts as $k => $v){
		if(!strcmp($k,$val)){
			return $v;
		}
	}
	return 0;
}

function url_parse($s){
	return preg_replace_callback('/(https?:\/\/)?[a-zA-Z][a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}([a-zA-Z0-9\/\?+=&#%~_\-]+)?/','url_parse_filter',$s);

}
function url_parse_filter($matches){
	$url2=urldecode($matches[0]);
	$url2=strtr($url2,array('"'=>'','\''=>'','<'=>'','>'=>''));
	return '<a href="'.$url2.'" rel="nofollow">'.htmlspecialchars($matches[0],ENT_QUOTES).'</a>';
}
/*****
除錯用
 ****/
function _e($param){
	if(_DEBUG){
		if(!headers_sent()){
			header('Content-Type: text/html; charset=utf-8');
		}
		echo '<pre>';
		echo print_r($param,true);
		echo '</pre>'."\n";
	}
}
function _e2(){
	if(_DEBUG){
		if(!headers_sent()){
			header('Content-Type: text/html; charset=utf-8');
		}
		if(func_num_args()>0){
			echo '<div>'.join('::::',func_get_args()).'</div>';
		}
	}
}

function elem_select_option_selected($value,$default,$type){
	switch($type){
		case 'integer':
		case 'boolean':
		case 'double':
			echo $value==$default?' selected="selected"':'';
			break;
		case 'string':
			echo !strcmp($value,$default)?' selected="selected"':'';
			break;
		default:
			break;
	}
}

function elem_select_option($options,$default=NULL,$optgroup=FALSE){
	$defaulttype=gettype($default);
	if(!$optgroup){
		foreach($options as $value => $text){
	?><option value="<?php VALUE($value); ?>"<?php elem_select_option_selected($value,$default,$defaulttype); ?>><?php HTML($text); ?></option><?php
		}
	}
	else{
		foreach($options as $optgroup => $option){
?><optgroup label="<?php VALUE($optgroup); ?>"><?php
			foreach($option as $value => $text){
	?><option value="<?php VALUE($value); ?>"<?php elem_select_option_selected($value,$default,$defaulttype); ?>><?php HTML($text); ?></option><?php
			}
?></optgroup><?php
		}

	}
}

function elem_select($options,$default=NULL,$req=FALSE,$classes=NULL,$params=NULL){
	$str='';
	if(is_array($params)){
		foreach($params as $p => $v){
			if($p=='class')continue;
			$str.=' '.$p.'="'.$v.'"';
		}
	}
	if($classes)$str.=' class="'.$classes.'"';
?>
<select size="1"<?php echo $str; ?>>
<?php
	foreach($options as $v => $t){
		echo '<option value="'.$v.'"'.($v==$default?' selected="selected"':'').'>'.H($t,FALSE).'</option>';
	}
?>
</select>
<?php
}

/**
 * image_conv 
 * 
 * @param fpath $src 來源檔案完整路徑
 * @param fpath $trg 目標檔案完整路徑
 * @param int $resize 縮放比例(倍數)
 * @access public
 * @return void
 */
function image_conv($src,$dst,$resize=1){
	if($resize==1){
		$cmd=sprintf(_BIN_IMAGEMAGICK.' %s %s',$src,$dst);
	}
	else{
		$cmd=sprintf(_BIN_IMAGEMAGICK.' %s -resize %s %s',$src,$resize,$dst);
	}
}

function projs_file_path($fname, $path){
	return (HTTPS?'https://':'http://')._URL_FILE.'/projs/'.$GLOBALS['cz']->site_val('info','urlhash').'/'.$path.$fname;
}

function host($mhost,$fname='',$extension=NULL){
	switch($mhost){
		case 'file':
			echo '/o/projs/'.$GLOBALS['cz']->site_val('info','urlhash').'/';
			break;
			echo (HTTPS?'https://':'http://')._URL_FILE.'/projs/'.$GLOBALS['cz']->site_val('info','urlhash').'/';
			break;
		case 'proj-css':
			if(is_null($extension))$extension='css';
			echo '/o/projs/'.$GLOBALS['cz']->site_info('urlhash').'/css/'.$fname.'.'.$extension.'?dt='.STYLESHEET_VERSION;
			break;
			echo (HTTPS?'https://':'http://')._URL_FILE.'/projs/'.$GLOBALS['cz']->site_info('urlhash').'/css/'.$fname.'.'.$extension.'?dt='.STYLESHEET_VERSION;
			break;
		case 'css':
			if(is_null($extension))$extension='css';
			echo '/css/'.$fname.'.min.'.$extension.'?dt='.STYLESHEET_VERSION;
			break;
			if(_ENVIRONMENT==0){
				echo (HTTPS?'https://':'http://')._URL_CSS.'/'.$fname.'.min.'.$extension.'?dt='.STYLESHEET_VERSION;
			}
			else{
				echo (HTTPS?'https://':'http://')._URL_CSS.'/'.$fname.'.'.$extension.'?dt='.STYLESHEET_VERSION;
			}
			break;
		case 'js':
			if(is_null($extension))$extension='js';
			echo '/js/'.$fname.'.min.'.$extension;
			break;
			if(_ENVIRONMENT==0){
				if(preg_match('/^words/',$fname)){
					echo (HTTPS?'https://':'http://')._URL_JS.'/'.$fname.'.min.'.$extension;
				}
				else{
					echo (HTTPS?'https://':'http://')._URL_JS.'/'.JAVASCRIPT_VERSION.'_'.$fname.'.'.$extension;
				}
			}
			else{
				echo (HTTPS?'https://':'http://')._URL_JS.'/'.JAVASCRIPT_VERSION.'_'.$fname.'.min.'.$extension;
			}
			break;
		default:
			$p=strpos($mhost,'//');
			if($p===0){
				$mhost=HTTPS?'https:':'http:'.$mhost;
			}
			else if($p===FALSE){
				$mhost=HTTPS?'https://':'https://'.$mhost;
			}
			if(!strcmp(substr($mhost,-1),'/')){
				$mhost.='/';
			}
			echo $mhost;
			break;
	}
}

/**
 * 根據$option取得$options的位置
 * $row_name_pky: 流水號的欄位名稱
 * $row_name_lnode: 左節點欄位名稱
 * $same_row: 只比對某個欄位必需值相同
 */
function get_ref_rel($options, $option, $row_name_pky, $row_name_lnode, $same_row=NULL){
	$last_i=0;
	for($i=0;$i<$options['args']['num'];$i++){
		if(!is_null($same_row) && strcmp($options['d'][$i][$same_row], $option[$same_row])) continue;
		if($option[$row_name_lnode] <= $options['d'][$i][$row_name_lnode]){
			// 找到最接近且小於的左節點
			if($last_i>0){
				$rr=array(
					'pky'=>$options['d'][$last_i][$row_name_pky],
					'rel'=>($option[$row_name_pky.'Ref']==$options['d'][$last_i][$row_name_pky])?'sub':'parallel',// 同一層(parallel)或是屬於下一層(sub)
				);
			}
			break;
		}
		$last_i=$i;
	}
	if(!isset($rr)){
		return $rr=NULL;
	}
	else{
		return $rr;
	}
}

/**
 * 重寫連結變成絕對路徑，目標是在樣板檔時不用寫絕對路徑，也能做到不受模組名稱影響，而且p,q都是以目錄型式呈現
 * ex: 
 * - /run/module/command -> 不處理
 * - http://.... -> 不處理
 * - ./command -> /[current method]/[current module]/command
 * - ../module/command -> /[current method]/module/command
 */
/**
 * delete by dinos 20130816
 * 改用 $GLOBALS['cz']->url();
function href($url){
	if(!strcasecmp(substr($url,0,7),'http://') || !strcasecmp(substr($url,0,8),'https://')){
		return $url;
	}
	else if(preg_match('/^\.\/[^\.]/',$url)){
	// ./開頭
		return preg_replace('/^\.\//','/'.$GLOBALS['cz']->method.'/'.$GLOBALS['cz']->module_id().'/',$url);
	}
	else if(preg_match('/^\.\.+\/[^\.]/',$url)){
	// ../開頭
		return preg_replace('/^\.\.+\//','/'.$GLOBALS['cz']->method.'/',$url);
	}
	else if(preg_match('/^\/[^\.]/',$url)){
	// 斜線開頭
		return $url;
	}
	return $url;
}
 **/

function vhost_path($pky,$media_type,$media_extname,$file_hash='',$mkdir=TRUE,$is_url=TRUE,$vhost_url_path=NULL){
	$rel_path='';
	if(is_null($vhost_url_path)){
		$rel_path.=$GLOBALS['cz']->vhost['url_path'].'/data/'.$media_type.'/';
	}
	else{
		$rel_path.=$vhost_url_path.'/data/'.$media_type.'/';
	}
	$rel_path.=floor($pky/100000).'/'.($pky%100).'/';
	if($is_url){
		$abs_path='//sw-o.mingann.com/'.$rel_path;
	}
	else{
		$abs_path=_DIR_SAVEFILE.$rel_path;
		if($mkdir){
			if(!is_dir($abs_path)){
				@mkdir($abs_path,0755,TRUE);
			}
		}
	}
	$abs_path.=$pky;
	if(strlen($file_hash))$abs_path.='_'.$file_hash;
	$abs_path.='.'.$media_extname;
	return $abs_path;
}

function xml_encode($data){
 /**
  * input
  *  $data: array
  */
	$str='';
	if(!is_array($data)){
		return htmlspecialchars($data,ENT_QUOTES);
	}   
	else{
		foreach($data as $index => $val){
			if(!preg_match('/^[a-zA-Z0-9][a-zA-Z0-9\-_\.:]*$/',$index))continue;
			if(is_array($val)){
				$str.='<'.$index.'>'.xml_encode($val).'</'.$index.'>';
			}   
			else if(is_bool($val)){
				$str.='<'.$index.'>'.($val?1:0).'</'.$index.'>';
			}
			else{
				$str.='<'.$index.'>'.htmlspecialchars($val,ENT_QUOTES).'</'.$index.'>';
			}
		}
		return $str; 
	}
}

function hash_encode($data, $salt){
 /**
  * input
  *  $data: array, string, number
  *  $salt: string, min-height=2
  * return
  *  string, length 16~31
  */
	if(strlen($salt)<2)return '';
	if(is_array($data)){
		ksort($data);  
		foreach($data as $index => $val){
			if(is_numeric($val))$data[$index]=strval($val);
		}
		$data=json_encode($data);
	}
	else if(is_string($data) || is_numeric($data)){
	}
	else return '';
	return strrev(substr(sha1($data.'Ct#'.$salt.'l'),3,16+(ord(substr($salt,1,1))%16)));
}

/*******
儲存檔案
$des 要是相對於 config _DIR_SAVEFILE 的路徑，而非絕對路徑
$src & $des 可以使用陣列一次複製多個檔案
 *******/
function fsave($src,$des){
	$tftp=array(
		'server'=>'211.76.137.169',
		'id'=>'spex_upload',
		'pw'=>'SPex_upload',
		'port'=>21
	);
	if(_ENV_SITE==12){
		@$ftp=ftp_connect($tftp['server'],$tftp['port'],20);
		if(@ftp_login($ftp, $tftp['id'], $tftp['pw'])){
			ftp_pasv($ftp, true);
			if(is_array($des)){
				for($i=0,$n=count($des);$i<$n;$i++){
					$toDir=dirname($des[$i]);
					ftp_chdir($ftp, '/'.$toDir);
					$src[$i]=ftp_nb_put($ftp, basename($des[$i]), $src[$i]);
				}
			}
			else{
				$toDir=dirname($des);
				ftp_chdir($ftp, '/'.$toDir);
				$src=ftp_nb_put($ftp, basename($des), $src);
			}
			ftp_close($ftp);
			return $src;
		}
		else{
			return 'Connect Failed.';
		}
	}
	else{
		if(is_array($des)){
			for($i=0,$n=count($des);$i<$n;$i++){
				@copy($src[$i], _DIR_SAVEFILE.$des[$i]);
			}
		}
		else{
			@copy($src, _DIR_SAVEFILE.$des);
		}
	}
	return true;
}

function response($response_id,$args=NULL){
	if($GLOBALS['cz']->page['phtml']){
		$GLOBALS['cz']->response_message['message']=$args;
		$GLOBALS['cz']->response_message['response_id']=$response_id;
	}
	else{
		$sql=array(
			'select'=>'responseContent',
			'from'=>'responses',
			'where'=>'responseId="'.mysql_real_escape_string($response_id).'"',
		);
		$sysmsg=$GLOBALS['db']->value('vgroup',$sql);
		if(!$sysmsg){
			_e('No response id: '.$response_id);
		}
		else{
			foreach($GLOBALS['cz']->response_message['message'] as $k => $v){
				$sysmsg=str_replace('[#'.$k.']',$v,$sysmsg);
			}
			die(json_encode(array('message'=>$sysmsg)));
		}
	}
}

function item_selector($id,$sel,$name,$pky,$sc='vgroup'){
?>
	<input type="text" id="<?php echo $id; ?>" class="row-selector" value="<?php value($name); ?>" datasource="/admin/site-admin/item-selector?sel=<?php echo $sel; ?>&sc=<?php echo $sc; ?>" keyid="<?php echo $id; ?>pky" />
	<input type="hidden" id="<?php echo $id; ?>pky" name="<?php echo $id; ?>" value="<?php value($pky); ?>" />
<?php
}

/**
 * 顯示一個月份的日曆
 *
 */
function calendar_month($y,$m,$caption=NULL,$week1_week=0){
?>
<table class="calendar">
<?php if(!is_null($caption)){ ?><caption><?php HTML($caption); ?></caption><?php } ?>
<colgroup>
	<col class="col-day" />
	<col class="col-day" />
	<col class="col-day" />
	<col class="col-day" />
	<col class="col-day" />
	<col class="col-day" />
	<col class="col-day" />
</colgroup>
<thead>
	<tr>
	<th><?php _tl('Ly_cal_sun'); ?></th>
	<th><?php _tl('Ly_cal_mon'); ?></th>
	<th><?php _tl('Ly_cal_tue'); ?></th>
	<th><?php _tl('Ly_cal_wed'); ?></th>
	<th><?php _tl('Ly_cal_thu'); ?></th>
	<th><?php _tl('Ly_cal_fri'); ?></th>
	<th><?php _tl('Ly_cal_sat'); ?></th>
	</tr>
</thead>
<?php
$dt=mktime(0,0,0,$m,1,$y);
$day1_week=date('w',$dt);
$month_maxday=date('t',$dt);
$last_month_day = date('d',$dt-86400);
$next_month_day=1;
for($i=1;$i<=35;$i++){
	if($i%7==1){ ?><tr><?php }

	if($i<=$day1_week){
		$class='cal-outrange';
		$days = $last_month_day-($day1_week-$i);
	}
	else if($i>$month_maxday+$day1_week){
		$class='cal-outrange';
		$days=$next_month_day;
		$next_month_day++;
	}
	else{
		$class='cal-day';
		$days=$i-$day1_week;
	}
	?><td class="<?php echo $class; ?> date-<?php printf('%02d-%02d',$m,$days); ?>"  title="<?php echo sprintf('%04d-%02d-%02d',$y,$m,$days); ?>"><?php echo $days; ?></td><?php

	if($i%7==0){ ?></tr><?php }
}
?>
	</tbody>
</table>
<?php
}

function response_message(){
	if(!empty($GLOBALS['cz']->response_message['response_id'])){
		$response_id=$GLOBALS['cz']->response_message['response_id'];
		unset($GLOBALS['cz']->response_message['response_id']);
		$sql=array(
			'select'=>'responseContent',
			'from'=>'responses',
			'where'=>'responseId="'.mysql_real_escape_string($response_id).'"',
		);
		$sysmsg=$GLOBALS['db']->value('vgroup',$sql);
		if(!$sysmsg){
			_e('No response id: '.$response_id);
		}
		else{
			if(!empty($GLOBALS['cz']->response_message['message'])){
				foreach($GLOBALS['cz']->response_message['message'] as $k => $v){
					$sysmsg=str_replace('[#'.$k.']',$v,$sysmsg);
				}
			}
?>
	<div class="response-message">
		<div class="head"></div>
		<div class="body"><div class="msg"><?php echo($sysmsg); ?></div></div>
		<div class="tail"></div>
	</div>
<?php
		}
		if(!empty($GLOBALS['cz']->response_message['redirect'])){
?>
<script type="text/javascript">
var redirect_to=function(){
	self.location.href='<?php echo $GLOBALS['cz']->response_message['redirect']; ?>';
}
setTimeout(redirect_to,1000);
</script>
<?php
		}
	}
	else if(!empty($GLOBALS['cz']->response_message['response'])){
		$sysmsg=$GLOBALS['cz']->response_message['response'];
		if(!is_null($GLOBALS['cz']->response_message['message'])){
			foreach($GLOBALS['cz']->response_message['message'] as $k => $v){
				$sysmsg=str_replace('[$'.$k.']',$v,$sysmsg);
			}
		}
?>
	<div class="response-message">
		<div class="head"></div>
		<div class="body"><div class="msg"><?php echo($sysmsg); ?></div></div>
		<div class="tail"></div>
	</div>
<?php
	}
	$GLOBALS['cz']->response_message=array();
}

/**
 * 轉換時間長度
 * @param options $unit sec(秒), min(分), hr(時), day(日), month(月)
 * 過長的時、分、秒可以轉換為以n日n時n分n秒顯示
 * 過長的月可以以n年n月顯示
 */
function change_time_unit($num,$unit='month'){
	switch($unit){
		case 'sec':
			if($num>86400){
				$d=floor($num/86400);
				$num=$num%86400;
				$h=floor($num/3600);
				$num=$num%3600;
				$m=floor($num/60);
				$num=$num%60;
				html('Ly_time_length_d_h_m_s',array('day'=>$d,'hour'=>$h,'min'=>$m,'sec'=>$num));
			}
			else if($num>3600){
				$h=floor($num/3600);
				$num=$num%3600;
				$m=floor($num/60);
				$num=$num%60;
				html('Ly_time_length_h_m_s',array('hour'=>$h,'min'=>$m,'sec'=>$num));
			}
			else if($num>60){
				$m=floor($num/60);
				$num=$num%60;
				html('Ly_time_length_m_s',array('min'=>$m,'sec'=>$num));
			}
			else{
				html('Ly_time_length_s',array('sec'=>$num));
			}
			break;
		case 'min':
			if($num>=60){
				$a=0;
				$b=ceil($num/60);
				$c=$num%60;
				if($b>24){
					$a=ceil($b/24);
					$b=$b%24;
				}
				if($a>0){
					if($b>0){
						html(/**[#day] 天 [#hour] 小時**/'Ly_time_length_d_h',array('day'=>$a,'hour'=>$b));
					}
					else{
						html(/**[#day] 天**/'Ly_time_length_d',array('day'=>$a));
					}
				}
				else if($c>0){
						html(/**[#hour] 時 [#min] 分**/'Ly_time_length_h_i',array('hour'=>$b,'min'=>$c));
				}
				else{
					html(/**[#hour] 小時 **/'Ly_time_length_h',array('hour'=>$b));
				}
			}
			else{
				html(/**[#min] 分鐘**/'Ly_time_length_i',array('min'=>$num));
			}
			break;
		case 'hr':
			if($num>=24){
				$a=ceil($num/24);
				$b=$num%24;
				if($b>0){
					html(/**[#day] 天 [#hour] 小時**/'Ly_time_length_d_h',array('day'=>$a,'hour'=>$b));
				}
				else{
					html(/**[#day] 天**/'Ly_time_length_d',array('day'=>$a));
				}
			}
			else{
				html(/**[#hour] 個小時**/'Ly_time_length_h',array('hour'=>$num));
			}
		case 'month':
			if($num>=12){
				$a=ceil($num/12);
				$b=$num%12;
				if($b>0){
					html(/**[#year] 年 [#month] 個月**/'Ly_time_length_y_m',array('year'=>$a,'month'=>$b));
				}
				else{
					html(/**[#year] 年**/'Ly_time_length_y',array('year'=>$a));
				}
			}
			else{
				html(/**[#month] 個月**/'Ly_time_length_m',array('month'=>$num));
			}
			break;
	}
}

function shift_by_month($date, $shift_months){
	list($y,$m,$d)=explode('-',$date);
	$y=intval($y,10);
	$m=intval($m,10);
	$m+=$shift_months;
	while($m>12){
		$y++;
		$m-=12;
	}
	return sprintf('%04d-%02d-%02d',$y,$m,$d);
}

function dir_clear($src) {
	if(preg_match('/\s+/',$src)){
		return FALSE;
	}
	if(is_dir($src) && is_writable($src)){
		shell_exec('rm -rf '.$src.'/*');		
		return TRUE;
	}
}
/**
 * read_filename 
 * 要儲存為實體檔案時，過濾檔名中的不合法字元
 * 
 * @param mixed $filename 
 * @access public
 * @return void
 */
function real_filename($filename){
	$filename=preg_replace('/[\'"\s\/\\\?\$\+\&\*\(\)\[\]\?`:,]+/','_',$filename);
	return $filename;
}

/**
 * download_header 
 * 
 * @param mixed $filename 完整檔名
 * @param string $extname 副檔名，帶句點(.)，若無時，擷取完整檔名的副檔名
 * @access public
 * @return void
 */
function download_header($filename,$extname=NULL){
	$filename=real_filename($filename);
	if(is_null($extname)){
		$extname=strrchr($filename,'.');
	}
	$mime_types=array(
		'.rtf'	=>'application/rtf',
		'.doc'	=>'application/msword',
		'.dot'	=>'application/msword',
		'.docx'	=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'.xls'	=>'application/vnd.ms-excel',
		'.xlt'	=>'application/vnd.ms-excel',
		'.xlsx'	=>'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'.ppt'	=>'application/vnd.ms-powerpoint',
		'.pot'	=>'application/vnd.ms-powerpoint',
		'.pptx'	=>'application/vnd.openxmlformats-officedocument.presentationml.presentation',
		'.pdf'	=>'application/pdf',
		'.odt'	=>'application/vnd.oasis.opendocument.text',
		'.ods'	=>'application/vnd.oasis.opendocument.spreadsheet',
		'.csv'	=>'text/csv',
		'.txt'	=>'text/plain',
		'.html'	=>'text/html',
		'.htm'	=>'text/html',
		'.gif'	=>'image/gif',
		'.png'	=>'image/png',
		'.jpg'	=>'image/jpg',
		'.jpeg'	=>'image/jpg',
		'.ico'	=>'image/vnd.microsoft.icon',
		'.svg'	=>'image/svg+xml',
		'.zip'	=>'application/zip',
		'.tgz'  =>'application/tar+gzip',
		'.rar'	=>'application/x-rar-compressed',
		'.mp3'	=>'audio/mpeg',
		'.mov'	=>'video/quicktime',
	);
	if(strstr($_SERVER['HTTP_USER_AGENT'],'MSIE') || stristr($_SERVER['HTTP_USER_AGENT'],'Trident/') || stristr($_SERVER['HTTP_USER_AGENT'],'Edge/')){
		if(isset($mime_types[$extname])){
			header('Content-Type: '.$mime_types[$extname]);
		}
		else{
			header('Content-Type: application/octet-stream');
		}
		header('Content-Disposition: attachment; filename="'.rawurlencode($filename).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header('X-Download-Options: noopen'); // For IE8
		header('X-Content-Type-Options: nosniff'); // For IE8
		header('Content-Transfer-Encoding: binary');
		header('Pragma: public');
	}
	else{
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Cache-Control: private',false);
		if(isset($mime_types[$extname])){
			header('Content-Type: '.$mime_types[$extname]);
		}
		else{
			header('Content-Type: application/octet-stream');
		}
		header('Content-Disposition: attachment; filename='.$filename);
		header('Content-Transfer-Encoding: binary');
	}
}

/**
 * counter_get 
 * 
 * @param mixed $counter_id 
 * @param int $num 一次要增加多少數值
 * @param string $lifetime 哪時候到期
 * @access public
 * @return void
 */
/**
function counter_get($counter_id,$num=1,$lifetime='3999-12-31'){
	if(!strlen($counter_id)){return false;}
	$data=array(
		'counterNum'=>array('counterNum+'.$num),
	);
	$sql=array(
		'select'=>'counterNum,counterMax',
		'from'=>'counters',
		'where'=>'counterId="'.addslashes($counter_id).'" AND vhostPky='.$GLOBALS['cz']->vhost_pky,
	);
	$c=$GLOBALS['db']->row('common',$sql);
	$GLOBALS['db']->lock('common','counters');
	if(!$c){
		$data=array(
			'counterId'=>$counter_id,
			'counterNum'=>$num,
			'vhostPky'=>$GLOBALS['cz']->vhost_pky,
			'counterDescription'=>'Add at '.xdate('Y-m-d H:i:s'),
		);
		$GLOBALS['db']->add('common','counters',$data);
		$c=array('counterNum'=>$num,'counterMax'=>0);
	}
	else{
		$GLOBALS['db']->update('common','counters',$data,'WHERE counterId="'.addslashes($counter_id).'" AND vhostPky='.$GLOBALS['cz']->vhost_pky);
	}
	$GLOBALS['db']->unlock('common','counters');
	return $c['counterNum'];
}
 */

/**
 * counter_serial
 *
 * 需要資料表 counter_serials
 *
 * CREATE TABLE `counter_serials` (
 *   `counterId` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '流水號識別代碼',
 *   `counterNumber` bigint(20) UNSIGNED NOT NULL DEFAULT '0' COMMENT '計數器',
 *   `counterDescription` char(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '描述'
 * ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='流水號計數器';
 * 
 * @param mixed $counter_id 
 * @param int $num 一次要增加多少數值
 * @access public
 * @return int 回傳值為最大值(若 $num > 1 時)
 */
function counter_serial($counter_id,$num=1){
	if(!strlen($counter_id) || $num<1 || !is_int($num)){return FALSE;}
	$data=array(
		'counterNumber'=>array('counterNumber+'.$num),
	);
	$sql=array(
		'select'=>'counterNumber',
		'from'=>'counter_serials',
		'where'=>array(
			'counterId=:id',
			array(
				':id'=>$counter_id
			)
		)
	);
	$c=DB::row('common',$sql);
	if(is_null($c)){
		$data=array(
			'counterId'=>$counter_id,
			'counterNumber'=>$num,
			'counterDescription'=>'Add at '.xdate('Y-m-d H:i:s'),
		);
		DB::add('common','counter_serials',$data);
		$c=array('counterNumber'=>$num);
	}
	else{
		DB::update('common','counter_serials',$data,array('WHERE counterId=:id',array(':id'=>$counter_id)));
		$c['counterNumber']+=$num;
	}
	return $c['counterNumber'];
}

/**
 * page_info
 * 分頁資訊
 */
function pageInfo($param,$url,$output=TRUE){
	$s=array(
		'[#from]'=>$param['page']*$param['per'],
		'[#to]'=>$param['page']*$param['per']+$param['count'],
		'[#result]'=>$param['num']
	);
	_tl('Ly_page_result',$s,$output);
}

/**
 * pagination
 * 只處理?之後
 * * 相容CoreBase的pdo.class
 * 
 * @param array $arg
 * @param array $query QueryString參數，'索引'=>值，若未傳入，則使用$_SERVER['QUERY_STRING']
 * @param uint $prev_next 顯示前後幾頁
 * @access public
 * @return void
 */
function pagination($arg, $query=NULL, $prev_next=2){
	if(is_null($query)){
		$query=isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'';
	}

	if(is_string($query)){
		parse_str($query,$query);
	}
	else if(is_array($query)){
	}
	if(isset($query['e'])) unset($query['e']);

	$arg['pages']=floor($arg['total']/$arg['per']);

	if($arg['spage']>=$arg['pages']) $arg['spage']=$arg['pages']-1; // 若指定的頁面超過頁數，則顯示最後一頁

	$query['s']=0;
?>
<div class="pagination">
	<ol class="pagination">
		<li class="page-link page-first<?php echo ($arg['spage']==0?' page-selected':''); ?>">
			<?php if($arg['spage']>0){ ?>
			<a href="?<?php echo http_build_query($query); ?>">1</a>
			<?php }else{ ?>
			<a href="#">1</a>
			<?php } ?>
			<?php if($arg['spage']>($prev_next+2)){ echo ' <span class="page-first-more">...</span>';} ?>
		</li>
<?php
	// s=4 => 1 2 3 4 5 6 7 8 ...10
	if($arg['pages']>1){
		$p_start=$arg['spage']-ceil($prev_next/2);
		if($p_start<0) $p_start=0;
		$p_end=$p_start+($prev_next*2+1);
		if($p_end>=$arg['pages'])$p_end=$arg['pages']-1;

		if($p_start<1) $p_start=1;
		for($i=$p_start;$i<=$p_end;$i++){
			$query['s']=$i;
?>
<?php if($arg['spage']==$i){ ?>
		<li class="page-link page-at page-selected"><a href="#"><?php echo ($i+1); ?></a></li>
<?php }else{ ?>
		<li class="page-link page-at"><a href="?<?php echo http_build_query($query); ?>"><?php echo ($i+1); ?></a></li>
<?php } ?>
<?php
		}

		if($p_end<$arg['pages']-1){
			$query['s']=$arg['pages']-1;
?>
	<li class="page-link page-end<?php echo ($arg['spage']==$arg['pages']-1?' page-selected':''); ?>">
		<?php if($p_end<$arg['pages']-$prev_next){ echo '<span class="page-first-more">...</span> ';} ?>
	<a href="?<?php echo http_build_query($query); ?>"><?php echo ($query['s']+1); ?></a>
</li>
<?php
		}
	}
?>
	</ol>
</div>
<?php
}

/**
 * page_selector
 * 只處理?之後
 * * 相容CoreBase的pdo.class
 * 
 * @param array $arg
 * @param array $query QueryString參數，'索引'=>值，若未傳入，則使用$_SERVER['QUERY_STRING']
 * @param uint $prev_next 顯示前後幾頁
 * @access public
 * @return void
 */
function page_selector($param,$url){
/**
page: current page
pages: total pages
num: total data count
url: link to url
	[#page]: 第幾頁，例如 /do/something/aaaa?s=[#page]
 */
	$url=$_SERVER['REQUEST_URI'];
	if(preg_match('/[\?&]s=(\d+)?/',$url)){
		$pos=1;
		$url=preg_replace('/([\?&])s=(\d+)?/','\\1s=[#page]',$url);
		$url['query']=parse_str($url['query']);
	}
	else{
		if(strpos($url,'?')){
			$url.='&s=[#page]';
		}
		else{
			$url.='?s=[#page]';
		}
	}
	$pageStart=$param['page']-5;
	if($pageStart<0)$pageStart=0;
	$pageEnd=$pageStart+10;
	if($pageEnd>=$param['pages']-1)$pageEnd=$param['pages']-1;
?>
<ul class="page-selector">
	<?php
	?>
	<li class="pageFirst"><a <?php echo ($pageStart>0)?'href="'.str_replace('[#page]',0,$url).'"':'href="#" onclick="return false;" class="unable"'; ?>><div class="icon18"><span>First</span></div></a></li>
	<li class="pagePrev"><a <?php echo ($param['page']>0)?'href="'.str_replace('[#page]',$param['page']-1,$url).'"':'href="#" onclick="return false;" class="unable"'; ?>><div class="icon18"><span><?php echo ($param['page']); ?></span></div></a></li>
	<?php
		for($i=$pageStart;$i<=$pageEnd;$i++){
			if($i==$param['page']){
	?>
	<li class="pageSelected"><a href="<?php echo str_replace('[#page]',$i,$url); ?>"><?php echo ($i+1); ?></a></li>
	<?php
			}
			else{
	?>
	<li class="pageOption"><a href="<?php echo str_replace('[#page]',$i,$url); ?>"><?php echo ($i+1); ?></a></li>
	<?php
			}
		}
	?>
	<li class="pageNext"><a <?php echo ($param['page']<$param['pages']-1)?'href="'.str_replace('[#page]',$param['page']+1,$url).'"':'href="#" onclick="return false;" class="unable"'; ?>><div class="icon18"><span><?php echo ($param['page']+2); ?></span></div></a></li>
	<li class="pageFinal"><a <?php echo ($pageEnd<$param['pages']-1)?'href="'.str_replace('[#page]',$param['pages']-1,$url).'"':'href="#" onclick="return false;" class="unable"'; ?>><div class="icon18"><span>Final</span></div></a></li>
	<?php
	?>
</ul>
<?php
}

/**
 * perm_int_to_char 
 * 
 * @param mixed $ch
 * @access public
 * @return void
 */
function perm_int_to_char($ch){
	if($ch<0 || $ch>63) return '0';
	return chr($ch+48);
}

/**
 * perm_char_to_int 
 * 
 * @param mixed $ch 
 * @access public
 * @return void
 */
function perm_char_to_int($ch){
	$ch=ord($ch);
	if($ch<48 || $ch>111) return 0;
	return ($ch-48);
}

/**
 * perm_sql_compute 
 * 
 * @param mixed $row_name 
 * @param uint $perm 第幾個權限，0~191
 * @access public
 * @return void
 */
function perm_sql_compute($row_name,$perm){
	$sql='((ORD(SUBSTRING('.$row_name.','.(floor($perm/6)+1).',1))-48) & '.(pow(2,$perm%6)).') = '.(pow(2,$perm%6));
	return $sql;
}

/**
 * 權限處理用函式
 * 權限的數量為0~191，至多192個權限種類
 * perm2 
 * $perm_str是否有$bit_pos的權限
 * perm operator: chr(48)~chr(112)
 * 
 * @param mixed $perm_str 
 * @param mixed $bit_pos 
 * @access public
 * @return void
 */
function perm_exist($perm_str, $bit_pos){
	$bit1=floor($bit_pos/6);
	$bit2=$bit_pos%6;
	$ch=substr($perm_str,$bit1,1);
	$ch=perm_char_to_int($ch);
	return (pow(2,$bit2) & $ch)?TRUE:FALSE;
}

/**
 * perm_str 
 * 
 * @param int $arguments 正整數(0~191)
 * @access public
 * @return void
 */
function perm_str(){
	$perm_str=array_fill(0,32,0);
	$num=func_num_args();
	if($num<1) return implode($perm_str);
	$perms=func_get_arg(0);
	if(!is_array($perms)){
		if(is_numeric($perms)){
			$perms=func_get_args();
		}
		else{
			return implode($perm_str);
		}
	}
	else{
		$num=count($perms);
	}
	for($i=0;$i<$num;$i++){
		$ch=intval($perms[$i],10);
		if($ch>191) continue;
		$bit1=floor($ch/6);
		$bit2=$ch%6;
		$perm_str[$bit1] = $perm_str[$bit1] | pow(2,$bit2);
	}
	for($i=0;$i<32;$i++){
		$perm_str[$i]=perm_int_to_char($perm_str[$i]);
	}
	return implode($perm_str);
}


/**
 * perm_operator 
 * 權限運算，將原權限字串加上(OR)或去除(NOT)指定的權限
 * 
 * @param string $perm_str 
 * @param array $perms 
 * @param mixed $operator TRUE)OR運算, FALSE)NOT運算
 * @access public
 * @return void
 */
function perm_operator($perm_str,$perms,$operator=TRUE){
	$ch=array_fill(0,32,0);
	for($i=0;$i<32;$i++){
		if(isset($perm_str[$i])){
			$ch[$i]=perm_char_to_int($perm_str[$i]);
		}
	}
	for($i=0,$n=count($perms);$i<$n;$i++){
		$bit1=floor($perms[$i]/6);	
		$bit2=$perms[$i]%6;
		if($operator){
			$ch[$bit1] = $ch[$bit1] | pow(2,$bit2);
		}
		else if($ch[$bit1] & pow(2,$bit2)) $ch[$bit1]-=pow(2,$bit2);
	}
	for($i=0;$i<32;$i++){
		$ch[$i]=perm_int_to_char($ch[$i]);
	}
	return implode($ch);
}

/**
 * perm_str_operator 
 * 將原權限加上權限
 * 
 * @param mixed $perm_str 
 * @param mixed $perms 
 * @param mixed $operator 
 * @access public
 * @return void
 */
function perm_str_operator($perm_str,$perms,$operator=TRUE){
	if(!is_array($perms) && is_string($perms)){
		$perms=array($perms);
	}

	$ch0=perm_to_array_keys($perm_str);

	if($operator){
		for($i=0,$n=count($perms);$i<$n;$i++){
			$ch=perm_to_ints($perms[$i]);
			for($j=0,$m=count($ch);$j<$m;$j++){
				$ch0[$ch[$j]]=1;
			}
		}
	}
	else{
		for($i=0,$n=count($perms);$i<$n;$i++){
			$ch=perm_to_ints($perms[$i]);
			for($j=0,$m=count($ch);$j<$m;$j++){
				$ch0[$ch[$j]]=0;
			}
		}
	}
	$ch=array();
	for($i=0;$i<192;$i++){
		if(!empty($ch0[$i])) $ch[]=$i;
	}
	return perm_str($ch);
}

/**
 * perm_to_array_keys 
 * 權限字串轉為陣列，當該位置設定為TRUE時，值為1，否則值為0
 * 跟perm_to_ints()是差不多的，只是差在位置設定是放在索引或是值
 * array_keys(perm_to_array_keys(...))===perm_to_ints(...)
 * 
 * @param mixed $perm_str 
 * @access public
 * @return void
 */
function perm_to_array_keys($perm_str){
	$ch=array_fill(0,32,0);
	$len=strlen($perm_str);
	for($i=0;$i<32;$i++){
		if($len<=$i){
			break;
		}
		else{
			$c=substr($perm_str,$i,1);
		}
		$c=perm_char_to_int($c);
		for($j=0;$j<6;$j++){
			$c1=pow(2,$j);
			if(($c & $c1)==$c1){
				$ch[6*$i+$j]=1;
			}
		}
	}
	return $ch;
}

/**
 * perm_to_ints 
 * 將權限字串轉為pow陣列
 * 
 * @param mixed $perm_str 
 * @access public
 * @return void
 */
function perm_to_ints($perm_str){
	$ch=array();
	$len=strlen($perm_str);
	if($len<1) return $ch;
	for($i=0;$i<32;$i++){
		if($len<=$i){
			break;
		}
		else{
			$c=substr($perm_str,$i,1);
		}
		$c=perm_char_to_int($c);
		for($j=0;$j<6;$j++){
			$c1=pow(2,$j);
			if(($c & $c1)==$c1){
				$ch[]=6*$i+$j;
			}
		}
	}
	return $ch;
}

/**
 * perm_cmp 
 * 
 * @param mixed $perm1 被比較的權限
 * @param mixed $perm2 需要的權限
 * @param mixed $operator -1)必須完全沒有重複 0)有部分重複即可 1)重複的部份必須完全符合$perm1 2)必須完全一致
 * @access public
 * @return void
 */
function perm_cmp($perm2,$perm1,$operator=0){
	if($operator>=0 && !strcmp($perm1,$perm2)) return TRUE;
	$p1=perm_to_ints($perm1);
	$p2=perm_to_ints($perm2);
	$intersect=array_intersect($p1, $p2);
	$num=count($intersect);
	$num_p1=count($p1);

	if(!$num_p1){
		return TRUE;
	}
	if($operator<0){
		if($num>0) return FALSE;
		else return TRUE;
	}
	else if($operator==1){
		if($num==count($p1)) return TRUE;
		else return FALSE;
	}
	else if($operator==2){
		if($num==count($p1) && $num==count($p2)) return TRUE;
		else return FALSE;	
	}
	else{
		if($num>0) return TRUE;
		else return FALSE;
	}
	return FALSE;
}

function no_permission(){
	include(_DIR_CORE.'lib/message/no_permission.phtml');
	exit(1);
}

function error_send($errmsg, $level=0){

}

function editable(){
/**
 * 搭配 value_update.php
 */

 
}

/**
 * 轉換時間格式，$t要是YYYY-mm-dd HH:ii:ss
 *
 */
function fdate($format,$t){
	switch($format){
		case 'date-min':
			return substr($t,0,-3);
			break;
	}
	return '';
}

/**
 * xdatetime 
 * 轉換資料庫要儲存的日期時間格式
 * _TIME_CHARSET 在設定檔中根據資料庫的欄位型別，設定為 datetime 或 bigint
 * 
 * @param mixed $t 
 * @access public
 * @return void
 */
function dbtime($t=NULL){
	if(is_null($t)){
		if(_TIME_CHARSET=='datetime'){
			return _SYS_DATETIME;
		}
		else{
			return _SYS_TIMESTAMP;
		}
	}
	else{
		if(!is_numeric($t) && _TIME_CHARSET=='datetime'){
			return $t;
		}
		else if(is_numeric($t) && _TIME_CHARSET=='bigint'){
			return $t;
		}
		else if(_TIME_CHARSET=='datetime'){
			return date('Y-m-d H:i:s',$t);
		}
		else{
			return strtotime($t);
		}
	}
	return $t;
}

/**
 * 時間函式，替代原生的date
 * @param string $format 時間格式
 * @param timestamp/null $t 時間戳記
 * @return string
 */
function xdate($format=NULL,$t=NULL){
	if(is_null($t)){
		$t=_SYS_TIMESTAMP;
		if($format=='datetime' || $format=='now' || $format=='Y-m-d H:i:s') return _SYS_DATETIME;
		else if($format=='date' || $format=='Y-m-d') return _SYS_DATE;
	}
	else if(!is_numeric($t) && strlen($t)>6){
		$t=strtotime($t);
	}
	if($format=='datetime' || $format=='now') $format='Y-m-d H:i:s';
	else if($format=='date') $format='Y-m-d';
	else if($format=='time') $format='H:i:s';
	else if($format=='date-min') $format='Y-m-d H:i';
	else if($format=='chinese-date'){
		return sprintf('%d-%s',date('Y',$t)-1911,date('m-d',$t));
	}
	else if($format=='chinese-date-min'){
		return sprintf('%d-%s',date('Y',$t)-1911,date('m-d H:i',$t));
	}
	else if($format=='chinese-date-time'){
		return sprintf('%d-%s',date('Y',$t)-1911,date('m-d H:i:s',$t));
	}
	return date($format,$t);
}

function pwdhash($pwd){
	return substr(sha1(md5($pwd)),3,20);
}

/**
 * pwd_encrypt
 * 加密使用者輸入的密碼，使用 etc/config.php 的 _USER_PASSWORD_HASHSEED 當作加密種子
 * 若未設定加密種子，則使用 pwdhash()
 *
 */
function pwd_encrypt($pwd){
	if(!defined('_USER_PASSWORD_HASHSEED')){
		return pwdhash($pwd);
	}
	return md5(sha1($pwd)._USER_PASSWORD_HASHSEED);
}

/**
 * upload_get
 * 取得上傳檔案的資訊
 * 
 * @param mixed $attaches 暫存檔流水號，numeric or array(numeric,...)，不檢查格式是否正確
 * @access public
 * @return array 若傳入值為一個，則回傳一個上傳檔案資訊的陣列(索引為name,ext,header,saved,capacity)
 *   若傳入值為多個，則回傳為以暫存檔流水號為索引的上傳檔案資訊陣列
 */
function upload_get($attaches){
	if(is_numeric($attaches)){
		$query=array(
			'select'=>'',
			'from'=>'tempfiles',
			'where'=>'tempfilePky='.$attaches,
		);
		$uploads=$GLOBALS['db']->row('sess',$query);
		if($uploads){
			$uploads=array(
				'name'=>$uploads['tempfileSourceName'],
				'ext'=>$uploads['tempfileExtname'],
				'header'=>$uploads['tempfileHeaderType'],
				'saved'=>$uploads['tempfileSaveName'],
				'capacity'=>$uploads['tempfileCapacity'],
			);
			return $uploads;
		}
	}
	else if(is_array($attaches)){
		$query=array(
			'select'=>'',
			'from'=>'tempfiles',
			'where'=>'tempfilePky IN ('.join(',',$attaches).')'
		);
		$uploads=$GLOBALS['db']->select('vgroup',$query);
		if($uploads['args']['num']){
			for($i=0;$i<$uploads['args']['num'];$i++){
				$uploads[$uploads['d'][$i]['tempfilePky']]=array(
					'name'=>$uploads['d'][$i]['tempfileSourceName'],
					'ext'=>$uploads['d'][$i]['tempfileExtname'],
					'header'=>$uploads['d'][$i]['tempfileHeaderType'],
					'saved'=>$uploads['d'][$i]['tempfileSaveName'],
					'capacity'=>$uploads['d'][$i]['tempfileCapacity'],
				);
			}
			return $uploads;
		}
	}
	return NULL;
}

/**
 * 取得表單資料
 * @param string $k 值的索引
 *	若是 array，則使用如 item.0 或 item.0~15 或 item. 或 item.0~
 *   1. item.0 代表取得 $_GET['item'][0]
 *   2. item.0~15 代表取得 $_GET['item'][0] ~ $_GET['item'][15]
 *   3. item.0~ 代表取得 $_GET['item'][0] 到連續數字陣列結束
 *   4. item. 代表取得 $_GET['item'] 裡的所有元素
 *   5. item,,, 代表取得 $_GET['item'] 裡 , 分隔的所有元素 ex: item,a,b,c 代表要取得 $_GET['item']['a'] , $_GET['item']['b'] , $_GET['item']['c']
 * @param string $charset 值的型別/型態
 * @param string $method 哪種HTTP變數，get/post/cookie/request
 * @param mixed $param 輸入字串過濾規則，若$charset為文字類型，則表示最大長度，若$charset為數字類型，則表示數字區間，若$charset為日期時間類型，則表示日期時間範圍，
 *   並且不考慮$param開發者輸入錯誤的狀況
 * @see cval()
 * @return mixed 根據$k的值比對是否符合$charset，若否則回傳預設值
 */
function form($k, $charset='int',$method='get',$param=''){
	$getarray=0;
	$defData=array(
		'int'=>0,
		'integer'=>0,
		'uint'=>0,		// unsigned integer
		'numeric'=>'0',	// number format,流水號使用,^[1-9]\d*$
		'pky'=>'0',		// for pky
		'uid'=>'',		// UUID
		'string'=>'',
		'safe'=>'', 	// [\w_\-\.:]*
		'strip_tag'=>'',
		'strip_unsafe'=>'',
		'ansii'=>'',
		'letter'=>'',
		'float'=>0.0,
		'float1'=>0.0,
		'float2'=>0.00,
		'float3'=>0.000,
		'float4'=>0.0000,
		'ufloat'=>0.0,
		'ufloat1'=>0.0,
		'ufloat2'=>0.00,
		'ufloat3'=>0.000,
		'ufloat4'=>0.0000,
		'boolean'=>0,
		'date'=>'',
		'date/valid'=>'',
		'year'=>'',
		'year-month'=>'',
		'time'=>'',
		'hour'=>0,	// 這是日期時間型別中比較特別的，是回傳0~23正整數
		'hour-min'=>'',
		'date-min'=>'',
		'date-min/valid'=>'',
		'time8601'=>'',
		'datetime'=>'',
		'datetime/valid'=>'',
		'email'=>'',
		'email/valid'=>'',
		'tel'=>'',// 電話號碼/手機號碼，^(\+\d+\-)?[\d\-]+\d$
		'color'=>'',
		'url'=>'',
	);
	if(!isset($defData[$charset]))return false;
	if(strpos($k,'.')){
		$k1=substr($k,0,strpos($k,'.'));
	}
	else if(strpos($k,',')){
		$k1=substr($k,0,strpos($k,','));
	}
	else{
		$k1=$k;
	}
	$method=strtolower($method);
	if(!strcmp($method,'post')){
		// $method=isset($_POST[$k1])?$_POST[$k1]:(strpos($k,'.')?array():$defData[$charset]);
		if(isset($_POST[$k1])){
			$method=$_POST[$k1];
		}
		else{
		   	if(strpos($k,'.') || strpos($k,',')){
				$method=array();
			}
			else{
				return $defData[$charset];
			}
		}
	}
	else if(!strcmp($method,'request')){
		$method=isset($_POST[$k1])?$_POST[$k1]:(isset($_GET[$k1])?$_GET[$k1]:$defData[$charset]);
	}
	else if(!strcmp($method,'cookie')){
		$method=isset($_COOKIE[$k1])?$_COOKIE[$k1]:$defData[$charset];
	}
	else if(!strcmp($method,'server')){
		$method=isset($_SERVER[$k1])?$_SERVER[$k1]:$defData[$charset];
	}
	else{
		if(isset($_GET[$k1])){
			$method=$_GET[$k1];
		}
		else{
			if(strpos($k,'.') || strpos($k,',')){
				$method=array();
			}
			else{
				return $defData[$charset];
			}
		}
	}
	if(is_array($method)){
		if(strpos($k,'.')){
			list($k,$s)=explode('.',$k);
			if(!strlen($s)){
				$getarray=4;
			}
			else{
				$s=explode('~',$s);
				$s1=$s[0];
				if(isset($s[1])){
					$s2=$s[1];
					$s=$s1.'~'.$s2;
				}
				else $s2='';
				if(is_numeric($s)){
					$getarray=1;
				}
				else if(is_numeric($s1) && is_numeric($s2)){
					$getarray=2;
				}
				else if(is_numeric($s1) && !is_numeric($s2)){
					$getarray=3;
				}
				else{
					$getarray=0;
				}
				$s1=intval($s1,10);
				$s2=intval($s2,10);
			}
		}
		else if(strpos($k,',')){
			$s=explode(',',$k);
			if(count($s)>1){
				$getarray=5;
			}
		}
	}
	if(is_null($method) && in_array($getarray,array(0,4))){
		return $defData[$charset];
	}
	if(!$getarray){
		if(in_array($charset,array('string','safe','ansii','letter','strip_tag','strip_unsafe','uid','pky'))){
			return (strlen($method)?cval($method,$charset,$param):$defData[$charset]);
		}
		else{
			return (!empty($method)?cval($method,$charset,$param):$defData[$charset]);
		}
	}
	else{
		$tmp=array();
		if($getarray==4){
			foreach($method as $k => $v){$tmp[$k]=!isset($v)?$defData[$charset]:cval($v,$charset,$param);}
			return $tmp;
		}
		else if($getarray==1){
			return !isset($method[$s1])?$defData[$charset]:cval($method[$s1],$charset,$param);
		}
		else if($getarray==2){
			for($i=$s1;$i<=$s2;$i++){$tmp[$i]=!isset($method[$i])?$defData[$charset]:cval($method[$i],$charset,$param);}
			return $tmp;
		}
		else if($getarray==3){
			for($i=$s1;isset($method[$i]);$i++){$tmp[$i]=!isset($method[$i])?$defData[$charset]:cval($method[$i],$charset,$param);}
			return $tmp;
		}
		else if($getarray==5){
			for($i=1,$n=count($s);$i<$n;$i++){$tmp[$s[$i]]=!isset($method[$s[$i]])?$defData[$charset]:cval($method[$s[$i]],$charset,$param);}
			return $tmp;
		}
	}
}

/**
 * form_grid 
 * 
  $tableset=array(
  		array('field'=>'name',	'charset'=>'string',	'valid'=>'empty',	'default'=>''),
  		array('field'=>'job',	'charset'=>'string',	'valid'=>'isset',	'default'=>''),
  		array('field'=>'onduty','charset'=>'date',		'valid'=>'isset',	'default'=>'2016-12-31'),
  		array('field'=>'duty',	'charset'=>'boolean',	'valid'=>'isset',	'default'=>0),
  		array('field'=>'cdate',	'charset'=>'date',		'valid'=>'isset',	'default'=>'2016-12-31'),
  		array('field'=>'hours',	'charset'=>'ufloat',	'valid'=>'1~5',	'default'=>1),
  );
 * field: 欄位名稱
 * charset: 欄位資料型別
 * valid: 驗證方式，預設 isset
 * default: 空值時的預設值
 *
 * 表單 input name: {$main}_{$tables[$i]['field']}[]
 *
 * @param mixed $main 
 * @param mixed $tableset 
 * @param string $method 
 * @param boolean $trim_for_execl 是否要 trim 掉無法 excel 匯出的字元，例如 /^=/
 * @access public
 * @return void
 */
function form_grid($main, $tableset, $method='post', $trim_for_excel=FALSE){
	$num_field=count($tableset);
	$num_count=array();
	$tf=array();
	for($i=0;$i<$num_field;$i++){
		$tf[$tableset[$i]['field']]=form($main.'_'.$tableset[$i]['field'].'.',$tableset[$i]['charset'], $method);
		$defaults[$tableset[$i]['field']]=$tableset[$i]['default'];
		if(is_array($tf[$tableset[$i]['field']]) && count($tf[$tableset[$i]['field']])>0){
			$keys=array_keys($tf[$tableset[$i]['field']]);
			$max_key=max($keys);
			$num_count[]=$max_key;
		}
	}
	if(count($num_count)){
		$num=max($num_count);
	}
	else{
		$num=0;
	}
	$c=0;
	$data=array();
	for($i=0;$i<=$num;$i++){
		$valid=TRUE;
		for($j=0;$j<$num_field;$j++){
			if(empty($tf[$tableset[$j]['field']][$i]) && $tableset[$j]['valid']=='empty'){
				$valid=FALSE;
				break;
			}
			else if(!isset($tf[$tableset[$j]['field']][$i])){
				$tf[$tableset[$j]['field']][$i]=$defaults[$tableset[$j]['field']];
			}
			else if(preg_match('/^(\d+)~(\d+)$/',$tableset[$j]['valid'],$m)){
				if($tf[$tableset[$j]['field']][$i]<$m[1] || $tf[$tableset[$j]['field']][$i]>$m[2]){
					$tf[$tableset[$j]['field']][$i]=$defaults[$tableset[$j]['field']];
				}
			}
			else if($trim_for_excel){
				$tf[$tableset[$j]['field']][$i]=ltrim($tf[$tableset[$j]['field']][$i], '=');
			}
		}
		if(!$valid){ continue; }
		for($j=0;$j<$num_field;$j++){
			$data[$tableset[$j]['field']][$c]=$tf[$tableset[$j]['field']][$i];
		}
		$c++;
	}
	return $data;
}

/**
 * form_hgrid 
 * 和 form_grid() 一樣，只是二維的方向相反，index 先於欄位
  $tableset=array(
  		array('field'=>'name',	'charset'=>'string',	'valid'=>'empty',	'default'=>''),
  		array('field'=>'job',	'charset'=>'string',	'valid'=>'isset',	'default'=>''),
  		array('field'=>'onduty','charset'=>'date',		'valid'=>'isset',	'default'=>'2016-12-31'),
  		array('field'=>'duty',	'charset'=>'boolean',	'valid'=>'isset',	'default'=>0),
  		array('field'=>'cdate',	'charset'=>'date',		'valid'=>'isset',	'default'=>'2016-12-31'),
  		array('field'=>'hours',	'charset'=>'ufloat',	'valid'=>'1~5',	'default'=>1),
  );
 * field: 欄位名稱
 * charset: 欄位資料型別
 * valid: 驗證方式，預設 isset
 * default: 空值時的預設值
 *
 * 表單 input name: {$main}_{$tables[$i]['field']}[]
 *
 * @param mixed $main 
 * @param mixed $tableset 
 * @param string $method 
 * @access public
 * @return void
 */
function form_hgrid($main, $tableset, $method='post'){
	$dtmp=form_grid($main, $tableset, $method);
	$data=array();
	if(!is_array($dtmp)) return $dtmp;
	foreach($dtmp as $field => $v){
		$n=count($v);
		for($i=0;$i<$n;$i++){
			if(!isset($data[$i])) $data[$i]=array();
			$data[$i][$field]=$v[$i];
		}
	}
	return $data;
}

/**
 * excel_setheader 
 * 設定 $sheet 的表頭欄位
	$headers=array(
		array(
			'cell'=>'A1',				// 表示指定儲存格
			'cell'=>array('A1','B1'),	// 陣列表示合併儲存格
			'text'=>'',					// 字串，表示儲存格內容
			'bgcolor'=>'FFFFFF',		// 色碼，儲存格的背景色
		),
		.....
	);
 * 
 * @param mixed $sheet 傳入 $objPHPExcel->getActiveSheet(0)
 * @param mixed $headers 
 * @access public
 * @return void
 */
function excel_setheader(&$sheet, $headers){
	for($i=0,$n=count($headers);$i<$n;$i++){
		if(is_array($headers[$i]['cell'])){
			$cell=$headers[$i]['cell'][0].':'.$headers[$i]['cell'][1];
			$sheet->mergeCells($cell);
			$sheet->getStyle($cell)->applyFromArray(
				array(
					'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('argb' => empty($headers[$i]['bgcolor'])?'FFFFFF':$headers[$i]['bgcolor'])
					),
				)
			);
			$sheet->setCellValue($headers[$i]['cell'][0], $headers[$i]['text']);
		}
		else{
			$sheet->getStyle($headers[$i]['cell'])->applyFromArray(
				array(
					'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('argb' => empty($headers[$i]['bgcolor'])?'FFFFFF':$headers[$i]['bgcolor'])
					),
				)
			);
			$sheet->setCellValue($headers[$i]['cell'], $headers[$i]['text']);
		}
	}
}

/**
 * excel_cell 
 * 將數字的 col, row 轉成 excel 的儲存格位置
 * 0, 1 會轉為 A1
 * 1, 2 會轉為 B2
 *
 * @param mixed $col 
 * @param mixed $row 
 * @access public
 * @return void
 */
function excel_cell($col, $row){
	if($col>25){
		$c1=chr(floor($col/26)+64);
		$c2=chr($col%26+65);
		$c=$c1.$c2;
	}
	else{
		$c=chr($col+65);
	}
	return $c.$row;
}

/**
 * json_viewer 
 * 傳入陣列，顯示json結構(for 開發使用)
 * 
 * @param mixed $str_json 
 * @access public
 * @return void
 */
function json_viewer_parse($ar,$dep){
	foreach($ar as $index => $val){
		if(is_array($val)){
			if(!count($val)){
				echo '<div class="is-empty-array" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> =&gt; array()</div>'."\n";
			}
			else{
				echo '<div class="is-array" data-index="'.HTML($index,FALSE).'"><span class="index array">'.$index.'</span> &nbsp; <span class="folder">-</span> =&gt; <div class="array-box">'."\n";
				json_viewer_parse($val, $dep+1);
				echo '</div></div>';
			}
		}
		else{
			$g=strtolower(gettype($val));
			switch($g){
				case 'boolean':
					echo '<div class="is-boolean" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> ('.$g.') =&gt; <span>'.($val?'TRUE':'FALSE').'</span></div>'."\n";
					break;
				case 'double':case 'float':case 'integer':
					echo '<div class="is-number" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> ('.$g.') =&gt; <span>'.$val.'</span></div>'."\n";
					break;
				case 'resource':case 'object':
					echo '<div class="is-resource" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> ('.$g.') =&gt; <span>'.$val.'</span></div>'."\n";
					break;
				case 'NULL':
					echo '<div class="is-null" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> ('.$g.') =&gt; <span>NULL</span></div>'."\n";
					break;
				case 'string':
					echo '<div class="is-string" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> ('.$g.') =&gt; <span>&quot;'.$val.'&quot;</span></div>'."\n";
					break;
				default:
					echo '<div class="is-unknown" data-index="'.HTML($index,FALSE).'"><span class="index">'.$index.'</span> ('.$g.') =&gt; <span>'.$val.'</span></div>'."\n";
					break;

			}
		}
	}
}

function remote_addr(){
	if(isset($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP'];
	if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
	if(isset($_SERVER['HTTP_X_FORWARDED'])) return $_SERVER['HTTP_X_FORWARDED'];
	if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
	if(isset($_SERVER['HTTP_FORWARDED_FOR'])) return $_SERVER['HTTP_FORWARDED_FOR'];
	if(isset($_SERVER['HTTP_FORWARDED'])) return $_SERVER['HTTP_FORWARDED'];
	if(isset($_SERVER['REMOTE_ADDR'])) return $_SERVER['REMOTE_ADDR'];
	if(isset($_SERVER['HTTP_VIA'])) return $_SERVER['HTTP_VIA'];
	return '0.0.0.0';
}

/**
 * remote_addr_log 
 * 回傳為字串型態，使用a10.10.100.100;b20.20.200.200;c.....
 * 用分號分隔，第一個字母代表IP類別
 * 
 * @access public
 * @return void
 */
function remote_addr_log(){
	$types=array(
		'a'=>'HTTP_CLIENT_IP',
		'b'=>'HTTP_X_FORWARDED_FOR',
		'c'=>'HTTP_X_FORWARDED',
		'd'=>'HTTP_X_CLUSTER_CLIENT_IP',
		'e'=>'HTTP_FORWARDED_FOR',
		'f'=>'HTTP_FORWARDED',
		'g'=>'REMOTE_ADDR',
		'h'=>'HTTP_VIA',
	);
	foreach($types as $id => $idnex){
		if(isset($_SERVER[$index])){
			$types[$id]=$id.$_SERVER[$index];
		}
		else{
			$types[$id]=$id;
		}
	}
	return join(';',$types);
}
/**
 * remote_addr_all 
 * 所有可能的 remote_add，全部紀錄，回傳為陣列
 * 
 * @access public
 * @return void
 */
function remote_addr_all(){
	$remotes=array();
	$server_list=array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR', 'HTTP_VIA');

	for($i=0,$n=count($server_list);$i<$n;$i++){
		if(!empty($_SERVER[$server_list[$i]])){
			$remotes[strtolower($server_list[$i])]=$_SERVER[$server_list[$i]];
		}
	}
	return $remotes;
}

function json_viewer($ar,$dep=0){
?>
<style type="text/css" media="screen">
div {font-size:12px;}
.is-empty-array {border:1px dashed #999;margin-bottom:2px;}	
.is-array .array-box {border:1px solid #ccc;border-right-style:solid;padding-left:1em;margin-bottom:2px;}
.array-hover {background-color:#ff9;}
.index {cursor:pointer;}
.is-boolean {color:#00f;}
.is-number {color:#f00;}
.is-resource {color:#0f0;}
.is-null {color:#666;}
.is-string {color:#009;}
.is-unknown {color:#aa0;}
.array-hover > .array-box > [class^=is] > span.index {font-weight:bold;color:#090;}
.array-hover > span.index {color:#c0c;font-weight:bold;}
.folder {padding-left:0.5em;padding-right:0.5em;}
.folder:before {content:"(";}
.folder:after {content:")";}
.folder:hover {background-color:#f00;color:#ff0;}
#info {position:fixed;left:5px;bottom:0;min-height:2.5em;width:90%;background:#ff0;border:1px solid #009;line-height:1.2em;}
</style>
<?php
	json_viewer_parse($ar,$dep);
?>
	<div id="info">
		<div>PATH: <span id="path"></span></div>
		<div>CHILDREN: <span id="children"></span></div>
	</div>
<script type="text/javascript" src="http://mac.localhost/tools/jquery.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.index').click(function(){
			var $obj=$(this);
			var path=[];
			var children=[];
			$obj.parents().each(function(){
				var $o=$(this);
				if($o.hasClass('array-box') || $o.is('body, html')){
				}
				else{
					path.unshift($o.data('index'));
				}
			});
			if($obj.hasClass('array')){
				$obj.nextAll().filter('.array-box').children().each(function(){
					var $o=$(this);
					children.push($o.data('index'));
				});
				$('#children').html(children.join(', '));
			}
			else{
				$('#children').html('');
			}
			$obj.parent().toggleClass('array-hover');
			$('#path').html(path.join(' / '));
		});
		$('.folder').click(function(){
			var $obj=$(this);
			var closed=$obj.data('closed');
			if(closed){
				$obj.nextAll().filter('.array-box').css({"display":"block"});
				$obj.html('-');
				$obj.data('closed',0);
			}
			else{
				$obj.nextAll().filter('.array-box').css({"display":"none"});
				$obj.html('+');
				$obj.data('closed',1);
			}

		});
		/**
		$('.array-box').mouseover(function(event){
			event.stopPropagation();
			$(this).addClass('array-hover');
		});
		$('.array-box').mouseout(function(event){
			event.stopPropagation();
			$(this).removeClass('array-hover');
		});
		**/
	});
</script>
<?php
}

/**
function setbox($module,$cmd,$args=NULL,&$returns=NULL){
	$prog=_DIR_APP.'module/'.$module.'/#'.$cmd.'.php';
	$tpl=_DIR_APP.'templet/'.$module.'/#'.$cmd.'.phtml';

	if(!is_file($prog) ||
		!is_file($tpl)){
		return false;
	}
	if(is_array($args)){
		foreach($args as $k => $v){
			$$k=$v;
		}
	}
	global $db;
	global $me;
	$module_temporary=isset($GLOBALS['cz']->page['module'])?$GLOBALS['cz']->page['module']:'';
	$GLOBALS['cz']->page['module']=$module;
	include($prog);
	include($tpl);
	$GLOBALS['cz']->page['module']=$module_temporary;
}
 **/

/**
 * maillink
 * 避免在 html source code 直接顯示 email address，需搭配 javascript 將正確的 email address 改寫回去
 * @param email $emailaddress 原本的email address
 * @param string $words 要顯示在連結上的文字
 * @param string $css 在連結上套用css class
 */
function maillink($emailaddress,$words,$css=''){
	$emailaddress=strrev($emailaddress);
	$emailaddress=strtr($emailaddress,array('.'=>'#46;','@'=>'#64;'));
	echo '<a href="http:/'.'/www.'.$GLOBALS['cf']['vhostFQDN'].'" m="'.$emailaddress.'" class="mailto '.$css.'">'.$words.'</a>';
}

/**
 * form的附屬函式
 * @see form()
 * @param mixed $var1 傳入的值
 * @param string $charset 值的型別/型態
 * @param mixed 數字及日期可以設定區間範圍，字串則設定長度範圍
 * @return mixed 根據$charset決定$var1是否符合，若符合則回傳$var1，若否則傳回預設值
 */
function cval($var1,$charset,$param){
//	$unicode_whitespaces=array(
//		chr(194).chr(160)=>' ',
//		chr(227).chr(128)=>' ',
//		chr(226).chr(128).chr(172).chr(226).chr(128).chr(172).chr(30)=>' ',
//	);
//	$var1=strtr($var1,$unicode_whitespaces);

	switch($charset){
	case 'int': case 'integer':
		$var1=trim(str_replace('$','',str_replace(',','',$var1)));
		$var1=intval($var1,10);
		if(strlen($param)){
			/**
			 * 表示數字範圍，空字串表示沒有極限
			 * 例如 ~1 表示 <=1，1~ 表示 >=1，-2~0x11 表示 -2<= <=17
			 */
			$param=explode('~',$param);
			if(strlen($param[0]) && strlen($param[1])){
				$param[0]=intval($param[0],10);
				$param[1]=intval($param[1],10);
				if($param[1]<$param[0]) return 0;
				if($var1<$param[0] || $var1>$param[1]) $var1=0;
			}
			else if(strlen($param[0])){
				$param[0]=intval($param[0],10);
				if($var1<$param[0]) $var1=0;
			}
			else if(strlen($param[1])){
				$param[1]=intval($param[1],10);
				if($var1>$param[1]) $var1=0;
			}
		}
		return $var1;
		break;
	case 'uint':
		$var1=trim(str_replace('$','',str_replace(',','',$var1)));
		$var1=intval($var1,10);
		if($var1<0) $var1=0;
		if(strlen($param)){
			/**
			 * 表示數字範圍，空字串表示沒有極限
			 * 例如 ~1 表示 <=1，1~ 表示 >=1，-2~0x11 表示 -2<= <=17
			 */
			$param=explode('~',$param);
			if(strlen($param[0]) && strlen($param[1])){
				$param[0]=intval($param[0],10);
				$param[1]=intval($param[1],10);
				if($param[1]<$param[0]) return 0;
				if($var1<$param[0] || $var1>$param[1]) $var1=0;
			}
			else if(strlen($param[1])){
				$param[1]=intval($param[1],10);
				if($var1>$param[1]) $var1=0;
			}
			else if(strlen($param[0])){
				$param[0]=intval($param[0],10);
				if($var1<$param[0]) $var1=0;
			}
		}
		return $var1;
	case 'numeric':
	case 'pky':
		$var1=trim($var1);
		if(!is_numeric($var1)) return '0';
		else if(!preg_match('/^[1-9][0-9]*$/',$var1)) return '0';
		return strval($var1);
		break;
	case 'uid':
		if(!preg_match('/^[a-z0-9]+([\-a-z0-9]+)[a-z0-9]+$/',$var1)) return '';
		return strval($var1);
		break;
	case 'string':
		$var1=trim(strval($var1));
		/**
		 * 表示字串最大長度
		 */
		if(strlen($param)){
			$len=mb_strlen($var1);
			$param=intval($param,10);
			if($len>$param) $var1=mb_substr($var1,0,$param);
		}
		return $var1;
		break;
	case 'safe':
		$var1=trim(strval($var1));
		$var1=preg_match('/^[\w_\.\-:]*$/',$var1)?$var1:'';
		/**
		 * 表示字串最大長度
		 */
		if(strlen($param)){
			$len=strlen($var1);
			$param=intval($param,10);
			if($len>$param) $var1=substr($var1,0,$param);
		}
		return $var1;
		break;
	case 'strip_tag':
		$var1=trim(strval($var1));
		$var1=strip_tags($var1);

		if(strlen($param)){
			$len=strlen($var1);
			$param=intval($param,10);
			if($len>$param) $var1=substr($var1,0,$param);
		}
		return $var1;
		break;
	case 'strip_unsafe':
		$var1=trim(strval($var1));
		$var1=strip_tags($var1);
		$var1=strtr($var1,array('\''=>'','"'=>'','\\'=>''));

		if(strlen($param)){
			$len=strlen($var1);
			$param=intval($param,10);
			if($len>$param) $var1=substr($var1,0,$param);
		}
		return $var1;
		break;
	case 'ansii':
		$var1=trim(strval($var1));
		$var1=preg_match('/^[\x01-\xfe]*$/',$var1)?$var1:'';
		/**
		 * 表示字串最大長度
		 */
		if(strlen($param)){
			$len=strlen($var1);
			$param=intval($param,10);
			if($len>$param) $var1=substr($var1,0,$param);
		}
		return $var1;
		break;
	case 'letter':
		$var1=trim(strval($var1));
		$var1=preg_match('/^[a-zA-Z0-9]*$/',$var1)?$var1:'';
		/**
		 * 表示字串最大長度
		 */
		if(strlen($param)){
			$len=strlen($var1);
			$param=intval($param,10);
			if($len>$param) $var1=substr($var1,0,$param);
		}
		return $var1;
		break;
	case 'float':case 'currency':
	case 'float1': case 'float2': case 'float3': case 'float4':
		$var1=trim(str_replace('$','',str_replace(',','',$var1)));
		$var1=floatval($var1);
		if(strlen($param)){
			/**
			 * 表示數字範圍，空字串表示沒有極限
			 * 例如 ~1.0 表示 <=1.0，1.1~ 表示 >=1.1，-2~13.1 表示 -2.0<= <=13.1
			 */
			$param=explode('~',$param);
			if(strlen($param[0]) && strlen($param[1])){
				$param[0]=floatval($param[0]);
				$param[1]=floatval($param[1]);
				if($param[1]<$param[0]) return 0;
				if($var1<$param[0] || $var1>$param[1]) $var1=0;
			}
			else if(strlen($param[0])){
				$param[0]=floatval($param[0],10);
				if($var1<$param[0]) $var1=0;
			}
			else if(strlen($param[1])){
				$param[1]=floatval($param[1],10);
				if($var1>$param[1]) $var1=0;
			}
		}
		if(in_array($charset,array('float1','float2','float3','float4'))){
			$im=substr($charset,-1);
			$var1=sprintf('%.'.$im.'f',$var1);
		}
		return $var1;
		break;
	case 'ufloat':
	case 'ufloat1': case 'ufloat2': case 'ufloat3': case 'ufloat4':
		$var1=trim(str_replace('$','',str_replace(',','',$var1)));
		$var1=floatval($var1);
		$var1=$var1<0.0 ? 0.0 : $var1;
		if(strlen($param)){
			/**
			 * 表示數字範圍，空字串表示沒有極限
			 * 例如 ~1 表示 <=1，1~ 表示 >=1，-2~0x11 表示 -2<= <=17
			 */
			$param=explode('~',$param);
			if(strlen($param[0]) && strlen($param[1])){
				$param[0]=floatval($param[0]);
				$param[1]=floatval($param[1]);
				if($param[1]<$param[0]) return 0.0;
				if($var1<$param[0] || $var1>$param[1]) $var1=0.0;
			}
			else if(strlen($param[1])){
				$param[1]=floatval($param[1]);
				if($var1>$param[1]) $var1=0.0;
			}
			else if(strlen($param[0])){
				$param[0]=floatval($param[0]);
				if($var1<$param[0]) $var1=0.0;
			}
		}
		if(in_array($charset,array('ufloat1','ufloat2','ufloat3','ufloat4'))){
			$im=substr($charset,-1);
			$var1=sprintf('%.'.$im.'f',$var1);
		}
		return $var1;
		break;
	case 'boolean':
		$var1=trim($var1);
		return !strcmp($var1,'off')?0:(empty($var1)?0:1);
		break;
	case 'date':
	case 'date/valid':
		$var1=trim($var1);
		if(!preg_match('/^(\d{4})[\.\-\/](\d{1,2})[\.\-\/](\d{1,2})(\s[0-9:]{5,8})?$/',$var1,$m))return '';
		if(strlen($charset)==10){
			if(!checkdate(intval($m[2],10), intval($m[3],10), intval($m[1],10))){ return ''; }
		}
		$var1=sprintf('%04d-%02d-%02d',$m[1],$m[2],$m[3]);
		/**
		 * $param 的格式為 Y-m-d~Y-m-d
		 */
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==10 && strlen($param[1])==10){
				if($var1<$param[0] || $var1>$param[1]) $var1='';		
			}
			else if(strlen($param[1])==10){
				if($var1>$param[1]) $var1='';
			}
			else if(strlen($param[0])==10){
				if($var1<$param[0]) $var1='';
			}		
		}
		return $var1;
		break;
	case 'year':
		$var1=trim($var1);
		if(!is_numeric($var1) || strlen($var1)!=4) return '';
		else if(!in_array(substr($var1,0,1),array('1','2'))) return '';
		/**
		 * $param 的格式為 Y~Y
		 */
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==4 && strlen($param[1])==4){
				if($var1<$param[0] || $var1>$param[1]) $var1='';		
			}
			else if(strlen($param[1])==4){
				if($var1>$param[1]) $var1='';
			}
			else if(strlen($param[0])==4){
				if($var1<$param[0]) $var1='';
			}		
		}
		return $var1;
		break;
	case 'year-month':
		$var1=trim($var1);
		if(!preg_match('/^(\d{4})[\-\.\/](\d{1,2})$/',$var1,$m))return '';
		$var1=sprintf('%04d-%02d',$m[1],$m[2]);
		/**
		 * $param 的格式為 Y~Y
		 ?*/
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==7 && strlen($param[1])==7){
				if($var1<$param[0] || $var1>$param[1]) $var1='';		
			}
			else if(strlen($param[1])==7){
				if($var1>$param[1]) $var1='';
			}
			else if(strlen($param[0])==7){
				if($var1<$param[0]) $var1='';
			}		
		}
		break;
	case 'time':
	case 'time1':
	case 'time2':
		$var1=trim($var1);
		if(!preg_match('/^(\d{1,2}):(\d{1,2})(?::(\d{1,2}))?$/',$var1,$m)) return ($charset=='time'?'':($charset=='time1'?'00:00:00':'23:59:59'));
		
		if(!isset($m[3])) $m[3]=($charset=='time'?0:59);
		else if($m[3]<0 || $m[3]>59) $m[3]=($charset=='time'?0:59);

		if($m[2]<0 || $m[2]>59) $m[2]=($charset=='time'?0:59);

		if($m[1]<0) $var1=($charset=='time'?'00:00:00':'23:59:59');
		else $var1=sprintf('%02d:%02d:%02d',$m[1],$m[2],$m[4]);
		/**
		 * $param 的格式為H:i:s~H:i:s
		 */
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==8 && strlen($param[1])==8){
				if($var1<$param[0] || $var1>$param[1]) $var1=($charset=='time'?'':($charset=='time1'?'00:00:00':'23:59:59'));
			}
			else if(strlen($param[1])==8){
				if($var1>$param[1]) $var1=($charset=='time'?'':($charset=='time1'?'00:00:00':'23:59:59'));
			}
			else if(strlen($param[0])==8){
				if($var1<$param[0]) $var1=($charset=='time'?'':($charset=='time1'?'00:00:00':'23:59:59'));
			}		
		}
		return $var1;
		break;
	case 'hour':
		$var1=trim($var1);
		$var1=intval($var1,10);
		if($var1<0 || $var>23) return 0;
		return $var1;
		break;
	case 'hour-min':
		$var1=trim($var1);
		if(!preg_match('/^(\d{1,2}):(\d{1,2})(:\d{1,2})?$/',$var1,$m))return '';
		$var1=sprintf('%02d:%02d',$m[1],$m[2]);
		/**
		 * $param 的格式為H:i:s~H:i:s
		 */
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==5 && strlen($param[1])==5){
				if($var1<$param[0] || $var1>$param[1]) $var1='';		
			}
			else if(strlen($param[1])==5){
				if($var1>$param[1]) $var1='';
			}
			else if(strlen($param[0])==5){
				if($var1<$param[0]) $var1='';
			}		
		}
		return $var1;
		break;
	case 'date-min':
	case 'date-min/valid':
		$var1=trim($var1);
		if(!preg_match('/^(\d{4})[\.\-\/](\d{1,2})[\.\-\/](\d{1,2})\s(\d{1,2}):(\d{1,2})(:\d{1,2})?$/',$var1,$m))return '';
		if(strlen($charset)==14){
			if(!checkdate(intval($m[2],10), intval($m[3],10), intval($m[1],10))){ return ''; }
		}

		if($m[4]==24 && $m[5]==0){
			$m[4]=0;
			$m[5]=0;
		}
		else if($m[4]>=24){
			$m[4]=0;
		}
		$var1=sprintf('%04d-%02d-%02d %02d:%02d',$m[1],$m[2],$m[3],$m[4],$m[5]);
		/**
		 * $param 的格式為Y-m-d H:i~Y-m-d H:i
		 */
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==16 && strlen($param[1])==16){
				if($var1<$param[0] || $var1>$param[1]) $var1='';		
			}
			else if(strlen($param[1])==16){
				if($var1>$param[1]) $var1='';
			}
			else if(strlen($param[0])==16){
				if($var1<$param[0]) $var1='';
			}		
		}

		return $var1;
		break;
	case 'datetime':
	case 'datetime/valid':
	case 'datetime1':
	case 'datetime1/valid':
	case 'datetime2':
	case 'datetime2/valid':
		$var1=trim($var1);
		if(!preg_match('/^(\d{4})[\.\-\/](\d{1,2})[\.\-\/](\d{1,2})(\s+(\d{1,2}):(\d{1,2})(?::(\d{1,2}))?)?$/',$var1,$m))return (in_array($charset,array('datetime2','datetime2/valid','datetime1','datetime1/valid'))?'0000-00-00 00:00:00':'');
		if($charset=='datetime/valid'){
			if(!checkdate(intval($m[2],10),intval($m[3],10),intval($m[1],10))){ return ''; }
		}
		else if($charset=='datetime1/valid' || $charset=='datetime2/valid'){
			if(!checkdate(intval($m[2],10),intval($m[3],10),intval($m[1],10))){ return '00-00-00 00:00:00'; }
		}
		if(!isset($m[5])) $m[5]=(in_array($charset,array('datetime2','datetime2/valid'))?23:0);
		if(!isset($m[6])) $m[6]=(in_array($charset,array('datetime2','datetime2/valid'))?59:0);
		if(!isset($m[7])) $m[7]=(in_array($charset,array('datetime2','datetime2/valid'))?59:0);
		$var1=sprintf('%04d-%02d-%02d %02d:%02d:%02d',$m[1],$m[2],$m[3],$m[5],$m[6],$m[7]);
		/**
		 * $param 的格式為Y-m-d H:i:s~Y-m-d H:i:s
		 */
		if(strlen($param)){
			$param=explode('~',$param);
			if(strlen($param[0])==19 && strlen($param[1])==19){
				if($var1<$param[0] || $var1>$param[1]) $var1='';		
			}
			else if(strlen($param[1])==19){
				if($var1>$param[1]) $var1='';
			}
			else if(strlen($param[0])==19){
				if($var1<$param[0]) $var1='';
			}
		}
		return $var1;
		break;
	case 'time8601':
		/** YYYY-mm-ddTHH:ii:ssTZD, ex: 2012-07-21T03:15:59+07:00 **/
		if(!preg_match('/^\d{4}\-\d{2}\-\d{2}T\d{2}:\d{2}:\d{2}[\+\-]\d{2}:?\d{2}$/',$var1))return '';
		return $var1;
		break;
	case 'email':
		$var1=trim(strval($var1));
		if(!preg_match('/^[a-z0-9\.\-_]+@[a-z0-9\-]+(\.[a-z0-9\-]+)+$/i',$var1)) return '';
		return $var1;
		break;
	case 'email/valid':
		$var1=trim(strval($var1));
		if(substr_count($var1,'@')!=1)return '';
		list($id,$host)=explode('@',$var1);
		if(!strlen($id) || !strlen($host))return '';
		$host=strtolower($host);
		if(!preg_match('/^[a-z0-9\.\-_]+$/',$id))return '';
		if(substr_count($host,'.')<1)return '';
		$record=dns_get_record($host,DNS_MX);
		if(empty($record))return '';
		return $id.'@'.$host;
		break;
	case 'tel':
		$var1=str_replace(' ','',$var1);
		if(preg_match('/^(\(?(\+\d+)\)?)?\d[\d\-#]+\d$/',$var1)){
			return $var1;
		}
		return '';
		break;
	case 'color':
		$var1=str_replace(' ','',$var1);
		if(preg_match('/^#([0-9a-fA-F]+)$/',$var1,$m)){
			if(strlen($m[0])==3){
				return $var1;
		    }
			else if(strlen($m[0])==6){
				$m[0]=strtolower($m[0]);
				if(substr($m[0],0,1)==substr($m[0],1,1) && substr($m[0],2,1)==substr($m[0],3,1) && substr($m[0],4,1)==substr($m[0],5,1)){
					return '#'.substr($m[0],0,1).substr($m[0],2,1).substr($m[0],4,1);
				}
				else{
					return $var1;
				}
			}
			return '';
		}
		else if(preg_match_all('/^rgb\((\d+,\d+,\d+)\)$/',$var1,$m)){
			list($r,$g,$b)=explode(',',$m[0]);
			$r=intval($r,10)%256;
			$g=intval($g,10)%256;
			$b=intval($b,10)%256;
			if(floor($r/16)==$r%16 && floor($g/16)==$g%16 && floor($b/16)==$b%16){
				return '#'.dechex($r%16).dechex($g%16).dechex($b%16);
			}
			else{
				return sprintf('#%02s%02s%02s',dechex($r),dechex($g),dechex($b));
			}
		}
		else{
			$colors=array(
				'aliceblue'=>'#f0f8ff', 'antiquewhite'=>'#faebd7', 'aqua'=>'#0ff', 'aquamarine'=>'#7fffd4', 'azure'=>'#f0ffff', 
				'beige'=>'#f5f5dc', 'bisque'=>'#ffe4c4', 'black'=>'#000', 'blanchedalmond'=>'#ffebcd', 'blue'=>'#00f', 'blueviolet'=>'#8a2be2', 'brown'=>'#a52a2a', 'burlywood'=>'#deb887', 
				'cadetblue'=>'#5f9ea0', 'chartreuse'=>'#7fff00', 'chocolate'=>'#d2691e', 'coral'=>'#ff7f50', 'cornflowerblue'=>'#6495ed', 'cornsilk'=>'#fff8dc', 'crimson'=>'#dc143c', 'cyan'=>'#0ff',
				'darkblue'=>'#00008b', 'darkcyan'=>'#008b8b', 'darkgoldenrod'=>'#b8860b', 'darkgray'=>'#a9a9a9', 'darkgrey'=>'#a9a9a9', 'darkgreen'=>'#006400', 'darkkhaki'=>'#bdb76b', 'darkmagenta'=>'#8b008b', 'darkolivegreen'=>'#556b2f', 'darkorange'=>'#ff8c00', 'darkorchid'=>'#9932cc', 'darkred'=>'#8b0000', 'darksalmon'=>'#e9967a', 'darkseagreen'=>'#8fbc8f', 'darkslateblue'=>'#483d8b', 'darkslategray'=>'#2f4f4f', 'darkslategrey'=>'#2f4f4f', 'darkturquoise'=>'#00ced1', 'darkviolet'=>'#9400d3', 'deeppink'=>'#ff1493', 'deepskyblue'=>'#00bfff', 'dimgray'=>'#696969', 'dimgrey'=>'#696969', 'dodgerblue'=>'#1e90ff', 
				'firebrick'=>'#b22222', 'floralwhite'=>'#fffaf0', 'forestgreen'=>'#228b22', 'fuchsia'=>'#f0f',
				'gainsboro'=>'#dcdcdc', 'ghostwhite'=>'#f8f8ff', 'gold'=>'#ffd700', 'goldenrod'=>'#daa520', 'gray'=>'#808080', 'grey'=>'#808080', 'green'=>'#008000', 'greenyellow'=>'#adff2f', 
				'honeydew'=>'#f0fff0', 'hotpink'=>'#ff69b4', 
				'indianred'=>'#cd5c5c', 'indigo'=>'#4b0082', 'ivory'=>'#fffff0', 
				'khaki'=>'#f0e68c',
				'lavender'=>'#e6e6fa', 'lavenderblush'=>'#fff0f5', 'lawngreen'=>'#7cfc00', 'lemonchiffon'=>'#fffacd', 'lightblue'=>'#add8e6', 'lightcoral'=>'#f08080', 'lightcyan'=>'#e0ffff', 'lightgoldenrodyellow'=>'#fafad2', 'lightgray'=>'#d3d3d3', 'lightgrey'=>'#d3d3d3', 'lightgreen'=>'#90ee90', 'lightpink'=>'#ffb6c1', 'lightsalmon'=>'#ffa07a', 'lightseagreen'=>'#20b2aa', 'lightskyblue'=>'#87cefa', 'lightslategray'=>'#789', 'lightslategrey'=>'#789', 'lightsteelblue'=>'#b0c4de', 'lightyellow'=>'#ffffe0', 'lime'=>'#0f0', 'limegreen'=>'#32cd32', 'linen'=>'#faf0e6', 
				'magenta'=>'#f0f', 'maroon'=>'#800000', 'mediumaquamarine'=>'#66cdaa', 'mediumblue'=>'#0000cd', 'mediumorchid'=>'#ba55d3', 'mediumpurple'=>'#9370db', 'mediumseagreen'=>'#3cb371', 'mediumslateblue'=>'#7b68ee', 'mediumspringgreen'=>'#00fa9a', 'mediumturquoise'=>'#48d1cc', 'mediumvioletred'=>'#c71585', 'midnightblue'=>'#191970', 'mintcream'=>'#f5fffa', 'mistyrose'=>'#ffe4e1', 'moccasin'=>'#ffe4b5', 
				'navajowhite'=>'#ffdead', 'navy'=>'#000080', 
				'oldlace'=>'#fdf5e6', 'olive'=>'#808000', 'olivedrab'=>'#6b8e23', 'orange'=>'#ffa500', 'orangered'=>'#ff4500', 'orchid'=>'#da70d6', 
				'palegoldenrod'=>'#eee8aa', 'palegreen'=>'#98fb98', 'paleturquoise'=>'#afeeee', 'palevioletred'=>'#db7093', 'papayawhip'=>'#ffefd5', 'peachpuff'=>'#ffdab9', 'peru'=>'#cd853f', 'pink'=>'#ffc0cb', 'plum'=>'#dda0dd', 'powderblue'=>'#b0e0e6', 'purple'=>'#800080',
				'red'=>'#f00', 'rosybrown'=>'#bc8f8f', 'royalblue'=>'#4169e1', 
				'saddlebrown'=>'#8b4513', 'salmon'=>'#fa8072', 'sandybrown'=>'#f4a460', 'seagreen'=>'#2e8b57', 'seashell'=>'#fff5ee', 'sienna'=>'#a0522d', 'silver'=>'#c0c0c0', 'skyblue'=>'#87ceeb', 'slateblue'=>'#6a5acd', 'slategray'=>'#708090', 'slategrey'=>'#708090', 'snow'=>'#fffafa', 'springgreen'=>'#00ff7f', 'steelblue'=>'#4682b4', 
				'tan'=>'#d2b48c', 'teal'=>'#008080', 'thistle'=>'#d8bfd8', 'tomato'=>'#ff6347', 'turquoise'=>'#40e0d0', 
				'violet'=>'#ee82ee', 
				'wheat'=>'#f5deb3', 'white'=>'#fff', 'whitesmoke'=>'#f5f5f5', 
				'yellow'=>'#ff0', 'yellowgreen'=>'#9acd32'
			);
			$var1=strtolower($var1);
			if(isset($colors[$var1])) return $colors[$var1];
		}
		return '';
		break;
	case 'url':
		$var1=trim($var1);
		if(preg_match('/^(https?:\/\/)?[\w\-]+(\.[\w\-]+)+(\/[a-zA-Z0-9#!\/+=%&_\.~?\-]*)?$/',$var1)){
			return $var1;
		}
		return '';
		break;
	}
}

/**
 * dbdate 
 * 給DB輸入資料用的，若為空字串則回傳預設值
 *
 * @param string $d
 * @param string $format
 * @access public
 * @return string
 */
function dbdate($d,$format='date'){
	if(strlen($d)) return $d;
	else {
		switch($format){
			case 'date':
				return '0000-00-00';
			case 'datetime':
				return '0000-00-00 00:00:00';
			case 'date-min':
				return '0000-00-00 00:00';
			case 'year':
				return '0000';
			default:
				return '0000-00-00 00:00:00';
		}
	}
}

/**
 * 延伸記錄
 * @deprecate
 * @param string $type 記錄的分類
 * @param string $dataKey 要統計的欄位代號
 * @param numeric $dataPky 該欄位的流水號(正整數)
 * @param int $val 值(正負整數)
 * @param string/null $logtime 記錄的時間(可以指定)
 * @param string/null $info 其它額外資訊(字串)
 */
function extlog($type,$dataKey,$dataPky,$val,$logtime=NULL,$info=NULL){
	$data=array(
		'logTime'=>is_null($logtime)?xdate('Y-m-d H:i:s'):$logtime,
		'userSky'=>$GLOBALS['session']->data['user']['pky'],
		'logType'=>$type,
		'logRowKey'=>$dataKey,
		'logDataKey'=>$dataPky,
		'logVal'=>$val,
		'logInfo'=>is_null($info)?'':$info
	);
	$GLOBALS['db']->add('system','log2',$data);
}

if(!function_exists('file_put_contents')){
	function file_put_contents($fname,$content){
		$fo=fopen($fname,"w");
		fputs($fo,$content);
		fclose($fo);
	}
}

/**
 * 解壓縮zip檔至指定的目錄
 */
function unzipFile2Dir($fileSrc,$dirTarget){
	$zip = zip_open($fileSrc);
	while(($zip_entry = zip_read($zip))!==false) {
		$entry = zip_entry_open($zip,$zip_entry);
		$filename = zip_entry_name($zip_entry);
		$filesize = zip_entry_filesize($zip_entry);
		if (is_dir($dirTarget) || mkdir($dirTarget)) {
			if ($filesize > 0) {
				$contents = zip_entry_read($zip_entry, $filesize);
				file_put_contents($dirTarget.$filename,$contents);
			}
		}
	}
}

function refreshURL($url,$delaySecond=0,$message=0,$showmessage=NULL){
	if(!$url){$url='index.php';}
	//else{$url='index.'._FILE_EXTEND.$url;}
	if(!$message && !$delaySecond)$message='';
	else if(!$delaySecond)$url=$url.'?'.$message;
	else if(!$message)$message='';
	if(class_exists('ME')){
		ME::close();
	}
?><!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta http-equiv="refresh" content="<?php echo $delaySecond; ?>;URL=<?php echo $url; ?>" />
<?php
if(!$delaySecond){
?>
<script language="javascript" type="text/javascript">
	self.location="<?php echo $url; ?>";
</script>

<?php
}
else if($message!=0){
?>
<script language="javascript" type="text/javascript">
	alert('<?php echo $showmessage; ?>');
</script>
<?php }?>
</head>
<?php
if(!is_null($showmessage)){
?>
	<body>
		<?php //echo $showmessage; ?>
	</body>
<?php } ?>
</html>
<?php
exit(1);
}


/**
 * e_parse 
 * 處理url中的$_GET['e']
 * rewrite會將網址的路徑轉換為$_GET['e']
 * 可能的路徑格式為
 * run/module/command/p/q
 * 這邊不處理直接要求menuPky的部份
 * 
 * @param mixed $e 
 * @access public
 * @return void
 */
function e_parse($e){
	$uri=array(
		'method'=>'',
		'module'=>'',
		'command'=>'',
		'resource'=>'html',// html, json, xml, csv
		'p'=>'',
		'q'=>'',
	);
	if(!$e) return $uri;
	if(strpos($e,'-<')){
		$e=preg_replace('/\-<[^>]*>/','',$e);
	}
	$e=trim($e, '/');
	$e=explode('/',$e);

	$e_num=count($e);
	if($e_num>0){
		$uri['method']=$e[0];
		if($e_num>1){
			$uri['module']=$e[1];
			if($e_num>2){
				$n=strrpos($e[2],'.');
				if($n){
					$resource=substr($e[2],$n+1);
					if(in_array($resource,array('json','xml','csv','txt','html','xhtml','cfm','asp','aspx','php','jsp','phtml','phtm'))){
						$uri['resource']=strtolower($resource);
						$uri['command']=substr($e[2],0,$n);
					}
					else $uri['command']=$e[2];
				}
				else $uri['command']=$e[2];
				if($e_num>3){
					$e[3]=explode('.',$e[3]);
					$uri['p']=$e[3][0];
					if(isset($e[3][1]) && strlen($e[3][1])){
						if(in_array($e[3][1],array('json','xml','csv','txt'))){
							$uri['resource']=strtolower($e[3][1]);
						}
						else if(in_array($e[3][1],array('html','xhtml','phtml','asp','aspx','jsp','php','cfm'))){
							$uri['resource']='html';
						}
					}
					if($e_num>4){
						$e[4]=explode('.',$e[4]);
						$uri['q']=$e[4][0];
						if(isset($e[4][1]) && strlen($e[4][1])){
							if(in_array($e[4][1],array('json','xml','csv','txt'))){
								$uri['resource']=strtolower($e[4][1]);
							}
							else if(in_array($e[4][1],array('html','xhtml','phtml','asp','aspx','jsp','php','cfm'))){
								$uri['resource']='html';
							}
						}
					}
				}
			}
			else{
				$uri['command']='index';
			}
		}
	}
	if(!strcmp($uri['method'],'crontab') && !_INSIDE) $uri['method']='run';
	return $uri;
}

/**
 * ef
 * 將路徑以escape_fragment字串輸出
 * 若傳入的$escape_fragment為string(ex:a=123&b=345&c=)，則以parse_str轉為陣列( array('a'=>'123','b'=>'345','c'=>'') )
 * 若傳入的$escape_fragment為array，則不處理
 * then,將陣列型式的$escape_fragment回傳為#!開頭的陣列，讓gpc_filter處理成$_GET
 *
 * @var array
 */
function ef($escape_fragment){
	if(is_string($escape_fragment)){
		parse_str($escape_fragment,$escape_fragment);
	}
	$url='';
	foreach($escape_fragment as $index => $v){
		$url.=','.$index.'='.urlencode($v);
	}
	if(strlen($url))$url=substr($url,1);
	return '#!'.$url;
}

/**
 * date_from_00000101
 * date_to_00000101
 * 計算自0000-01-01起第幾日的日期(只考慮閏年，不考慮曆法轉換的缺日)
 * 只能求1985~2035
 * 
 * @param mixed $d 第幾日
 *   sample:
 *     date_from_00000101(734868) // 2012-12-31
 *     734562 // 2012-02-29
 *     734502 // 2011-12-31
 *     734197 // 2011-03-01
 *     734137 // 2010-12-31
 * @access public
 * @return string
 */
function date_to_00000101($d){// yyyy-mm-dd
	$y2000=array(
		/*2036*/739239, 739240, 739241, 739242, 739243, 739244, 739245, //2030
		739246, 739247, 739248, 739249, 739250, 739251, 738885, 738520, 738155, 737790,//2020
		737424, 737059, 736694, 736329, 735963, 735598, 735233, 734868, 734502, 734137,//2010
		733772, 733407, 733041, 732676, 732311, 731946, 731580, 731215, 730850, 730485,//2000年,
		730119, 729754, 729389, 729024, 728658, 728293, 727928, 727563, 727197, 726832,//1990
		726467, 726102, 725736, 725371, 725006// 1985
	);
	$mdays=array(334, 304, 273, 243, 212, 181, 151, 120, 90, 59, 31, 0);
	list($y,$m,$d)=explode('-',$d);
	$y=intval($y,10);
	$m=intval($m,10);
	$d=intval($d,10);
	$i=$y2000[2037-$y]+$mdays[12-$m]+$d;
	if($m>2){
		if(!($y%400)) $i+=1;
		else if(($y%100) && !($y%4)) $i+=1;	
	}
	return $i;
}

function date_from_00000101($d){
	$y2000=array(
		/*2036*/739239, 739240, 739241, 739242, 739243, 739244, 739245, //2030
		739246, 739247, 739248, 739249, 739250, 739251, 738885, 738520, 738155, 737790,//2020
		737424, 737059, 736694, 736329, 735963, 735598, 735233, 734868, 734502, 734137,//2010
		733772, 733407, 733041, 732676, 732311, 731946, 731580, 731215, 730850, 730485,//2000年,
		730119, 729754, 729389, 729024, 728658, 728293, 727928, 727563, 727197, 726832,//1990
		726467, 726102, 725736, 725371, 725006// 1985
	);
	$mdays=array(334, 304, 273, 243, 212, 181, 151, 120, 90, 59, 31);
	$leap=0;
	for($i=0,$n=count($y2000);$i<$n;$i++){
		if($d>$y2000[$i]){
			$y=2037-$i;
			$d=$d-$y2000[$i];
			if(($i%4)==1){
				//凡被4整除表示剛過閏年
				$leap=1;
			}
			$m=0;
			if($d>(59+$leap)){
				for($j=0;$j<11;$j++){
					if($d>($mdays[$j]+$leap)){
						$m=12-$j;
						$d=$d-$mdays[$j]-$leap;
						break;
					}
				}
			}
			else if($d>31){
				$m=2;
				$d=$d-31;
			}
			else{
				$m=1;
			}
			break;
		}
	}
	return sprintf('%04d-%02d-%02d',$y,$m,$d);
}

/**
 * image_resize 
 * test by 20151022
 * 
 * @param mixed $img_src 
 * @param mixed $img_dst 
 * @param mixed $new_size 
 *		[正整數]表示縮放到指定寬度(等比例)
 *		[正整數x正整數]表示縮放到指定尺寸
 *		[數字%]表示等比例縮放
 *		[正整數*正整數]表示將圖縮放到至少長或寬等於指定的尺寸，然後取中段的指定尺寸範圍
 * @param mixed $extname 
 * @access public
 * @return void
 */
function image_resize($img_src, $img_dst, $new_size, $src_extname='jpg', $dst_extname='png'){
	$need_catch_central=FALSE;// 是否需要擷取一部分(取中段)
	$img_info = getimagesize($img_src);
	$width    = $img_info[0];
	$height   = $img_info[1];

	if(!strcmp(substr($new_size,-1),'%')){
		$new_size=floatval(substr($new_size,0,-1));
		if($new_size == 100){
			$new_width = $width;
			$new_height = $height;
		}
		else{
			$new_width=$width * $new_size / 100;
			$new_height=$height * $new_size / 100;
		}
	}
	else if(strpos($new_size,'x')){
		list($new_width, $new_height)=explode('x',$new_size);
		$new_width=intval($new_width,10);
		$new_height=intval($new_height,10);
	}
	else if(strpos($new_size,'*')){
		list($new_width, $new_height)=explode('*',$new_size);
		// renew_ 是要儲存指定輸出尺寸用的
		// new_ 則是圖檔要轉存成至少有一邊等於指定尺寸(另一邊要較大)，然後在用 renew_ 裁切中段
		$new_width=$renew_width=intval($new_width,10);
		$new_height=$renew_height=intval($new_height,10);
		$p_width = $renew_width / $width;
		$p_height = $renew_height / $height;
		if($p_width == $p_height){
		}
		else if($p_width > $p_height){
			$need_catch_central=TRUE;
			$new_height = ceil($height * $renew_width / $width);
		}
		else if($p_width < $p_height){
			$need_catch_central=TRUE;
			$new_width = ceil($width * $renew_height / $height);
		}
	}
	else if(is_numeric($new_size)){
		$new_width=$new_size;
		$new_height=intval($new_size*$height/$width);
	}
	else{
		return FALSE;
	}
	switch($src_extname){
		case 'jpg':case 'jpeg':
			$image = imagecreatefromjpeg($img_src);
			break;
		case 'png':
			$image = imagecreatefrompng($img_src);
			break;
		case 'gif':
			$image = imagecreatefromgif($img_src);
			break;
	}

    if(!$image)
        return FALSE ;

	if(!$need_catch_central){
		$image_new = imagecreatetruecolor($new_width, $new_height);

        if(!$image_new)
            return FALSE;

		$imagecopy = imagecopyresampled($image_new, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

        if(!$imagecopy)
            return FALSE;
	}
	else{
		$image_new = imagecreatetruecolor($renew_width, $renew_height);

        if(!$image_new)
            return FALSE;

		$imagecopy = imagecopyresampled($image_new, $image, 0, 0, floor(max(0,$new_width-$renew_width)*$width/(2*$new_width)), floor(max(0,$new_height-$renew_height)*$height/(2*$new_height)), $new_width, $new_height, $width, $height);

        if(!$imagecopy)
            return FALSE;
	}

	$image_dst_dir=dirname($img_dst);
	if(!is_dir($image_dst_dir)){
		@mkdir($image_dst_dir,0755,TRUE);
	}
	switch($dst_extname){
		case 'jpg':case 'jpeg':
			$save = imagejpeg($image_new, $img_dst);
			break;
		case 'png':
			$save = imagepng($image_new, $img_dst);
			break;
		case 'gif':
			$save = imagegif($image_new, $img_dst);
			break;
	}

    if(!$save)
        return FALSE;

	return TRUE;
}

function random_code($len, $seeds=NULL){
	if($seeds===NULL || !is_array($seeds)){
		$seeds=array('2','E','F','G','T','U','N','5','6','X','Y','Z','J','8','9','A','C','K','M','7','H','Q','R','S','P','3','4');
	}
	$num=count($seeds);

	$str='';
	for($i=0;$i<$len;$i++){
		$str.=$seeds[mt_rand(0,$num)];
	}
	return $str;
}

/**
 * 非貪婪模式用
 */
function reg_not($pattern){
	$head = substr($pattern,0,1); // <
	$body = substr($pattern,1);   // \/b>
	return "(?>[^$head]*)(?>$head(?!$body)[^$head]*)*";
}

function fileperm_to_rwx($perms){
	if (($perms & 0xC000) == 0xC000) {
		// Socket
		$info = 's';
	} elseif (($perms & 0xA000) == 0xA000) {
		// Symbolic Link
		$info = 'l';
	} elseif (($perms & 0x8000) == 0x8000) {
		// Regular
		$info = '-';
	} elseif (($perms & 0x6000) == 0x6000) {
		// Block special
		$info = 'b';
	} elseif (($perms & 0x4000) == 0x4000) {
		// Directory
		$info = 'd';
	} elseif (($perms & 0x2000) == 0x2000) {
		// Character special
		$info = 'c';
	} elseif (($perms & 0x1000) == 0x1000) {
		// FIFO pipe
		$info = 'p';
	} else {
		// Unknown
		$info = 'u';
	}

	// Owner
	$info .= (($perms & 0x0100) ? 'r' : '-');
	$info .= (($perms & 0x0080) ? 'w' : '-');
	$info .= (($perms & 0x0040) ?
				(($perms & 0x0800) ? 's' : 'x' ) :
				(($perms & 0x0800) ? 'S' : '-'));

	// Group
	$info .= (($perms & 0x0020) ? 'r' : '-');
	$info .= (($perms & 0x0010) ? 'w' : '-');
	$info .= (($perms & 0x0008) ?
				(($perms & 0x0400) ? 's' : 'x' ) :
				(($perms & 0x0400) ? 'S' : '-'));

	// World
	$info .= (($perms & 0x0004) ? 'r' : '-');
	$info .= (($perms & 0x0002) ? 'w' : '-');
	$info .= (($perms & 0x0001) ?
				(($perms & 0x0200) ? 't' : 'x' ) :
				(($perms & 0x0200) ? 'T' : '-'));
	return $info;
}

function random_string($num_words){
	$num_words-=6;
	$sample_text='房地產是指覆蓋土地並永久附著於土地的一類實物，比如建築物。房地產一般也被稱為不動產，與之相對應的是私產，即動產。在技術層面上，一些人試圖將房地產與土地和設備等其他不動產相分離，同時將房地產所有權與房地產本身相分離。在不動產歸於民法權限下的同時，普通法中使用的是房地產和不動產來這類權利。可以有三種存在形態：即土地 、建築物、房地合一的。在房地產拍賣中，其拍賣標的也可以有三種存在形態，即土地（或土地使用權），建築物和房地合一狀態下的物質實體及其權益，隨著個人財產所有權的發展，房地產已經成為商業交易的主要組成部分。購買房地產是一種重要的投資方式。近年來，很多經濟學家都認識到有效的不動產法律的缺位將使得在開發中國家的投資面臨很大的障礙。無論一個國家是貧窮還是富足，土地和依存土地的房產都是社會財富的一個重要組成部分。在大多數發達經濟體中，個人和小型公司用來購買或開發土地和建築物的資金主要來源於銀行抵押貸款，也就是用不動產的價值來開發其本身。銀行大多非常樂於以比較低的利率發放這樣的貸款，因為一旦借款人無法清償貸款，銀行可以取消借款人的抵押物贖回權，然後請求法庭將土地所有權轉移給銀行並將土地出售以拿回屬於他們的錢。然而許多開發中國家並沒有對抵押物贖回的有效法律保障，所以抵押貸款在那裡要麼沒有開展，要麼只有特定的社會階層才能使用。';
	$len=mb_strlen($sample_text);
	if($num_words >= $len){
		$sample_text=str_repeat($sample_text, ceil($num_words / $len)+1);
		$len=mb_strlen($sample_text);
	}
	$start_pos=rand(0,$len-$num_words);
	return '測試TEST'.mb_substr($sample_text, $start_pos, $num_words);
}

