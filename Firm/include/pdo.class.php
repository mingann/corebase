<?php
/**
 * SELECT 的索引包含
 *	select:	查找的欄位，預設為 *
 *	from:	查找的資料表，包含 LEFT/RIGHT/INNER/OUTTER JOIN
 *	where:	WHERE 條件，若為陣列時，0 為 where 條件字串，1 為 bind，例如
 *		'where'=>array( 'userUid=:user_uid', array( ':user_uid' => $user_uid))		==> 夾帶 bind value
 *		'where'=>'userUid="b4saz9lh-u60wgo44-csk4ks88gg"'							==> 不夾帶 bind value
 *		也可以把要 bind 的值放在 bind 裡
 *	order:	ORDER BY 條件
 *	group:	GROUP BY 條件
 *	limit:	筆數條件
 *	spage:	從第幾頁開始取資料(會取代 limit 條件)
 *	per:	一頁要多少筆資料，需要搭配 spage，可不指定，預設為 50
 *	key:	要當作 key 的欄位名稱，一個 key 對映一筆資料，為 1 on 1，若與 keys 同時設定，則採用 keys 當作索引
 *	keys:	要當作 key 的欄位名稱，和 key 類似，但是為 1 on multiple
 *	val:	搭配 key 使用，表示只儲存指定的欄位，變成 key=>val
 *	bind:	要 bind 的值
 *	bind_array:	要 bind 的 array，通常用在 IN，例如 'where'=>'userUid IN (:user_uid)', 'bind_array'=>array(.....)
 *
 * UPDATE/INSERT 的使用方式
 *
 *
 * DELETE 的使用方式
 *
 *
 * QUERY
 *  query() 時若為 select，則回傳等同於 data 的陣列
 *
 */

class DB {

	/**
	 * driver_mapping
	 * 給 DB::dbid_driver_select 用的 driver/function 對照實際的 function
	 *
	 * 根據設定檔的 driver 欄位判斷，預設為 mysql
	 */
	static private $driver_mapping=array();

	/**
	 * database connect resource
	 *  
	 */
	static private $dbo=array();

	static private $dbo_seek='';

	static private $dbo_statement='';

	static private $conf=array();

	static private $sql='';

	static private $schema=array();

	/**
	 * debug_sql
	 * show sql schema 
	 */
	static private $debug_sql=FALSE;

	/**
	 * debug_stop
	 * stop script when query error
	 */
	static private $debug_stop=FALSE;

	static public function init(){
		self::$driver_mapping=array(
			'mysql'=>array(
				'value'=>'mysql_value',
				'row'=>'mysql_row',
				'data'=>'mysql_data',
				'page'=>'mysql_page',
				'add'=>'sql_add',
				'adds'=>'sql_adds',
				'update'=>'sql_update',
				'replace'=>'sql_replace',
				'del'=>'sql_del',
				'lock'=>'sql_lock',
				'unlock'=>'sql_unlock',
	//			'row_lock'=>'sql_rowlock',
	//			'row_unlock'=>'sql_rowunlock',
				'table_meta'=>'mysql_table_meta',
				'utc_timestamp_get'=>'mysql_utc_timestamp_get',
			),
		);
	}

	/**
	 * conf_load 
	 * load conf file by path
	 * 
	 * @param mixed $path 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function conf_load($path){
		self::$conf=include($path);
	}

	/**
	 * conf_setter 
	 * set conf by dbid & parameters
	 * 
	 * @param mixed $conf 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function conf_setter($dbid,$conf){
		self::$conf[$dbid]=$conf;
	}

	static public function dbo_connect($dbid){
		$attr_init_command='';
		if(isset(self::$conf[$dbid]['charset'])){
			$attr_init_command.='set names '.self::$conf[$dbid]['charset'];
			if(isset(self::$conf[$dbid]['collate'])){
				$attr_init_command.=' collate '.self::$conf[$dbid]['collate'];
			}
		}
		if(empty($attr_init_command)){
			$attr_init_command='set names utf8';
		}
		if(!isset(self::$conf[$dbid]['driver'])){
			self::$dbo[$dbid]=new PDO('mysql:dbname='.self::$conf[$dbid]['dbname'].';host='.self::$conf[$dbid]['server'],
				self::$conf[$dbid]['account'],
				self::$conf[$dbid]['password'],
				array(PDO::MYSQL_ATTR_INIT_COMMAND => $attr_init_command)) or self::err();
		}
		else{
			switch(self::$conf[$dbid]['driver']){
				case 'pgsql':
				case 'mmcache':
				case 'nosql':
					return self::$conf[$dbid].'_'.$select_model;
				case 'mysql':
				default:
					try {
						self::$dbo[$dbid]=new PDO('mysql:dbname='.self::$conf[$dbid]['dbname'].';host='.self::$conf[$dbid]['server'].';',
							self::$conf[$dbid]['account'],
							self::$conf[$dbid]['password'],
							array(PDO::MYSQL_ATTR_INIT_COMMAND => $attr_init_command)) or self::err();
					}
					catch (PDOException $e){
						die($dbid.'<br />'.$e->getMessage());
					}
					mb_internal_encoding('UTF-8');
					mb_http_output('UTF-8');
					self::$dbo[$dbid]->exec('SET TIME_ZONE = "UTC"');
	//				if(isset(self::$conf[$dbid]['charset'])){
	//					self::$dbo[$dbid]->exec('SET CHARACTER SET \''.self::$conf[$dbid]['charset'].'\'');
	//				}
	//				if(isset(self::$conf[$dbid]['collate'])){
	//					self::$dbo[$dbid]->exec('SET collation_connection = \''.self::$conf[$dbid]['collate'].'\'');
	//				}
			}
		}

	}

	/**
	 * empty_data 
	 * 產生空的data用陣列
	 * 
	 * @static
	 * @access private
	 * @return void
	 */
	static public function empty_data(){
		return array(
			'd'=>array(),
			'args'=>array(
				'num'=>0,	// 這次總共抓出幾筆資料
				'total'=>0,	// 不分頁狀況下總共會有幾筆資料
				'spage'=>0,	// 在第幾頁
				'per'=>50,	// 每頁有幾筆資料
			)
		);
	}

	/**
	 * page_data 
	 * 傳入$data['args']，計算頁數
	 * 
	 * @param mixed $args 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function page_data($args){
		if($args['total']<=$args['per']){
			$args['pages']=1;
		}
		else{
			$args['pages']=ceil($args['total']/$args['per']);
		}	
		return $args;
	}

	static public function utc_timestamp_get($dbid){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('utc_timestamp_get')}();
	}
	static public function table_meta($dbid=NULL,$table){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('table_meta')}($table);
	}

	static public function value($dbid=NULL,$schema=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('value')}($schema);
	}
	static public function row($dbid=NULL,$schema=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('row')}($schema);
	}

	/**
	 * data 
	 * 
	 * @param mixed $dbid 
	 * @param mixed $schema 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function data($dbid=NULL,$schema=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('data')}($schema);
	}

	/**
	 * update 
	 * 
	 * @param mixed $dbid 
	 * @param mixed $table 
	 * @param mixed $data 
	 * @param mixed $where 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function update($dbid,$table,$data,$attach=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('update')}($table,$data,$attach);
	}

	static public function replace($dbid,$table,$data,$attach=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('replace')}($table,$data,$attach);
	}

	/**
	 * add 
	 * 
	 * @param mixed $dbid 
	 * @param mixed $table 
	 * @param mixed $data 
	 * @param mixed $auto_increment 是否有auto_increment欄位
	 * @static
	 * @access public
	 * @return void
	 */
	static public function add($dbid,$table,$data,$auto_increment=TRUE){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('add')}($table,$data,$auto_increment);
	}

	/**
	 * adds 
	 * 
	 * @param mixed $dbid 
	 * @param mixed $table 
	 * @param mixed $data 
	 * @param boolean $auto_increment 是否有auto_increment欄位
	 * @static
	 * @access public
	 * @return void
	 */
	static public function adds($dbid,$table,$data,$auto_increment=TRUE){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('adds')}($table,$data,$auto_increment);
	}
	static public function del($dbid,$table,$attach=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('del')}($table,$attach);
	}
	/**
	 * lock 
	 * 鎖表，必須傳入為 array('table'=>'w')，w 表示鎖住 WRITE，r 表示鎖住 READ，可以允許多個
	 * 
	 * @param mixed $dbid 
	 * @param mixed $table 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function lock($dbid,$table){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('lock')}($table);
	}
	static public function unlock($dbid,$table=NULL){
		self::dbid_check($dbid);
		return self::{self::dbid_driver_select('unlock')}($table);
	}

	static public function query($dbid,$sql){
		self::dbid_check($dbid);
		if(is_array($sql)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql[0]);
			if(!strcasecmp(substr(trim($sql[0]),0,7),'select ')){
				$st->execute($sql[1]) or self::err($st->errorInfo());
				$data=array('d'=>$st->fetchAll(PDO::FETCH_ASSOC));
				$data['args']=array(
					'num'=>count($data['d']),
				);
				return $data;
			}
			else{
				return $st->execute($sql[1]) or self::err($st->errorInfo());
			}
		}
		else{
			if(!strcasecmp(substr(trim($sql),0,7),'select ')){
				$st=self::$dbo[self::$dbo_seek]->exec($sql) or self::err($st->errorInfo());
				$data=array('d'=>$st->fetchAll(PDO::FETCH_ASSOC));
				$data['args']=array(
					'num'=>count($data['d']),
				);
				return $data;
			}
			else{
				return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();
			}
		}
		//return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();	
	}

	static private function dbid_driver_select($f){
		if(!isset(self::$conf[self::$dbo_seek])){
			die('Error: nonexists '.self::$dbo_seek.'\' config.');
		}
		else if(isset(self::$conf[self::$dbo_seek]['driver'])){
			if(isset(self::$driver_mapping[self::$conf[self::$dbo_seek]['driver']][$f])){
				return self::$driver_mapping[self::$conf[self::$dbo_seek]['driver']][$f];

			}
			die('Error: nonexists '.self::$dbo_seek.'\' '.$f.' function.');
		}
		else{
			if(isset(self::$driver_mapping['mysql'][$f])){
				return self::$driver_mapping['mysql'][$f];
			}
			die('Error: nonexists '.self::$dbo_seek.'\' '.$f.' function.');
		}
	}

	/**
	 * dbid_check 
	 * 確認$dbid是否已連線/設定存在，若設定不存在則使用common，若未連線則建立連線
	 * 
	 * @param mixed $dbid 
	 * @static
	 * @access private
	 * @return void
	 */
	static private function dbid_check($dbid){
		if(is_null($dbid) || !isset(self::$conf[$dbid])){
			$dbid='common';
		}
		if(isset(self::$conf[$dbid]) && !isset(self::$dbo[$dbid])){
			self::dbo_connect($dbid);
		}
		self::$dbo_seek=$dbid;
	}
	static public function err($st_error_info=NULL){
		$c=self::$dbo[self::$dbo_seek]->errorInfo();
		error_log('sql error on '.(isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'No user-agent'));
		error_log('refer '.(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:'no request_uri'));
		error_log(self::$dbo[self::$dbo_seek]->errorCode());
		error_log(print_r($c,TRUE));
		if(!is_null($st_error_info)){
			error_log(print_r($st_error_info,TRUE));
		}
		if(_DEBUG){
			echo '<div>Error Code: '.self::$dbo[self::$dbo_seek]->errorCode().'</div>';
			echo '<div>Error Info: ';
			_e(self::$dbo[self::$dbo_seek]->errorInfo());
			echo '</div>';
			if(!is_null($st_error_info)){
				echo '<div>Statement Error Info: ';
				_e($st_error_info);
				echo '</div>';
			}
			if(self::$debug_stop){
				die('');
			}
		}
		else{
			die('system '.self::$dbo_seek.' busy, waiting....');
		}
	}

	/**
	 * debug 
	 * switch debug mode
	 * 
	 * @param int $mode TRUE/FALSE/0/1/2/3 
	 * @static
	 * @access public
	 * @return void
	 */
	static public function debug($mode=0){
		if($mode===TRUE){
			self::$debug_sql=TRUE;
			self::$debug_stop=TRUE;
		}
		else if($mode===FALSE){
			self::$debug_sql=FALSE;
			self::$debug_stop=FALSE;
		}
		else{
			self::$debug_sql=($mode & 1);
			self::$debug_stop=($mode & 2);
		}
	}

	static private function sql_lock($table){
		$sql='LOCK TABLES ';
		foreach($table as $table_name => $mode){
			$sql.='`'.$table_name.'` '.($mode=='r'?'READ':'WRITE').',';
		}
		$sql=substr($sql,0,-1);
		return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();
	}
	static private function sql_unlock($table){
		self::$dbo[self::$dbo_seek]->exec('UNLOCK TABLES');
	}
	static private function mysql_utc_timestamp_get(){
		$sql='SELECT UNIX_TIMESTAMP()';
		if(self::$debug_sql){
			_e($sql);
		}
		$data=self::$dbo[self::$dbo_seek]->query($sql)->fetch(PDO::FETCH_NUM);
		return $data[0];
	}

	static private function mysql_table_meta($table){
		$fields=array();
		$sql='DESCRIBE '.$table;
		$res=self::$dbo[self::$dbo_seek]->query($sql);
		while($field=$res->fetch(PDO::FETCH_ASSOC)){
			$r=strpos($field['Type'],'(');
			$type2='';
			if($r>1){
				$charset=substr($field['Type'],$r);
				if(in_array($charset,array('enum','set'))){
					$type2=substr($r+1,-1);
				}
				else if(in_array($charset,array('decimal'))){
					$type2=substr($r+1,-1);
				}
				else{
					$type2=substr($r+1,-1);
				}
			}
			$fields[]=array(
				'field'=>$field['Field'],
				'charset'=>$charset,
				'option'=>$type2,
				'allow_null'=>strcasecmp($field['Null'],'NO')?0:1,
				'default'=>$field['Default'],
				'auto_increment'=>$field['Extra']
			);
		}
		return $fields;
	}

	static private function mysql_value($schema=NULL){
		if(is_null($schema)){
			$schema=self::$schema;
		}
		$bind_params=array();
		$sql='SELECT ';
		if(empty($schema['select'])){
			return NULL;
		}
		else{
			$sql.=$schema['select'];
		}
		$sql.=' FROM '.$schema['from'];
		if(isset($schema['bind']) && is_array($schema['bind'])){
			$bind_params=$schema['bind'];	
		}
		if(isset($schema['where'])){
			if(isset($schema['bind_array'])){
				$binds=array();
				foreach($schema['bind_array'] as $bind_id => $bind_value){
					$replaced=$bind_id;
					$replace_to=array();
					for($i=0,$n=count($bind_value);$i<$n;$i++){
						$replace_to[]=$bind_id.'_'.$i;
						$binds[$bind_id.'_'.$i]=$bind_value[$i];
					}
					if(is_array($schema['where'])){
						$schema['where'][0]=str_replace($bind_id,join(',',$replace_to),$schema['where']);
					}
					else{
						$schema['where']=str_replace($bind_id,join(',',$replace_to),$schema['where']);
					}
				}
				$bind_params=array_merge($bind_params,$binds);
			}
			if(is_array($schema['where'])){
				$sql.=' WHERE '.$schema['where'][0];
				if(isset($schema['where'][1])){
					$bind_params=array_merge($bind_params,$schema['where'][1]);
				}
			}
			else{
				$sql.=' WHERE '.$schema['where'];
			}
		}
		if(isset($schema['group'])){
			if(is_array($schema['group'])){
				$sql.=' GROUP BY '.$schema['group'][0];
				if(isset($schema['group'][1])){
					$bind_params=array_merge($bind_params,$schema['group'][1]);
				}
			}
			else{
				$sql.=' GROUP BY '.$schema['group'];
			}
		}
		if(isset($schema['order'])){
			if(is_array($schema['order'])){
				$sql.=' ORDER BY '.$schema['order'][0];
				if(isset($schema['order'][1])){
					$bind_params=array_merge($bind_params,$schema['order'][1]);
				}
			}
			else{
				$sql.=' ORDER BY '.$schema['order'];
			}
		}
		if(isset($schema['limit'])){
			$sql.=' LIMIT '.$schema['limit'];
		}
		else{
			$sql.=' LIMIT 1';
		}
		if(self::$debug_sql){
			_e($sql);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			$st->execute($bind_params) or self::err($st->errorInfo());
			$data=$st->fetch(PDO::FETCH_COLUMN);
		}
		else{
			$data=self::$dbo[self::$dbo_seek]->query($sql)->fetch(PDO::FETCH_COLUMN) or self::err();
		}
		return isset($data)?$data:NULL;
	}

	static private function mysql_row($schema){
		if(is_null($schema)){
			$schema=self::$schema;
		}
		$bind_params=array();
		$sql='SELECT ';
		if(isset($schema['bind']) && is_array($schema['bind'])){
			$bind_params=$schema['bind'];	
		}
		if(empty($schema['select'])){
			$sql.='*';
		}
		else if(is_array($schema['select'])){
			$sel_rows=array();
			foreach($schema['select'] as $alias_name => $sel_field){
				if(is_int($alias_name)){
					$sel_rows[]=$sel_field;
				}
				else{
					$sel_rows[]=$sel_field.' AS `'.$alias_name.'`';
				}
			}
			$sql.=join(',',$sel_rows);
		}
		else{
			$sql.=$schema['select'];
		}
		$sql.=' FROM '.$schema['from'];
		if(isset($schema['where'])){
			if(isset($schema['bind_array'])){
				$binds=array();
				foreach($schema['bind_array'] as $bind_id => $bind_value){
					$replaced=$bind_id;
					$replace_to=array();
					for($i=0,$n=count($bind_value);$i<$n;$i++){
						$replace_to[]=$bind_id.'_'.$i;
						$binds[$bind_id.'_'.$i]=$bind_value[$i];
					}
					if(is_array($schema['where'])){
						$schema['where'][0]=str_replace($bind_id,join(',',$replace_to),$schema['where'][0]);
					}
					else{
						$schema['where']=str_replace($bind_id,join(',',$replace_to),$schema['where']);
					}
				}
				$bind_params=array_merge($bind_params,$binds);
			}
			if(is_array($schema['where'])){
				$sql.=' WHERE '.$schema['where'][0];
				if(!empty($schema['where'][1])){
					$bind_params=array_merge($bind_params,$schema['where'][1]);
				}
			}
			else{
				$sql.=' WHERE '.$schema['where'];
			}
		}
		if(isset($schema['group'])){
			if(is_array($schema['group'])){
				$sql.=' GROUP BY '.$schema['group'][0];
				if(isset($schema['group'][1])){
					$bind_params=array_merge($bind_params,$schema['group'][1]);
				}
			}
			else{
				$sql.=' GROUP BY '.$schema['group'];
			}
		}
		if(isset($schema['order'])){
			if(is_array($schema['order'])){
				$sql.=' ORDER BY '.$schema['order'][0];
				if(isset($schema['order'][1])){
					$bind_params=array_merge($bind_params,$schema['order'][1]);
				}
			}
			else{
				$sql.=' ORDER BY '.$schema['order'];
			}
		}
		if(isset($schema['limit'])){
			$sql.=' LIMIT '.$schema['limit'];
		}
		else{
			$sql.=' LIMIT 1';
		}
		if(self::$debug_sql){
			_e($sql);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			$st->execute($bind_params) or self::err($st->errorInfo());
			$data=$st->fetch(PDO::FETCH_ASSOC);
		}
		else{
			$data=self::$dbo[self::$dbo_seek]->query($sql);
			if($data===FALSE){
				self::err();
			}
			else{
				$data=$data->fetch(PDO::FETCH_ASSOC);
			}
		}
		if(empty($data)){
			return NULL;
		}
		return $data;
	}

	static private function mysql_data($schema){
		if(is_null($schema)){
			$schema=self::$schema;
		}
		if(isset($schema['spage'])){
			return self::mysql_page($schema);
		}
		$bind_params=array();
		$sql='SELECT ';
		if(isset($schema['bind']) && is_array($schema['bind'])){
			$bind_params=$schema['bind'];	
		}
		if(empty($schema['select'])){
			$sql.='*';
		}
		else if(is_array($schema['select'])){
			$sel_rows=array();
			foreach($schema['select'] as $alias_name => $sel_field){
				if(is_int($alias_name)){
					$sel_rows[]=$sel_field;
				}
				else{
					$sel_rows[]=$sel_field.' AS `'.$alias_name.'`';
				}			}
			$sql.=join(',',$sel_rows);
		}
		else{
			$sql.=$schema['select'];
		}
		$sql.=' FROM '.$schema['from'];
		if(isset($schema['where'])){
			if(isset($schema['bind_array'])){
				$binds=array();
				foreach($schema['bind_array'] as $bind_id => $bind_value){
					$replaced=$bind_id;
					$replace_to=array();
					for($i=0,$n=count($bind_value);$i<$n;$i++){
						$replace_to[]=$bind_id.'_'.$i;
						$binds[$bind_id.'_'.$i]=$bind_value[$i];
					}
					if(is_array($schema['where'])){
						$schema['where'][0]=str_replace($bind_id,join(',',$replace_to),$schema['where'][0]);
					}
					else{
						$schema['where']=str_replace($bind_id,join(',',$replace_to),$schema['where']);
					}
				}
				$bind_params=array_merge($bind_params,$binds);
			}
			if(is_array($schema['where'])){
				$sql.=' WHERE '.$schema['where'][0];
				if(isset($schema['where'][1])){
					$bind_params=array_merge($bind_params,$schema['where'][1]);
				}
			}
			else{
				$sql.=' WHERE '.$schema['where'];
			}
		}
		if(isset($schema['group'])){
			if(is_array($schema['group'])){
				$sql.=' GROUP BY '.$schema['group'][0];
				if(isset($schema['group'][1])){
					$bind_params=array_merge($bind_params,$schema['group'][1]);
				}
			}
			else{
				$sql.=' GROUP BY '.$schema['group'];
			}
		}
		if(isset($schema['order'])){
			if(is_array($schema['order'])){
				$sql.=' ORDER BY '.$schema['order'][0];
				if(isset($schema['order'][1])){
					$bind_params=array_merge($bind_params,$schema['order'][1]);
				}
			}
			else{
				$sql.=' ORDER BY '.$schema['order'];
			}
		}
		if(isset($schema['limit'])){
			$sql.=' LIMIT '.$schema['limit'];
		}
		if(self::$debug_sql){
			_e($sql);
			_e($bind_params);
		}
		$data=self::empty_data();

		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			$st->execute($bind_params) or self::err($st->errorInfo());
			if(!isset($schema['keys']) && !isset($schema['key'])){
				$data['d']=$st->fetchAll(PDO::FETCH_ASSOC);
			}
			else if(isset($schema['keys'])){
				$tmp=$st->fetchAll(PDO::FETCH_ASSOC);
				for($i=0,$n=count($tmp);$i<$n;$i++){
					if(!isset($data['d'][$tmp[$i][$schema['keys']]])){
						$data['d'][$tmp[$i][$schema['keys']]]=array();
					}
					$data['d'][$tmp[$i][$schema['keys']]][]=$tmp[$i];
				}
			}
			else {
				$tmp=$st->fetchAll(PDO::FETCH_ASSOC);
				if(isset($schema['val'])){
					for($i=0,$n=count($tmp);$i<$n;$i++){
						$data['d'][$tmp[$i][$schema['key']]]=$tmp[$i][$schema['val']];
					}
				}
				else{
					for($i=0,$n=count($tmp);$i<$n;$i++){
						$data['d'][$tmp[$i][$schema['key']]]=$tmp[$i];
					}
				}
			}
		}
		else{
			$res=self::$dbo[self::$dbo_seek]->query($sql) or self::err();	
			if(!isset($schema['keys']) && !isset($schema['key'])){
				$data['d']=$res->fetchAll(PDO::FETCH_ASSOC);
			}
			else if(isset($schema['keys'])){
				$tmp=$res->fetchAll(PDO::FETCH_ASSOC);
				for($i=0,$n=count($tmp);$i<$n;$i++){
					if(!isset($data['d'][$tmp[$i][$schema['keys']]])){
						$data['d'][$tmp[$i][$schema['keys']]]=array();
					}
					$data['d'][$tmp[$i][$schema['keys']]][]=$tmp[$i];
				}
			}
			else {
				$tmp=$res->fetchAll(PDO::FETCH_ASSOC);
				if(isset($schema['val'])){
					for($i=0,$n=count($tmp);$i<$n;$i++){
						$data['d'][$tmp[$i][$schema['key']]]=$tmp[$i][$schema['val']];
					}
				}
				else{
					for($i=0,$n=count($tmp);$i<$n;$i++){
						$data['d'][$tmp[$i][$schema['key']]]=$tmp[$i];
					}
				}
			}
		}
		$data['args']['num']=$data['args']['total']=count($data['d']);
		return $data;
	}

	static private function mysql_page($schema){
		if(is_null($schema)){
			$schema=self::$schema;
		}
		$bind_params=array();
		$schema['per']=isset($schema['per'])?$schema['per']:50;
		if(isset($schema['bind']) && is_array($schema['bind'])){
			$bind_params=$schema['bind'];	
		}
		$sql='SELECT SQL_CALC_FOUND_ROWS ';
		if(empty($schema['select'])){
			$sql.='*';
		}
		else if(is_array($schema['select'])){
			$sel_rows=array();
			foreach($schema['select'] as $alias_name => $sel_field){
				if(is_int($alias_name)){
					$sel_rows[]=$sel_field;
				}
				else{
					$sel_rows[]=$sel_field.' AS `'.$alias_name.'`';
				}			}
			$sql.=join(',',$sel_rows);
		}
		else{
			$sql.=$schema['select'];
		}
		$sql.=' FROM '.$schema['from'];
		if(isset($schema['where'])){
			if(isset($schema['bind_array'])){
				$binds=array();
				foreach($schema['bind_array'] as $bind_id => $bind_value){
					$replaced=$bind_id;
					$replace_to=array();
					for($i=0,$n=count($bind_value);$i<$n;$i++){
						$replace_to[]=$bind_id.'_'.$i;
						$binds[$bind_id.'_'.$i]=$bind_value[$i];
					}
					if(is_array($schema['where'])){
						$schema['where'][0]=str_replace($bind_id,join(',',$replace_to),$schema['where'][0]);
					}
					else{
						$schema['where']=str_replace($bind_id,join(',',$replace_to),$schema['where']);
					}
				}
				$bind_params=array_merge($bind_params,$binds);
			}
			if(is_array($schema['where'])){
				$sql.=' WHERE '.$schema['where'][0];
				if(isset($schema['where'][1])){
					$bind_params=array_merge($bind_params,$schema['where'][1]);
				}
			}
			else{
				$sql.=' WHERE '.$schema['where'];
			}
		}
		if(isset($schema['group'])){
			if(is_array($schema['group'])){
				$sql.=' GROUP BY '.$schema['group'][0];
				if(isset($schema['group'][1])){
					$bind_params=array_merge($bind_params,$schema['group'][1]);
				}
			}
			else{
				$sql.=' GROUP BY '.$schema['group'];
			}
		}
		if(isset($schema['order'])){
			if(is_array($schema['order'])){
				$sql.=' ORDER BY '.$schema['order'][0];
				if(isset($schema['order'][1])){
					$bind_params=array_merge($bind_params,$schema['order'][1]);
				}
			}
			else{
				$sql.=' ORDER BY '.$schema['order'];
			}
		}
		$sql.=' LIMIT '.($schema['spage']*$schema['per']).','.$schema['per'];
		$data=self::empty_data();
		if(self::$debug_sql){
			_e($sql);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			$st->execute($bind_params) or self::err($st->errorInfo());
			$data['d']=$st->fetchAll(PDO::FETCH_ASSOC);
		}
		else{
			$st=self::$dbo[self::$dbo_seek]->query($sql);	
			$data['d']=$st->fetchAll(PDO::FETCH_ASSOC);
		}
		$data['args']['num']=count($data['d']);
		$st=self::$dbo[self::$dbo_seek]->query('SELECT FOUND_ROWS()');
		$rows=$st->fetchColumn();
		$data['args']['total']=$rows;
		$data['args']['spage']=$schema['spage'];
		$data['args']['per']=$schema['per'];
		$data['args']=self::page_data($data['args']);
		return $data;
	}

	static private function sql_add($table,$data){
		$fields=array();
		$values=array();
		$bind_params=array();
		$sql='INSERT INTO '.$table;
		$xval=0;
		foreach($data as $field => $value){
			$fields[]=$field;
			if(is_array($value)){
				$values[]=$value[0];
			}
			else if(is_null($value)){
				$values[]='NULL';
			}
			else{
				$values[]=':xval_'.$xval;
				$bind_params[':xval_'.$xval]=$value;
				$xval++;
			}
		}
		$sql.='('.join(',',$fields).') VALUES('.join(',',$values).')';
		if(self::$debug_sql){
			_e($sql);
			_e($bind_params);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			return $st->execute($bind_params) or self::err($st->errorInfo());
		}
		else{
			return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();	
		}
	}

	/**
	 * sql_adds
	 *
	 * 多筆新增
	 * $data 必需傳入索引為 default 的陣列，該陣列索引為欄位名稱，值為預設值
	 * 若要插入的值是要以函式出現，則將函式表示及值包在 array[0] 裡
	 * 
	 * @param string $table 
	 * @param array $data 
	 * @access public
	 * @return array
	 */
	static private function sql_adds($table,$data){
		if(!is_array($data) || (!isset($data['default']))){
			return FALSE;
		}
		$tmp_rows=array();
		$tmp_row_names=array();
		$values=array();
		$keys=array_keys($data['default']);
		$bind_params=array();
		$bind_id=1;
		for($i=0,$keys_length=count($keys);$i<$keys_length;$i++){
			$tmp_rows[$keys[$i]]=$data['default'][$keys[$i]];
		}
		foreach($data as $k=>$v){
			if(!strcmp($k,'default')){
				continue;
			}
			$value_datum=array();
			for($i=0;$i<$keys_length;$i++){
				$value_datum[$i]=':val'.$bind_id;
				if(isset($v[$keys[$i]])){
					$bind_params[':val'.$bind_id]=$v[$keys[$i]];
				}
				else{
					$bind_params[':val'.$bind_id]=$tmp_rows[$keys[$i]];
				}
				$bind_id++;
			}

			$values[]='('.join(',',$value_datum).')';
		}
		$sql='INSERT INTO `'.$table.'`('.join(',',$keys).') VALUES'.join(',',$values);
		//self::$schema_log($table);
		if(self::$debug_sql){
			_e($sql);
			_e($bind_params);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			$st->execute($bind_params) or self::err($st->errorInfo());
		}
		else{
			self::$dbo[self::$dbo_seek]->exec($sql) or self::err();
		}
	}

	static private function sql_update($table,$data,$attach=NULL){
		$sets=array();
		$bind_params=array();
		$sql='UPDATE '.$table.' SET ';
		foreach($data as $field => $value){
			if(is_array($value)){
				$sets[]=$field.'='.$value[0];	
			}
			else if(is_null($value)){
				$sets[]=$field.'=NULL';
			}
			else{
				$sets[]=$field.'=:'.$field;
				$bind_params[':'.$field]=$value;
			}
		}
		$sql.=join(',',$sets);
		if(!is_null($attach)){
			if(is_array($attach) && isset($attach[2])){
				$binds=array();
				foreach($attach[2] as $bind_id => $bind_value){
					$replaced=$bind_id;
					$replace_to=array();
					for($i=0,$n=count($bind_value);$i<$n;$i++){
						$replace_to[]=$bind_id.'_'.$i;
						$binds[$bind_id.'_'.$i]=$bind_value[$i];
					}
					$attach[0]=str_replace($bind_id,join(',',$replace_to),$attach[0]);
				}
				$bind_params=array_merge($bind_params,$binds);
			}
			if(is_array($attach)){
				$sql.=' '.$attach[0];
				$bind_params=array_merge($bind_params,$attach[1]);
			}
			else{
				$sql.=$attach;
			}
		}
		if(self::$debug_sql){
			_e($sql);
			_e($bind_params);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			return $st->execute($bind_params) or self::err($st->errorInfo());
		}
		else{
			return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();
		}
	}

	static private function sql_replace($table,$data,$attach=NULL){
		$sets=array();
		$bind_params=array();
		$sql='REPLACE INTO '.$table.' SET ';
		foreach($data as $field => $value){
			if(is_array($value)){
				$sets[]=$field.'='.$value[0];	
			}
			else if(is_null($value)){
				$sets[]=$field.'=NULL';
			}
			else{
				$sets[]=$field.'=:'.$field;
				$bind_params[':'.$field]=$value;
			}
		}
		$sql.=join(',',$sets);

		if(self::$debug_sql){
			_e($sql);
			_e($bind_params);
		}
		if(count($bind_params)){
			$st=self::$dbo[self::$dbo_seek]->prepare($sql);
			return $st->execute($bind_params) or self::err($st->errorInfo());
		}
		else{
			return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();
		}
	}

	static private function sql_del($table,$attach=NULL){
		if(empty($attach)){
			// TRUNCATE 需要有 ALERT or DROP 權限
			$sql='TRUNCATE TABLE '.$table;
			if(self::$debug_sql){
				_e($sql);
			}
			return self::$dbo[self::$dbo_seek]->exec($sql);
		}
		else{
			$sql='DELETE FROM `'.$table.'`';
			$bind_params=array();
			if(is_array($attach)){
				if(isset($attach[1])){
					$bind_params=$attach[1];
				}
				if(isset($attach[2])){
			//		$binds=array();
					foreach($attach[2] as $bind_id => $bind_value){
						$replaced=$bind_id;
						$replace_to=array();
						for($i=0,$n=count($bind_value);$i<$n;$i++){
							$replace_to[]=$bind_id.'_'.$i;
					//		$binds[$bind_id.'_'.$i]=$bind_value[$i];
							$bind_params[$bind_id.'_'.$i]=$bind_value[$i];
						}
						$attach[0]=str_replace($bind_id,join(',',$replace_to),$attach[0]);
					}
			//		$bind_params=array_merge($bind_params,$binds);
				}

				$sql.=' '.$attach[0];
				if(self::$debug_sql){
					_e($sql);
					_e($bind_params);
				}
				if(count($bind_params)){
					$st=self::$dbo[self::$dbo_seek]->prepare($sql);
					return $st->execute($bind_params) or self::err($st->errorInfo());
				}
				else{
					return self::$dbo[self::$dbo_seek]->query($sql) or self::err();
				}
			}
			else{
				$sql.=' '.$attach;
				if(self::$debug_sql){
					_e($sql);
				}
				return self::$dbo[self::$dbo_seek]->exec($sql) or self::err();
			}
		}
	}
}
DB::init();

