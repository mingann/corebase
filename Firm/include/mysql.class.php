<?php
/**
$sql 為一陣列，用以組成 sql 查詢指令
method:
	method 的第一個參數為 dbcon，若傳入為 true，則代表使用建立 DBO 時的連線
	db($database=NULL)					: 指定或重設資料庫
	value($sql)							: 取一個值
	row($sql,$first=0)					: 取一筆資料
	data($sql,$index=NULL,$value=NULL)	: 取出資料
		.若沒有指定 $value，則取出的資料都放在陣列中，若 $index 是 unique key，則用 [$index][0] 取得資料
	page($sql,$page=0,$rows=30)			: 以分頁取出資料
		.回傳為陣列，d 表示資料，page 表示目前頁數，pages 表示總頁數，num 表示全部資料筆數
	add($table,$data)					: 一筆資料新增
		.若要插入的值為 mysql function，則將值(如 md5("test") )用 array() 包起來，如 array('md5("test")')
	adds($table,$data)					: 多筆資料新增，必需要有索引為 default 的預設值
		.插入 mysql function 時同上
	update($table,$data,$attach)		: 更新資料
		.插入 mysql function 時同上
	del($table,$attach=NULL)			: 刪除資料
	query($sql)							: 直接執行 sql schema
*/

class MODEL_BASE {
	/**
	 * cache 
	 * 批次新增時使用
	 * $GLOBALS['cz']->model->index_setter($index); // 也可以不指定，但若有多項資料要多重新增時，用來避免混淆
	 * for(....) { $GLOBALS['cz']->model->xxxx_add_cache(.....); }
	 * $GLOBALS['cz']->model->xxxx_add_default(....);
	 * $GLOBALS['cz']->model->table($table_name);
	 * $GLOBALS['cz']->model->xxxx_add_do($index);
	 * 
	 * @var array
	 * @access private
	 */
	static protected $cache=array();
	static protected $cache_index=0;
	static protected $table='';

	static protected $dbcon_id='';
	static public function index_setter($index){
		self::$cahce_index=$index;
		if(!isset(self::$cache[$index])) self::$cache[$index]=array();
	}

	/**
	 * cache_adds 
	 * 透過$GLOBALS['cz']->model->xxxx_add_do($index)操作時，呼叫self::$cache_adds()
	 * 
	 * @param mixed $index 
	 * @param string $dbcon_type
	 * @param uint $dbcon_id 
	 * @access public
	 * @return void
	 */
	static public function cache_adds($index,$dbcon_id,$dbcon_type='vgroup'){
		if(isset(self::$cache[$index]['default'])){
			$GLOBALS['db']->adds($dbcon,self::$table,self::$cache[$index]);
		}
		else if(isset(self::$cache[$index][0])){
			self::$cache[$index]['default']=self::$cache[$index][0];
			$GLOBALS['db']->adds($dbcon,self::$table,self::$cache[$index]);
		}
		else {
			_e($index.' for multi insert is nonexists.');
			die('Error #1192');
		}
		unset(self::$cache[$index]);
	}

	/**
	 * table_setter 
	 * 設定要操作的資料表
	 * 
	 * @param mixed $table 
	 * @access public
	 * @return void
	 */
	static public function table_setter($table){
		self::$table=$table;
	}
}

static class DB {
	/**
	 * @var boolean show: 是否要顯示sql schema和錯誤訊息在畫面上
	 * @var boolean stop: 是否要die,必需要show才能stop
	 * 可以在物件外操作
	 */
	static public $show=FALSE;
	static public $stop=FALSE;

	/**
	 * conns: 所有的mysql connection
	 * dbcon_seek: 目前的dbcon
	 */
	static private $conns=array();
	static private $dbcon_seek='';

	/**
	 * query_counter: 計算共執行了多少次的mysql_query, 統計用資訊
	 */
	static private $query_counter=0;

	/**
	 * database: 目前操作的資料庫名稱
	 * query: query string, 設為物件內通用是減少method傳值需要的記憶體
	 * status: mysql操作狀態
	 * _default: 預設的dbcon key
	 */
	static private $database='';
	static private $query='';
	static private $status=array();
	static private $_default='common';

	/**
	 * 紀錄sql schema
	 */
	static protected $log_enable=FALSE;
	static protected $log_select=TRUE;
	static protected $log_update=TRUE;
	static protected $log_insert=TRUE;
	static protected $log_delete=TRUE;
	static protected $log_usedb=TRUE;// 變更操作資料庫
	static protected $log_create=TRUE;// 新增資料庫

	function __construct(){
	}
	function __destruct(){
		foreach(self::$conns as $k=>$v){
			@mysql_close(self::$conns[$k]);
		}
	}
	static public function connect($dbcon,$server,$admin,$password,$database,$port=3306){
	/**
	 * 建立mysql connection
	 */
		self::$conns[$dbcon]=mysql_connect($server,$admin,md5(base64_decode($password)),TRUE) or die('ERROR: can not connect to database server.'.$dbcon);
		self::$dbcon_seek=$dbcon;
		if($database){
			self::$database=$database;
			@mysql_select_db($database,self::$conns[self::$dbcon_seek]) or die('ERROR: can not select database '.(_DEBUG?$database:'').$dbcon);
		}
		mysql_query('SET NAMES UTF8',self::$conns[self::$dbcon_seek]);
	}
	static public function reconnect($dbcon){
	/**
	 * 重新建立連線,通常用在更改$GLOBALS['dbs']裡的參數後使用
	 */
		if(isset(self::$conns[$dbcon])){
			if(!empty($GLOBALS['dbs'][$dbcon])){
				@mysql_close($GLOBALS['dbs'][$dbcon]);
			}
			self::$connect($dbcon,
				$GLOBALS['dbs'][$dbcon]['server'],
				$GLOBALS['dbs'][$dbcon]['admin'],
				$GLOBALS['dbs'][$dbcon]['password'],
				$GLOBALS['dbs'][$dbcon]['database'],
				$GLOBALS['dbs'][$dbcon]['port']);
		}
	}
	static public function add_connect($dbcon,$server,$admin,$password,$database,$port=3306){
	/**
	 * 增加$GLOBALS['dbs']並建立連線
	 */
		if(!isset(self::$conns[$dbcon]) && !isset($GLOBALS['dbs'][$dbcon])){
			$GLOBALS['dbs'][$dbcon]=array(
				'server'=>$server,
				'admin'=>$admin,
				'password'=>$password,
				'database'=>$database,
				'port'=>$port
			);
			self::$connect($dbcon,
				$server,
				$admin,
				$password,
				$database,
				$port);
		}
	}

	/**
	* 變更可選擇的資料庫
	*/
	static public function dbname_set($dbcon,$id){
		if(!_SINGLE){
			switch($dbcon){
				case 'vgroup':
					self::$dbcon_seek='vgroup';
					self::$db($dbcon,$GLOBALS['dbs']['prefix'].'vgroup_'.$id);
					break;
				case 'user':
					self::$db($dbcon,$GLOBALS['dbs']['prefix'].'user_'.$id);
					break;
				case 'admin':
					self::$db($dbcon,$GLOBALS['dbs']['prefix'].$id);
					break;
				case 'super':case 'super admin':
					self::$db('admin',$id);
					break;
			}
		}
		else{
			self::$db(NULL);
		}
	}

	/**
	* 變更dbcon_seek的資料庫
	*/
	static public function db($dbcon,$database=NULL){
		self::$set_dbcon($dbcon);
		if(is_null($database)){
			@mysql_select_db(self::$database,self::$conns[self::$dbcon_seek]) or die('ERROR: db1 in change database, can not select database '.(_DEBUG?$database:''));
		}
		else{
			@mysql_select_db($database,self::$conns[self::$dbcon_seek]) or die('ERROR: db2 in change database, can not select database '.(_DEBUG?$database:''));
		}
	}

	/**
	 * create_database 
	 *
	 * 新增資料庫
	 * 
	 * @param mixed $dbcon 
	 * @param mixed $id 
	 * @access public
	 * @return void
	 */
	static public function create_database($dbcon, $id){
		switch($dbcon){
			case 'vgroup':
				self::$query='CREATE DATABASE `'.$GLOBALS['dbs']['prefix'].'vgroup_'.$id.'` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci';
				$GLOBALS['db']->query('admin',self::$query);
				self::$schema_log($GLOBALS['dbs']['prefix'].'vgroup_'.$id);
				break;
			case 'user':

				break;
		}
	}

	/**
	 * value 
	 * 
	 * @param mixed $dbcon 
	 * @param mixed $sql 
	 * @access public
	 * @return void
	 */
	static public function value($dbcon,$sql){
		 self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;

		if(isset($sql['schema'])){
			self::$query=$sql['schema'];
		}
		else{
			if(empty($sql['select']))$sql['select']='*';
			self::$query='SELECT '.$sql['select'].' FROM '.$sql['from'];
			if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
			if(!empty($sql['group']))self::$query.=' GROUP BY '.$sql['group'];
			if(!empty($sql['order']))self::$query.=' ORDER BY '.$sql['order'];
			self::$query.=' LIMIT 1';
		}
		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
		}
		self::$schema_log();
		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		if(mysql_num_rows($restmp)){
			$res=mysql_fetch_row($restmp);
			return $res[0];
		}
		return NULL;
	}
	static public function row($dbcon,$sql,$first=0){
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;

		if(empty($sql['select']))$sql['select']='*';
		self::$query='SELECT '.$sql['select'].' FROM '.$sql['from'];
		if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
		if(!empty($sql['group']))self::$query.=' GROUP BY '.$sql['group'];
		if(!empty($sql['order']))self::$query.=' ORDER BY '.$sql['order'];
		self::$query.=' LIMIT '.($first?($first.',1'):'1');
		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}
		self::$schema_log();
		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		if(mysql_num_rows($restmp)){
			$res=mysql_fetch_assoc($restmp);
			return $res;
		}
		return NULL;
	}

	static public function empty_array(){
		return array('d'=>array(),'args'=>array('total'=>0,'num'=>0));
	}

	/**
	 * UNION JOIN
	 * [sql schema index]
	 *	 [select]	非必填，但union時會以第一個sql schema的欄位為準
	 *   [from]		必填
	 *	 [where]	非必填
	 *   [order]	非必填
	 *   [group]	非必填
	 *   [limit]	非必填
	 * [order]		全部union後的排序
	 * [group]		全部union後的群組分類
	 * [where]		全部union後的條件
	 */
	static public function union($dbcon,$sqls){
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;

		$queries=array();
		$i=0;
		while(isset($sqls[$i])){
			if(empty($sqls[$i]['select']))$sqls[$i]['select']=$sqls[$i]['from'].'.*';
			$queries[$i]='(SELECT '.$sqls[$i]['select'].' FROM '.$sqls[$i]['from'];
			if(!empty($sqls[$i]['where']))$queries[$i].=' WHERE '.$sqls[$i]['where'];
			if(!empty($sqls[$i]['group']))$queries[$i].=' GROUP BY '.$sqls[$i]['group'];
			if(!empty($sqls[$i]['order']))$queries[$i].=' ORDER BY '.$sqls[$i]['order'];
			if(!empty($sqls[$i]['limit']))$queries[$i].=' LIMIT '.$sqls[$i]['limit'];
			$queries[$i].=')';
			$i++;
		}
		self::$query=join(' UNION ',$queries);
		if(!empty($sqls['where'])) self::$query.=' WHERE '.$sqls['where'];
		if(!empty($sqls['group'])) self::$query.=' GROUP BY '.$sqls['group'];
		if(!empty($sqls['order'])) self::$query.=' ORDER BY '.$sqls['order'];
		$tmp=self::$empty_array();
		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		$tmp['args']['total']=mysql_num_rows($restmp);
		$tmp['args']['num']=$tmp['args']['total'];
		
		self::$schema_log();

		while($res=mysql_fetch_assoc($restmp)){
			$tmp['d'][]=$res;
		}
		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}
		return $tmp;
	}

	/**
	 * [select]	非必填，若為空表示用 *
	 * [from]	必填，從哪個資料表
	 * [where]	非必填，但不需包含排序的欄位
	 * [row]	必填，比對用的欄位
	 * [val]	[row]指定的值
	 * [count]	前後各幾筆資料
	 */
	static public function nearby($dbcon,$sql){
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;

		$query1='';
		$query2='';
		$val=is_int($sql['val'])?$sql['val']:'"'.addslashes($sql['val']).'"';
		if(empty($sql['select']))$sql['select']=$sql['from'].'.*';
		if(!empty($sql['where']))$sql['where']=' AND '.$sql['where'];	
		else $sql['where']='';
		self::$query='(SELECT -1 as sr,'.$sql['select'].' FROM '.$sql['from'].' WHERE '.$sql['row'].'<='.$val.$sql['where'].' ORDER BY '.$sql['row'].' DESC LIMIT '.($sql['count']+1).') UNION (SELECT 1 as sr,'.$sql['select'].' FROM '.$sql['from'].' WHERE '.$sql['row'].'>'.$val.$sql['where'].' ORDER BY '.$sql['row'].' LIMIT '.$sql['count'].') ORDER BY sr,'.$sql['row'];

		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		$tmp=self::$empty_array();
		$tmp['args']['beofre']=0;
		$tmp['args']['after']=0;
		$tmp['args']['current']=0;
		$tmp['args']['total']=mysql_num_rows($restmp);
		$tmp['args']['num']=$tmp['args']['total'];
		while($res=mysql_fetch_assoc($restmp)){
			if($res['sr']<0){
				if($res[$sql['row']]===$sql['val']){
					$tmp['args']['current']++;
					$res['sr']=0;
				}
				else $tmp['args']['before']++;
			}
			else $tmp['args']['after']++;
			$tmp['d'][]=$res;
		}
		self::$schema_log();
		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}
		return $tmp;
	}


	/**
	 * 取代原本的data, sub, page等method
	 * [schema] 完整的sql schema,若有schema則不需要select,from,...etc
	 * select	必填
	 * from		必填，若是要使用非查詢資料表，則使用query
	 * [where]
	 * [order]	order by
	 * [group]	group by
	 * [limit]	不和page&bypage混用，若有設定page且>0
	 * [page]	若為>0的正整數,則表示以每頁[page]筆資料分頁,否則就使用預設值
	 * [spage]	第幾頁,若有設定表示要分頁
	 * [index]	
	 * [value]
	 *		沒有指令index，且沒有指定value 時: 	用 $vars['d'][$i]['row name'] 			取得第 $i 筆資料的 row name 欄位值
	 *		沒有指令index，且有指定value 時: 	用 $vars['d'][$i] 						取得第 $i 筆資料的 $value 欄位值
	 *		有指令index，且沒有指定value 時: 	用 $vars['d'][$index][$i]['row name'] 	取得索引為 $index 第 $i 筆資料的 row name 欄位值
	 *		有指令index，且有指定value 時: 	用 $vars['d'][$index] 					取得索引為 $index 的 $value 欄位值
	 *		若有sub就使用index&value
	 * [sub]	使用次查詢，[sub][include0]表示將次查詢的資料放在$vars['d']['include0']裡
	 *	select
	 *	from
	 *	[dbcon]	指定dbcon
	 *	[order]
	 *	[group]
	 *	[limit]	必須為正整數，表示每一項至多取幾個，和SQL的LIMIT有點不一樣
	 *	key		(儲存key1=key2)會拿母查詢的key2作為要查詢的條件，WHERE key1 IN (母查詢的key2值清單), key1必須為unique
	 * ================================
	 * return
	 * d		回傳的資料陣列
	 * args
	 *  total	總共頁數共有多少筆資料
	 *  num		目前這一頁有多少筆資料
	 *  page	每頁幾筆資料
	 *  pages	總共有多少頁
	 *  spage	第幾頁,從0開始
	 * 
	 * @param string $dbcon 
	 * @param array $sql 
	 * @access public
	 * @return array
	 */
	static public function select($dbcon,$sql){
		$is_page=FALSE;
		$is_sub=FALSE;
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;
		$tmp=self::$empty_array();
		$tmp['args']['page']=30;// 一頁幾筆資料
		$tmp['args']['pages']=0;// 共有幾頁
		$tmp['args']['spage']=0;// 現在在哪一頁
		if(isset($sql['spage']) && is_int($sql['spage']) && $sql['spage']>=0){
			$is_page=TRUE;
			$tmp['args']['spage']=$sql['spage'];
			if(isset($sql['page']) && is_int($sql['page']) && $sql['page']>0){
				$tmp['args']['page']=$sql['page'];
			}
			$sql['limit']=0;
		}
		if(isset($sql['sub'])){
			// 取出關聯的 key
			$is_sub=TRUE;
			$key1=array();
			$key2=array();
			$sql['index']=NULL;
			$sql['value']=NULL;
			foreach($sql['sub'] as $include_k => $v){
				// 這邊 key1, key2 拿到的是欄位名稱
				list($key1[$include_k],$key2[$include_k])=explode('=',$v['key']);
			}
		}
		if(empty($sql['select']))$sql['select']='*';
		if($is_page){
			self::$query='SELECT SQL_CALC_FOUND_ROWS '.$sql['select'].' FROM '.$sql['from'];
		}
		else{
			self::$query='SELECT '.$sql['select'].' FROM '.$sql['from'];
		}
		if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
		if(!empty($sql['group']))self::$query.=' GROUP BY '.$sql['group'];
		if(!empty($sql['order']))self::$query.=' ORDER BY '.$sql['order'];
		if($is_page){
			self::$query.=' LIMIT '.($tmp['args']['page']*$tmp['args']['spage']).','.$tmp['args']['page'];
			$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
			$rescount=mysql_query('SELECT FOUND_ROWS();',self::$conns[self::$dbcon_seek]);
			$rescount=mysql_fetch_row($rescount);
			$tmp['args']['total']=$rescount[0];
			$tmp['args']['pages']=ceil($tmp['args']['total']/$tmp['args']['page']);
			$tmp['args']['num']=mysql_num_rows($restmp);
		}
		else{
			if(!empty($sql['limit']))self::$query.=' LIMIT '.$sql['limit'];
			$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
			$tmp['args']['total']=mysql_num_rows($restmp);
			$tmp['args']['num']=$tmp['args']['total'];
		}

		self::$schema_log();

		if(!isset($sql['index']))$sql['index']=NULL;
		if(!isset($sql['value']))$sql['value']=NULL;
		if(!$is_sub){
			if(is_null($sql['index'])){
				if(is_null($sql['value'])){
					while($res=mysql_fetch_assoc($restmp)){$tmp['d'][]=$res;}
				}
				else{
					while($res=mysql_fetch_assoc($restmp)){
						$tmp['d'][]=$res[$sql['value']];
					}
				}
			}
			else{
				if(is_null($sql['value'])){
					while($res=mysql_fetch_assoc($restmp)){
						if(!isset($tmp['d'][$res[$sql['index']]])){$tmp['d'][$res[$sql['index']]]=array();}
						$tmp['d'][$res[$sql['index']]][]=$res;
					}
				}
				else{
					while($res=mysql_fetch_assoc($restmp)){
						$tmp['d'][$res[$sql['index']]]=$res[$sql['value']];
					}
				}
			}
		}
		else{
		/** is_sub **/
			/** 
			$key2: include代碼 => $母查詢欄位
			$key1: include代碼 => $子查詢的欄位
			**/
			/** $key2是代表所有要從母結果中取得的欄位 **/
			$key2rows=array_values($key2);
			$n=count($key2rows);
			/** $inkey2[欄位名稱][序]=母查詢所得到的值 **/
			$inkey2=array();
			/** $at_row[欄位名稱][母查詢所得到的值][序]=在哪一個母查詢的資料 **/
			$at_row=array();
			$res_index=0;
			while($res=mysql_fetch_assoc($restmp)){
				$tmp['d'][]=$res;
				for($i=0;$i<$n;$i++){
					$inkey2[$key2rows[$i]][]=$res[$key2rows[$i]];	
					$at_row[$key2rows[$i]][$res[$key2rows[$i]]][]=$res_index;
				}
				$res_index++;
			}
			for($i=0;$i<$n;$i++){
				/** $inkey2存的是$tmp['d']的key2值 **/
				$inkey2[$key2rows[$i]]=array_unique($inkey2[$key2rows[$i]]);
			}
			foreach($sql['sub'] as $include_k => $v){
				if(($m=count($inkey2[$include_k]))){
					self::$query='';
					if(empty($v['select']))$v['select']='*';
					self::$query='SELECT '.$v['select'].' FROM '.$v['from'];
					if(!empty($v['where']))self::$query.=' WHERE ('.sql_condition($v['where']).') AND '.$key1[$include_k].' IN '.sql_condition_value($inkey2[$include_k]);
					else if($m==1)self::$query.=' WHERE '.$key1[$include_k].'='.sql_condition_value($inkey2[$include_k][0]);
					else self::$query.=' WHERE '.$key1[$include_k].' IN '.sql_condition_value($inkey2[$include_k]);

					if(!empty($v['group']))self::$query.=' GROUP BY '.$v['group'];
					if(!empty($v['order']))self::$query.=' ORDER BY '.$v['order'];
					if(isset($v['dbcon']) && !isset(self::$conns[$v['dbcon']]))self::$set_dbcon($v['dbcon']);
					$restmp=mysql_query(self::$query,self::$conns[$v['dbcon']]) or die(self::dbo_error());
					while($res=mysql_fetch_assoc($restmp)){
						for($j=0,$m=count($at_row[$key2[$include_k]][$res[$key2[$include_k]]]);$j<$m;$j++){
							if(!isset($tmp['d'][$at_row[$key2[$include_k]][$res[$key2[$include_k]]][$j]][$include_k])) $tmp['d'][$at_row[$key2[$include_k]][$res[$key2[$include_k]]][$j]][$include_k]=array();
							$tmp['d'][$at_row[$key2[$include_k]][$res[$key2[$include_k]]][$j]][$include_k][]=$res;
						}
					}
				}
				else{

				}

			}
		}

		if(!count($tmp['d']))$tmp['d']=array();

		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}
		return $tmp;
	}



	function data($dbcon,$sql,$index=NULL,$value=NULL){
	/*****
	data
	回傳為陣列，索引：
		num: 符合 where 條件的資料總筆數
		count: 指定頁數中的資料筆數(=num)
		d: 資料陣列
	沒有指令 $index，沒有指定 $value 時: 	用 $vars['d'][$i]['row name'] 			取得第 $i 筆資料的 row name 欄位值
	沒有指令 $index，有指定 $value 時: 		用 $vars['d'][$i] 						取得第 $i 筆資料的 $value 欄位值
	有指令 $index，沒有指定 $value 時: 		用 $vars['d'][$index][$i]['row name'] 	取得索引為 $index 第 $i 筆資料的 row name 欄位值
	有指令 $index，有指定 $value 時: 		用 $vars['d'][$index] 					取得索引為 $index 的 $value 欄位值
	*****/
		if(isset($sql['sub'])){
			return self::$sub($dbcon,$sql,$index,$value);
		}
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;
		$tmp=array('d'=>array(),'args'=>array('count'=>0,'num'=>0));

		if(is_array($sql)){
			if(empty($sql['select']))$sql['select']='*';
			self::$query='SELECT '.$sql['select'].' FROM '.$sql['from'];
			if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
			if(!empty($sql['groupby']))self::$query.=' GROUP BY '.$sql['groupby'];
			if(!empty($sql['orderby']))self::$query.=' ORDER BY '.$sql['orderby'];
		}
		else if(is_string($sql)){
			self::$query=$sql;
		}

		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}
		if(!strlen($index))$index=NULL;
		if(!strlen($value))$value=NULL;
		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		if(!$restmp){return NULL;}
		$tmp['args']['num']=mysql_num_rows($restmp);
		$tmp['args']['count']=$tmp['args']['num'];
		if(is_null($index)){
			if(is_null($value)){
				while($res=mysql_fetch_assoc($restmp)){$tmp['d'][]=$res;}
			}
			else{
				while($res=mysql_fetch_assoc($restmp)){
					$tmp['d'][]=$res[$value];
				}
			}
		}
		else{
			if(is_null($value)){
				while($res=mysql_fetch_assoc($restmp)){
					if(!isset($tmp['d'][$res[$index]])){$tmp['d'][$res[$index]]=array();}
					$tmp['d'][$res[$index]][]=$res;
				}
			}
			else{
				while($res=mysql_fetch_assoc($restmp)){
					$tmp['d'][$res[$index]]=$res[$value];
				}
			}
		}
		return $tmp;
	}

	function page($dbcon,$sql,$page=0,$rows=30){
	/*****
	page
	回傳為陣列，索引：
		args:
			num: 符合 where 條件的資料總筆數
			count: 指定頁數中的資料筆數
			page: 在第幾頁，由 0 開始
			pages: 所有頁數，除 num==0 時為 0 外，最少 1 頁
			per: 一頁幾筆資料
		d: 資料陣列
	*****/
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;
		$tmp=array('d'=>array(),'args'=>array('count'=>0,'page'=>0,'pages'=>0,'num'=>0,'per'=>30));
		if($page<0)$page=0;
		if($rows<1)$rows=30;
		$tmp['args']['per']=$rows;
		/******
		取得資料
		******/
		$sql['limit']=($page*$rows).','.$rows;

		if(empty($sql['select']))$sql['select']='*';
		self::$query='SELECT SQL_CALC_FOUND_ROWS '.$sql['select'].' FROM '.$sql['from'];
		if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
		if(!empty($sql['groupby']))self::$query.=' GROUP BY '.$sql['groupby'];
		if(!empty($sql['orderby']))self::$query.=' ORDER BY '.$sql['orderby'];
		self::$query.=' LIMIT '.$sql['limit'];
		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		$rescount=mysql_query('SELECT FOUND_ROWS();',self::$conns[self::$dbcon_seek]);
		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}

		$tmp['args']['count']=mysql_num_rows($restmp);
		if($restmp){
			while($res=mysql_fetch_assoc($restmp)){$tmp['d'][]=$res;}
		}
		if(!count($tmp['d']))$tmp['d']=NULL;

		$tmp['args']['page']=$page;
		/******
		計算頁數
		******/
		$rescount=mysql_fetch_row($rescount);
		$rescount=$rescount[0];
		$tmp['args']['pages']=ceil($rescount/$rows);
		$tmp['args']['num']=$rescount;
		return $tmp;
	}

	function sub($dbcon,$sql,$sub,$page=0,$rows=0){
	/*****
	sub
	$sql
	$sub
		ex:
		array(
			'select'=>row list, default is *,
			'from'=>table,
			'where'=>Statements... 不列出 relkey
			'relkey'=>column name (必需有在 $sql 的結果中)
		);
		sub 查尋出來的結果會在 $sql 的每筆結果中新增一個 include 的欄位
	回傳為陣列，索引：
		args:
			num: 符合 where 條件的資料總筆數
			count: 指定頁數中的資料筆數
			page: 在第幾頁，由 0 開始
			pages: 所有頁數，除 num==0 時為 0 外，最少 1 頁
			per: 一頁幾筆資料
		d: 資料陣列
	*****/
		self::$set_dbcon($dbcon);
		self::$query='';
		self::$query_counter++;
		$tmp=array('d'=>array(),'args'=>array('count'=>0,'page'=>0,'pages'=>0,'num'=>0,'per'=>30));
		if($page<0)$page=0;
		if($rows<1)$rows=1;
		$tmp['args']['per']=$rows;
		/******
		取得資料
		******/
		$sql['limit']=($page*$rows).','.$rows;

		if(empty($sql['select']))$sql['select']='*';
		self::$query='SELECT SQL_CALC_FOUND_ROWS '.$sql['select'].' FROM '.$sql['from'];
		if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
		if(!empty($sql['groupby']))self::$query.=' GROUP BY '.$sql['groupby'];
		if(!empty($sql['orderby']))self::$query.=' ORDER BY '.$sql['orderby'];
		if($rows>1){
			self::$query.=' LIMIT '.$sql['limit'];
		}
		$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		$rescount=mysql_query('SELECT FOUND_ROWS();',self::$conns[self::$dbcon_seek]);
		if(self::$show && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
		}

		$tmp['args']['count']=mysql_num_rows($restmp);
		if($restmp){
			while($res=mysql_fetch_assoc($restmp)){$tmp['d'][]=$res;}
		}
		if(!count($tmp['d']))$tmp['d']=NULL;

		$tmp['args']['page']=$page;
		/******
		計算頁數
		******/
		$rescount=mysql_fetch_row($rescount);
		$rescount=$rescount[0];
		$tmp['args']['pages']=ceil($rescount/$rows);
		$tmp['args']['num']=$rescount;
		if(!is_null($tmp['d'])){
			$inrows=array();
			$atrows=array();
			for($i=0;$i<$tmp['args']['count'];$i++){
				$tmp['d'][$i]['include']=array();
				if(!in_array($tmp['d'][$i][$sub['relkey']],$inrows)){
					$atrows[$tmp['d'][$i][$sub['relkey']]]=array();
					if(is_string($tmp['d'][$i][$sub['relkey']])){
						$inrows[]='"'.mysql_real_escape_string($tmp['d'][$i][$sub['relkey']]).'"';
					}
					else if(is_numeric($tmp['d'][$i][$sub['relkey']])){
						$inrows[]=$tmp['d'][$i][$sub['relkey']];
					}
					else{
						$inrows[]='"'.mysql_real_escape_string($tmp['d'][$i][$sub['relkey']]).'"';
					}
				}
				$atrows[$tmp['d'][$i][$sub['relkey']]][]=$i;
			}
			if(($n=count($inrows))){
				if(empty($sub['select']))$sub['select']='*';
				self::$query='SELECT '.$sub['select'].' FROM '.$sub['from'].' WHERE ';
				if($n==1){
					self::$query.= (empty($sub['where'])?'':'('.$sub['where'].') AND ').$sub['relkey'].'='.$inrows[0];
				}
				else{
					self::$query.= $sub['relkey'].' IN ('.join(',',$inrows).')'.(empty($sub['where'])?'':' AND ('.$sub['where'].')');
				}
				if(!empty($sub['orderby']))self::$query.=' ORDER BY '.$sub['orderby'];
				$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
				if($restmp){
					while($res=mysql_fetch_assoc($restmp)){
						for($i=0,$m=count($atrows[$res[$sub['relkey']]]);$i<$m;$i++){
							$tmp['d'][$atrows[$res[$sub['relkey']]][$i]]['include'][]=$res;
						}
					}
				}
			}
		}

		return $tmp;
	}

	/**
	 * add 
	 * 
	 * 單筆新增
	 *
	 * @param string $dbcon 
	 * @param string $table 
	 * @param array $data 
	 * @param boolean $return_insert_id 
	 * @access public
	 * @return array/numeric
	 */
	function add($dbcon,$table,$data,$return_insert_id=TRUE){
		self::$set_dbcon($dbcon);
		self::$query_counter++;
		$tmp=array('key'=>array(),'val'=>array());
		self::$query='INSERT INTO `'.$table.'`(';
		foreach($data as $k => $v){
			$tmp['key'][]=$k;
			$tmp['val'][]=sql_modify_value($v);
		}
		self::$query.=join(',',$tmp['key']).') VALUES('.join(',',$tmp['val']).');';
		self::$schema_log($table);
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		self::$status=array();
		if($return_insert_id){
			self::$status[]=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
			return mysql_insert_id(self::$conns[self::$dbcon_seek]);
		}
		else{
			self::$status[]=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		}
	}

	/**
	 * adds
	 *
	 * 多筆新增
	 * $data 必需傳入索引為 default 的陣列，該陣列索引為欄位名稱，值為預設值
	 * 若要插入的值是要以函式出現，則將函式表示及值包在 array[0] 裡
	 * 
	 * @param string $dbcon 
	 * @param string $table 
	 * @param array $data 
	 * @access public
	 * @return array
	 */
	static public function adds($dbcon,$table,$data){
		if(!is_array($data) || (!isset($data['default'])))return false;
		self::$set_dbcon($dbcon);
		self::$query_counter++;
		$tmpRows=array();
		$tmpRowsName=array();
		$values=array();
		$keys=array_keys($data['default']);
		for($i=0,$keys_length=count($keys);$i<$keys_length;$i++){
			//$tmp['val'][]=sql_modify_value($v);
			$tmpRows[$keys[$i]]=sql_modify_value($data['default'][$keys[$i]]);
		}
		foreach($data as $k=>$v){
			if(!strcmp($k,'default'))continue;
			$valuesData=array();
			for($i=0;$i<$keys_length;$i++){
				if(isset($v[$keys[$i]])){
					$valuesData[$i]=sql_modify_value($v[$keys[$i]]);
				}
				else{
					$valuesData[$i]=$tmpRows[$keys[$i]];
				}
			}

			$values[]='('.join(',',$valuesData).')';
		}
		self::$query='INSERT INTO `'.$table.'`('.join(',',$keys).') VALUES'.join(',',$values);
		self::$schema_log($table);
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		self::$status=array();
		self::$status[]=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
	}

	/**
	 * skip_update 
	 *
	 * 確認是否需要update，0:需要，other:不需要
	 * 
	 * @param string $dbcon 
	 * @param array $sql 
	 * @param array $data 
	 * @access public
	 * @return boolean
	 */
	static public function skip_update($dbcon,$sql,$data){
		self::$set_dbcon($dbcon);
		$tmp=self::$row($dbcon,$sql);
		if(!$tmp)return 2;
		
		$keys=array_keys($data);
		$num_keys=count($keys);
		$need_update=0;

		for($i=0;$i<$num_keys;$i++){
			if(!isset($tmp[$keys[$i]]))return 3;
			if($tmp[$keys[$i]]!=$data[$keys[$i]])$need_update++;
		}
		if($need_update)return 0;
		else return 1;
	}

	/**
	 * update
	 *
	 * 若為對單一筆資料更新，建議搭配check_need_update
	 * if(!$GLOBALS['db']->skip_update($dbcon,$sql,$data))$GLOBALS['db']->update($dbcon,$table,$data,$attach);
	 * 
	 * @param string $dbcon 
	 * @param string $table 
	 * @param array $data 
	 * @param string $attach 
	 * @access public
	 * @return void
	 */
	static public function update($dbcon, $table, $data, $attach=''){
		self::$set_dbcon($dbcon);
		self::$query_counter++;

		$keysRows=array_keys($data);
		$numRows=count($keysRows);
		$attach=trim($attach);

		if($numRows<1)return FALSE;

		$updater=array();
		self::$query='UPDATE `'.$table.'` SET ';
		for($i=0;$i<$numRows;$i++){
			$updater[]='`'.$keysRows[$i].'`='.sql_modify_value($data[$keysRows[$i]]);
		}
		if(!is_null($attach)){
			self::$query.=join(',',$updater).' '.$attach;
		}
		else{
			self::$query.=join(',',$updater);
		}
		self::$schema_log($table);
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		self::$status=array();
		self::$status[]=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
	}
	/**
	 * del
	 *
	 * 刪除指定的資料
	 *
	 * @param string $dbcon
	 * @param string $table 資料表
	 * @param string $attach 附加條件
	 * @access public
	 * @return void
	 */
	static public function del($dbcon,$table,$attach=NULL){
		self::$set_dbcon($dbcon);
		self::$query_counter++;

		if(is_null($attach)){
			self::$query='TRUNCATE '.$table;
		}
		else{
			self::$query='DELETE FROM `'.$table.'` '.$attach;
		}
		self::$schema_log($table);
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		self::$status=array();
		self::$status[]=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
	}

	/**
	 * multi_modify 
	 *
	 * 多重修改
	 * 
	 * @param string $dbcon 
	 * @param array $sqls 
	 * @access public
	 * @return void
	 */
	static public function multi_modify($dbcon, $sqls){

		
	}

	/**
	 * exec_sql
	 *
	 * 不需要 dbcon 的多 array schema 執行
	 * array schema
	 * array(
	 *	'method'=>[update/add/insert/delete/select/insert-select],	:: select 只有在最後一個時才會回傳陣列，當 add(alias insert) 在最後一個時回傳 insert_id
	 *	'select'=>[select rows],	:: select 時可以不設定，會自動補上*，insert-select 時，需輸入要 select 的欄位名稱
	 *	'from'=>[table name],
	 *	'table'=>[table name],		:: insert-select 專用
	 *	'data'=>[data elements],	:: select 不需使用，insert-select 陣列內只要儲存要寫入的欄位名稱array('row1','row2')
	 *	'condition'=>[condition],	:: add/insert 不需使用，update 時為一般附加條件，select 時則為 where condition
	 *	'order'=>[order by row],	:: select 限定
	 *	'group'=>[group by row],	:: select 限定
	 *	'limit'=>[limit counter],	:: select 限定
	 *	'schema'=>[full sql schema]	:: 使用 schema 時，只要 method 搭配使用
	 * );
	 * 
	 * @param string $dbcon 
	 * @param array $sqls 
	 * @access public
	 * @return void
	 */
	static public function exec_sql($dbcon,$sqls){
		self::$set_dbcon($dbcon);
		self::$status=array();
		self::$query_counter++;

		$schemas=array();
		$num=count($sqls);
		$last_num=$num-1;
		$last_method=0;
		if(!$num)die('Error: no sqls number');
		if(!strcmp($sqls[$last_num]['method'],'select'))$last_method=1;
		else if(!strcmp($sqls[$last_num]['method'],'add') || !strcmp($sqls[$last_num]['method'],'insert'))$last_method=2;
		for($seek=0;$seek<$num;$seek++){
			if(!isset($sqls[$seek]['method']))die('Error: exec_sql');
			switch($sqls[$seek]['method']){
				case 'update':
					if(isset($sqls[$seek]['schema'])){
						$schemas[]=$sqls[$seek]['schema'];
						break;
					}
					$rows=array();
					foreach($data as $row => $val){
						$rows[]=$row.'='.(is_array($val)?$val[0]:(is_int($val)?$val:'"'.mysql_real_escape_string($val).'"'));
					}
					$schemas[]='UPDATE '.$sqls[$seek]['from'].' SET '.join(',',$rows).' '.$sqls[$seek]['condition'];
					break;

				case 'add':
				case 'insert':
					if(isset($sqls[$seek]['schema'])){
						$schemas[]=$sqls[$seek]['schema'];
						break;
					}
					$rows=array();
					$vals=array();
					$multi=FALSE;
					if(isset($sqls[$seek]['data']['default']) && isset($sqls[$seek]['data'][0])){
					// 若要新增多筆資料，必須要有 default 和 0 這兩個索引
						$multi=TRUE;
						foreach($sqls[$seek]['data']['default'] as $row => $default_val){
							$rows[]=$row;
							$sqls[$seek]['data']['default'][$row]=is_array($default_val) ?
								$default_val[0] : (is_int($default_val) ? 
										$default_val : '"'.mysql_real_escape_string($default_val).'"');
						}
						$rows_num=count($rows);
						foreach($sqls[$seek]['data'] as $key => $datum){
							if(!strcmp($key,'default'))continue;
							$tmp_vals=array();
							for($j=0;$j<$rows_num;$j++){
								$tmp_vals[]=isset($datum[$rows[$j]]) ? (
										is_array($datum[$rows[$j]]) ?
											$datum[$rows[$j]][0] : (is_int($datum[$rows[$j]]) ?
												$datum[$rows[$j]] : '"'.mysql_real_escape_string($datum[$rows[$j]]).'"'
											)
										) : $sqls[$seek]['data']['default'][$rows[$j]];
							}
							$vals[]='('.join(',',$tmp_vals).')';
						}
						$vals=join(',',$vals);
						unset($tmp_vals);
					}// end of 多筆資料新增
					else{
						foreach($sqls[$seek]['data'] as $row => $val){
							$rows[]=$row;
							$vals[]=(is_array($val)?$val[0]:(is_int($val)?$val:'"'.mysql_real_escape_string($val).'"'));
						}
						$vals=join(',',$vals);
					}// end of 單筆資料新增
					$rows=join(',',$rows);
					$schemas[]='INSERT INTO '.$sqls[$seek]['from'].'('.$rows.') VALUES '.$vals;
					break;

				case 'select':
					if(isset($sqls[$seek]['schema'])){
						$schemas[]=$sqls[$seek]['schema'];
						break;
					}
					if(empty($sqls[$seek]['from']))die('Error: no set from for select method.');
					$str='SELECT '.(empty($sqls[$seek]['select'])?'*':$sqls[$seek]['select']).' FROM '.$sqls[$seek]['from'];
					if(!empty($sqls[$seek]['condition']))$str.=' WHERE '.$sqls[$seek]['condition'];
					if(!empty($sqls[$seek]['groupby']))$str.=' GROUP BY '.$sqls[$seek]['groupby'];
					if(!empty($sqls[$seek]['orderby']))$str.=' ORDER BY '.$sqls[$seek]['orderby'];
					if(!empty($sqls[$seek]['limit']))$str.=' LIMIT '.$sqls[$seek]['limit'];
					$schemas[]=$str;
					break;

				case 'insert-select':
					if(isset($sqls[$seek]['schema'])){
						$schemas[]=$sqls[$seek]['schema'];
						break;
					}
					if(empty($sqls[$seek]['data']))die('Error: no set insert columns for insert-select method.');
					else if(empty($sqls[$seek]['from']))die('Error: no set select table for insert-select method.');
					if(empty($sqls[$seek]['select']) || substr_count($sqls[$seek]['select'],',')!=count($sqls[$seek]['data'])-1){
						die('Error: not match for insert-select method');
					}
					$str='SELECT '.$sqls[$seek]['select'].' FROM '.$sqls[$seek]['from'];
					if(!empty($sqls[$seek]['condition']))$str.=' WHERE '.$sqls[$seek]['condition'];
					if(!empty($sqls[$seek]['groupby']))$str.=' GROUP BY '.$sqls[$seek]['groupby'];
					if(!empty($sqls[$seek]['orderby']))$str.=' ORDER BY '.$sqls[$seek]['orderby'];
					if(!empty($sqls[$seek]['limit']))$str.=' LIMIT '.$sqls[$seek]['limit'];
					$schemas[]='INSERT INTO '.$sql[$seek]['table'].'('.join(',',$sqls[$seek]['data']).') '.$str;
					break;
				default:
					die('Error: not allow method');
					break;
			}// end of switch
		}// end for $num
	
		if((self::$stop || self::$show) && _DEBUG){
			_e($schemas);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		$n=$last_method==1?$last_num:$num;
		for($i=0;$i<$n;$i++){
			self::$query=$schemas[$i];
			self::$schema_log($table);
			self::$status[]=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		}
		if($last_method==1){
			self::$query=$schemas[$last_num];
			$restmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
			$tmp=array();
			while($res=mysql_fetch_assoc($restmp)){
				$tmp[]=$res;
			}
			return $tmp;
		}
		else if($last_method==2){
			return mysql_insert_id(self::$conns[self::$dbcon_seek]);
		}
		return TRUE;
	}
	/**
	 * tables
	 *
	 * 取得所有資料表
	 *
	 * @param string $dbcon
	 * @access public
	 * @return void
	 */
	static public function tables($dbcon){
		self::$set_dbcon($dbcon);
		$sql='SHOW TABLES';
		$restmp=mysql_query($sql,self::$conns[self::$dbcon_seek]);
		$tmp=array();
		while(($res=mysql_fetch_row($restmp))){
			$tmp[]=$res[0];
		}
		return $tmp;
	}

	/**
	 * query_array 
	 *
	 * 直接執行查詢語法，不建議使用，因為返回的resource跟資料庫是相依的
	 * 跟query()類似，只是sql是傳入陣列再兜成sql schema
	 * 
	 * @param string $dbcon 
	 * @param array $sql 
	 * @access public
	 * @return void
	 */
	static public function query_array($dbcon,$sql){
		self::$set_dbcon($dbcon);
		self::$query_counter++;
		if(empty($sql['select']))$sql['select']='*';
		self::$query='SELECT '.$sql['select'].' FROM '.$sql['from'];
		if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
		if(!empty($sql['group']))self::$query.=' GROUP BY '.$sql['group'];
		if(!empty($sql['order']))self::$query.=' ORDER BY '.$sql['order'];

		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		$tmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		if(!mysql_num_rows($tmp)) return array();
		else{
			$tmp2=array();
			while($res=mysql_fetch_assoc($tmp)){
				$tmp2[]=$res;
			}
			return $tmp2;
		}
		return array();
	}

	/**
	 * query 
	 *
	 * 直接執行查詢語法，不建議使用，因為返回的resource跟資料庫是相依的
	 * 
	 * @param string $dbcon 
	 * @param string $sql sql schema
	 * @access public
	 * @return void
	 */
	static public function query($dbcon,$sql){
		self::$set_dbcon($dbcon);
		self::$query_counter++;
		self::$query=$sql;

		self::$schema_log();
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		$tmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		return $tmp;
	}

	/**
	 * lock table
	 *
	 * 鎖定資料表
	 * 
	 * @param string $dbcon 
	 * @param string $table 資料表名稱
	 * @access public
	 * @return void
	 */
	static public function lock($dbcon,$table){
		self::$set_dbcon($dbcon);
		self::$query_counter++;
		self::$query='LOCK TABLE '.$table.' WRITE';
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		$tmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		return $tmp;
	}

	/**
	 * unlock 
	 *
	 * 解鎖資料表
	 * 
	 * @param string $dbcon 
	 * @param string $table 資料表名稱
	 * @access public
	 * @return void
	 */
	static public function unlock($dbcon,$table){
		self::$set_dbcon($dbcon);
		self::$query_counter++;
		self::$query='UNLOCK TABLE';
		if((self::$stop || self::$show) && _DEBUG){
			echo htmlspecialchars(self::$query);
			self::$show=FALSE;
			if(self::$stop){
				die(1);
			}
		}
		$tmp=mysql_query(self::$query,self::$conns[self::$dbcon_seek]) or die(self::dbo_error());
		return $tmp;
	}

	static private function condition($query_condition, $logic=0){
		if(empty($query_condition))return FALSE;
		else if(is_string($query_condition)) return $condition;
		else if(is_array($query_condition)){
			return sql_condition($query_condition, $logic);
		}
		else return FALSE;
	}

	/**
	 * set_dbcon 
	 * 
	 * @param string/null $dbcon 指定切換為哪個dbcon
	 * @access private
	 * @return void
	 */
	static private function set_dbcon($dbcon){
		if(_SINGLE || is_null($dbcon)){
			$dbcon='default';
		}
		if(!strcmp($dbcon,'default') || is_null($dbcon)){
			self::$dbcon_seek=self::$_default;
		}
		else{
			if(isset(self::$conns[$dbcon])){
				self::$dbcon_seek=$dbcon;
			}
			else{
				if(isset($GLOBALS['dbs'][$dbcon])){
					self::$connect($dbcon,
						$GLOBALS['dbs'][$dbcon]['server'],
						$GLOBALS['dbs'][$dbcon]['admin'],
						$GLOBALS['dbs'][$dbcon]['password'],
						$GLOBALS['dbs'][$dbcon]['database'],
						$GLOBALS['dbs'][$dbcon]['port']
					);
				}
				else{
					_e($GLOBALS['dbs']);
					die('Error: can not set dbcon: '.(_DEBUG?$dbcon:''));
				}
			}
		}
	}
	/**
	 * get_dbcon_seek
	 *
	 * 顯示目前的dbcon_seek
	 *
	 * @access public
	 * @return void
	 */
	static public function get_dbcon_seek(){
		if(_DEBUG){
			echo self::$dbcon_seek;
		}
	}

	/**
	 * schema_log 
	 * 只有非debug狀態、對syslog操作的sql schema會紀錄
	 * 
	 * @param string $table 
	 * @access protected
	 * @return void
	 */
	static protected function schema_log($table=''){
		if(self::$log_enable && !self::$show && strcmp(self::$dbcon_seek,'syslog')){
			$cmd_type=strtolower(substr(self::$query,0,3));
			$cmd_types=array(
				'sel'=>'SELECT',
				'(se'=>'SELECT',
				'ins'=>'INSERT',
				'del'=>'DELETE',
				'tru'=>'DELETE',
				'upd'=>'UPDATE',
				'cre'=>'CREATE TABLE',
				'use'=>'USE DB',
			);
			if(isset($cmd_types[$cmd_type])){
			   	$cmd_type=$cmd_types[$cmd_type];
				if(!strcmp($cmd_type,'SEELCT')) if(!self::$log_select) return FALSE;
				else if(!strcmp($cmd_type,'INSERT')) if(!self::$log_insert) return FALSE;
				else if(!strcmp($cmd_type,'DELETE')) if(!self::$log_delete) return FALSE;
				else if(!strcmp($cmd_type,'UPDATE')) if(!self::$log_update) return FALSE;
				else if(!strcmp($cmd_type,'CREATE TABLE')) if(!self::$log_create) return FALSE;
				else if(!strcmp($cmd_type,'USE DB')) if(!self::$log_usedb) return FALSE;
			}
			else $cmd_type='UNKNOW';

			$user_pky=0;
			$user_id='';
			$user_name='';
			if(isset($GLOBALS['me'])){
				$user_pky=$GLOBALS['me']->user_pky;
				$user_pky=$GLOBALS['me']->user_id;
				$user_pky=$GLOBALS['me']->user_name;
			}
			$data=array(
				'slogUserPky'=>$user_pky,
				'slogUserId'=>$user_id,
				'slogUserName'=>$user_name,
				'slogCommand'=>$cmd_type,
				'slogTable'=>$table,
				'slogSchema'=>self::$query,
				'slogRemote'=>empty($_SERVER['REMOTE_ADDR'])?'':$_SERVER['REMOTE_ADDR'],
				'slogTime'=>defined('_SYS_DATETIME')?_SYS_DATETIME:array('NOW()'),
				'slogUA'=>empty($_SERVER['HTTP_USER_AGENT'])?'':$_SERVER['HTTP_USER_AGENT'],
				'slogURL'=>$_SERVER['QUERY_STRING'],
				'slogRefer'=>empty($_SERVER['HTTP_REFERER'])?'':$_SERVER['HTTP_REFER'],
				'slogMethod'=>empty($_SERVER['REQUEST_METHOD'])?'GET':$_SERVER['REQUEST_METHOD'],
			);
			self::$add('syslog','sqlogs',$data);
		}
	}

	/**
	 * debug 
	 *
	 * 開啟/關閉除錯模式
	 * 
	 * @param boolean $debug 預設為開啟
	 * @access public
	 * @return void
	 */
	static public function debug($show=TRUE){
		self::$show=$show;
	}
	/**
	 * schema 
	 * 
	 * @param array $sql 陣列，根據索引兜出sql schema 
	 * @access public
	 * @return void
	 */
	function schema($sql){
		if(empty($sql['select']))$sql['select']='*';
		self::$query='SELECT '.$sql['select'].' FROM '.$sql['from'];
		if(!empty($sql['where']))self::$query.=' WHERE '.$sql['where'];
		if(!empty($sql['groupby']))self::$query.=' GROUP BY '.$sql['groupby'];
		if(!empty($sql['orderby']))self::$query.=' ORDER BY '.$sql['orderby'];
		return self::$query;
	}
	/**
	 * dbo_error
	 *
	 * 顯示sql錯誤
	 * 
	 * @access private
	 * @return void
	 */
	static private function dbo_error(){
		$log='@SQL Error Number ID:'.mysql_errno(self::$conns[self::$dbcon_seek])."\n";
		if(_DEBUG){
			$log.='@File: ?'.($_SERVER['QUERY_STRING'])."\n";
			$log.='@DBCON: '.self::$dbcon_seek."\n";
			$log.='@Host: '.($_SERVER['HTTP_HOST'])."\n";
			$log.='@Remote: '.($_SERVER['SERVER_ADDR'])."\n";
			$log.='mysql error:'.mysql_error(self::$conns[self::$dbcon_seek]).'
			query counts:'.self::$query_counter.'
			schema:'.self::$query."\n";
			for($i=0,$n=count(self::$status);$i<$n;$i++){
				$log.=$i.'.'.self::$status[$i]."\n";
			}
			echo nl2br(htmlspecialchars($log));
			$fo=fopen(_DIR_DOCS.'files/hide/systems/mysql_error.testing',"a");
			fputs($fo,$log);
			fclose($fo);
		}
		else{
			$log=':::: '.xdate('Y-m-d H:i:s').' ===='."\n";
			$log.='@Host: '.($_SERVER['HTTP_HOST'])."\n";
			$log.='@File: '.($_SERVER['QUERY_STRING'])."\n";
			$log.='mysql error:'.mysql_error(self::$conns[self::$dbcon_seek])."\n";
			$log.='query counts:'.self::$query_counter."\n";
			$log.='schema: '.self::$query."\n\n";
			$fo=fopen(_DIR_DOCS.'files/hide/systems/mysql_error.testing',"a");
			fputs($fo,$log);
			fclose($fo);
		}
	}
}
/**
 * 將陣列兜成WHERE條件
 *
 * @example:
 * 'where'=>array(
 *   'unitPky'=>1,
 * 	 array(
 *     'orgPky'=>2,
 *     'vhostPky'=>3,
 *   ),
 * ),
 * ======> WHERE unitPky=1 AND (orgPky=2 OR vhostPky=3)
 * 'LIKE:unitName'=>'a',
 *		====> unitName LIKE '%a%',
 *
 * 'LIKER:unitName'=>'a'
 *		====> unitName LIKE 'a%',
 *
 * 'LIKEL:unitName'=>'a'
 *		====> unitName LIKE '%a',
 *
 * 'NOT:unitName'=>NULL
 *		====> unitName IS NO NULL,
 *
 * 'unitName'=>NULL
 *		====> unitName IS NULL,
 *
 * 'RANGE:unitDateStart:unitDateClose'=>'2011-11-12 00:11:21'
 *		====> "2011-11-12 00:11:21" BETWEEN unitDateStart AND unitDateClose,
 *
 * 'RANGE:unitDate'=>array('2011-11-12 00:11:12','2011-12-12 00:11:12')
 *		====> unitDate BETWEEN "2011-11-12 00:11:12" AND "2011-12-12 00:11:12"
 *
 * 'IN:unitPky'=>array(1,2,3,4)
 *		====> unitPky IN (1,2,3,4),
 * 
 * @param array $condition 
 * @param boolean $rel 預設0/FALSE為AND，其餘為FALSE
 * @access public
 * @return void
 */
function sql_condition($condition,$rel=0){
	$req='';
	if(is_string($condition)){
		return $condition;
	}
	else if(is_array($condition)){
		$w=array();
		foreach($condition as $key => $v){
			if(strpos($key,':')){
				$k=explode(':',$key);
				$n=count($k);
				if($n==3 && !strcmp($k[0],'RANGE')){
					$w[]=sql_condition_value($v).' BETWEEN '.$k[1].' AND .'.$k[2];
				}
				else if($n==2){
					switch($k[0]){
					case 'RANGE':
						if(is_array($v)){
							$w[]=$k[1].' BETWEEN '.sql_condition_value($v[0]).' AND '.sql_condition_value($v[1]);
						}
						else $w[]=$k[1].'='.sql_condition_value($v);
						break;
					case 'LIKE':
						$w[]=$k[1].' LIKE "%'.str_replace('%','_%',addslashes($v)).'%"';
						break;
					case 'LIKER':
						$w[]=$k[1].' LIKE "'.str_replace('%','_%',addslashes($v)).'%"';
						break;
					case 'LIKEL':
						$w[]=$k[1].' LIKE "%'.str_replace('%','_%',addslashes($v)).'"';
						break;
					case 'NOT':
						if(is_null($v)) $w[]= $k[1].' IS NOT NULL';
						else $w[]=$k[1].'<>'.sql_condition_value($v);
						break;
					case 'IN':
						if(empty($v)){
							$w[]=$k[1].' IN (0)';
						}
						else $w[]=$k[1].' IN '.sql_condition_value($v);
						break;
					}
				}
			}// end process : in key index
			else if(is_int($key)){
				$w[]='('.sql_condition($v, 1-$rel).')';
			}
			else{
				$w[]=$key.'='.sql_condition_value($v);
			}
		}
		return join($rel?' OR ':' AND ',$w);
	}
	else return 'error';
}

/**
 * sql_condition_value 
 *
 * 兜成condition值，根據值的型別
 * 
 * @param mixed $v 
 * @access public
 * @return void
 */
function sql_condition_value($v){
	if(is_int($v)){
		return $v;
	}
	else if(is_string($v)){
		return '"'.addslashes($v).'"';
	}
	else if(is_array($v)){
		$w=array();
		for($i=0,$n=count($v);$i<$n;$i++){
			$w[]=is_int($v[$i])?$v[$i]:'"'.addslashes($v[$i]).'"';
		}
		return '('.join(',',$w).')';
	}
	else if(is_null($v)){
		return NULL;
	}
	else if(is_float($v))return $v;
	else return '"'.$v.'"';
}
/**
 * sql_modify_value 
 *
 * 用在insert,update的值,差在對如果$v是陣列的判斷方式不同
 * 
 * @param mixed $v 
 * @access public
 * @return void
 */
function sql_modify_value($v){
	if(is_int($v)){
		return $v;
	}
	else if(is_string($v)){
		return '"'.addslashes($v).'"';
	}
	else if(is_array($v)){
		return $v[0];
	}
	else if(is_null($v)){
		return 'NULL';
	}
	else if(is_float($v))return $v;
	else return '"'.$v.'"';
}

function set_vgroup_db($database){
	if(empty($database))die('Error: no assign database name.');
	$GLOBALS['dbs']['vgroup']['database']='adapt_'.$database;
}

function set_user_db($database){
	if(empty($database))die('Error: no assign database name.');
	$GLOBALS['dbs']['user']['database']='adapt_'.$database;
}


