<?php

function is_private_lan($ip){
	if(strpos($ip,'.')){
		if(!strcmp(substr($ip,0,3),'10.')){
			return TRUE;
		}
		else if(!strcmp(substr($ip,0,8),'192.168.')){
			return TRUE;
		}
		else if(!strcmp(substr($ip,0,4),'172.') && substr($ip,6,1)=='.'){
			$class_b=intval(substr($ip,4,2),10);
			if($class_b>=16 && $class_b<=31) return TRUE;
		}
		else if(!strcmp($ip,'127.0.0.1')){
			return TRUE;
		}
		else if(!strcmp($ip,'175.180.133.72')){
			return TRUE;
		}
	}
	else if(strpos($ip,':')){
		if(!strcmp($ip,'::1')){
			return TRUE;
		}
		else if(!strcmp(substr($ip,0,5),'fe80:')){
			return TRUE;
		}
	}
	return FALSE;
}


