<?php
/*
########################################################################                                        
Copyright 2007, Michael Schrenk                                                                                 
   This software is designed for use with the book,                                                             
   "Webbots, Spiders, and Screen Scarpers", Michael Schrenk, 2007 No Starch Press, San Francisco CA             
                                                                                                                
W3C? SOFTWARE NOTICE AND LICENSE                                                                                
                                                                                                                
This work (and included software, documentation such as READMEs, or other                                       
related items) is being provided by the copyright holders under the following license.                          
 By obtaining, using and/or copying this work, you (the licensee) agree that you have read,                     
 understood, and will comply with the following terms and conditions.                                           

Permission to copy, modify, and distribute this software and its documentation, with or                         
without modification, for any purpose and without fee or royalty is hereby granted, provided                    
that you include the following on ALL copies of the software and documentation or portions thereof,             
including modifications:                                                                                        
   1. The full text of this NOTICE in a location viewable to users of the redistributed                         
      or derivative work.                                                                                       
   2. Any pre-existing intellectual property disclaimers, notices, or terms and conditions.                     
      If none exist, the W3C Software Short Notice should be included (hypertext is preferred,                  
      text is permitted) within the body of any redistributed or derivative code.                               
   3. Notice of any changes or modifications to the files, including the date changes were made.                
      (We recommend you provide URIs to the location from which the code is derived.)                           
                                                                                                                
THIS SOFTWARE AND DOCUMENTATION IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO REPRESENTATIONS OR           
WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS          
FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE OR DOCUMENTATION WILL NOT INFRINGE ANY THIRD         
PARTY PATENTS, COPYRIGHTS, TRADEMARKS OR OTHER RIGHTS.                                                          
                                                                                                                
COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT     
OF ANY USE OF THE SOFTWARE OR DOCUMENTATION.                                                                    
                                                                                                                
The name and trademarks of copyright holders may NOT be used in advertising or publicity pertaining to the      
software without specific, written prior permission. Title to copyright in this software and any associated     
documentation will at all times remain with copyright holders.                                                  
########################################################################                                        
*/

#-----------------------------------------------------------------------
# LIB_http                                                              
#                                                                       
# F U N C T I O N S                                                     
#                                                                       
# http_get()                                                            
#    Fetches a file from the Internet with the HTTP protocol            
# http_header()                                                         
#    Same as http_get(), but only returns HTTP header instead of        
#    the normal file contents                                           
# http_get_form()                                                       
#    Submits a form with the GET method                                 
# http_get_form_withheader()                                            
#    Same as http_get_form(), but only returns HTTP header instead of   
#    the normal file contents                                           
# http_post_form()                                                      
#    Submits a form with the POST method                                
# http_post_withheader()                                                
#    Same as http_post_form(), but only returns HTTP header instead of  
#    the normal file contents                                           
# http_header()                                                         
#                                                                       
# http()                                                                
#   A common routine called by all of the previously described          
#   functions. You should always use one of the other wrappers for this 
#   routine and not call it directly.                                   
#                                                                       
#-----------------------------------------------------------------------
# R E T U R N E D   D A T A                                             
# All of these routines return a three dimensional array defined as     
# follows:	    												        
#    $return_array['FILE']   = Contents of fetched file, will also      
#                              include the HTTP header if requested    
#    $return_array['STATUS'] = CURL generated status of transfer        
#    $return_array['ERROR']  = CURL generated error status              
########################################################################

/***********************************************************************
Webbot defaults (scope = global)                                       
----------------------------------------------------------------------*/
# Define how your webbot will appear in server logs
$webbot_name=array(
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_7_4) AppleWebKit/534.56.5 (KHTML, like Gecko) Version/5.1.6 Safari/534.56.5',
	'Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405',
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.151 Safari/535.19',
	'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_1_1 like Mac OS X; en) AppleWebKit/534.46.0 (KHTML, like Gecko) CriOS/19.0.1084.60 Mobile/9B206 Safari/7534.48.3',
	'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; )',
	'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.114 Safari/537.36',
	'Mozilla/5.0 (Linux; U; Android 4.0.4; fr-fr; MIDC409 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Safari/534.30',
	'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)',
	'Mozilla/5.0 (; U; Linux SH4; en) AppleWebkit/534.1 (KHTML, like Gecko) HbbTV/1.1.1 (;GMI-SS-Browser- 3.0; mB/7111;1.0.0;1.00;)',
	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Q312461; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.5.30729; .NET CLR',
	'Mozilla/5.0 (X11; U; Linux i686; da-DK; rv:1.9.2.2) Gecko/20131031',
	'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
	'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; Alexa Toolbar)',
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.71 Safari/537.36',
);
define('WEBBOT_NAME', $webbot_name[rand(0,count($webbot_name)-1)]);

# Length of time cURL will wait for a response (seconds)
define("CURL_TIMEOUT", 25);

# Location of your cookie file. (Must be fully resolved local address)
if(is_dir('/var/tmp')){
	define("COOKIE_FILE", "/var/tmp/cookie.txt");
}
else if(is_dir('/tmp')){
	define("COOKIE_FILE", "/tmp/cookie.txt");
}
else{
	define("COOKIE_FILE", "C:\cookie.txt");
}

# DEFINE METHOD CONSTANTS
define("HEAD", "HEAD");
define("GET",  "GET");
define("POST", "POST");

# DEFINE HEADER INCLUSION
define("EXCL_HEAD", FALSE);
define("INCL_HEAD", TRUE);

/***********************************************************************
User interfaces         	                                            
-------------------------------------------------------------			
*/

/***********************************************************************
function http_get($target, $ref)                                        
-------------------------------------------------------------           
DESCRIPTION:                                                            
        Downloads an ASCII file without the http header                 
INPUT:                                                                  
        $target       The target file (to download)                     
        $ref          The server referer variable                       
OUTPUT:                                                                 
        $return_array['FILE']   = Contents of fetched file, will also   
                                 include the HTTP header if requested   
        $return_array['STATUS'] = CURL generated status of transfer     
        $return_array['ERROR']  = CURL generated error status           
***********************************************************************/
function http_get($target, $ref){
    return http($target, $ref, $method="GET", $data_array="", EXCL_HEAD);
}

/***********************************************************************
http_get_withheader($target, $ref)                                      
-------------------------------------------------------------           
DESCRIPTION:                                                            
        Downloads an ASCII file with the http header                    
INPUT:                                                                  
        $target       The target file (to download)                     
        $ref          The server referer variable                       
OUTPUT:                                                                 
        $return_array['FILE']   = Contents of fetched file, will also   
                                 include the HTTP header if requested   
        $return_array['STATUS'] = CURL generated status of transfer     
        $return_array['ERROR']  = CURL generated error status           
***********************************************************************/
function http_get_withheader($target, $ref){
	return http($target, $ref, $method="GET", $data_array="", INCL_HEAD);
}

/***********************************************************************
http_get_form($target, $ref, $data_array)                               
-------------------------------------------------------------           
DESCRIPTION:                                                            
        Submits a form with the GET method and downloads the page       
        (without a http header) referenced by the form's ACTION variable
INPUT:                                                                  
        $target       The target file (to download)                     
        $ref          The server referer variable                       
        $data_array   An array that defines the form variables          
                      (See "Webbots, Spiders, and Screen Scrapers" for  
                      more information about $data_array)               
OUTPUT:                                                                 
        $return_array['FILE']   = Contents of fetched file, will also   
                                 include the HTTP header if requested   
        $return_array['STATUS'] = CURL generated status of transfer     
        $return_array['ERROR']  = CURL generated error status           
***********************************************************************/
function http_get_form($target, $ref, $data_array){
	return http($target, $ref, $method="GET", $data_array, EXCL_HEAD);
}

/***********************************************************************
http_get_form_withheader($target, $ref, $data_array)                    
-------------------------------------------------------------           
DESCRIPTION:                                                            
        Submits a form with the GET method and downloads the page       
        (with http header) referenced by the form's ACTION variable     
INPUT:                                                                  
        $target       The target file (to download)                     
        $ref          The server referer variable                       
        $data_array   An array that defines the form variables          
                      (See "Webbots, Spiders, and Screen Scrapers" for  
                      more information about $data_array)               
OUTPUT:                                                                 
        $return_array['FILE']   = Contents of fetched file, will also   
                                 include the HTTP header if requested   
        $return_array['STATUS'] = CURL generated status of transfer     
        $return_array['ERROR']  = CURL generated error status           
***********************************************************************/
function http_get_form_withheader($target, $ref, $data_array){
	return http($target, $ref, $method="GET", $data_array, INCL_HEAD);
}

/***********************************************************************
http_post_form($target, $ref, $data_array)                              
-------------------------------------------------------------           
DESCRIPTION:                                                            
        Submits a form with the POST method and downloads the page      
        (without http header) referenced by the form's ACTION variable  
INPUT:                                                                  
        $target       The target file (to download)                     
        $ref          The server referer variable                       
        $data_array   An array that defines the form variables          
                      (See "Webbots, Spiders, and Screen Scrapers" for  
                      more information about $data_array)               
OUTPUT:                                                                 
        $return_array['FILE']   = Contents of fetched file, will also   
                                 include the HTTP header if requested   
        $return_array['STATUS'] = CURL generated status of transfer     
        $return_array['ERROR']  = CURL generated error status           
***********************************************************************/
function http_post_form($target, $ref, $data_array){
	return http($target, $ref, $method="POST", $data_array, EXCL_HEAD);
}

function http_post_withheader($target, $ref, $data_array){
	return http($target, $ref, $method="POST", $data_array, INCL_HEAD);
}

function http_header($target, $ref){
	return http($target, $ref, $method="HEAD", $data_array="", INCL_HEAD);
}

/***********************************************************************
http($url, $ref, $method, $data_array, $incl_head)                      
-------------------------------------------------------------			
DESCRIPTION:															
	This function returns a web page (HTML only) for a web page through	
	the execution of a simple HTTP GET request.							
	All HTTP redirects are automatically followed.						
                                                                        
    IT IS BEST TO USE ONE THE EARLIER DEFINED USER INTERFACES           
    FOR THIS FUNCTION                                                   
                                                                        
INPUTS:																	
	$target      Address of the target web site		 					
	$ref		 Address of the target web site's referrer				
	$method		 Defines request HTTP method; HEAD, GET or POST         
	$data_array	 A keyed array, containing query string                 
	$incl_head	 TRUE  = include http header                            
                 FALSE = don't include http header                      
                                                                        
RETURNS:																
    $return_array['FILE']   = Contents of fetched file, will also       
                              include the HTTP header if requested      
    $return_array['STATUS'] = CURL generated status of transfer         
    $return_array['ERROR']  = CURL generated error status               
***********************************************************************/
function http($target, $ref, $method, $data_array, $incl_head){
    # Initialize PHP/CURL handle
	$request_header_set=array();
	$is_https=substr($target,0,5);
	if(!strcasecmp($is_https,'https')){
		$is_https=TRUE;
	}
	else{
		$is_https=FALSE;
	}
	$ch = curl_init();

    # Prcess data, if presented
    if(is_array($data_array)){
	    # Convert data array into a query string (ie animal=dog&sport=baseball)
        foreach ($data_array as $key => $value){
			if($key=='request_header_set'){
				$request_header_set=$value;
				continue;
			}
            if(strlen(trim($value))>0)
                $temp_string[] = $key . "=" . urlencode($value);
            else
                $temp_string[] = $key;
		}
        $query_string = join('&', $temp_string);
	}
        
    # HEAD method configuration
	if(!empty($request_header_set)){
		curl_setopt($ch, CURLOPT_HTTPHEADER , $request_header_set);
	}
    if($method == HEAD){
    	curl_setopt($ch, CURLOPT_HEADER, TRUE);                // No http head
	    curl_setopt($ch, CURLOPT_NOBODY, TRUE);                // Return body
	}
    else{
        # GET method configuration
        if($method == GET)
            {
            if(isset($query_string))
                $target = $target . "?" . $query_string;
            curl_setopt ($ch, CURLOPT_HTTPGET, TRUE); 
            curl_setopt ($ch, CURLOPT_POST, FALSE); 
            }
        # POST method configuration
        if($method == POST){
            if(isset($query_string))
                curl_setopt ($ch, CURLOPT_POSTFIELDS, $query_string);
			curl_setopt ($ch, CURLOPT_POST, TRUE); 
			curl_setopt ($ch, CURLOPT_HTTPGET, FALSE); 
		}
    	curl_setopt($ch, CURLOPT_HEADER, $incl_head);   // Include head as needed
	    curl_setopt($ch, CURLOPT_NOBODY, FALSE);        // Return body
	}
        
	curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE);   // Cookie management.
	curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE);
	curl_setopt($ch, CURLOPT_TIMEOUT, CURL_TIMEOUT);    // Timeout
	curl_setopt($ch, CURLOPT_USERAGENT, WEBBOT_NAME);   // Webbot name
	curl_setopt($ch, CURLOPT_URL, $target);             // Target site
	curl_setopt($ch, CURLOPT_REFERER, $ref);            // Referer value
	curl_setopt($ch, CURLOPT_VERBOSE, FALSE);           // Minimize logs
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    // No certificate
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);     // Follow redirects
	curl_setopt($ch, CURLOPT_MAXREDIRS, 4);             // Limit redirections to four
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);     // Return in string
	curl_setopt($ch, CURLOPT_ENCODING, '');
    
    # Create return array
    $return_array['FILE']   = curl_exec($ch); 
    $return_array['STATUS'] = curl_getinfo($ch);
    $return_array['ERROR']  = curl_error($ch);
    
    # Close PHP/CURL handle
  	curl_close($ch);
    
    # Return results
  	return $return_array;
}

/***********************************************************************
http_youtube_watch($link){
-------------------------------------------------------------           
DESCRIPTION:                                                            
	取得youtube網址或影片代碼的資料
INPUT:                                                                  
	$link	youtube的網址或影片代碼
OUTPUT:                                                                 
	$return_array['watch']	= 影片代碼
	$return_array['url']	= 完整的影片網址，含http:
	$return_array['image_small']	= 小張擷圖網址
	$return_array['image_big']	= 大張的截圖網址
	$return_array['error']	= 1 表示發生錯誤，無法取得
***********************************************************************/
function http_youtube_watch($link){
	$return_array=array(
		'error'=>1,
		'watch'=>'',
		'url'=>'',
		'image_small'=>'',
		'image_big'=>'',
	);
	if(empty($link)){
		return $return_array;
	}
	if(preg_match('@youtube\.com\/watch\?v=([\w\-_\.]+)$@s',$link,$m)){
		$return_array['watch']=$m[1];
		$return_array['url']=$link;
	}
	else if(preg_match('@youtu\.be\/([\w\-_\.]+)$@s',$link,$m)){
		$return_array['watch']=$m[1];
		$return_array['url']=$link;
	}
	else if(preg_match('@youtube\.com\/watch\?v=([\w\-_\.]+)\&.*$@s',$link,$m)){
		$return_array['watch']=$m[1];
		$return_array['url']=$link;
	}
	else if(preg_match('@^[a-zA-Z0-9_\.]+$@',$link)){
		$return_array['url']='http://www.youtube.com/watch?v='.$link;
		$return_array['watch']=$link;
	}
	else{
		return $return_array;
	}
	$content=http_get($return_array['url'],$link);
	if(empty($content['FILE']))return $return_array;
	$return_array['error']=0;
	if(preg_match('@<meta property=\"og:image\" content=\"(https?:[\/a-zA-Z0-9_\-\.]+)\">@i',$content['FILE'],$m)){
		$return_array['image_small']=$m[1];
	}
	if(preg_match('@<link itemprop=\"thumbnailUrl\" href=\"(https?:[\/a-zA-Z0-9_\-\.]+)\">@i',$content['FILE'],$m)){
		$return_array['image_big']=$m[1];
	}
	return $return_array;
}

/**
 * get_aspx 
 * get aspx parameters by $html
 * 
 * @param mixed $html 
 * @access public
 * @return void
 */
function get_aspx($html){
	$aspx=array(
		'__VIEWSTATE'=>'', 
		'__EVENTVALIDATION'=>'', 
		'__VIEWSTATEGENERATOR'=>'', 
	);
	$pos=strpos($html, ' id="__VIEWSTATE" value="');
	if($pos){
		$pos_end=strpos($html, '" />', $pos);
		$aspx['__VIEWSTATE']=substr($html, $pos+25, $pos_end-$pos-25);

		$pos=strpos($html, ' id="__EVENTVALIDATION" value="', $pos);
		if($pos){
			$pos_end=strpos($html, '" />', $pos);
			$aspx['__EVENTVALIDATION']=substr($html, $pos+31, $pos_end-$pos-31);
		}

		$pos=strpos($html, ' id="__VIEWSTATEGENERATOR" value="', $pos);
		if($pos){
			$pos_end=strpos($html, '" />', $pos2);
			$aspx['__VIEWSTATEGENERATOR']=substr($html, $pos+34, $pos_end-$pos-34);
		}
	}
	return $aspx;
}




