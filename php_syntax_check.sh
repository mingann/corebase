#!/bin/sh
PHP=$(which php)
if [ "$1" == "" ]; then
	echo ">> All diectories under /srv/web/ :"
	ls -F | grep "/" | sed 's/.$//'
	echo ">> Unknow directory name at first args......................."
else
	cd /srv/web/$1
	find . -name "*.ph*" | xargs -n2 $PHP -l
fi

