<?php
date_default_timezone_set('Asia/Taipei');

$GLOBALS['LANG_LABEL']=array();
$GLOBALS['PHP_LABEL']=array();

$GLOBALS['LANG_JSLABEL']=array();
$GLOBALS['JS_LABEL']=array();

$GLOBALS['DIR_BASE']='/srv/web/';
$GLOBALS['FILE_LANGUAGE']='';
$GLOBALS['DIR_SEARCH']='';
$LOG='/tmp/language_label_found-'.date('YmdTH').'.log';

$SETTING='/tmp/language_search_setting';

if(is_file($SETTING)){
	include($SETTING);
	$GLOBALS['FILE_LANGUAGE']=isset($config['file_language'])?$config['file_language']:'';
	$GLOBALS['DIR_SEARCH']=isset($config['dir_search'])?$config['dir_search']:'';
	$LOG=isset($config['log'])?$config['log']:$LOG;
}

if($argc>1){
	for($i=1;$i<$argc;$i++){
		if(!strcasecmp(substr($argv[$i],0,3),'fn=')){
			$GLOBALS['FILE_LANGUAGE']=trim(substr($argv[$i],3));
		}
		else if(!strcasecmp(substr($argv[$i],0,4),'dir=')){
			$GLOBALS['DIR_SEARCH']=trim(substr($argv[$i],4));
		}
		else if(!strcasecmp(substr($argv[$i],0,4),'log=')){
			$LOG=trim(substr($argv[$i],4));
		}
	}
	if(file_exists($LOG)){
		echo('log is exists '.$LOG."\n");
	}
	else{
		$dir_log=dirname($LOG);
		if(!is_dir($dir_log)){
			die('log directory is not exists '.$dir_log."\n");
		}
		else if(!is_writable($dir_log)){
			die('log directory '.$dir_log.' is not writable'."\n");
		}
	}
	if(substr($GLOBALS['DIR_SEARCH'],0,1)!='/'){
		$GLOBALS['DIR_SEARCH']=$GLOBALS['DIR_BASE'].$GLOBALS['DIR_SEARCH'];
	}
	if(!is_dir($GLOBALS['DIR_SEARCH'])){
		die('search directory is nonexists '.$GLOBALS['DIR_SEARCH']."\n");
	}
	if(substr($GLOBALS['DIR_SEARCH'],-1)!='/'){
		$GLOBALS['DIR_SEARCH'].='/';
	}
	if(substr($GLOBALS['FILE_LANGUAGE'],0,1)!='/'){
		$GLOBALS['FILE_LANGUAGE']=$GLOBALS['DIR_BASE'].$GLOBALS['FILE_LANGUAGE'];
	}
	if(!is_file($GLOBALS['FILE_LANGUAGE'])){
		die('language file is nonexists '.$GLOBALS['FILE_LANGUAGE']."\n");
	}
	else if(!is_readable($GLOBALS['FILE_LANGUAGE'])){
		die('laguage file is not readable '.$GLOBALS['FILE_LANGUAGE']."\n");
	}
}
else{
	echo $argc;
	die('no argv input,'."\n".'Usage:'."\n".'  fn=語系檔，絕對路徑或是相對於 /srv/web/ 的檔案。'."\n".'  dir=需要掃描的目錄，絕對路徑或是相對於 /srv/web 的目錄。'."\n".'  log=紀錄檔位置，絕對路徑。'."\n");
}

include($GLOBALS['FILE_LANGUAGE']);
if(!isset($GLOBALS['string'])){
	die('incorrect language file '.$GLOBALS['FILE_LANGUAGE']);
}
$GLOBALS['LANG_LABEL']=array_keys($GLOBALS['string']);
unset($GLOBALS['string']);

language_finder($GLOBALS['FILE_LANGUAGE']);
finder_do($GLOBALS['DIR_SEARCH']);
$GLOBALS['PHP_LABEL']=array_unique($GLOBALS['PHP_LABEL']);

$not_in=array_diff($GLOBALS['PHP_LABEL'],$GLOBALS['LANG_LABEL']);
$not_used=array_diff($GLOBALS['LANG_LABEL'],$GLOBALS['PHP_LABEL']);
$not_in=array_values($not_in);
$not_used=array_values($not_used);

file_put_contents($LOG, 'In php/phtml, but not in languages:'."\n".print_r($not_in,TRUE)."\n".'In language, but not in php/phtml:'."\n".print_r($not_used,TRUE));
echo '........... done ................'."\n";



function language_finder($fname){
	$c=file_get_contents($fname);
	preg_match_all('/([\'\"])([LME][xy]_[a-zA-Z0-9_]+)\1/',$c,$m);
	$GLOBALS['PHP_LABEL']=array_merge($GLOBALS['PHP_LABEL'],$m[2]);
}

function language_js_finder($fname){
	$c=file_get_contents($fname);
	preg_match_all('/jword\(([\'\"])([LME][xy]_[a-zA-Z0-9_]+)\1/',$c,$m);
	$GLOBALS['PHP_LABEL']=array_merge($GLOBALS['PHP_LABEL'],$m[2]);
}

function finder_do($dirname){
	$d=opendir($dirname);
	while(($f=readdir($d))!=FALSE){
		if($f=='.' || $f=='..'){
			continue;
		}
		else if(substr($f,0,1)=='.'){
			continue;
		}
		if(is_dir($dirname.$f)){
			finder_do($dirname.$f.'/');
		}
		else if(is_file($dirname.$f) && strcmp($dirname.$f, $GLOBALS['FILE_LANGUAGE'])){
			$sub=strrchr($f,'.');
			if(in_array($sub,array('.php','.phtml'))){
				language_finder($dirname.$f);
			}
		}
	}
	closedir($d);
}

